<div id="mstacja_content"><center><img></center>
<div id="mstacja_content-left">
<h1>LUK SPRZĘGŁO MITSUBISHI CARISMA 1.6 16V 90/95/99KM</h1>
<p style="text-align: center;">
<img alt="">
</p>
<p></p><div style="clear:both;"></div><br><h2>Informacje o produkcie</h2><table class="mstacja_table" cellspacing="0" cellpadding="0" width="100%"> <tbody><tr>  <td class="light">Stan:</td>  <td>   <b>Towar oryginalny, fabrycznie nowy i zapakowany</b>  </td> </tr>     <tr>      <td width="180" class="light">Przedmiot zakupu:</td>       <td>  <b>Zestaw sprzęgła firmy LUK</b>   </td>    </tr>      <tr>      <td class="light">Zakup dotyczy (ilość):</td>      <td>   <b>1 sztuki / Komplet       <br>   </b></td>     </tr>    <tr>  <td class="light">Numer katalogowy:</td>  <td>       <b style="color: Green;">620 2215 00</b>       </td>    </tr>    <tr>   <td colspan="2" height="40"> </td>  </tr>  <tr>      <td class="car2" colspan="2">    <b>Informacje katalogowe     620 2215 00</b>         / 620221500       </td>  </tr>        <tr>      <td class="light">Średnica [mm]</td>      <td> 200</td>  </tr>  <tr>      <td class="light">Jednostka pakowania</td>      <td> 1</td>  </tr>  <tr>      <td class="light">Ilość w jednostce pakowania</td>      <td> 1</td>  </tr>  <tr>      <td class="light">Zestaw zawiera</td>      <td></td>  </tr>  <tr>   <td colspan="2" height="40"> </td>  </tr>  <tr>      <td class="car2" colspan="2">    <b>Informacje katalogowe    Kod części Wymagana ilość Nazwa części</b>         / KodczęściWymaganailośćNazwaczęści       </td>  </tr>        <tr>      <td class="light"> 120 0155 10            1 Tarcza dociskowa sprzęgła</td>      <td></td>  </tr>  <tr>      <td class="light"> 320 0178 10            1 Tarcza sprzęgła</td>      <td></td>  </tr>  <tr>      <td class="light"> 500 0758 10            1 Łożysko oporowe</td>      <td></td>  </tr>        </tbody>        </table>    <h2>Licytowany towar występuje w samochodach</h2> <p style="line-height: 12px; font-size: 11px">  </p><table cellspacing="0" cellpadding="5" width="100%" class="mstacja_table10" style="font-size:11px;line-height:12px;">          <tbody><tr>      <td class="car" colspan="3">MITSUBISHI CARISMA (DA_) (1995/07 -&gt; 2006/06)</td>     </tr>           <tr>      <th>Wersja</th>      <th>Silnik / Moc</th>       <th>Kody silników</th>     </tr>                     <tr>      <td>1.6  (DA1A)</td>      <td>90KM / 66KW  - 1597 ccm</td>      <td></td>     </tr>           <tr>      <td>1.6  (DA1A)</td>      <td>95KM / 70KW  - 1597 ccm</td>      <td>4 G 92</td>     </tr>           <tr>      <td class="car" colspan="3">MITSUBISHI CARISMA sedan (DA_) (1996/09 -&gt; 2006/06)</td>     </tr>           <tr>      <th>Wersja</th>      <th>Silnik / Moc</th>       <th>Kody silników</th>     </tr>                     <tr>      <td>1.6  (DA1A)</td>      <td>99KM / 73KW  - 1597 ccm</td>      <td></td>     </tr>        </tbody></table>   <p></p>  <p><b>Nie ma Twojego samochodu?</b> Zadzwoń - dobierzemy odpowiednią część.</p> <p>  Data w nawiasach oznacza całościowe lata produkcji danego modelu auta. Nie są to konkretne lata zastosowania części w tym samochodzie. Producent mógł w tych samych latach stosować 2 lub więcej różnych typów. Jeżeli nie jesteś pewien dopasowania części po wymiarze i numerze katalogowym, wyślij do Nas email przed zakupem z zapytaniem. Podaj niezbędne dane do auta: model, nadwozie, rok produkcji, silnik, moc i numer nadwozia vin. Mamy możliwość sprawdzenia i wskazania właściwej części. Jeżeli nie jesteś pewien, przed zakupem porównaj wymiary części z założonymi w twoim samochodzie, ponieważ wyświetlone występowanie nie powinno być traktowane jako jedyne źródło informacji podczas doboru części zamiennych. </p>  <h2>Kody OE / Numery porównawcze / Zamienniki</h2> <div style="FONT-SIZE: 11px" class="mstacja_multicolumn">  MITSUBISHI RL210137<br>QUINTON HAZELL QKT1963AF<br>VALEO 821077 </div>      <p></p>
<center><img></center>
</div>
<div id="mstacja_content-right">
<div class="mstacja_box"><br> <a><img alt=""></a></div>
<a> <img> </a>
<img> 
<a> 
 <img> 
</a> 
<img> 
<a> 
<img></a> 
<img> 

<a> 
<img></a> 
<img> 

<a> 
<img> </a> 
<img> 

<a> 
<img> </a> 
<img> 
<a> 
<img> </a> 
<img> 

<img>
<br>
<div class="mstacja_box" style="background: none; padding: 3px 0px;"><img alt=""> <img alt="">
<div style="text-align: center; font-weight: Bold;">13 1140 2004 0000 3202 7178 4428</div>
<img alt="">
<img alt="">
</div>
<div class="clear"> </div>
</div>
<div class="clear"></div>
<img alt="">
<div class="clear"></div>
<div class="clear"> </div>
<!-- Panele Start -->
<style><!--
#user_field img{border: none;} #user_field .aobrazek2 { width: 9px; } #user_field .aobrazek3 { height: 9px; } #user_field .aobrazek4 { height: 9px; width: 9px; } #user_field #tabpanel {padding: 0;} #user_field #tabpanel a{background-color: transparent;display: block;background-repeat: no-repeat;text-decoration: none; color: #000000; font-family:Arial; font-size:12px;} #user_field #tabpanel a:hover {text-decoration: underline;background-color: transparent;background-position: 0 0;} #user_field #tabpanel .astopka1 a { display:inline; } #user_field #tabpanel td {border:0; border-spacing: 0;} #user_field #alinkfoto a {font-size: 9px;margin:0;border:0;color: #999999;font-family: Arial;text-decoration: underline;} #user_field #alinkfoto a:hover{margin:0;border:0;text-decoration: underline; }   #user_field .astopka1 { margin: 0; padding-left: 4px;font-family:Arial; } #user_field .astopka1 a { margin-right: 10px; text-decoration:underline; } #user_field .astopka2 { font-size: 12px;margin: 0; padding-right: 0;font-family:Arial;} #user_field .astopka2 img { padding-left: 5px;} #user_field #tabpanel .astopka2 a { text-decoration: underline; display: inline; margin-right: 2px; vertical-align: -3px; } #user_field #tabpanel .fotoin984271 {background-image: ; background-position: 0 -382px;}#user_field #tabpanel .fotoin984272 {background-image: ; background-position: 0 -382px;}#user_field #tabpanel .fotoin984273 {background-image: ; background-position: 0 -382px;}#user_field #tabpanel .fotoin984274 {background-image: ; background-position: 0 -382px;}#user_field #tabpanel .fotoin984275 {background-image: ; background-position: 0 -382px;}#user_field #tabpanel .fotoin984276 {background-image: ; background-position: 0 -382px;}#user_field #tabpanel .fotoin984277 {background-image: ; background-position: 0 -382px;}#user_field #tabpanel .fotoin984278 {background-image: ; background-position: 0 -382px;}#user_field #tabpanel .fotoin984279 {background-image: ; background-position: 0 -382px;}#user_field #tabpanel .fotoin9842710 {background-image: ; background-position: 0 -382px;}#user_field #tabpanel .fotoin9842711 {background-image: ; background-position: 0 -382px;}#user_field #tabpanel .fotoin9842712 {background-image: ; background-position: 0 -382px;}#user_field #tabpanel .fotoin9842713 {background-image: ; background-position: 0 -382px;}#user_field #tabpanel .fotoin9842714 {background-image: ; background-position: 0 -382px;}
--></style>
<div style="clear: both;"></div>
<div id="tabpanel"><center>
<table cellpadding="0" cellspacing="0" style="width: 1040px;">
<tbody>
<tr>
<td colspan="7" style="height: 19px; text-align: left; color: #000000;">
<p style="padding-left: 4px; margin: 0; font-weight: bold; font-family: Arial; font-size: 12px; padding-top: 2px; color: #000000;">Zobacz nasze pozostałe aukcje: <img alt="PaneleAllegro.pl" width="1" height="1" style="background: none; border: none; margin: 0; padding: 0;"></p>
</td>
</tr>
</tbody>
</table>
<table cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td>
<div style="background-image: ; background-position: 0px 0px; background-repeat: no-repeat; padding: 0 1px 1px 0;">
<table cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td align="center" style="padding: 1px 0 0 1px;">
<table cellpadding="0" cellspacing="0" border="0" class="atabelka2">
<tbody>
<tr>
<td style="position: relative; overflow: hidden;"><a target="_blank" class="fotoin984271" style="position: absolute;"><img style="margin: 0; background: none;"></a><img></td>
</tr>
</tbody>
</table>
</td>
<td align="center" style="padding: 1px 0 0 1px;">
<table cellpadding="0" cellspacing="0" border="0" class="atabelka2">
<tbody>
<tr>
<td style="position: relative; overflow: hidden;"><a target="_blank" class="fotoin984272" style="position: absolute;"><img style="margin: 0; background: none;"></a><img></td>
</tr>
</tbody>
</table>
</td>
<td align="center" style="padding: 1px 0 0 1px;">
<table cellpadding="0" cellspacing="0" border="0" class="atabelka2">
<tbody>
<tr>
<td style="position: relative; overflow: hidden;"><a target="_blank" class="fotoin984273" style="position: absolute;"><img style="margin: 0; background: none;"></a><img></td>
</tr>
</tbody>
</table>
</td>
<td align="center" style="padding: 1px 0 0 1px;">
<table cellpadding="0" cellspacing="0" border="0" class="atabelka2">
<tbody>
<tr>
<td style="position: relative; overflow: hidden;"><a target="_blank" class="fotoin984274" style="position: absolute;"><img style="margin: 0; background: none;"></a><img></td>
</tr>
</tbody>
</table>
</td>
<td align="center" style="padding: 1px 0 0 1px;">
<table cellpadding="0" cellspacing="0" border="0" class="atabelka2">
<tbody>
<tr>
<td style="position: relative; overflow: hidden;"><a target="_blank" class="fotoin984275" style="position: absolute;"><img style="margin: 0; background: none;"></a><img></td>
</tr>
</tbody>
</table>
</td>
<td align="center" style="padding: 1px 0 0 1px;">
<table cellpadding="0" cellspacing="0" border="0" class="atabelka2">
<tbody>
<tr>
<td style="position: relative; overflow: hidden;"><a target="_blank" class="fotoin984276" style="position: absolute;"><img style="margin: 0; background: none;"></a><img></td>
</tr>
</tbody>
</table>
</td>
<td align="center" style="padding: 1px 0 0 1px;">
<table cellpadding="0" cellspacing="0" border="0" class="atabelka3">
<tbody>
<tr>
<td style="position: relative; overflow: hidden;"><a target="_blank" class="fotoin984277" style="position: absolute;"><img style="margin: 0; background: none;"></a><img></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="center" style="padding: 1px 0 0 1px;">
<table cellpadding="0" cellspacing="0" border="0" class="atabelka2">
<tbody>
<tr>
<td style="position: relative; overflow: hidden;"><a target="_blank" class="fotoin984278" style="position: absolute;"><img style="margin: 0; background: none;"></a><img></td>
</tr>
</tbody>
</table>
</td>
<td align="center" style="padding: 1px 0 0 1px;">
<table cellpadding="0" cellspacing="0" border="0" class="atabelka2">
<tbody>
<tr>
<td style="position: relative; overflow: hidden;"><a target="_blank" class="fotoin984279" style="position: absolute;"><img style="margin: 0; background: none;"></a><img></td>
</tr>
</tbody>
</table>
</td>
<td align="center" style="padding: 1px 0 0 1px;">
<table cellpadding="0" cellspacing="0" border="0" class="atabelka2">
<tbody>
<tr>
<td style="position: relative; overflow: hidden;"><a target="_blank" class="fotoin9842710" style="position: absolute;"><img style="margin: 0; background: none;"></a><img></td>
</tr>
</tbody>
</table>
</td>
<td align="center" style="padding: 1px 0 0 1px;">
<table cellpadding="0" cellspacing="0" border="0" class="atabelka2">
<tbody>
<tr>
<td style="position: relative; overflow: hidden;"><a target="_blank" class="fotoin9842711" style="position: absolute;"><img style="margin: 0; background: none;"></a><img></td>
</tr>
</tbody>
</table>
</td>
<td align="center" style="padding: 1px 0 0 1px;">
<table cellpadding="0" cellspacing="0" border="0" class="atabelka2">
<tbody>
<tr>
<td style="position: relative; overflow: hidden;"><a target="_blank" class="fotoin9842712" style="position: absolute;"><img style="margin: 0; background: none;"></a><img></td>
</tr>
</tbody>
</table>
</td>
<td align="center" style="padding: 1px 0 0 1px;">
<table cellpadding="0" cellspacing="0" border="0" class="atabelka2">
<tbody>
<tr>
<td style="position: relative; overflow: hidden;"><a target="_blank" class="fotoin9842713" style="position: absolute;"><img style="margin: 0; background: none;"></a><img></td>
</tr>
</tbody>
</table>
</td>
<td align="center" style="padding: 1px 0 0 1px;">
<table cellpadding="0" cellspacing="0" border="0" class="atabelka3">
<tbody>
<tr>
<td style="position: relative; overflow: hidden;"><a target="_blank" class="fotoin9842714" style="position: absolute;"><img style="margin: 0; background: none;"></a><img></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
<table cellpadding="0" cellspacing="0" style="background-image: ; background-position: 0px 0px; background-repeat: no-repeat; padding: 0; margin-left: 0; width: 100%;">
<tbody>
<tr>
<td colspan="7" style="text-align: left; padding-top: 0; color: #000000;"><a target="_blank"><img alt="Komentarze"></a></td>
</tr>
<tr>
<td colspan="7" style="height: 19px; text-align: left; padding-top: 1px; color: #000000;">
<table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
<tbody>
<tr>
<td>
<p class="astopka1"><a>Strona "o mnie"</a><a>Wszystkie aukcje</a></p>
</td>
<td style="text-align: right;">
<p class="astopka2">PaneleAllegro.pl  <a title="galerie sprzedazy"><img style="background: none; border: none; margin: 0; padding: 0; display: inline;" alt="darmowy panel allegro"></a></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</center></div>
<!-- Panele Koniec --> 
<span id="kodTowaru" style="text-align: right; color: #aaaaaa;">620 2215 00</span> mod</div>
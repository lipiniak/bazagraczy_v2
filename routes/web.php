<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Linki po polsku !!
use Illuminate\Support\Facades\DB;
use App\User;
use App\Games;
use App\Clan;
use App\Team;
use App\FriendlyMatch;
use App\Push;
use App\StatsLogin;
use Carbon\Carbon;

Route::post('tablica/nowy','WallController@newPost')->name('wall.new');
Route::post('tablica/nowy/komentarz','WallController@newComment')->name('wall.new.comment');
Route::get('/tablica/komentarz/{comment_id}/usun/', 'WallController@deleteComment')->name('wall.comment.delete');
Route::get('/tablica/post/{post_id}/usun/', 'WallController@deletePost')->name('wall.post.delete');

Route::get('/tablica/post/{post_id}/unfollow/', 'WallController@unfollowPost')->name('wall.post.unfollow');
Route::get('/tablica/post/{post_id}/follow/', 'WallController@followPost')->name('wall.post.follow');


Route::post('/tablica/post/{post_id}/edit/', 'WallController@editPost')->name('wall.post.edit');
Route::post('/tablica/komentarz/{comment_id}/edit/', 'WallController@editComment')->name('wall.comment.edit');
Route::get('/tablica/post/{post_id}', 'WallController@showPost')->name('wall.post.show');
Route::get('/tablica/online', 'WallController@showOnline')->name('wall.online');
Route::get('/tablica', 'WallController@showWall')->name('wall');

Route::post('/zglos/{type}','TicketsController@makeReport')->name('report.make');
// Main one page    

Route::get('/heartbeep',function(){
    if(Auth::check()) {
        
        $expiresAt = Carbon::now()->addMinutes(5);
        
        $status = Cache::get('user-is-online-'.Auth::user()->id);

        if($status == 'away')
            Cache::put('user-is-online-' . Auth::user()->id, 'away', $expiresAt);
        else {
            Cache::put('user-is-online-' . Auth::user()->id, 'away', $expiresAt);
            $user = Auth::user();
        
            // pobrać all online userów i wysłać im info ze jestem away.
            //Ci mnie followują
            if(Cache::has('online_users_list')) {
                $onlineUsers = Cache::get('online_users_list');
                
                foreach($onlineUsers as $onlineUser) {
                    
                    broadcast(new \App\Events\FollowerAway($onlineUser,$user->id));
                }
            }
        }
        
    }

    return response()->json(['result' => 'away','user_id'=>$user->id,'online'=>$onlineUsers]);
}); 

Route::get('/online/check',function(){
    if(Auth::check()) {
        $expiresAt = Carbon::now()->addMinutes(5);
        
        if(Cache::has('user-is-online-'.Auth::user()->id)) {
            
            $status = Cache::get('user-is-online-'.Auth::user()->id);
            
            if($status == 'online')
               Cache::put('user-is-online-' . Auth::user()->id, 'online', $expiresAt);
            else {
                Cache::put('user-is-online-' . Auth::user()->id, 'online', $expiresAt);
                
                //$user = Auth::user();
                // wyslac wszystkim userom ze jestem online
                //echo "jedziemy z koksem";
                //if(Cache::has('online_users_list')) {
                 //   $onlineUsers = Cache::get('online_users_list');
                  //  echo "online users";
                  //  foreach($onlineUsers as $onlineUser) {
                        
                   //     broadcast(new \App\Events\FollowerLogin($onlineUser,$user->id));
                   // }
        
               // }
            }
        }
        
    }

    return response()->json(['result' => 'online']);
}); 
Route::get('/push/delete/{id}', function($id) {
    $data = array('is_seen'=>1);
    
    $push = Push::find($id);

    $push->is_seen = 1;
    $push->save();

    return response()->json(['result' => 'success']);

});
Route::get('/', function () {
    if(Auth::check()) {
        // Couunt online users;
        $users = User::all();
        $count = 0;
        $online =0;
        foreach($users as $user) {
            $count++;
            if($user->getStatus() == 'online' or $user->getStatus() == 'away') 
                $online++;
        }

        $user = Auth::user();
        
        $selectedGame = $user->selectedGame();

        $selectedUsers = \App\UserSettings::where('selected_game_id',$selectedGame['game_id'])->where('selected_platform_id',$selectedGame['platform_id'])->get();
        
        $userList = array();
        foreach($selectedUsers as $user) {
            $userList[] = $user->user_id;
        }

        $selectedUsers = User::whereIn('id',$userList)->get();
        $topbarCount = 0;
        $topbarOnlineCount = 0;
        foreach($selectedUsers as $user) {
            $topbarCount++;
            if($user->getStatus() == 'online' or $user->getStatus() == 'away')
                $topbarOnlineCount++;
        }
        
        $totalGames = Games::count();
        $totalClans = Clan::count();
        $totalTeams = Team::count();
        $totalMatches = FriendlyMatch::where('status',3)->count();

        $maxLogin = StatsLogin::sum('count');
        $todayLogin = StatsLogin::where('stat_date',date('Y-m-d'))->first();
        if(!$todayLogin) {
            $todayLogin = 1;
        } else {
            $todayLogin = $todayLogin->count;
        }
        $data = array(
            'stats_registered' => User::count(),
            'stats_online' => $online,
            'topbar_registred' => $topbarCount,
            'topbar_online' => $topbarOnlineCount,
            'stats_total_games' => $totalGames,
            'stats_total_clans' => $totalClans,
            'stats_total_teams' => $totalTeams,
            'stats_total_matches' => $totalMatches,
            'stats_total_login' => $maxLogin,
            'stats_today_login' => $todayLogin,
        );

        return view('dashboard',compact('data'));
    }
    else
        return view('auth.login');
});
Route::get('/dashboard', function () {
    return redirect('/');
});
Route::get('/home', function () {
    return redirect('/');
});
// Routing autentykacji
Auth::routes();

Route::get('/wiecej', function() {
    return redirect('/');
})->name('wiecej');

Route::get('/aktywacja/{token}', 'Auth\RegisterController@activateUser')->name('user.activate');

Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('zapomnialem','Auth\PasswordResetController@showSendEmailForm')->name('zapomnialem.email');
Route::post('zapomnialem/wyslij','Auth\PasswordResetController@sendResetEmail')->name('zapomnialem.wyslij');
Route::get('resetuj/{token}','Auth\PasswordResetController@showResetForm')->name('resetuj');
Route::post('resetuj/zapisz','Auth\PasswordResetController@resetPassword')->name('zapomnialem.reset');
// Profile

Route::prefix('profil')->name('profile.')->group(function () {
    
    Route::get('edytuj/', 'ProfileController@showEdit')->name('edit');
    Route::post('edytuj/zapisz', 'ProfileController@profileUpdate')->name('update');
    Route::get('biblioteka/', 'ProfileController@showLibrary')->name('library');

    Route::post('biblioteka/dodaj', 'ProfileController@addToLibrary')->name('library.add');
    Route::get('biblioteka/usun/{game_id}/{platform_id}', 'ProfileController@removeFromLibrary')->name('library.remove');
    Route::get('/{id}/obserwuj', 'FollowersController@followerAdd')->name('follow');
    Route::get('/{id}/nieobserwuj', 'FollowersController@followerRemove')->name('unfollow');
    Route::get('/{id}/zablokuj', 'BlockController@blockAdd')->name('block');
    Route::get('/{id}/odblokuj', 'BlockController@blockRemove')->name('unblock');

    Route::post('/{id}/notatka/dodaj', 'ProfileController@addNote')->name('note.add');
    Route::get('/{id}/notatka/usun', 'ProfileController@deleteNote')->name('note.delete');
    
    Route::get('avatar/', 'ProfileController@showAvatar')->name('avatar');
    Route::post('avatar/zmiana','ProfileController@avatarChange')->name('avatar.change');
    Route::get('avatar/usun', 'ProfileController@deleteAvatar')->name('avatar.delete');
    Route::get('ustawienia/', 'ProfileController@showSettings')->name('settings');

    Route::get('ustawienia/{setting}/{value}', 'ProfileController@chageSetting')->name('settings.change');
    Route::get('ustawienia/gra/{game_id}/{platform_id}', 'ProfileController@changePickedGame')->name('settings.change.game');
    Route::get('konto/', 'ProfileController@showAccount')->name('account');
    Route::get('konto/zmiana/hasla', 'ProfileController@profilePasswordUpdate')->name('account.password');
    Route::get('konto/zmiana/email', 'ProfileController@profileEmailUpdate')->name('account.email');
    
    Route::get('/obserwowani','ProfileController@showFollowers')->name('followers');
    Route::get('/zablokowani','ProfileController@showBlocked')->name('blocked');
    
    Route::get('/powiadomienia','ProfileController@showNotifications')->name('notifications.show');
    Route::post('/powiadomienia/oznacz','ProfileController@markNotification')->name('notifications.read');
    Route::post('/powiadomienia/odznacz','ProfileController@unMarkNotification')->name('notifications.unread');
    Route::post('/powiadomienia/usun','ProfileController@deleteNotification')->name('notifications.delete');
    Route::get('/powiadomienia/usun/wszystkie','ProfileController@deleteAllNotifications')->name('notifications.delete.all');
    
    Route::post('/powiadomienia/przekieruj','ProfileController@markAndRedirect')->name('notification.redirect');

    Route::get('/powiadomienia/przeczytaj','ProfileController@markAllNotifications')->name('notifications.all');
    Route::get('/klany/zarzadzanie','ProfileController@showFollowers')->name('clans.menagment');
    Route::get('/klany/stworz','ProfileController@showFollowers')->name('clan.create');
    
    Route::get('{login}/', 'ProfileController@showProfile')->name('user');
    Route::get('/', 'ProfileController@showProfile')->name('show');

    
});
//Android app post data 

Route::post ('/user/notifications/info', 'PublicController@getUserInfo')->name('app.user.notifications');

// Szukajka
Route::prefix('szukaj')->name('search.')->group(function () {
    Route::post('/wyniki', 'SearchController@doSearching')->name('searching');
    Route::get('/wyniki', 'SearchController@search')->name('searchingGet');
    Route::get('', 'SearchController@search')->name('search');
    
});

// messenger
Route::prefix('wiadomosci')->name('messages.')->group(function () {
    
    Route::post('/wyslij','MessengerController@messageSend')->name('send');

    Route::get('{user_id}/', 'MessengerController@conversationShow')->name('conversation');
    
    Route::get('/przeczytaj/{id}/', 'MessengerController@makeSeen')->name('makeseen');
    Route::get('/doladuj/{user_id}/{offset}', 'MessengerController@loadMessages')->name('load');
    Route::get('/usun/{id}', 'MessengerController@deleteConversation')->name('delete');
});

Route::get('/conversation/close/{reciverId}', function($reciverId) {
    // znajdz cache z userem ?? kupa kodu ..
    // podanie id konwersacji? lepiej tylko jak ... fuk nie do zrobienia  
    $convId1 = $reciverId.'-'.Auth::user()->id;
    $convId2 = Auth::user()->id.'-'.$reciverId;
    if(Cache::has('conversation-'.$convId1)) {
        $cachedUsers = Cache::get('conversation-'.$convId1);
        $elId = array_search(Auth::user()->id,$cachedUsers);
        if($elId != Null)
            unset($cachedUsers[$elId]);
        
        if(!empty($cachedUsers))
            Cache::forever('conversation-'.$convId1,$cachedUsers);
        else 
            Cache::forget('conversation-'.$convId1);
    } elseif(Cache::has('conversation-'.$convId2)) {
        $cachedUsers = Cache::get('conversation-'.$convId2);
        $elId = array_search(Auth::user()->id,$cachedUsers);
        if($elId != Null)
            unset($cachedUsers[$elId]);
        
        if(!empty($cachedUsers))
            Cache::forever('conversation-'.$convId2,$cachedUsers);
        else 
            Cache::forget('conversation-'.$convId2);
    }

    return response()->json(['success'=>'true']);
});

// Clans
Route::prefix('klan')->name('clan.')->group(function () {

    Route::post('/post/edit','ClanController@editPost')->name('post.post.edit');
    
    Route::get('/lista','ClanController@showClans')->name('list');
    Route::get('/stworz', 'ClanController@showCreate')->name('create');
    
    Route::post('/stworz', 'ClanController@clanCreate')->name('post.create');
    Route::post('/zapisz', 'ClanController@updateClan')->name('post.edit');
    
    Route::get('/{clan_id}/avatar/usun','ClanController@deleteAvatar')->name('delete.avatar');

    Route::get('/{clan_id}/edytuj', 'ClanController@editClan')->name('edit');
    Route::get('/{clan_id}/czlonkowie', 'ClanController@showMembers')->name('members');
    Route::get('/{clan_id}/dolacz', 'ClanController@joinClan')->name('join');
    
    Route::post('/{clan_id}/akceptuj','ClanController@acceptMember')->name('accept.member');
    Route::post('/{clan_id}/odrzuc','ClanController@rejectMember')->name('accept.member');

    Route::post('/post','ClanController@addPost')->name('post');

    Route::post('/{clan_id}/oposc','ClanController@leaveClan');
    Route::post('/{clan_id}/przekaz','ClanController@changeLeader');

    Route::post('/druzyna/dodaj','TeamController@createTeam')->name('team.create');
    Route::post('/druzyna/usun','TeamController@deleteTeam')->name('team.delete');
    Route::post('/druzyna/edit','TeamController@updateTeam')->name('team.update');
    Route::post('/usun', 'ClanController@deleteClan')->name('delete');

    Route::post('/post/usun','ClanController@deletePost')->name('post.delete');
    
    Route::get('/szukaj', 'ClanController@searchClan')->name('search');
    Route::post('/szukaj', 'ClanController@doSearching')->name('do.search');
    Route::get('/{link}/gracze', 'ClanController@showClanMembers')->name('clan.members');
    Route::get('/{link}/druzyny', 'ClanController@showClanTeams')->name('clan.teams');
    Route::get('/{link}/rozgrywki', 'ClanController@showClanMatches')->name('clan.matches');
    Route::get('/{link}', 'ClanController@showClan')->name('clan');
});

// News
Route::prefix('aktualnosci')->name('news.')->group(function () {

    Route::get('users', 'HomeController@index')->name('users');
});
// Classfieds
Route::prefix('ogloszenia')->name('classfieds.')->group(function () {

    Route::get('users', 'HomeController@index')->name('users');
});
// Chat
Route::prefix('chat')->name('chat.')->group(function () {

    Route::get('users', 'HomeController@index')->name('users');
});
// Games
Route::prefix('gry')->name('games.')->group(function () {

    Route::get('users', 'HomeController@index')->name('users');
});
// Reports
Route::prefix('zgloszenie')->name('raport.')->group(function () {

    Route::post('/dodaj', 'TicketsController@addTicket')->name('new');
});
// Admin

Route::get('/feeds', 'FeedsController@list')->name('feeds');
Route::get('/poczekalnia', function() {
    return view('lobby.show');
});
Route::get('/poczekalnia/user/{id}', 'Lobby\LobbyController@getUser')->name('getLobbyuser');
Route::get('/poczekalnia/join/{user_id}', function($user_id) {
    Cache::forever('user-is-in-lobby-'.$user_id,'true');
    if(App::environment('local'))
        $keys = Redis::keys('laravel_cache:user-is-online-*');
    else {
        $keys = Redis::keys('bazagraczy_cache:user-is-online-*');
    }

    $users = array();
    foreach($keys as $user) {
        $split = explode('-',$user);

        $users[] = array_pop($split);
        
    }

    if(!empty($users)) {
        
        $info = [
            'user_id' => $user_id,
            'status' => 'szukam',
            'game_name' => Auth::user()->selectedGame()['game'],
            'platform_name' => Auth::user()->selectedGame()['platform'],
        ];

        $users = User::find($users);

        if($users)
            foreach($users as $user){
                if($user->id != Auth::user()->id)
                    broadcast(new \App\Events\LobbyStatusChanged($user->id,$info));
            }
            
        
    }
});

Route::get('/poczekalnia/leave/{user_id}', function($user_id) {
    
    Cache::forget('user-is-in-lobby-'.$user_id);
    Cache::forget('lobby-status-'.$user_id);

    if(App::environment('local'))
        $keys = Redis::keys('laravel_cache:user-is-online-*');
    else {
        $keys = Redis::keys('bazagraczy_cache:user-is-online-*');
    }

    $users = array();
    foreach($keys as $user) {
        $split = explode('-',$user);

        $users[] = array_pop($split);
        
    }

    if(!empty($users)) {
        
        $info = [
            'user_id' => $user_id,
            'status' => 'leave',
            'game_name' => Auth::user()->selectedGame()['game'],
            'platform_name' => Auth::user()->selectedGame()['platform'],
        ];

        $users = User::find($users);

        if($users)
            foreach($users as $user){
                if($user->id != Auth::user()->id)
                    broadcast(new \App\Events\LobbyStatusChanged($user->id,$info));
            }
            
        
    }
});

Route::get('/poczekalnia/set/{status}/{user_id}', function($status,$user_id){
    if(Cache::has('lobby-status-'.$user_id)) {
        $cachedStatus = Cache::get('lobby-status-'.$user_id);
        
        if($cachedStatus != $status) {
            Cache::forever('lobby-status-'.$user_id,$status);
        } 
    } else {
        Cache::forever('lobby-status-'.$user_id,$status);
    }

    // broadcast to all online users status changed ... 
    
    if(App::environment('local'))
        $keys = Redis::keys('laravel_cache:user-is-online-*');
    else {
        $keys = Redis::keys('bazagraczy_cache:user-is-online-*');
    }

    $users = array();
    foreach($keys as $user) {
        $split = explode('-',$user);

        $users[] = array_pop($split);
        
    }

    if(!empty($users)) {
        
        $info = [
            'user_id' => $user_id,
            'status' => $status,
            'game_name' => Auth::user()->selectedGame()['game'],
            'platform_name' => Auth::user()->selectedGame()['platform'],
        ];

        $users = User::find($users);

        if($users)
            foreach($users as $user){
                if($user->id != Auth::user()->id)
                    broadcast(new \App\Events\LobbyStatusChanged($user->id,$info));
            }
            
        
    }

    return response()->json(['status'=>$status]);
})->name('lobbystatus');

Route::get('/poczekalnia/mark/{status}/{user_id}','Lobby\LobbyController@markInfo')->name('lobby.mark.info');

Route::post('/poczekalnia/info/set','Lobby\LobbyController@setInfo')->name('lobby.set.info');
Route::get('/poczekalnia','Lobby\LobbyController@show')->name('lobby');


Route::get('/user/{user_id}/status', function($user_id) {
    $user = User::find($user_id);

    $status = $user->getStatus();

    return response()->json($status);
})->name('user_status');

Route::prefix('admin')->name('admin.')->group(function () {

    Route::get('dashboard', 'Admin\AdminController@dashboardShow')->name('dashboard');
    
    Route::get('user', 'Admin\UserController@list')->name('user.list');
    Route::get('user/{id}','Admin\UserController@show')->name('user.show');

    Route::get('menu', 'Admin\AdminController@menuShow')->name('menu');
    Route::get('settings', 'Admin\AdminController@settingsShow')->name('settings');
    Route::get('language', 'Admin\AdminController@languageShow')->name('language');
    Route::get('posts', 'Admin\AdminController@postListShow')->name('posts');
    Route::get('tickets', 'Admin\AdminController@ticketsListShow')->name('tickets');
    
    Route::get('game/list','Admin\GamesController@index')->name('game.list');
    Route::get('game/create', 'Admin\GamesController@createGame')->name('game.create');
    Route::post('game/save', 'Admin\GamesController@saveGame')->name('game.save');
    Route::get('game/scrapper', 'Admin\ScrapperController@index')->name('game.scrapper');
    Route::get('game/scrapper/scrap', 'Admin\ScrapperController@scrap')->name('game.scrapper.scrap');
    Route::get('game/{id}/edit', 'Admin\GamesController@editGame')->name('game.edit');
    Route::post('game/{id}/update', 'Admin\GamesController@updateGame')->name('game.update');
    Route::post('game/{id}/delete', 'Admin\GamesController@deleteGame')->name('game.delete');
    Route::get('game/{id}','Admin\GamesController@showGame')->name('game.show');
    
    
    //Posts
    Route::get('post', 'Admin\PostController@list')->name('post.list');
    Route::get('post/create', 'Admin\PostController@create')->name('post.create');
    Route::post('post/store', 'Admin\PostController@store')->name('post.store');
    Route::get('post/{id}/edit', 'Admin\PostController@edit')->name('post.edit');
    Route::get('post/{id}/update', 'Admin\PostController@update')->name('post.update');
    Route::get('post/{id}/delete', 'Admin\PostController@delete')->name('post.delete');
    Route::get('post/{id}', 'Admin\PostController@show')->name('post.show');

    Route::get('stats/users/append', 'Admin\AdminController@getUsersDailyCount')->name('stats.users.append');
    Route::get('stats/posts/append', 'Admin\AdminController@getPostsDailyCount')->name('stats.posts.append');
    Route::get('stats/comments/append', 'Admin\AdminController@getCommentsDailyCount')->name('stats.comments.append');
    Route::get('stats/logins/append', 'Admin\AdminController@getLoginsDailyCount')->name('stats.logins.append');
    
    Route::get('stats/users/count', 'Admin\AdminDashboardController@countUsers')->name('stats.users.count');

    Route::get('tickets', 'Admin\TicketsController@list')->name('tickets.list');
    Route::get('tickets/{id}', 'Admin\TicketsController@show')->name('tickets.show');
    Route::get('tickets/{id}/close', 'Admin\TicketsController@closeTicket')->name('tickets.close');
    Route::get('tickets/{id}/open', 'Admin\TicketsController@openTicket')->name('tickets.open');
    Route::post('tickets/wyslij','Admin\TicketsController@messageSend')->name('tickets.send');

    Route::get('giveaway', 'Admin\GiveawayController@list')->name('giveaway.list');
    Route::get('giveaway/create', 'Admin\GiveawayController@create')->name('giveaway.create');
    Route::post('giveaway/store', 'Admin\GiveawayController@store')->name('giveaway.store');
    Route::get('giveaway/{id}/edit', 'Admin\GiveawayController@edit')->name('giveaway.edit');
    Route::get('giveaway/{id}/update', 'Admin\GiveawayController@update')->name('giveaway.update');
    Route::get('giveaway/{id}/delete', 'Admin\GiveawayController@delete')->name('giveaway.delete');
    Route::get('giveaway/{id}/activate', 'Admin\GiveawayController@activate')->name('giveaway.activate');
    Route::get('giveaway/{id}/deactivate', 'Admin\GiveawayController@deactivate')->name('giveaway.deactivate');
    Route::get('giveaway/{id}', 'Admin\GiveawayController@show')->name('giveaway.show');

    Route::get('quest', 'Admin\QuestController@list')->name('quest.list');
    Route::get('quest/create', 'Admin\QuestController@create')->name('quest.create');
    Route::post('quest/store', 'Admin\QuestController@store')->name('quest.store');
    Route::get('quest/{id}/edit', 'Admin\QuestController@edit')->name('quest.edit');
    Route::get('quest/{id}/update', 'Admin\QuestController@update')->name('quest.update');
    Route::get('quest/{id}/delete', 'Admin\QuestController@delete')->name('quest.delete');
    Route::get('quest/{id}/activate', 'Admin\QuestController@activate')->name('quest.activate');
    Route::get('quest/{id}/deactivate', 'Admin\QuestController@deactivate')->name('quest.deactivate');
    Route::get('quest/{id}', 'Admin\QuestController@show')->name('quest.show');
});

Route::prefix('helper')->name('help.')->group(function () {
    Route::get('voivodeship', 'HelperController@getVoivodeships')->name('voivodeships');
    Route::get('cities/{voivodeship_id}', 'HelperController@getCities')->name('cities');
    Route::get('games', 'HelperController@getGames')->name('games');
    Route::get('platforms/{game_id}', 'HelperController@getGamesPlatform')->name('platforms');
    Route::get('teams/{clan_id}', 'HelperController@getClanTeams')->name('teams');
    Route::get('members/{clan_id}', 'HelperController@getTeamMembers')->name('members');
    Route::get('set/friends/list','HelperController@setFriendList')->name('friend.list');

});


Route::prefix('mecz')->name('match.')->group(function () {
    Route::prefix('towarzyski')->name('friendly.')->group(function() {
        
        Route::get('stworz','Match\FriendlyController@createFriendly')->name('create');
        Route::post('zapisz','Match\FriendlyController@saveFriendly')->name('save');
        
        Route::get('tablica','Match\FriendlyController@showFriendly')->name('list');
        Route::post('tablica','Match\FriendlyController@doSearching')->name('list.search');
        
        Route::get('zobacz/{id}', 'Match\FriendlyController@showMatch')->name('show');
        Route::get('edytuj/{id}', 'Match\FriendlyController@editMatch')->name('edit');
        Route::post('edytuj','Match\FriendlyController@updateMatch')->name('update');
        Route::get('usun/{ids}','Match\FriendlyController@deleteMatch')->name('delete');
        Route::post('dolacz','Match\FriendlyController@joinMatch')->name('join');
        Route::post('potwierdz','Match\FriendlyController@applyMatch')->name('apply');
        Route::get('akceptuj/{id}','Match\FriendlyController@acceptAplication')->name('accept');
        Route::get('podsumowanie','Match\FriendlyController@confirmFriendly')->name('confirm');
        Route::get('cancel/{id}','Match\FriendlyController@cancelMatch')->name('cancel');

        Route::get('aktywne','Match\FriendlyController@showActive')->name('show.active');
        Route::get('otwarte','Match\FriendlyController@showOpen')->name('show.open');
        Route::get('zamkniete','Match\FriendlyController@showClosed')->name('show.closed');
        Route::get('edit/stop/{id}','Match\FriendlyController@stopEdit')->name('change.edit');
        Route::post('wyslij/wynik','Match\FriendlyController@sendResult')->name('send.result');
        Route::get('aplikacja/wycofaj/{aplication_id}/{match_id}','Match\FriendlyController@aplicationCancel')->name('aplication.cancel');


        Route::get('akceptuj/wynik/{match_id}','Match\FriendlyController@acceptResult')->name('accept.result');
        Route::get('odrzuc/wynik/{match_id}','Match\FriendlyController@rejectResult')->name('reject.result');

        Route::post('archiwum/szukaj','Match\FriendlyController@archiveSearching')->name('archive.search');
        Route::get('archiwum/szukaj','Match\FriendlyController@showArchive')->name('archive.search.get');
        Route::get('archiwum','Match\FriendlyController@showArchive')->name('show.archive');
        
    });


});

Route::get('/public/bg_avatar', function() {
    $image = asset('images/logo/BG_Avatar.png');
    return Image::make($image)->response('png');
});
Route::get('regulamin','PageController@showPage')->name('regulamin');
Route::get('polityka','PageController@showPage')->name('polityka_prywatnosci');
Route::get('kontakt','PageController@showContact')->name('kontakt');
Route::post('kontakt/wyslij','PageController@sendForm')->name('kontakt.wyslij');


/*
Route::prefix('giveaway')->name('giveaway.')->group(function () {

    Route::prefix('/tickets')->name('tickets.')->group(function () {
        Route::get('','Giveaway\Tickets\TicketsController@list')->name('list');
        
    
        
    });

    Route::get('','Giveaway\GiveawayController@list')->name('list');
    Route::get('{id}/join','Giveaway\GiveawayController@join')->name('join');
    Route::get('{id}','Giveaway\GiveawayController@show')->name('show');

});
*/
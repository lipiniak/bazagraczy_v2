<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/


// kanał do konversacji per id conversacji
Broadcast::channel('conversation-{survey_id}-{user2_id}', function ($user, $survey_id,$user2_id) {
    return [
        'id' => $user->id,
        'image' => $user->avatar,
        'full_name' => $user->login
    ];
});
// kanał do powiadomień i krótkich informacji
Broadcast::channel('user-{user_id}', function ($user, $user_id) {
    return [
        'id' => $user->id,
        'image' => $user->avatar,
        'full_name' => $user->login
    ];
});

// kanał do tablicy
Broadcast::channel('wall-{game_id}-{platform_id}', function($user, $game_id,$platform_id){
    return [
        'id' => $user->id,
        'image' => $user->avatar,
        'full_name' => $user->login
    ];
}); 

//Kanał do nowych komentarzy w poście.
Broadcast::channel('post-{post_id}', function ($user, $post_id) {
    return [
        'id' => $user->id,
        'image' => $user->avatar,
        'full_name' => $user->login
    ];
});

//Kanał do powiadomień meczowych
Broadcast::channel('match_{user_id}', function($user,$user_id) {
    return ['id'=> $user->id];
});

Broadcast::channel('lobby-{game_id}-{platform_id}', function($user,$game_id,$platform_id) {
    //get status? 
    if(Cache::has('lobby-status-'.$user->id)) {
        $status = Cache::get('lobby-status-'.$user->id);
    } else {
        $status = 'szukam';
    }
    return ['id'=> $user->id,'status'=>$status];
});
@extends('layouts.app')

@section('content')


<div id="feed" class="content">

        <div class="container-fluid">

            <div class="row">

                <div class="col-12">
                    <h1 class="mb5">Aktualności</h1>
                    <p class="text-muted"><a class="text-muted-link" data-toggle="modal" data-target="#InfoZawartosc">{{$selectedGame['game']}} <span class="badge badge-muted">{{$selectedGame['platform']}}</span></a><p>
                    <div class="modal fade" id="InfoZawartosc" tabindex="-1" role="dialog" aria-labelledby="InfoZawartoscTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <form>
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Info</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body text-left">
                                        <div class="twoja-notatka text-muted">
                                            <p class="mb15">Zawartość aktualnie przeglądanej strony, jej treść oraz wyniki, zawężona jest do treści związanych z <span class="text-white">{{$selectedGame['game']}}</span> <span class="badge badge-white">{{$selectedGame['platform']}}</span></p>
                                            <p class="mb0">Aby zmienić, wybierz inną grę w prawym górnym rogu aplikacji.</p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-12 mb30">
                    <div class="feed-content">
                        @if(!empty($feeds))
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <?php
                                $active = '';
                            ?>
                            @foreach($feeds as $feed)
                                @if($active == '')
                                    @php
                                        $active = 'active';    
                                    @endphp
                                @else 
                                    @php
                                        $active = ' ';
                                    @endphp
                                @endif
                            <li class="nav-item">
                                <a class="nav-link highlight {{$active}}" id="{{$feed['name']}}-tab" data-toggle="tab" href="#{{$feed['name']}}" role="tab" aria-controls="{{$feed['name']}}" aria-selected="true">{{'@'.$feed['name']}}</a>
                            </li>
                            @endforeach                            

                        </ul>
                        <div class="tab-content" id="myTabContent">
                                <?php
                                $active1 = '';
                            ?>
                            @foreach($feeds as $feed)
                            @if($active1 == '')
                                    @php
                                        $active1 = 'active';    
                                    @endphp
                                @else 
                                    @php
                                        $active1 = ' ';
                                    @endphp
                                @endif
                            <div class="tab-pane fade show {{$active1}}" id="{{$feed['name']}}" role="tabpanel" aria-labelledby="{{$feed['name']}}-tab">
                                <a class="twitter-timeline" data-lang="pl" data-width="1200" data-theme="dark" href="https://twitter.com/{{$feed['name']}}">Trwa ładowanie...</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                            </div>
                            @endforeach

                        </div>
                        @else 
                             <p>Brak aktywnych feedów.</p>
                        @endif
                    </div>
                </div>

            </div>

        </div>

    </div>
@endsection

<!doctype html>
<html lang="pl">

    <head>
        <!-- Meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>BAZAGRACZY.pl - Multigamingowa społeczność graczy</title>

        <!-- CSS Bootstrap -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap-grid.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap-reboot.css') }}">
        <!-- CSS Select2 -->
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
        <!-- CSS Horizontal Menu Scroller -->
        <link rel="stylesheet" href="{{ asset('css/horizontal-menu-scroller.css') }}">
        <!-- CSS Strona logowania -->
        <link rel="stylesheet" href="{{ asset('css/login-page.css') }}">
        <!-- CSS Custom -->
        <link rel="stylesheet" href="{{ asset('css/bazagraczy.css') }}">

        <!-- JS -->
        <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
        <!-- JS Bootstrap-->
        <script src="{{ asset('js/popper.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <!-- JS Bootstrap Tooltip i Popover-->
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
            $(function () {
                $('[data-toggle="popover"]').popover()
            })
        </script>
        <!-- JS FontAwesome -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
        <script>
                window.setTimeout(function () {
                    $(".auto-close").fadeTo(500, 0).slideUp(500, function () {
                        $(this).remove();
                    });
                }, 4000);
        </script>
    </head>

    <body>
        <div class="body-wrapper">
            <div id="dowiedz-sie-wiecej" class="content">

                <div class="row">
                    <div class="col-12">
                        <div class="logreg-logo">
                            <a href="/"><img class="mb10" src="{{ asset('images/logo/logo-bg-small-full.png')}}" alt="BAZAGRACZY.PL Logo"></a><br>TUTAJ GRAMY!
                        </div>
                        <div style="width: 100%; max-width: 700px; margin:0 auto;">

                            <div class="row mb15">
                                <div class="col-12">
                                    <p>BAZAGRACZY.pl to serwis zrzeszający wszystkich graczy w jedną społeczność. Jest także niezastąpionym narzędziem, umożliwiającym poszukiwanie towarzyszy do wspólnych rozgrywek.</p>
                                    <p>Funkcjonalności serwisu są stale rozwijane, co czyni go idealnym miejscem dla graczy umożliwiając im wzajemny kontakt i rozrywkę. Dołącz do nas i sam się przekonaj!</p>
                                </div>
                            </div>

                            <div class="row mb15">
                                <div class="col-md-auto text-center">
                                    <img class="img-fluid img-funkcje" src="{{ asset('images/gfx/multi-platform.png')}}" alt="Pc PS4 XBOX - Multiplatform">
                                </div>
                                <div class="col">
                                    <h3>PC? PlayStation? Xbox? To nie ma znaczenia!</h3>
                                    <p>BAZAGRACZY.pl jest multigamingową aplikacją przygotowaną do użytku przez wszystkich graczy, bez względu na grę oraz platformę na jakiej grają.</p>
                                </div>
                            </div>

                            <div class="row mb15">
                                <div class="col-md-auto text-center">
                                    <img class="img-fluid img-funkcje" src="{{ asset('images/gfx/multi-devices.png')}}" alt="Bazagraczy.pl - Aplikacja na wszytkie urządzenia">
                                </div>
                                <div class="col">
                                    <h3>Łatwość i wygoda w użytkowaniu</h3>
                                    <p>Serwis wykonany jest w oparciu o aktualne technologie, co umożliwia korzystanie z niego na różnych urządzeniach. Zaprojektowana została także aplikacja na smartfona, która pozwola na łatwiejszy dostęp do jego zawartości.</p>
                                </div>
                            </div>

                            <h2 class="mt50 mb30">Funkcjonalności</h2>

                            <div class="row mb15">
                                <div class="col-md-auto text-center">
                                    <img class="img-fluid img-funkcje" src="{{ asset('images/gfx/user-profile-funkcje.png')}}" alt="Bazagraczy.pl - Stwórz swój profil">
                                </div>
                                <div class="col">
                                    <h3>Twój profil gracza</h3>
                                    <p>Stwórz własny profil gracza. Uzupełnij swój opis, stwórz bibliotekę gier w które grasz oraz uzupełnij dodatkowe informacje, co pozwoli innym graczom cię odnaleźć i zaprosić do wspólnego grania.</p>
                                </div>
                            </div>

                            <div class="row mb15">
                                <div class="col-md-auto text-center">
                                    <img class="img-fluid img-funkcje" src="{{ asset('images/gfx/user-szukaj-funkcje.png')}}" alt="Bazagraczy.pl - Przeglądarka profilów graczy">
                                </div>
                                <div class="col">
                                    <h3>Wyszukiwarka profilów graczy</h3>
                                    <p>Wybierz kryteria wyszukiwania i znajdź odpowiedniego gracza do wspólnych rozgrywek. Wyszukiwarka ułatwia także znalezienie graczy będących idealnymi kandydatami na członków twojego klanu.</p>
                                </div>
                            </div>

                            <div class="row p15 m0 mb30" style="border: 1px solid white;">
                                <div class="col-md-auto text-center">
                                    <img class="img-fluid img-funkcje" src="{{ asset('images/gfx/znajdz-do-gry-funkcje.png')}}" alt="Bazagraczy.pl - Zbajdź graczy do gry">
                                </div>
                                <div class="col">
                                    <h3>Znajdź graczy do wspólnej gry</h3>
                                    <p>Opublikuj swój post na tablicy informując innych graczy o wolnym miejscu w lobby twojej gry. Poczekaj na odpowiedź i zaproś ich do gry.</p>
                                </div>
                            </div>

                            <div class="row mb15">
                                <div class="col-md-auto text-center">
                                    <img class="img-fluid img-funkcje" src="{{ asset('images/gfx/rozmowy-funkcje.png')}}" alt="Bazagraczy.pl - Prywatne wiadomości">
                                </div>
                                <div class="col">
                                    <h3>Prywatne wiadomości</h3>
                                    <p>Do dyspozycji użytkowników przygotowany został system prywatnych wiadomości umożliwiający szybkie nawiązywanie kontaktów z interesującymi cię graczami.</p>
                                </div>
                            </div>

                            <div class="row mb15">
                                <div class="col-md-auto text-center">
                                    <img class="img-fluid img-funkcje" src="{{ asset('images/gfx/klany-funkcje.png')}}" alt="Bazagraczy.pl - Klany">
                                </div>
                                <div class="col">
                                    <h3>Klany</h3>
                                    <p>Stwórz swój klan i załóż własną stronę klanową, na której zaprezentujesz siebie, swoich zawodników, osiągnięcia oraz będziesz informował odwiedzających ją graczy o waszych aktywnościach.</p>
                                </div>
                            </div>

                            <div class="row mb15">
                                <div class="col-md-auto text-center">
                                    <img class="img-fluid img-funkcje" src="{{ asset('images/gfx/powiadomienia-funkcje.png')}}" alt="Bazagraczy.pl - Powiadomienia">
                                </div>
                                <div class="col">
                                    <h3>Powiadomienia</h3>
                                    <p>Dzięki powiadomieniom nie przegapisz, żadnej aktywności w serwisie.</p>
                                </div>
                            </div>

                            <div class="row mb15">
                                <div class="col-12 text-center">
                                    <a href="{{route('register')}}" class="btn btn-primary">Dołącz do nas!</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>



            </div>
            <!-- KONTENT /Koniec -->   
            @if(session('success'))
            <div class="alert alert-success alert-main alert-dismissible auto-close fade show" role="alert">
                    {!!session('success')!!}<br>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if(session('error'))
            <div class="alert alert-danger alert-main alert-dismissible auto-close fade show" role="alert">
                    {!!session('error')!!}<br>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            
                                
        </div>
        <!-- JS Horizontal Menu Scroller -->
        <script src="assets/js/horizontal-menu-scroller.js"></script>
    </body>

</html>
<div id="menu-profil" class="menu-div" style="display: none;">
        <div class="menu-div-wrapper">
            <div class="menu-inner">

                <h6 class="dark mt20 mb5">PROFIL</h6>
                <div class="submenu-item-wrapper">
                    <a href="/profil/{{Auth::user()->id}}">
                        <div class="submenu-item">
                            <i class="fas fa-user"></i> Zobacz profil</span>
                        </div>
                    </a>
                </div>
                <?php
                $notifications = count(Auth::user()->unreadNotifications);
                ?>
                <div class="submenu-item-wrapper">
                    <a href="{{route('profile.notifications.show')}}">
                        <div class="submenu-item">
                            <i class="fas fa-bullhorn"></i> Powiadomienia @if($notifications > 0)<span class="badge badge-danger"><span class="notification-count1">{{$notifications}}</span></span>@else<span class="badge badge-danger" style='display:none;' id='notification_show1'><span class='notification-count1'></span></span>@endif
                        </div>
                    </a>
                </div>
                <div class="submenu-item-wrapper">
                    <a href="{{route('profile.library')}}">
                        <div class="submenu-item">
                            <i class="fas fa-gamepad"></i> Biblioteka gier</span>
                        </div>
                    </a>
                </div>
                <div class="submenu-item-wrapper">
                    <a href="{{route('profile.edit')}}">
                        <div class="submenu-item">
                            <i class="fas fa-address-card"></i> Edytuj profil</span>
                        </div>
                    </a>
                </div>
                <div class="submenu-item-wrapper">
                    <a href="{{route('profile.avatar')}}">
                        <div class="submenu-item">
                            <i class="fas fa-user-circle"></i> Edytuj avatar</span>
                        </div>
                    </a>
                </div>
                <div class="submenu-item-wrapper">
                    <a href="{{route('profile.account')}}">
                        <div class="submenu-item">
                            <i class="fas fa-key"></i> Dane logowania</span>
                        </div>
                    </a>
                </div>
                <div class="submenu-item-wrapper">
                    <a href="{{route('profile.settings')}}">
                        <div class="submenu-item">
                            <i class="fas fa-cog"></i> Ustawienia</span>
                        </div>
                    </a>
                </div>

                <h6 class="dark mt20 mb5">GRACZE</h6>
                <a href="{{route('profile.followers')}}">
                    <div class="submenu-item">
                        <i class="fas fa-user-friends"></i> Znajomi
                    </div>
                </a>
                <a href="{{route('profile.blocked')}}">
                    <div class="submenu-item">
                        <i class="fas fa-user-alt-slash"></i> Zablokowani
                    </div>
                </a>

                <h6 class="dark mt20 mb5">KLANY</h6>
                <a href="{{route('clan.list')}}">
                    <div class="submenu-item">
                        <i class="fas fa-user-shield"></i> Moje klany
                        @if(Auth::user()->UserClanAplications())
                            <span  id='clan-join' class="badge badge-danger clan-join">
                                <i class="fas fa-exclamation-circle"></i>
                            </span>
                        @else 
                            <span id='clan-join' class="badge badge-danger clan-join" style="display: none;">
                                <i class="fas fa-exclamation-circle"></i>
                            </span> 
                        @endif
                    </div>
                </a>
                <a href="{{route('clan.create')}}">
                    <div class="submenu-item">
                        <i class="fas fa-plus-circle"></i> Stwórz klan 
                    </div>
                </a>

                <h6 class="dark mt20 mb5">WOJNY KLANOWE</h6>
                <a href="{{route('match.friendly.show.open')}}">
                    <div class="submenu-item">
                        <i class="fas fa-calendar-alt"></i> Moje klanówki 
                        
                        @if(Auth::user()->AllFriendlyMatchesAction())
                            <span  id='match-warning-menu' class="badge badge-danger klany-action">
                                <i class="fas fa-exclamation-circle"></i>
                            </span>
                        @else 
                            <span id='match-warning-menu' class="badge badge-danger klany-action" style="display: none;">
                                <i class="fas fa-exclamation-circle"></i>
                            </span> 
                        @endif
                    </div>
                </a>
                <a href="{{route('match.friendly.create')}}">
                    <div class="submenu-item">
                        <i class="fas fa-plus-circle"></i> Stwórz klanówkę
                    </div>
                </a>

            </div>
        </div>
    </div>
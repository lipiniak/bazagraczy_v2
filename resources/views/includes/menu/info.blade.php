<div id="menu-info" class="menu-div" style="display: none;">
    <div class="menu-div-wrapper">
        <div class="menu-inner">

            <h6 class="dark mt30 mb5">BAZAGRACZY.PL</h6>
            <a href="{{route('kontakt')}}">
                <div class="submenu-item">
                    Kontakt
                    <div class="submenu-item-action">
                        <i class="fas fa-chevron-right fa-sm"></i>
                    </div>
                </div>
            </a>
            <a href="{{route('regulamin')}}">
                <div class="submenu-item">
                    Regulamin
                    <div class="submenu-item-action">
                        <i class="fas fa-chevron-right fa-sm"></i>
                    </div>
                </div>
            </a>
            <a href="{{route('polityka_prywatnosci')}}">
                <div class="submenu-item">
                    Polityka prywatności
                    <div class="submenu-item-action">
                        <i class="fas fa-chevron-right fa-sm"></i>
                    </div>
                </div>
            </a>

            <div class="copyright">
                Korzystając z serwisu<br>
                akceptujesz jego <a href="#">Regulamin</a>.<br>
                © BAZAGRACZY.pl 2018<br>
                Wszelkie prawa zastrzeżone.<br>
            </div>

        </div>
    </div>
</div>
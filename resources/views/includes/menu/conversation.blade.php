<div id="menu-komunikator" class="menu-div" style="display: none;">
        <div class="menu-div-wrapper">
            <div class="menu-inner">

                <h6 class="dark mt30 mb15">PRYWATNE WIADOMOŚCI</h6>
                <?php 
                Messenger::setAuthUserId(Auth::user()->id);

                $user = Auth::user();
                
                $conversations = Messenger::user(Auth::user()->id)->threads()?>

                @if($conversations->isEmpty())

                <div class="submenu-item-wrapper no-messages">
                    Brak wiadomości
                </div>
                <div id='conversations'>
                </div>
                @else
                <div id='conversations'>
                @foreach($conversations as $conversation)
                <?php
                    if($conversation->thread->is_seen == 0 AND $conversation->thread->user_id != Auth::user()->id)
                        $new_message = 'nowa-wiadomosc';
                    else 
                        $new_message = '';
                ?>
                

                <div class="submenu-item-wrapper conversation {{$new_message}}" id="conversation-{{$conversation->withUser->id}}">
                    <a onclick='showConversation({{$conversation->withUser->id}},"{{$conversation->withUser->login}}")'>
                        <div class="submenu-item">
                            <div class="avatar-rozmowcy-wrapper">
                                
                                @if($conversation->withUser->avatar == 'no-avatar.jpg')
                                <img src="{{asset('images/avatars/')}}/{{$conversation->withUser->avatar}}" class="menu-avatar" alt="Avatar">
                            @else
                                <img src="{{ Storage::url($conversation->withUser->avatar) }}" class="menu-avatar" alt="Avatar">
                            @endif
                            </div>
                            <div class="nick-rozmowcy">
                                {{$conversation->withUser->login}}
                            </div>
                        </div>
                    </a>
                    <div class="usun-rozmowce-bg"></div>
                    <div class="usun-rozmowce">
                        <a onclick="removeConversation({{$conversation->withUser->id}})">
                            <span>&times;</span>
                        </a>
                    </div>
                </div>

                @endforeach
                </div>
                
                
                @endif
                <div class="submenu-item-wrapper conversation newConversation" id="" style='display:none;'>
                    <a href="" class='adress'>
                        <div class="submenu-item">
                            <div class="avatar-rozmowcy-wrapper">
                                <img src="" class="menu-avatar" alt="Avatar">
                            
                            </div>
                            <div class="nick-rozmowcy">
                                
                            </div>
                        </div>
                    </a>
                    <div class="usun-rozmowce-bg"></div>
                    <div class="usun-rozmowce">
                        <a onclick="">
                            <span>&times;</span>
                        </a>
                    </div>
                </div>
                @if($user->settings()->first()->notification_sound == 1)
                <?php $notification_sound = 1;?>
                @else 
                <?php $notification_sound = 0;?>
                @endif
                @if($user->settings()->first()->messenger_sound == 1)
                <?php $messenger_sound = 1;?>
                @else 
                <?php $messenger_sound = 0;?>
                @endif

                <script>

                    function removeConversation(id) {
                        var url = '/wiadomosci/usun/'+id;
                        $.ajax({
                            method: "GET",
                            url: url,
                        })
                        .done(function( data ) {
                            var element = $('#conversation-'+id);
                            
                            element.remove();

                            $.each($('.conversation'), function(key,value) {
                                if($(value).hasClass('nowa-wiadomosc')) {
                                    count++;
                                } 
                            });
                            
                            if(count == 0) {
                                $('#messages-count').hide();
                            } else {
                                $('#messages-count').show();
                                $('#messages-count').html(count);
                                var title =  $(document).attr('title');
                                $(document).attr('title','('+count+') '+title);
                            }
                        });
                    }

                    // klasa nowa-wiadomosc dla nowej
                    var userId = {{Auth::user()->id}};
                    var newMessageInfo = false;;
                    var messengerSound = {{$messenger_sound}};
                    var notificationSound = {{$notification_sound}};
                    var newConversation = $('.newConversation');

                    Echo.private('user-'+userId)
                    .listen('unReadMessage', (e) => {

                        if(!newMessageInfo)  {
                            
                            var element = $('#conversation-'+e.data.sender_id);
                            
                            if(element.length > 0) {
                                
                                element.remove();

                                element.addClass('nowa-wiadomosc');                        

                                $('#conversations').prepend(element);

                                var count = 0;

                                $.each($('.conversation'), function(key,value) {
                                    if($(value).hasClass('nowa-wiadomosc')) {
                                        count++;
                                    } 
                                });
                                
                                if(count == 0) {
                                    $('#messages-count').hide();
                                } else {
                                    $('#messages-count').show();
                                    $('#messages-count').html(count);
                                }
                            } else {
                                
                                $('.no-messages').remove();

                                var elementNew = newConversation.clone();

                                elementNew.addClass('nowa-wiadomosc');                        
                                
                                elementNew.find('.nick-rozmowcy').html(e.data.sender_login);
                                elementNew.find('.menu-avatar').attr("src",e.data.sender_avatar);
                                elementNew.attr("id",'conversation-'+e.data.sender_id);
                                elementNew.find('.usun-rozmowce a').attr("onclick",'removeConversation('+e.data.sender_id+')');
                                elementNew.find('.adress').attr("href",'/wiadomosci/'+e.data.sender_id);
                                elementNew.toggleClass('newConversation');
                                elementNew.show();

                                $('#conversations').prepend(elementNew);

                                var count = 0;
                                
                                $.each($('.conversation'), function(key,value) {
                                    if($(value).hasClass('nowa-wiadomosc')) {
                                        count++;
                                    } 
                                });
                                
                                if(count == 0) {
                                    $('#messages-count').hide();
                                } else {
                                    $('#messages-count').show();
                                    $('#messages-count').html(count);
                                    var title =  $(document).attr('title');
                                    $(document).attr('title','('+count+') '+title);
                                }
                                
                            }
                            if(messengerSound) {
                                
                                audio.play();
                            }
                            
                            
                        }  

                    
                    }).notification((notification) => {
                       // powiadomienia :)
                       var notificationCount = $('.notification-count').html();
                       var notificationCount1 = $('.notification-count1').html();
                    
                       if(notificationCount == '') {
                            $('#notification_show').show();
                            $('.notification-count').html(1);
                            $('#notification_show1').show();
                            $('.notification-count1').html(1);
                       } else {
                           notificationCount = parseInt(notificationCount,10) + 1;
                           $('.notification-count').html(notificationCount);
                           notificationCount1 = parseInt(notificationCount1,10) + 1;
                           $('.notification-count1').html(notificationCount1);
                       }
                       if(notificationSound) {
                            audio.play();   
                       }
                        
                    }).listen('LobbyStatusChanged', (e) => {
                        
                        var szukam = '<i class="fas fa-clock text-muted"></i>';
                        var afk = '<i class="fas fa-bed text-muted"></i>';
                        var gram = '<i class="fas fa-gamepad text-muted"></i>';
                        var status;
                        if(e.data.status == 'gram')
                            status = gram;
                        if(e.data.status == 'afk')
                            status = afk;
                        if(e.data.status == 'szukam')
                            status = szukam
                        
                        
                        if(e.data.status == 'leave') {
                            $('.user_'+e.data.user_id).find('._lobby_info').remove();
                        } else {
                            $('.user_'+e.data.user_id).find('._lobby_info').remove();
                            $('.user_'+e.data.user_id).find('._fl_info').append('<js class="_lobby_info">'+status+' <u>'+e.data.game_name+' <b>'+e.data.platform_name+'</b></u></js>');
                        }
                        
                        

                    }).listen('MatchHostAction', (e) => {
                        
                        $('#match-warning').show();
                        $('#match-warning-menu').show();
                        
                    }).listen('MatchActionremove', (e) => {
                        $('#match-warning').hide();
                        $('#match-warning-menu').hide();
                        
                    }).listen('FollowerLogin', (e) => {
                        
                            $('.user_'+e.data.user_id).each(function() {
                            
                            // "_user_"+e.data.user_id+"_status";
                            $(this).find('.status').removeClass('offline');
                            $(this).find('.status').removeClass('away');
                            $(this).removeClass('opacity05');
                            $(this).find('.status').addClass('online');

                        });
                    
                        
                        $('#user-'+e.data.user_id).each(function() {
                            $(this).find('.status-color').removeClass('offline');
                            $(this).find('.status-color').removeClass('away');
                        
                            $(this).find('.status-color').addClass('online');
                        });
                        

                        var online = $('#friends_list li .online').length
                        var away = $('#friends_list li .away').length
                        
                        $('#online_friends').html(online+away);
                        
                        var elements = $('#friends_list').children('li').sort(function (a, b) {

                            var elA = $(a).find('.online')
                            var elB = $(b).find('.online')
                            
                
                            if(elA.length > elB.length)
                                return -1;
                            if(elB.length > elA.length)
                                return 1;
                            
                        })
                        $('#friends_list').append(elements);
                
                
                        var offlineElements = $('#friends_list').find('.offline').closest('.lista-graczy')

                        offlineElements.sort(function(a,b) {
                            return  $(a).find('._fl').text().toUpperCase().localeCompare($(b).find('._fl').text().toUpperCase()); 
                        })
                        

                        var awayElements = $('#firends_list').find('.away').closest('.lista-graczy');

                        awayElements.sort(function(a,b) {
                            return  $(a).find('._fl').text().toUpperCase().localeCompare($(b).find('._fl').text().toUpperCase()); 
                        }) 

                        
                        var onlineElements = $('#friends_list').find('.online').closest('.lista-graczy')

                        onlineElements.sort(function(a,b) {
                            return  $(a).find('._fl').text().toUpperCase().localeCompare($(b).find('._fl').text().toUpperCase()); 
                        })
                        
                        $('#friends_list').prepend(onlineElements);
                        $('#firends_list').append(awayElements);
                        $('#friends_list').append(offlineElements);
                        
                    }).listen('FollowerLogout', (e) => {
                        
                        $('.user_'+e.data.user_id).each(function() {
                            $(this).find('.status').removeClass('online');
                            $(this).find('.status').removeClass('away');
                            $(this).addClass('opacity05');
                            $(this).find('.status').addClass('offline');
                        });
                        

                        $('#use-_'+e.data.user_id).each(function() {
                            $(this).find('.status-color').removeClass('online');
                            $(this).find('.status-color').removeClass('away');
                            $(this).find('.status-color').addClass('offline');

                        });
                        

                        var online = $('#friends_list li .online').length
                        var away = $('#friends_list li .away').length
                        
                        $('#online_friends').html(online+away);
                        
                        var elements = $('#friends_list').children('li').sort(function (a, b) {

                            var elA = $(a).find('.online')
                            var elB = $(b).find('.online')
                            
                
                            if(elA.length > elB.length)
                                return -1;
                            if(elB.length > elA.length)
                                return 1;
                            
                        })
                        $('#friends_list').append(elements);
                
                
                        var offlineElements = $('#friends_list').find('.offline').closest('.lista-graczy')

                        offlineElements.sort(function(a,b) {
                            return  $(a).find('._fl').text().toUpperCase().localeCompare($(b).find('._fl').text().toUpperCase()); 
                        })
                        

                        var awayElements = $('#friends_list').find('.away').closest('.lista-graczy');

                        awayElements.sort(function(a,b) {
                            return  $(a).find('._fl').text().toUpperCase().localeCompare($(b).find('._fl').text().toUpperCase()); 
                        }) 

                        
                        var onlineElements = $('#friends_list').find('.online').closest('.lista-graczy')

                        onlineElements.sort(function(a,b) {
                            return  $(a).find('._fl').text().toUpperCase().localeCompare($(b).find('._fl').text().toUpperCase()); 
                        })
                        
                        $('#friends_list').prepend(onlineElements);
                        $('#friends_list').append(awayElements);
                        $('#friends_list').append(offlineElements);
                        
                    }).listen('FollowerAway', (e) => {
                        
                        $('.user_'+e.data.user_id).each(function() {
                            
                            $(this).find('.status').removeClass('online');
                            $(this).find('.status').addClass('away');
                        });
                        
                        

                        $('#user-'+e.data.user_id).each(function() {
                            $(this).find('.status-color').removeClass('online');
                            $(this).find('.status-color').addClass('away');
                        })
                        
                        


                        var online = $('#friends_list li .online').length
                        var away = $('#friends_list li .away').length
                        
                        $('#online_friends').html(online+away);
                        
                        var elements = $('#friends_list').children('li').sort(function (a, b) {

                            var elA = $(a).find('.online')
                            var elB = $(b).find('.online')
                            
                
                            if(elA.length > elB.length)
                                return -1;
                            if(elB.length > elA.length)
                                return 1;
                            
                        })
                        $('#friends_list').append(elements);
                
                
                        var offlineElements = $('#friends_list').find('.offline').closest('.lista-graczy')

                        offlineElements.sort(function(a,b) {
                            return  $(a).find('._fl').text().toUpperCase().localeCompare($(b).find('._fl').text().toUpperCase()); 
                        })


                        var awayElements = $('#friends_list').find('.away').closest('.lista-graczy');

                        awayElements.sort(function(a,b) {
                            return  $(a).find('._fl').text().toUpperCase().localeCompare($(b).find('._fl').text().toUpperCase()); 
                        }) 


                        var onlineElements = $('#friends_list').find('.online').closest('.lista-graczy')

                        onlineElements.sort(function(a,b) {
                            return  $(a).find('._fl').text().toUpperCase().localeCompare($(b).find('._fl').text().toUpperCase()); 
                        })

                        $('#friends_list').prepend(onlineElements);
                        $('#friends_list').append(awayElements);
                        $('#friends_list').append(offlineElements);
                        
                    }).listen('FollowerChangedGame', (e) => {
                        
                        var userElement = $('#user_'+e.data.user_id);
                        
                        userElement.find('._game_name').html(e.data.game_name);
                        userElement.find('._platform_name').html(e.data.platform_name)

                        
                    })
                    ;
                </script>

            </div>
        </div>
    </div>
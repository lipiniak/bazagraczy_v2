<div class="menu-item" onclick="extrasDIV()">
                <i class="fas fa-star"></i>
            </div>
            <script>
                function extrasDIV() {
                    var x = document.getElementById("menu-extras");
                    if (x.style.display === "none") {
                        x.style.display = "block";
                    } else {
                        x.style.display = "none";
                    }
                }
                $(document).ready(function ()
                {
                    $(document).mouseup(function (e)
                    {
                        var subject = $("#menu-extras");

                        if (e.target.id !== subject.attr('id') && !subject.has(e.target).length)
                        {
                            subject.hide();
                        }
                    });
                });
            </script>
<div id="menu-extras" class="menu-div" style="display: none;">
    <div class="menu-div-wrapper">
        <div class="menu-inner">

            <h6 class="dark mt30 mb5">ACHIVMENTS</h6>
            
            <a href="{{route('giveaway.tickets.list')}}">
                <div class="submenu-item">
                    Osiągnięcia
                    <div class="submenu-item-action">
                        <i class="fas fa-chevron-right fa-sm"></i>
                    </div>
                </div>
            </a>
            <h6 class="dark mt30 mb5">GIVEAWAY</h6>
            
            <a href="{{route('giveaway.list')}}">
                <div class="submenu-item">
                    Giveaway'e
                    <div class="submenu-item-action">
                        <i class="fas fa-chevron-right fa-sm"></i>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
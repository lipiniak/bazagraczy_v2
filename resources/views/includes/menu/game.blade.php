<div id="menu-menugra" class="menu-div" style="display: none;">
        <div class="menu-div-wrapper">
            <div class="menu-inner">

                <h6 class="dark mt30 mb5">{{Auth::user()->selectedGame()['game']}} <span class="badge badge-muted">{{Auth::user()->selectedGame()['platform']}}</span></h6>
                <a href="{{route('wall')}}">
                    <div class="submenu-item">
                        <i class="fas fa-clipboard-list"></i> Tablica ogłoszeń
                    </div>
                </a>
                <a href="{{route('lobby')}}">
                    <div class="submenu-item">
                        <i class="fas fa-gamepad"></i> Poczekalnia
                    </div>
                </a>
                <a href="{{route('feeds')}}">
                        <div class="submenu-item">
                            <i class="fas fa-newspaper"></i> Aktualności
                        </div>
                    </a>
                <h6 class="dark mt30 mb5">GRACZE I KLANY</h6>
                <a href="{{route('search.search')}}">
                    <div class="submenu-item">
                        <i class="fas fa-search"></i> Wyszukiwarka graczy
                    </div>
                </a>
                <a href="{{route('clan.search')}}">
                    <div class="submenu-item">
                        <i class="fas fa-search"></i> Wyszukiwarka klanów
                    </div>
                </a>
                <a href="{{route('clan.create')}}">
                    <div class="submenu-item">
                        <i class="fas fa-plus-circle"></i> Stwórz klan 
                    </div>
                </a>
                <h6 class="dark mt30 mb5">WOJNY KLANOWE</h6>
                <a href="{{route('match.friendly.list')}}">
                    <div class="submenu-item">
                        <i class="fas fa-clock"></i> Aktualne klanówki
                    </div>
                </a>
                <a href="{{route('match.friendly.show.archive')}}">
                    <div class="submenu-item">
                        <i class="fas fa-history"></i> Archiwalne klanówki
                    </div>
                </a>
                <a href="{{route('match.friendly.create')}}">
                    <div class="submenu-item">
                        <i class="fas fa-plus-circle"></i> Stwórz klanówkę
                    </div>
                </a>

            </div>
        </div>
    </div>
<div class="wrapper">
        <div class="logo">
            <a href="/"><img src="{{ asset('images/logo/logo-bg-small.png') }}" alt="BAZAGRACZY.pl"></a>
        </div>
        <div class="game">
            <form id='select-game'>
                <div class="title">
                    <div class="form-group" style="width:100%">
                        <label for="topbar-title" class="sr-only">Tytuł gry</label>
                        <select class="form-control" id="topbar-title" style="width:100%">
                           <option></option>
                           @foreach($games as $game) 
                                <option value='{{$game['id']}}' @if($game['id']==$user_select['game_id']) selected @endif>{{$game['text']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="platform">
                    <div class="form-group" style="width:100%">
                        <label for="topbar-platform" class="sr-only">Platforma</label>
                        <select class="form-control" id="topbar-platform" style="width:100%" placholder='Wybierz...'>
                            <option></option>
                            @foreach($platforms as $platform) 
                                <option value='{{$platform['id']}}' @if($platform['id'] === $user_select['platform_id']) selected @endif>{{$platform['text']}}</option>
                            @endforeach
                        </select>
                        
                    </div>
                </div>
                <script>
                    $(document).ready(function () {
                        $('#topbar-platform').select2({
                            placeholder: "Wybierz...",
                            dropdownCssClass : "platforma"
                        }).on('change',function() {
                            sendData();
                        });
                        
                    });
                    
                    $("#topbar-title").select2({
                        placeholder: "Wybierz...",
                        dropdownCssClass : "gra"
                    }).on('change',function() {
                        $('#topbar-platform option[value="4"]').prop("selected", true);
                        var game_id= $('#topbar-title option:selected').attr('value');

                        var url = "/helper/platforms/"+game_id;

                        $.ajax({
                            type: "GET",
                            url: url,        
                        }).done(function( data ) {
                            console.log(data);
                            $('#topbar-platform').empty();
                            $("#topbar-platform").select2({
                                placeholder: "Wybierz...",
                                data: data,
                                dropdownCssClass : "platforma"
                            });

                            sendData();
                        });    
                    });  
                    $('#topbar-platform').on('select2:open', function (e) {
                                          
                        $('.platforma .select2-search input').prop('focus',false);
                    });
                    $('#topbar-title').on('select2:open', function (e) {
                                          
                        $('.gra .select2-search input').prop('focus',false);
                    });
                    
                    function sendData() {
                        var game_id= $('#topbar-title option:selected').attr('value');
                        var platform_id= $('#topbar-platform option:selected').attr('value');

                        var setUrl = '/profil/ustawienia/gra/'+game_id+'/'+platform_id;

                        $.ajax({
                            method: "GET",
                            url: setUrl,                               
                        }).done(function(data) {
                            window.location.replace("{{Request::url()}}");
                        });
                    }
                    </script>
            </form>
        </div>
    </div>

<script>


    function showConversation(user_id,user_name) {

        var modal = $('#conversation_modal');
        var item = $('#user_'+user_id);
        
        var conversation = $('#conversation-'+user_id);

        var item = item.find('.status');

        if(conversation.hasClass('nowa-wiadomosc')) {
            conversation.removeClass('nowa-wiadomosc');
            var count = 0;
                                
            $.each($('.conversation'), function(key,value) {
                if($(value).hasClass('nowa-wiadomosc')) {
                    count++;
                } 
            });
            
            if(count == 0) {
                $('#messages-count').hide();
            } else {
                $('#messages-count').show();
                $('#messages-count').html(count);
            }
        }

        var status;
       

        // get user status;
        $.ajax({url: '/user/'+user_id+'/status', success: function(result){
            console.log(result);
            status = result;
            console.log('status: '+status);

            modal.find('._user_name').html(user_name);
            modal.find('a').attr('href','/profil/'+user_id);
            modal.find('#reciverId').val(user_id);
            
            modal.find('.modal-header').removeClass(function (index, className) {
                return (className.match (/(^|\s)user\_\S+/g) || []).join(' ');
            });

            modal.find('.modal-header').addClass("user_"+user_id)
            modal.find('.status').removeClass('away');
            modal.find('.status').removeClass('offline');
            modal.find('.status').removeClass('online');
            modal.find('.status').addClass(status);
            
            var file_src = '/wiadomosci/'+user_id+'/?type=modal';
            $('<iframe>')                      // Creates the element
                    .attr('src',file_src) // Sets the attribute spry:region="myDs"
                    .attr('height','100%')
                    .attr('frameBorder',0)
                    .attr('style','height: 100%; margin: 0px; padding: 0px; border: none;')
                    .appendTo(modal.find('._content'));
            modal.modal();
        }});
        
       

    }
    
    $(document).ready(function() {
        $('#conversation_modal').on('hide.bs.modal', function (e) {
            $('#conversation_modal').find('._content').html('');
            var reciverId = $('#reciverId').val();
            
            $.ajax({url: '/conversation/close/'+reciverId, success: function(result){
                
            }});


          })
    });
    
</script>

<div class="modal fade chatmodal" id="conversation_modal" role="dialog" aria-labelledby="RozmowaUser001Title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document" style="height: 100%; margin: 0 auto;">
        <div class="modal-content" style="height: 100%;">
            <div class="modal-header pt10 pl15 pb10 pr15 ">
                
                <h5 class="modal-title"><a href="" class="highlight"><span class="status"></span> <span class="_user_name"></span></a></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <input type='hidden' id='reciverId' value=''>
            <div class="modal-body text-left p0 _content">
                
            </div>
        </div>
    </div>
</div>

<!-- MENU ZNAJOMI /Start --> 
<div class="friends-menu">

    <div class="row">
        <div class="col-12">
            <input type="text" id="friendsInput" onkeyup="FiltrowanieListyZnajomych()" placeholder="Znajdź...">
            <ul id="friends_list">

                        
                @foreach($friends as $friend)
                <?php
                    $note = $friend->note()->where('user_id',Auth::user()->id)->get();
                                            ?>
                <li class="lista-graczy @if($friend->getStatus() == 'offline') opacity05 @endif user_{{$friend->id}}">
                    <div class="pojedynczy-wynik">
                        <a class="pojedynczy-wynik-link" onclick='showConversation({{$friend->id}},"{{$friend->login}}")'></a>

                        <div class="avatar-wrapper">
                                @if($friend->avatar == 'no-avatar.jpg')
                                <img src="{{asset('images/avatars/')}}/{{$friend->avatar}}" class="menu-avatar" alt="Avatar">
                            @else
                                <img src="{{ Storage::url($friend->avatar) }}" class="menu-avatar" alt="Avatar">
                            @endif
                            <div id="_user_{{$friend->id}}_status" class="status status-color {{$friend->getStatus()}}"></div>
                        </div>
                        <div class="gracz-info">
                            
                            <div class="friends-akcje">
                                    @if(!empty($note[0]))
                                <a class="btn btn-secondary" data-toggle="modal" data-target="#note-info" onclick='showNoteModal("{{$friend->login}}",{{$friend->id}})'><i class="fas fa-info-circle"></i></a>
                                    <input type="hidden" value="{!!nl2br(e($note[0]->note))!!}" name="{{$friend->id}}_note" id="{{$friend->id}}_note">
                                @endif
                            </div>
                            
                            <span class='_fl_info'>
                                <p class="_fl">{{$friend->login}}</p><br>
                                {!!$friend->isInLobby()!!}
                            </span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </li>
                
                @endforeach

            </ul>
        </div>
    </div>

</div>

<button class="btn btn-secondary btn-sm btn-friends switch"><span class="btn-friends-show">Pokaż znajomych</span><span class="btn-friends-hide">Ukryj znajomych</span> (<span class="text-success" id='online_friends'>0</span>/{{$friends_count}})</button>
<script>
    var online = $('#friends_list li .online').length
    var away = $('#friends_list li .away').length
    
    $('#online_friends').html(online+away);

    
        var elements = $('#friends_list').children('li').sort(function (a, b) {

            var elA = $(a).find('.online')
            var elB = $(b).find('.online')

            if(elA.length > elB.length)
                return -1;
            if(elB.length > elA.length)
                return 1;
            
        }) // End Sort

        $('#friends_list').append(elements);
        
        var offlineElements = $('#friends_list').find('.offline').closest('.lista-graczy')

        offlineElements.sort(function(a,b) {
            return  $(a).find('._fl').text().toUpperCase().localeCompare($(b).find('._fl').text().toUpperCase()); 
        })
        

        var awayElements = $('#friends_list').find('.away').closest('.lista-graczy');

        awayElements.sort(function(a,b) {
            return  $(a).find('._fl').text().toUpperCase().localeCompare($(b).find('._fl').text().toUpperCase()); 
        }) 

        
        var onlineElements = $('#friends_list').find('.online').closest('.lista-graczy')

        onlineElements.sort(function(a,b) {
            return  $(a).find('._fl').text().toUpperCase().localeCompare($(b).find('._fl').text().toUpperCase()); 
        })
        
        $('#friends_list').prepend(onlineElements);
        $('#friends_list').append(awayElements);
        $('#friends_list').append(offlineElements);

        $('.switch').on('click', function (e) {
            $('.body').toggleClass("show-friends show-friends-xs"); //you can list several class names 

            var url = '/helper/set/friends/list';
                        $.ajax({
                            method: "GET",
                            url: url,
                        })
            e.preventDefault();
        });



    function sortFriendsList() {
        var friendsList = $('#friends_list');

    }
    var onlineList = $('.online').closest('.lista-graczy');

    var offlineList = $('.offline').closest('.lista-graczy');

    var friendsList = $('#friends_list');

    


</script>
<a href="{{route('profile.followers')}}" class="btn btn-secondary btn-sm btn-friends-cog"><i class="fas fa-cog"></i></a>
<!-- MENU ZNAJOMI /Koniec -->  
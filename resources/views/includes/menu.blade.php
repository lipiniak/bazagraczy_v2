<div class="wrapper">
        <div class="menu">


            <!-- Pojedyncza ikona menu /start -->
            <div class="menu-item" onclick="profilDIV()">
                    <a href='#'>  
                    @if(Auth::user()->avatar == 'no-avatar.jpg')
                        <img src="{{asset('images/avatars/')}}/{{Auth::user()->avatar}}" class="menu-avatar" alt="Avatar">
                    @else
                        <img src="{{ Storage::url(Auth::user()->avatar) }}" class="menu-avatar" alt="Avatar">
                    @endif
                    </a>
                <?php
                $notifications = count(Auth::user()->unreadNotifications);
                ?>
                @if($notifications > 0)
                
                <span class="badge badge-danger menu-badge" >
                    <span class='notification-count'>{{$notifications}}</span>
                </span>
                
                @else
                
                <span class="badge badge-danger menu-badge" style='display:none;' id='notification_show'>
                        <span class='notification-count'></span>
                </span>
                
                @endif
                @if(Auth::user()->AllFriendlyMatchesAction())
                    <span  id='match-warning' class="badge badge-danger menu-badge klany-action">
                        <i class="fas fa-exclamation-circle"></i>
                    </span>
                @else 
                    <span id='match-warning' class="badge badge-danger menu-badge klany-action" style="display: none;">
                        <i class="fas fa-exclamation-circle"></i>
                    </span> 
                @endif
                
            </div>
            <script>
                    function profilDIV() {
                        var x = document.getElementById("menu-profil");
                        if (x.style.display === "none") {
                            x.style.display = "block";
                        } else {
                            x.style.display = "none";
                        }
                    }
                    $(document).ready(function ()
                    {
                        $(document).mouseup(function (e)
                        {
                            var subject = $("#menu-profil");

                            if (e.target.id !== subject.attr('id') && !subject.has(e.target).length)
                            {
                                subject.hide();
                            }
                        });
                    });
                </script>
            <!-- Pojedyncza ikona menu /koniec -->
            
            <!-- Pojedyncza ikona menu /start -->
            <div class="menu-item" onclick="komunikatorDIV()">
                <i class="fas fa-comments"></i>
                <?php
                    $count = Messenger::countUnSeen();
                ?>
                @if($count > 0)
                <span class="badge badge-danger menu-badge" id='messages-count'>
                        {{$count}}
                    </span>
                @else 
                <span class="badge badge-danger menu-badge" id='messages-count' style='display: none;'>
                        
                    </span>
                @endif
            </div>

            

            <script>
                function komunikatorDIV() {
                    var x = document.getElementById("menu-komunikator");
                    if (x.style.display === "none") {
                        x.style.display = "block";
                    } else {
                        x.style.display = "none";
                    }
                }
                $(document).ready(function ()
                {
                    $(document).mouseup(function (e)
                    {
                        var subject = $("#menu-komunikator");

                        if (e.target.id !== subject.attr('id') && !subject.has(e.target).length)
                        {
                            subject.hide();
                        }
                    });
                });
            </script>
           
            <div class="menu-item" onclick="menugraDIV()">
                <i class="fas fa-gamepad"></i>
            </div>
            <script>
                function menugraDIV() {
                    var x = document.getElementById("menu-menugra");
                    if (x.style.display === "none") {
                        x.style.display = "block";
                    } else {
                        x.style.display = "none";
                    }
                }
                $(document).ready(function ()
                {
                    $(document).mouseup(function (e)
                    {
                        var subject = $("#menu-menugra");

                        if (e.target.id !== subject.attr('id') && !subject.has(e.target).length)
                        {
                            subject.hide();
                        }
                    });
                });
            </script>
            
           
            <!-- Pojedyncza ikona menu /start -->
            <div class="menu-item" onclick="infoDIV()">
                <i class="fas fa-info-circle"></i>
            </div>
            <script>
                function infoDIV() {
                    var x = document.getElementById("menu-info");
                    if (x.style.display === "none") {
                        x.style.display = "block";
                    } else {
                        x.style.display = "none";
                    }
                }
                $(document).ready(function ()
                {
                    $(document).mouseup(function (e)
                    {
                        var subject = $("#menu-info");

                        if (e.target.id !== subject.attr('id') && !subject.has(e.target).length)
                        {
                            subject.hide();
                        }
                    });
                });
            </script>
            
            <!-- Pojedyncza ikona menu /koniec -->

            @if(Auth::user()->role_id == 2)
            <div class="menu-item">
                <a href="{{route('admin.dashboard')}}"><i class="fas fa-cogs"></i></a>
            </div>
            @endif
            <div class="menu-item">
                <a href="#" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="fas fa-sign-out-alt"></i>        
                </a>
                </p>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                    </form>
            </div>
            
        </div>  
    </div>
<!-- Meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>BAZAGRACZY.pl - Portal każdego gracza!</title>
       
        
       
        <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
       
        
        
        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('icon/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('icon/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('icon/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('icon/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('icon/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('icon/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('icon/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('icon/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('icon/apple-icon-180x180.png') }}">
        <link rel="icon" sizes="192x192"  href="{{ asset('icon/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('icon/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('icon/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('icon/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('icon/manifest.json') }}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ asset('icon/ms-icon-144x144.png') }}">
        <meta name="theme-color" content="#ffffff">


        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script src="{{ asset('js/app.js') }}"></script>
        <!-- CSS Bootstrap -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap-grid.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap-reboot.css') }}">
        <!-- CSS Select2 -->
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
        <!-- CSS Horizontal Menu Scroller -->
        <link rel="stylesheet" href="{{ asset('css/horizontal-menu-scroller.css') }}">
        <!-- CSS Strona logowania -->
        <!-- CSS Custom -->
        <link rel="stylesheet" href="{{ asset('css/bazagraczy.css?v=11') }}">
        
        <!-- JS -->
        <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
        <script src="{{ asset('js/anchorme.min.js') }}"></script>
        <!-- JS Bootstrap-->
        <script src="{{ asset('js/popper.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        
        <!-- JS Bootstrap Tooltip i Popover-->
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
            $(function () {
                $('[data-toggle="popover"]').popover()
            })
        //page audio
            var audio = new Audio('/sounds/wiadomosc.mp3');
        </script>
        <!-- JS FontAwesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

        <!-- JS Select2-->
        <script src="{{ asset('js/select2.full.js') }}"></script>     
        <!-- JS Select2 dla pól input -->
        
        <!-- JS po upływie czasu ukrywa elementy z klasą auto-close -->
        <script>
            window.setTimeout(function () {
                $(".auto-close").fadeTo(500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 4000);
        </script>
        <!-- JS Pole input filtrujące wyniki listy -->
        <script>
            function FiltrowanieListy() {
                // Declare variables
                var input, filter, ul, li, a, i;
                input = document.getElementById('gamesInput');
                filter = input.value.toUpperCase();
                ul = document.getElementById("gamesUL");
                li = ul.getElementsByTagName('li');

                // Loop through all list items, and hide those who don't match the search query
                for (i = 0; i < li.length; i++) {
                    a = li[i].getElementsByTagName("span")[0];
                    if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        li[i].style.display = "";
                    } else {
                        li[i].style.display = "none";
                    }
                }
            }

            function FiltrowanieListyZnajomych() {
                // Declare variables
                var input, filter, ul, li, a, i;
                input = document.getElementById('friendsInput');
                filter = input.value.toUpperCase();
                ul = document.getElementById("friends_list");
                li = ul.getElementsByTagName('li');
                
                // Loop through all list items, and hide those who don't match the search query
                for (i = 0; i < li.length; i++) {
                    a = li[i].getElementsByTagName("span")[0];
                    if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        li[i].style.display = "";
                    } else {
                        li[i].style.display = "none";
                    }
                }
            }

            // po 5 minutach wysyłamy sygnał do serwera że strona jest otwarta.
            
            var timer = null;

            function startTimer(){
                
                timer = window.setTimeout(function() {
                    
                    $.ajax({url: '/heartbeep', success: function(result){
                        
                        clearTimeout(timer);
                        startTimer();
                    }});
                    
                    
                },294000); //294000

                
            }

            startTimer();

            // FUnckja wywoływana po kliknięciu na body
            function awayCheckerFn() {
               
                $.ajax({url: '/online/check', success: function(result){
                
                }});
                

                clearTimeout(timer);
                startTimer();
            } 
            Date.prototype.customFormat = function(formatString){
                var YYYY,YY,MMMM,MMM,MM,M,DDDD,DDD,DD,D,hhhh,hhh,hh,h,mm,m,ss,s,ampm,AMPM,dMod,th;
                YY = ((YYYY=this.getFullYear())+"").slice(-2);
                MM = (M=this.getMonth()+1)<10?('0'+M):M;
                MMM = (MMMM=["January","February","March","April","May","June","July","August","September","October","November","December"][M-1]).substring(0,3);
                DD = (D=this.getDate())<10?('0'+D):D;
                DDD = (DDDD=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][this.getDay()]).substring(0,3);
                th=(D>=10&&D<=20)?'th':((dMod=D%10)==1)?'st':(dMod==2)?'nd':(dMod==3)?'rd':'th';
                formatString = formatString.replace("#YYYY#",YYYY).replace("#YY#",YY).replace("#MMMM#",MMMM).replace("#MMM#",MMM).replace("#MM#",MM).replace("#M#",M).replace("#DDDD#",DDDD).replace("#DDD#",DDD).replace("#DD#",DD).replace("#D#",D).replace("#th#",th);
                h=(hhh=this.getHours());
                if (h==0) h=24;
                if (h>12) h-=12;
                hh = h<10?('0'+h):h;
                hhhh = hhh<10?('0'+hhh):hhh;
                AMPM=(ampm=hhh<12?'am':'pm').toUpperCase();
                mm=(m=this.getMinutes())<10?('0'+m):m;
                ss=(s=this.getSeconds())<10?('0'+s):s;
                return formatString.replace("#hhhh#",hhhh).replace("#hhh#",hhh).replace("#hh#",hh).replace("#h#",h).replace("#mm#",mm).replace("#m#",m).replace("#ss#",ss).replace("#s#",s).replace("#ampm#",ampm).replace("#AMPM#",AMPM);
              };
              
        </script>
        <script>
                function FilterWaiting() {
                    // Declare variables
                    var input, filter, ul, li, a, i;
                    input = document.getElementById('WaitingInput');
                    filter = input.value.toUpperCase();
                    ul = document.getElementById("WaitingUL");
                    li = ul.getElementsByTagName('li');
    
                    // Loop through all list items, and hide those who don't match the search query
                    for (i = 0; i < li.length; i++) {
                        a = li[i].getElementsByTagName("span")[0];
                        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                            li[i].style.display = "";
                        } else {
                            li[i].style.display = "none";
                        }
                    }
                }
            </script>
            <script>
                function FilterInGame() {
                    // Declare variables
                    var input, filter, ul, li, a, i;
                    input = document.getElementById('InGameInput');
                    filter = input.value.toUpperCase();
                    ul = document.getElementById("InGameUL");
                    li = ul.getElementsByTagName('li');
    
                    // Loop through all list items, and hide those who don't match the search query
                    for (i = 0; i < li.length; i++) {
                        a = li[i].getElementsByTagName("span")[0];
                        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                            li[i].style.display = "";
                        } else {
                            li[i].style.display = "none";
                        }
                    }
                }
            </script>
        <meta property="og:title" content="BAZAGRACZY.pl - Portal każdego gracza!" />
        <meta property="og:image" content="https://app.playershub.net/images/logo/bg_avatar.png" />
        <meta property="og:description" content="BAZAGRACZY.pl oraz BAZAGRACZY App to serwis i aplikacja umożliwiająca jej użytkownikom znalezienie graczy do wspólnych rozgrywek online w różne gry multiplayer bez względu na platformę na jakich grają. Dodatkowo BAZAGRACZY udostępnia za darmo narzędzia niezbędne każdemu graczowi. Możliwość rejestracji klanów, wyszukiwarka klanów, wojny klanowe, komunikator to tylko niektóre z nich." />


<script>

    function showNoteModal(login,id) {
        console.log(id);
        var modal = $('#note-info');
        var input = $('#'+id+'_note');
        console.log($('#'+id+'_note'));
        console.log(input.val());
        modal.find('.modal-title').html(login);
        modal.find('#note_info').html(input.val());
    }
</script>
<div class="modal fade" id="note-info" tabindex="-1" role="dialog" aria-labelledby="IdGraczaNotatkaTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form>
            
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-left">
                    <div class="twoja-notatka">
                        <h6 class="text-muted">Notatka</h6>
                        <p class="mb0" id='note_info'></p>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
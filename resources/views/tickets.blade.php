<header>
    <title>My website - @yield('page')</title>
    </header>
<body>
    @yield('content')
    
    <script src="/js/jquery.min.js"></script>
    
    @yield('footer')
    </body>
@foreach($items as $item)
  <li @lm_attrs($item) @if($item->hasChildren()) class="treeview" @endif @lm_endattrs>
    @if($item->link) <a@lm-attrs($item->link) @lm-endattrs href="{!! $item->url() !!}">
        {!! $item->title !!}
        @if($item->hasChildren())
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        @endif
    </a>
    @else
      <span>{!! $item->title !!}</span>
    @endif
    @if($item->hasChildren())
      <ul class="treeview-menu">
        @include(config('laravel-menu.views.bootstrap-items'), array('items' => $item->children()))
      </ul>
    @endif
  </li>
  @if($item->divider)
  	<li{!! Lavary\Menu\Builder::attributes($item->divider) !!}></li>
  @endif
@endforeach

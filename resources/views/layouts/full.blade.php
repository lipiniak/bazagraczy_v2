<!doctype html>
<html lang="pl">

    <head>
        @include('includes.header')
    </head>

    <body>
        <div class="body" id="nobars">

            <!-- KONTENT /Start -->
            <div class="body-wrapper" id='content-wrapper'>
                @yield('content')
                
            </div>
            
            <!-- KONTENT /Koniec -->   


            <!-- ALERTY /Start -->
            
            
            @if(Session::has('error')) 
                <div class="alert alert-danger alert-main alert-dismissible auto-close fade show" role="alert">
                    {!!session('error')!!}<br>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>  
            @endif
            @if(Session::has('success'))
            <div class="alert alert-success alert-main alert-dismissible auto-close fade show" role="alert">
                {!!session('success')!!}<br>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <!-- ALERTY /Koniec -->


        </div>
        
    </body>

</html>
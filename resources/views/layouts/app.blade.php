<!doctype html>
<html lang="pl">

    <head>
        @include('includes.header')
    </head>

    <body onClick='awayCheckerFn();'>
        <div class="body @if(Auth::user()->settings()->first()->friends_list_status == 1) show-friends @endif" id='body'>


            <!-- MENU GÓRNE /Start -->
            <div id="topbar">
                @include('includes.topmenu')
            </div>
            <!-- MENU DOLNE /Start -->


            <!-- MENU LEWE /Start -->
            <div id="leftbar">
                @include('includes.menu')
            </div>
            <!-- MENU LEWE /Koniec -->


            <!-- SUBMENU DIV /Start -->
            <!-- Submenu /start -->
                @include('includes.menu.info')
                @include('includes.menu.game')
                @include('includes.menu.conversation')
                @include('includes.menu.profil')
                
                
            <!-- Submenu /koniec -->
            <!-- SUBMENU DIV /Koniec -->


            <!-- KONTENT /Start -->
            <div class="body-wrapper" id='content-wrapper'>
                    <?php $currentRouteName = Route::currentRouteName();
                    
                    $user = Auth::user();
                    ?>
                    <div id="top-alerts">
                        @foreach($user->pushMessages->where('route_name',$currentRouteName) as $push)
                            @if($push->is_seen == 0)
                            <div class="col-12 p0">
                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                    {!!$push->content!!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true" onClick='pushRead({{$push->id}})'>&times;</span>
                                    </button>
                                </div>
                            </div>
                            @endif
                        @endforeach
                        <script>
                            function pushRead(id) {
                                var url = '/push/delete/'+id;
                                $.ajax({
                                    method: "GET",
                                    url: url,
                                })
                                .done(function( data ) {
                                    
                                });
                            }

                        </script>
                    </div>
                    
                @yield('content')
            </div>
            
            <!-- KONTENT /Koniec -->   

            @include('includes.friends')
            @include('includes.conversation')
            @include('includes.note')
            <!-- ALERTY /Start -->
            
            
            @if(Session::has('error')) 
                <div class="alert alert-danger alert-main alert-dismissible auto-close fade show" role="alert">
                    {!!session('error')!!}<br>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>  
            @endif
            @if(Session::has('success'))
            <div class="alert alert-success alert-main alert-dismissible auto-close fade show" role="alert">
                {!!session('success')!!}<br>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <!-- ALERTY /Koniec -->


        </div>
        
    </body>

</html>
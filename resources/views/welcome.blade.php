<!doctype html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        
        <title>BAZAGRACZY.pl - Portal każdego gracza!</title>

        
        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('icon/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('icon/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('icon/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('icon/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('icon/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('icon/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('icon/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('icon/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('icon/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('icon/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('icon/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('icon/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('icon/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('icon/manifest.json') }}">

        <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" type="text/css" media="all" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('css/animate.css') }}" type="text/css" media="all" />
        <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}" type="text/css" media="all" />
        <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css" media="all" />

        <!--[if lt IE 9]>
            <script src="{{ asset('js/html5.js') }}"></script>
            <script src="{{ asset('js/respond.min.js') }}"></script>
        <![endif]-->
    </head>

    <body id="landing-page">

        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = 'https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v3.2&appId=619489001724893&autoLogAppEvents=1';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

        <!-- Preloader -->
        <div id="mask">
            <div id="loader"></div>
        </div>

        <header>
            <nav class="navigation navigation-header">
                <div class="container">
                    <div class="navigation-brand">
                        <div class="brand-logo">
                            <a href="#" class="logo"></a>
                            <span class="sr-only">BAZAGRACZY.pl</span>
                        </div>
                        <button class="navigation-toggle visible-xs" type="button" data-toggle="dropdown" data-target=".navigation-navbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="navigation-navbar">
                        <ul class="navigation-bar navigation-bar-left">
                            <li class="active"><a href="#hero">Home</a></li>
                            <li><a href="#about">PROJEKT</a></li>
                            <li><a href="#features-list">FUNKCJE</a></li>
                            <li><a href="#movies">WPROWADZENIE</a></li>
                            <li><a href="#footer">KONTAKT</a></li>
                        </ul>
                        <ul class="navigation-bar navigation-bar-right">
                            <li><a href="{{route('login')}}">Zaloguj</a></li>
                            <li class="featured"><a href="{{route('register')}}">Załóż konto</a></li>
                        </ul>  
                    </div>
                </div>
            </nav>
        </header>

        <div id="hero" class="static-header plain-version light clearfix">
            <div class="text-heading">
                <h1 class="animated hiding" data-animation="bounceInDown" data-delay="0">Wszyscy gracze i klany w <span class="highlight">jednym</span> miejscu!</h1>
                <p class="animated hiding" data-animation="fadeInDown" data-delay="500">Tutaj tworzymy największą polską multiplatformową społeczność graczy.</p>
                <ul class="list-inline">
                    <li style="display: block;"><a href="{{route('register')}}" class="btn btn-primary animated hiding" data-animation="bounceIn" data-delay="700" style="margin-bottom: 15px;">Zarejestruj się jako gracz</a></li>
                    <li style="display: block;"><a href="{{route('login')}}" class="btn btn-default animated hiding" data-animation="bounceIn" data-delay="900" style="padding-top: 8px; padding-bottom: 8px; margin-bottom: 15px;">Zaloguj</a></li>
                </ul>
            </div>
        </div>


        <!-- Miejsce na reklamę partnerów
        <div id="clients">
            <div class="container">
                <ul class="list-inline logos">
                    <li><img class="animated hiding" data-animation="fadeInUp" data-delay="0" src="assets/img/logos/logo-1.png" alt="mashable" /></li>
                    <li><img class="animated hiding" data-animation="fadeInUp" data-delay="200" src="assets/img/logos/logo-2.png" alt="tnw" /></li>
                    <li><img class="animated hiding" data-animation="fadeInUp" data-delay="400" src="assets/img/logos/logo-3.png" alt="virgin" /></li>
                    <li><img class="animated hiding" data-animation="fadeInUp" data-delay="600" src="assets/img/logos/logo-4.png" alt="microsoft" /></li>
                    <li><img class="animated hiding" data-animation="fadeInUp" data-delay="800" src="assets/img/logos/logo-5.png" alt="forbes" /></li>
                </ul>
            </div>
        </div>
        -->




        <a id="showHere"></a>

        <section id="about" class="section dark">
            <div class="container">

                <div class="tab-pane cutom-wrapper">
                    <div class="section-content row">        
                        <div class="col-sm-6 animated hiding" data-animation="fadeInLeft">
                            <img src="{{ asset('/images/features/misjabazygraczy.png') }}" class="img-responsive" alt="Misja BAZAGRACZY" style="margin: 0px auto;"/>
                        </div>
                        <div class="col-sm-6 animated hiding" data-animation="fadeInRight">
                            <br/>
                            <article class="center">
                                <h3><span class="highlight">TWOJA</span> BAZAGRACZY</h3>
                                <div class="sub-title mb10">Łączymy graczy w jedną społeczność<br/>bez względu na gry i platformy na jakich grają.</div>
                                <p>BAZAGRACZY jest projektem będącym odpowiedzią na problem osób poszukujących innych graczy i klanów do wspólnych rozgrywek online.<br/>Oprócz podstawowych rozwiązań, jak wyszukiwarka czy komunikator, serwis posiada wiele dodatkowych funkcji i rozwiązań przydatnych każdemu graczowi.</p>
                            </article>
                        </div>
                    </div>
                </div>

                <div class="tab-pane cutom-wrapper">
                    <div class="section-content row"> 
                        <div class="col-sm-6 animated hiding" data-animation="fadeInRight">
                            <br/>
                            <article class="center">
                                <h3><span class="highlight">WIELE</span> GIER</h3>
                                <p>BAZAGRACZY obsługuje wiele tytułów gier i nie tylko tych najpopularniejszych. Jeżeli jednak nie znajdziesz interesującej cię gry, możesz nam to zgłosić. Administracja na bieżąco monitoruje zapotrzebowanie na nowe tytuły.</p>
                            </article>
                        </div>
                        <div class="col-sm-6 animated hiding" data-animation="fadeInLeft">
                            <img src="{{ asset('/images/features/wielegier.png') }}" class="img-responsive" alt="Misja BAZAGRACZY" style="margin: 0px auto;"/>
                        </div>
                    </div>
                </div>

                <div class="tab-pane cutom-wrapper">
                    <div class="section-content row">        
                        <div class="col-sm-6 animated hiding" data-animation="fadeInLeft">
                            <img src="{{ asset('/images/features/rozneplatformy.png') }}" class="img-responsive" alt="Misja BAZAGRACZY" style="margin: 0px auto;"/>
                        </div>
                        <div class="col-sm-6 animated hiding" data-animation="fadeInRight">
                            <br/>
                            <article class="center">
                                <h3><span class="highlight">RÓŻNE</span> PLATFORMY</h3>
                                <p>BAZAGRACZY to serwis multiplatformowy. Jego zawartość i funkcje są dostępne dla wszystkich graczy bez względu na platformy na jakich grają.<br><span class="highlight">Łączymy graczy, nie dzielimy!</span></p>
                            </article>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section id="features" class="section inverted p0">
            <div class="bg-spacer">
                <div class="container">
                    <div class="section-content">
                        <div class="tab-pane">
                            <div class="section-content row">
                                <div class="col-sm-4">
                                    <article class="text-center animated hiding cutom-wrapper mt30" data-animation="fadeInLeft" data-delay="0">
                                        <span class="h7 mt10 highlight">GRACZ JEST NAJWAŻNIEJSZY</span>
                                        <p class="thin" >Spełniamy oczekiwania graczy!<br />Przesyłajcie swoje opinie i sugestie.<br />Na wszystkie odpowiemy.</p>
                                    </article>
                                    <!--<span class="icon icon-arrows-04"></span>-->
                                </div>
                                <div class="col-sm-4">
                                    <article class="text-center animated hiding cutom-wrapper mt30" data-animation="fadeInLeft" data-delay="400">
                                        <span class="h7 mt10 highlight">KORZYSTAJ ZA DARMO</span>
                                        <p class="thin" >Korzystanie z serwisu jest<br />i zawsze będzie darmowe!<br />Wszelka pomoc i wsparcie są dobrowolne.</p>
                                    </article>
                                    <!--<span class="icon icon-arrows-04"></span>-->
                                </div>
                                <div class="col-sm-4">
                                    <article class="text-center animated hiding cutom-wrapper mt30" data-animation="fadeInLeft" data-delay="800">
                                        <span class="h7 mt10 highlight">"NIE" DLA REKLAM</span>
                                        <p class="thin" >Łatwa obsługa i czytelność jest priorytetm!<br />Obiecujemy brak spamu i agresywnych reklam<br />utrudniających korzystanie z serwisu.</p>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="features-list" class="section dark">
            <div class="container animated hiding" data-animation="fadeInDown">
                <div class="mb50"><img src="{{ asset('/images/icon/funkcje.jpg') }}" class="img-responsive" alt="Funkcje i narzędzia" style="margin: 0px auto;"/></div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="center cutom-wrapper pt15 pb15">
                        <div class="funkcja-heading-wrapper"><span class="h7 funkcja-heading">PROFIL<br>GRACZA</span></div>
                        <p class="thin">Rejestrując konto tworzysz profil gracza, który możesz wypełnić danymi o sobie i swoich grach. Pozwól innym się odnaleźć.</p>
                    </article>
                </div>  
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="center cutom-wrapper pt15 pb15">
                        <div class="funkcja-heading-wrapper"><span class="h7 funkcja-heading">WYSZUKIWARKA<br>GRACZY</span></div>
                        <p class="thin">Szukasz graczy podobnych do siebie? Skorzystaj z wyszukiwarki i jej filtrów, aby wyszukać interesującą cię osobę. </p>
                    </article>
                </div>  
                <div class="clearfix hidden-xs hidden-md hidden-lg hidden-xl"></div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="center cutom-wrapper pt15 pb15">
                        <div class="funkcja-heading-wrapper"><span class="h7 funkcja-heading">REJESTRACJA<br>KLANÓW</span></div>
                        <p class="thin">Jesteś liderem klanu?<br>Szukasz nowych rekrutów?<br>Zarejestruj swój klan i stwórz jego stronę. Werbuj nowych członków i walcz z innymi klanami.</p>
                    </article>
                </div>  
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="center cutom-wrapper pt15 pb15">
                        <div class="funkcja-heading-wrapper"><span class="h7 funkcja-heading">WYSZUKIWARKA<br>KLANÓW</span></div>
                        <p class="thin">Szukasz odpowiedniego klanu aby do niego dołączyć?<br>Użyj wyszukiwarki i znajdź dokładnie taki, który spełnia twoje oczekiwania.</p>
                    </article>
                </div>  
                <div class="clearfix hidden-xs hidden-md"></div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="center cutom-wrapper pt15 pb15">
                        <div class="funkcja-heading-wrapper"><span class="h7 funkcja-heading">WOJNY<br>KLANOWE</span></div>
                        <p class="thin">Wyzywaj inne klany na pojedynek w dowolnej grze i pochwal się swoim zwycięstwem na stronie klanowej!</p>
                    </article>
                </div>     
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="center cutom-wrapper pt15 pb15">
                        <div class="funkcja-heading-wrapper"><span class="h7 funkcja-heading">TABLICA<br>OGŁOSZEŃ</span></div>
                        <p class="thin">Zamieszczaj swoje i przeglądaj ogłoszenia innych graczy. Ogłoszenia na tablicy widzą wszyscy gracze zainteresowani tym samym tytułem gry co ty.</p>
                    </article>
                </div>  
                <div class="clearfix hidden-xs hidden-md hidden-lg hidden-xl"></div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="center cutom-wrapper pt15 pb15">
                        <div class="funkcja-heading-wrapper"><span class="h7 funkcja-heading">PRYWATNE<br>WIADOMOŚCI</span></div>
                        <p class="thin">Zbuduj swoją listę znajomych i bądź z nimi w bezpośrednim kontakcie dzięki systemowi prywatnych wiadomości.</p>
                    </article>
                </div>  
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <article class="center cutom-wrapper pt15 pb15">
                        <div class="funkcja-heading-wrapper"><span class="h7 funkcja-heading">SYSTEM<br>POWIADOMIEŃ</span></div>
                        <p class="thin">Dzięki wbudowanym powiadomieniom będziesz informowany o zajściach, które będą z tobą związane.</p>
                    </article>
                </div>  
                <div class="clearfix hidden-xs hidden-md"></div>
            </div>
        </section>

        <section id="fanpage" class="long-block light">
            <div class="bg-spacer">
                <div class="container center">
                    <div class="col-sm-12 col-lg-7">
                        <article>
                            <h3 class="m0 mt15">POLUB <strong>FANPAGE</strong> BAZAGRACZY</h3>
                            <p class="thin mb15">Aktualności, informacje, konkursy, akcje i projekty.</p>
                        </article>
                    </div>
                    <div class="col-sm-12 col-lg-5">
                        <div class="fb-like" data-href="https://facebook.com/bazagraczy" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
                    </div>
                </div>
            </div>
        </section>

        <!-- <section id="partnerzy" class="section dark">
            <div class="container">
                <div class="section-header animated hiding" data-animation="fadeInDown">
                    <h2><span class="highlight">OUR</span> AWARDS</h2>
                    <div class="sub-heading">
                        <a href="https://www.youtube.com/watch?v=q_yh8TzZGA4" class="html5lightbox" title="Toronto">Image</a>
                        
                    </div>
                </div>
                <div class="section-content">                
                    <ul class="list-inline logos">
                        <li><a href="#" target="_blank"><img class="animated hiding" data-animation="fadeInUp" data-delay="0" src="assets/img/logos/award-5.jpg" alt="mashable" /></a></li> 
                        <li><a href="#" target="_blank"><img class="animated hiding" data-animation="fadeInUp" data-delay="400" src="assets/img/logos/award-3.jpg" alt="virgin" /></a></li> 
                        <li><a href="#" target="_blank"><img class="animated hiding" data-animation="fadeInUp" data-delay="600" src="assets/img/logos/award-4.jpg" alt="forbes" /></a></li> 
                        <li><a href="#" target="_blank"><img class="animated hiding" data-animation="fadeInUp" data-delay="800" src="assets/img/logos/award-1.jpg" alt="microsoft" /></a></li> 					
                    </ul>
                </div>
            </div>
        </section> -->



        <section id="movies" class="section dark">
            <div class="container">
                <div class="section-header animated hiding" data-animation="fadeInDown">
                    <h2>WPROWADZENIE</h2>
                    <div class="sub-title mb10">Kilka podstawowych informacji o funkcjonalnościach serwisu.</div>
                </div>
                <div class="section-content row">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
                                <li data-target="#myCarousel" data-slide-to="3"></li>
                                <li data-target="#myCarousel" data-slide-to="4"></li>
                                <li data-target="#myCarousel" data-slide-to="5"></li>
                                <li data-target="#myCarousel" data-slide-to="6"></li>
                                <li data-target="#myCarousel" data-slide-to="7"></li>
                                <li data-target="#myCarousel" data-slide-to="8"></li>
                                <li data-target="#myCarousel" data-slide-to="9"></li>
                                <li data-target="#myCarousel" data-slide-to="10"></li>
                                <li data-target="#myCarousel" data-slide-to="11"></li>
                                <li data-target="#myCarousel" data-slide-to="12"></li>
                                <li data-target="#myCarousel" data-slide-to="13"></li>
                            </ol>
                            
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="row">
                                    <img src="{{asset('images/info/Czy-wiesz-ze_01.jpg')}}">
                                    </div>
                                </div>
                            
                                <div class="item">
                                    <div class="row">
                                    <img src="{{asset('images/info/Czy-wiesz-ze_02.jpg')}}">
                                    </div>
                                </div>
                            
                                <div class="item">
                                    <div class="row">
                                    <img src="{{asset('images/info/Czy-wiesz-ze_03.jpg')}}" >
                                    </div>
                                </div>
                                <div class="item">
                                        <div class="row">
                                        <img src="{{asset('images/info/Czy-wiesz-ze_04.jpg')}}" >
                                        </div>
                                    </div>
                                    <div class="item">
                                            <div class="row">
                                            <img src="{{asset('images/info/Czy-wiesz-ze_05.jpg')}}" >
                                            </div>
                                        </div>
                                <div class="item">
                                        <div class="row">
                                        <img src="{{asset('images/info/Czy-wiesz-ze_06.jpg')}}" >
                                        </div>
                                    </div>
                            <div class="item">
                                    <div class="row">
                                    <img src="{{asset('images/info/Czy-wiesz-ze_07.jpg')}}" >
                                    </div>
                                </div>
                            <div class="item">
                                    <div class="row">
                                    <img src="{{asset('images/info/Czy-wiesz-ze_08.jpg')}}" >
                                    </div>
                                </div>
                            <div class="item">
                                    <div class="row">
                                    <img src="{{asset('images/info/Czy-wiesz-ze_09.jpg')}}" >
                                    </div>
                                </div>
                            <div class="item">
                                    <div class="row">
                                    <img src="{{asset('images/info/Czy-wiesz-ze_10.jpg')}}" >
                                    </div>
                                </div>
                            <div class="item">
                                    <div class="row">
                                    <img src="{{asset('images/info/Czy-wiesz-ze_11.jpg')}}" >
                                    </div>
                                </div>
                            <div class="item">
                                    <div class="row">
                                    <img src="{{asset('images/info/Czy-wiesz-ze_12.jpg')}}" >
                                    </div>
                                </div>
                            <div class="item">
                                    <div class="row">
                                    <img src="{{asset('images/info/Czy-wiesz-ze_13.jpg')}}" >
                                    </div>
                                </div>
                            <div class="item">
                                    <div class="row">
                                    <img src="{{asset('images/info/Czy-wiesz-ze_14.jpg')}}" >
                                    </div>
                                </div>
                            </div>
                            
                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="fa fa-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="fa fa-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>

                    
                </div>
            </div>
        </section>



        <section id="support" class="long-block light mb50">
            <div class="bg-spacer">
                <div class="container">
                    <div class="col-md-12 col-lg-9">
                        <article class="pull-left">
                            <h3 class="mb0">WSPARCIE TECHNICZNE</h3>
                            <p class="thin mb15">Pojawił się problem, lub coś nie działa? A może masz jakąś sugestię? Pisz śmiało!</p>
                        </article>
                    </div>

                    <div class="col-md-12 col-lg-3">
                        <a href="https://m.me/bazagraczy" target="_blank" class="btn btn-default mt25">WYŚLIJ ZGŁOSZENIE</a>
                    </div>
                </div>
            </div>
        </section>



        <footer id="footer" class="footer light">
            <div class="container">
                <div class="footer-content row">
                    <div class="col-sm-4">
                        <div class="logo-wrapper">
                            <img src="{{ asset('images/logo/bazagraczy-logo-small.png') }}" class="img-responsive" alt="BAZAGRACZY Logo" />
                        </div>
                        <p>BAZAGRACZY.pl jest projektem tworzonym dla graczy przez graczy. Mamy nadzieję, że spotka się on z waszymi oczekiwaniami i pomoże w nawiązywaniu nowych kontaktów i znajomości.</p>
                        <p><strong>Administracja</strong>.</p>
                    </div>
                    <div class="col-sm-5 social-wrap">
                        <div class="footer-title">POLUB FANPAGE BAZAGRACZY</div>
                        <div class="fb-page" data-href="https://www.facebook.com/bazagraczy/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/bazagraczy/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/bazagraczy/">BAZAGRACZY.pl</a></blockquote></div>
                    </div>
                    <div class="col-sm-3">
                        <div class="footer-title">INFORMACJE</div>
                        <p>
                            <a href="/regulamin">Regulamin</a><br>
                            <a href="/polityka">Polityka prywatności</a><br>
                            <a href="/kontakt">Kontakt</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="copyright">BAZAGRACZY.pl {{date('Y')}}. Wszelkie prawa zastrzeżone.</div>
        </footer>

        <div class="back-to-top"><i class="fa fa-angle-up fa-3x"></i></div>

        <!--[if lt IE 9]>
            <script type="text/javascript" src="{{ asset('js/jquery-1.11.0.min.js') }}?ver=1"></script>
        <![endif]-->  
        <!--[if (gte IE 9) | (!IE)]><!-->  
        <script type="text/javascript" src="{{ asset('js/jquery-2.1.0.min.js') }}?ver=1"></script>
        <!--<![endif]-->  

        <script type="text/javascript" src="{{ asset('js/bootstrap.min4.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.flexslider-min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.appear.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.plugin.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.countdown.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/waypoints.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/waypoints-sticky.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.validate.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/toastr.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/headhesive.min.js') }}"></script>
        
        <script type="text/javascript" src="{{ asset('js/scripts.js') }}"></script>
        
    </body>
</html>
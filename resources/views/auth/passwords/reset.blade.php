
<!doctype html>
<html lang="pl">

    <head>
        <!-- Meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta property="og:title" content="BAZAGRACZY.pl - Portal każdego gracza!" />
        <meta property="og:image" content="https://app.playershub.net/images/logo/bg_avatar.png" />
        <meta property="og:description" content="BAZAGRACZY.pl oraz BAZAGRACZY App to serwis i aplikacja umożliwiająca jej użytkownikom znalezienie graczy do wspólnych rozgrywek online w różne gry multiplayer bez względu na platformę na jakich grają. Dodatkowo BAZAGRACZY udostępnia za darmo narzędzia niezbędne każdemu graczowi. Możliwość rejestracji klanów, wyszukiwarka klanów, wojny klanowe, komunikator to tylko niektóre z nich." />

        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('icon/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('icon/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('icon/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('icon/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('icon/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('icon/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('icon/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('icon/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('icon/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('icon/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('icon/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('icon/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('icon/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('icon/manifest.json') }}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ asset('icon/ms-icon-144x144.png') }}">
        <meta name="theme-color" content="#ffffff">

        <!-- CSS Bootstrap -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap-grid.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap-reboot.css') }}">
        <!-- CSS Select2 -->
        <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
        <!-- CSS Horizontal Menu Scroller -->
        <link rel="stylesheet" href="{{ asset('css/horizontal-menu-scroller.css') }}">
        <!-- CSS Strona logowania -->
        <link rel="stylesheet" href="{{ asset('css/login-page.css') }}">
        <!-- CSS Custom -->
        <link rel="stylesheet" href="{{ asset('css/bazagraczy.css') }}">

        <!-- JS -->
        <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
        <!-- JS Bootstrap-->
        <script src="{{ asset('js/popper.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <!-- JS Bootstrap Tooltip i Popover-->
        <script>
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
            $(function () {
                $('[data-toggle="popover"]').popover()
            })
        </script>
        <!-- JS FontAwesome -->
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
        <script>
                window.setTimeout(function () {
                    $(".auto-close").fadeTo(500, 0).slideUp(500, function () {
                        $(this).remove();
                    });
                }, 4000);
        </script>
    </head>

    <body>
        <div class="body" id="nobars">
            <!-- KONTENT /Start -->
            <div class="body-wrapper">
                <div class="content">



                    <!-- FORMULARZ LOGOWANIA /Start -->
                    <div class="logreg-logo">
                        <a href='/'><img src="{{ asset('images/logo/logo-bg-small-full.png') }}" alt="BAZAGRACZY.PL Logo"></a>
                    </div>

                    <div class="logreg-form">
                            <h5 class="text-center">NADAJ NOWE HASŁO</h5>
                            <p class="text-muted text-center"><small>dla</small></p>
                            <p class="text-center">{{$reset->email}}</p>
                            <form method="POST" action="{{ route('zapomnialem.reset') }}">
                                    {{ csrf_field() }}
            
                                    <input type="hidden" name="token" value="{{ $reset->token }}">
            
                                    <input id="email" type="hidden" class="form-control" name="email" value="{{ $reset->email or old('email') }}" required autofocus>
                                <label class="sr-only" for="reset-haslo">HASŁO</label>
                                <div class="input-group mb-2 mr-sm-2 mb15">
                                    <input name='password' type="password" class="form-control br0" id="reset-haslo" placeholder="Hasło">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text ikona-po-prawej"><i class="fas fa-lock"></i></div>
                                    </div>
                                    @if ($errors->has('password'))
                                    <div class="error-feedback">
                                            {{ $errors->first('password') }}<br>
                                    </div>
                                    @endif
                                </div>
                                <label class="sr-only" for="reset-rehaslo">HASŁO</label>
                                <div class="input-group mb-2 mr-sm-2 mb15">
                                    <input type="password" name="password_confirmation" class="form-control br0" id="reset-rehaslo" placeholder="Powtórz hasło">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text ikona-po-prawej"><i class="fas fa-lock"></i></div>
                                    </div>
                                    @if ($errors->has('password_confirmation'))
                                    <div class="error-feedback">
                                            {{ $errors->first('password_confirmation') }}<br>
                                    </div>
                                    @endif
                                </div>
                                <div style="position: relative; max-width: 100px; height: 35px; float: right; text-align: right;">
                                    <button type="submit" class="btn btn-primary">Zapisz</button>
                                </div>
                                <div class="clearfix mb0"></div>
                            </form>
                        </div>
                        <p class="text-center mt15">
                                <small>
                                    <a href="/kontakt">Kontakt</a><br>
                                    <a href="/regulamin">Regulamin</a><br>
                                    <a href="/polityka">Polityka prywatności</a>
                                </small>
                            </p>
                    <div class="copyright text-muted mb30">
                        <p><a href="{{route('wiecej')}}"><span class="logreg-wiecej">DOWIEDZ SIĘ WIĘCEJ</span></a></p>
                        © BAZAGRACZY.pl {{date('Y')}}.<br>
                        Wszelkie prawa zastrzeżone.<br>
                    </div>
                    <!-- FORMULARZ LOGOWANIA /Koniec -->



                </div>
            </div>
            <!-- KONTENT /Koniec -->   
            @if(session('success'))
            <div class="alert alert-success alert-main alert-dismissible auto-close fade show" role="alert">
                    {!!session('success')!!}<br>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if(session('error'))
            <div class="alert alert-danger alert-main alert-dismissible auto-close fade show" role="alert">
                    {!!session('error')!!}<br>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            
                                
        </div>
    </body>

</html>

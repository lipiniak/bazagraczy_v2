<h3>Wystąpił błąd na stronie {{env('APP_URL')}}:</h3>
<strong>Date:</strong> {{ date('M d, Y H:iA') }}

<br><strong>Method:</strong>{{$request->getMethod()}}
<br><strong>Uri:</strong> {{$request->getUri()}}
<br><strong>Ip:</strong> {{$request->getClientIp()}}
<br><strong>Referer:</strong> {{$request->server('HTTP_REFERER')}}
<br><strong>Is secure:</strong> {{$request->isSecure()}}
<br><strong>Is ajax:</strong> {{$request->ajax()}}
<br><strong>User info:</strong>
@if(isset($request->user()->id))
<br>&nbsp;&nbsp;&nbsp;&nbsp;<strong>User id:</strong> {{$request->user()->id}}
<br>&nbsp;&nbsp;&nbsp;&nbsp;<strong>User login:</strong> {{$request->user()->login}}
<br>&nbsp;&nbsp;&nbsp;&nbsp;<strong>User email:</strong> {{$request->user()->email}}
@endif
<br><strong>User agent:</strong> {{$request->server('HTTP_USER_AGENT')}}
<br><strong>Post/get data:</strong> <?php print_r($request->all())?>

{!! $e !!}

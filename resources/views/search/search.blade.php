@extends('layouts.app')

@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.3.9/jquery.jscroll.min.js"></script>
<script>
        
        $(document).ready(function () {
            $('#wyszukiwarka-tytul').select2();
        });
        $(document).ready(function () {
            $('#wyszukiwarka-platforma').select2({dropdownCssClass : "mszuk-platforma"});
        });
        $(document).ready(function () {
            $('#wyszukiwarka-wojewodztwo').select2({dropdownCssClass : "szuk-woje"});
        });
        $(document).ready(function () {
            $('#wyszukiwarka-miasto').select2({dropdownCssClass : "szuk-miasto"});
        });
        $(document).ready(function () {
            $('#wyszukiwarka-wiek').select2({dropdownCssClass : "szuk-wiek",minimumResultsForSearch: Infinity});
        });
        $(document).ready(function () {
            $('#wyszukiwarka-plec').select2({dropdownCssClass : "szk-plec",minimumResultsForSearch: Infinity});
        });
        $(document).ready(function () {
            $('#wyszukiwarka-filtry').select2({dropdownCssClass : "szuk-filtry",minimumResultsForSearch: Infinity});
        });

        
    </script>
    
                                        
<div id="wyszukiwarka" class="content">

        <div class="container-fluid">

            <div class="row">

                <div class="col-12">


                    {{ Form::open(['route' => 'search.searching'],['autocomplete'=>'off']) }}
                    <input type='hidden' name='game' value='{{$selectedGame["game_id"]}}'>
                                        <input type='hidden' name='platform' value='{{$selectedGame["platform_id"]}}'>
                        <h1 class="mb15">Wyszukiwarka graczy</h1>

                        
                        <div class="accordion" id="SearchAccordion">
                            <div class="card wyszukiwarka-clear-accordion">
                                <div class="card-header wyszukiwarka-clear-accordion p0 m0 mb15" id="SearchHeader">
                                    <h2 class="mb-0">
                                        <button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Nowe wyszukiwanie...
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse" aria-labelledby="SearchHeader" data-parent="#SearchAccordion">
                                    <div class="card-body wyszukiwarka-clear-accordion p0 pt5 m0">

                                        <div class="wyszukiwarka-form-wrapper">
                                            <div class="row">
                                                
                                                
                                                <div class="col-12 col-lg-6 col-xl-4">
                                                    <div class="form-group mb5" style="width:100%">
                                                        <label class="form-label-custom" for="wyszukiwarka-tytul">Tytuł gry</label>
                                                        
                                                        {{Form::select('game',$data['games'],null,['class'=> 'form-control','id'=>'wyszukiwarka-tytul','style'=>'width:100%'])}}
                                                        <script>
                
                                                                
                                                                $('#wyszukiwarka-tytul').select2({
                                                                    
                                                                  }).on('change',function() {
                                                                      
                                                                      var value= $('#wyszukiwarka-tytul option:selected').attr('value');
                            
                                                                      if(value == 0) {
                                                                        $('#wyszukiwarka-platforma').empty();
                                                                        $("#wyszukiwarka-platforma").select2();
                                                                      }
                
                                                                      var url = "/helper/platforms/"+value;
                                                                        $.ajax({
                                                                            type: "GET",
                                                                            url: url,
                                                                            success: function(data)
                                                                            {
                                                                                
                                                                                $('#wyszukiwarka-platforma').empty();
                                                                                $("#wyszukiwarka-platforma").select2({
                                                                                    data: data
                                                                                });
                                                                            }
                                                                        });
                                                                      
                                                                });
                            
                                                                </script>
                                                    </div>
                                                </div> 
                                                <div class="col-12 col-lg-6 col-xl-4">
                                                    <div class="form-group mb5" style="width:100%">
                                                        <label class="form-label-custom" for="wyszukiwarka-platforma">Platforma</label>
                                                        
                                                        @if(session()->has('platform'))
                                                        <?php $city = session('platform');?>
                                                        @endif
                                                        {{Form::select('platform',$data['platform'],null,['class'=> 'form-control','id'=>'wyszukiwarka-platforma','style'=>'width:100%'])}}
                                                        <script>
                                                                $("#wyszukiwarka-platforma").select2();
                                                        </script>
                                                    </div>
                                                </div> 
                                        
                                                <div class="col-12 col-lg-6 col-xl-4">
                                                    <div class="form-group mb5" style="width:100%">
                                                        <label class="form-label-custom" for="wyszukiwarka-wojewodztwo">Województwo</label>
                                                        
                                                        {{Form::select('voivodeship',$data['voivodeship'],null,['class'=> 'form-control','id'=>'wyszukiwarka-wojewodztwo','style'=>'width:100%'])}}
                                                <script>
                                                    $('#wyszukiwarka-wojewodztwo').select2({
                                                        dropdownCssClass : "szuk-woje"
                                                      }).on('change',function() {
                                                          
                                                          var value= $('#wyszukiwarka-wojewodztwo option:selected').attr('value');
                
                                                          var url = "/helper/cities/"+value;
                                    
                                                            $.ajax({
                                                                type: "GET",
                                                                url: url,
                                                                success: function(data)
                                                                {
                                                                    
                                                                    $('#wyszukiwarka-miasto').empty();
                                                                    $("#wyszukiwarka-miasto").select2({
                                                                        data: data['results']
                                                                    });
                                                                }
                                                            });
                                                          
                                                    });
                                                    $('#wyszukiwarka-wojewodztwo').on('select2:open', function (e) {
                                                          
                                                        $('.szuk-woje .select2-search input').prop('focus',false);
                                                    });
                                                    </script>
                                                    </div>
                                                </div> 
                                                <div class="col-12 col-lg-6 col-xl-4">
                                                    <div class="form-group mb5" style="width:100%">
                                                        <label class="form-label-custom" for="wyszukiwarka-miasto">Miasto</label>
                                                       
                                                        @if(session()->has('city'))
                                                        <?php $city = session('city');?>
                                                        @endif
                                                        {{Form::select('city',$data['city'],null,['class'=> 'form-control','id'=>'wyszukiwarka-miasto','style'=>'width:100%'])}}
                                                        <script>
                                                                $("#wyszukiwarka-miasto").select2({dropdownCssClass : "szuk-miasto"});
                                                                $('#wyszukiwarka-miasto').on('select2:open', function (e) {
                                                          
                                                                    $('.szuk-miasto .select2-search input').prop('focus',false);
                                                                });
                                                        </script>
                                                    </div>
                                                </div> 
                                                <div class="col-12 col-lg-6 col-xl-4">
                                                    <div class="form-group mb5" style="width:100%">
                                                        <label class="form-label-custom" for="wyszukiwarka-wiek">Wiek</label>
                                                        
                                                        {{Form::select('age', [''=>'','1' => 'mniej niż 10', '2' => '10-12','3'=>'13-15','4'=>'16+','5'=>'18+','6'=>'25+','7'=>'30+'],null,['class'=> 'form-control','id'=>'wyszukiwarka-wiek','style'=>'width:100%'])}}
                                                        <script>
                                                                
                                                                $('#wyszukiwarka-wiek').on('select2:open', function (e) {
                                                          
                                                                    $('.szuk-wiek .select2-search input').prop('focus',false);
                                                                });
                                                        </script>
                                                    </div>
                                                </div> 
                                                <div class="col-12 col-lg-6 col-xl-4">
                                                    <div class="form-group mb5" style="width:100%">
                                                        <label class="form-label-custom" for="wyszukiwarka-plec">Płeć</label>
                                                        {{Form::select('sex', ['','meale' => 'Mężczyzna', 'female' => 'Kobieta'],null,['class'=> 'form-control','id'=>'wyszukiwarka-plec','style'=>'width:100%'])}}
                                                        <script>
                                                                
                                                                $('#wyszukiwarka-plec').on('select2:open', function (e) {
                                                          
                                                                    $('.szuk-plec .select2-search input').prop('focus',false);
                                                                });
                                                        </script>
                                                    </div>
                                                </div> 
                                                <div class="col-12 col-lg-6 col-xl-4">
                                                    <div class="form-group mb5" style="width:100%">
                                                        <label class="form-label-custom" for="wyszukiwarka-filtry">Filtry</label>
                                                        
                                                        <select name='filters[]' class="form-control js-example-basic-multiple" id="wyszukiwarka-filtry" name="filtry[]" multiple="multiple" style="width:100%">
                                                                @if(isset($formData['filters']))
                                                                @foreach($formData['filters'] as $filter)
                                                                    <?php $filters[$filter] = $filter ;?>
                                                                    
                                                                @endforeach
                                                                    <option value="1" <?php echo isset($filters[1]) ? 'selected' : null; ?>>Obserwuję</option>
                                                                    <option value="2" <?php echo isset($filters[2]) ? 'selected' : null; ?>>Szuka klanu</option>
                                                                    <option value="3" <?php echo isset($filters[3]) ? 'selected' : null; ?>>Z notatką</option>
                                                                    <option value="4" <?php echo isset($filters[4]) ? 'selected' : null; ?>>Tylko online</option>
                                                                @else
                                                                    <option value="1">Obserwuję</option>
                                                                    <option value="2">Szuka klanu</option>
                                                                    <option value="3">Z notatką</option>
                                                                    <option value="4">Tylko online</option>
                                                                @endif
                                                            
                                                        </select>
                                                    </div>
                                                </div> 
                                                <div class="col-12 col-lg-6 col-xl-4">
                                                    <div class="form-group mb5" style="width:100%">
                                                        <label class="form-label-custom">Nazwa <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content='Wyszukuje podaną frazę wśród nicków graczy oraz ich tagów. Przykładowo, wpisanie frazy "glo" wyszuka gracza o nicku "Gloster" oraz każdego innego, który otagowany jest np. jako "cs:global".'><i class="fas fa-question-circle"></i></a></label>
                                                        
                                                        {{Form::text('keyword',null,['class' => 'form-control', 'id'=>'wyszukiwarka-fraza','maxlength'=>'25'])}}
                                                    </div>
                                                </div>
                                                <div class="col-12 col-lg-6 offset-lg-6 col-xl-4 offset-xl-8">
                                                        <div class="btn-group special mb5" role="group" style="margin-top:22px">
                                                           <a href='/szukaj/'class="btn btn-secondary">Resetuj</a>
                                                           <button type="submit" class="btn btn-primary" @if($user_select['game_id'] == 0) disabled @endif>Wyszukaj</button>
                                                       </div>
                                                   </div> 
                                                
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                   
                    </form>
                    <div class='infinite-scroll'>
                        <div class="row">
                            <div class="col-12">
                                @if(!empty($users))
                                    @foreach($users as $user)
                                        <div class="wynik-wrapper user_{{$user->id}}">
                                            <div class="avatar-wrapper">
                                       
                                                <a href="/profil/{{$user->id}}">
                                                    @if($user->avatar == 'no-avatar.jpg')
                                                        <img src="{{asset('images/avatars/')}}/{{$user->avatar}}" class="menu-avatar" alt="Avatar">
                                                    @else
                                                        <img src="{{ Storage::url($user->avatar) }}" class="menu-avatar" alt="Avatar">
                                                    @endif
                                                </a>
                                                <span class="status {{$user->getStatus()}}"></span>
                                            </div>
                                            <div class="nick-gracza">
                                                {{$user->login}}
                                            </div>
                                            <?php
                                                $note = $user->note()->where('user_id',Auth::user()->id)->get();
                                            ?>
                                            <div class="gracz-info">
                                                <div class="btn-group special" role="group" aria-label="...">
                                                    <a href='/profil/{{$user->id}}'class="btn btn-secondary">Zobacz profil</a>
                                                    @if(!empty($note[0]))
                                                        <a class="btn btn-secondary" data-toggle="modal" data-target="#profil-note-{{$user->id}}"><i class="fas fa-info"></i></a>
                                                    @endif
                                                </div>
                                    
                                                <!-- Modal -->
                                                @if(!empty($note[0]))
                                                <div class="modal fade" id="profil-note-{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="profilInfoID1Title" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="profilZablokujTitle">{{$user->login}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p class="text-muted">Notakta</p>
                                                                <p class="mb0 text-left">{!! nl2br(e($note[0]->note))!!}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                            {{ $users->appends(Request::except('page'))->links() }}
                            <!-- Pojedynczy wynik wyszukiwania /koniec -->
                            @elseif(empty($users) and $new == false)
                            <!-- Brak wyników /start -->
                            <p style="clear:both">Brak wyników spełniających wybrane kryteria. Spróbuj ponownie.</p>
                            <!-- Brak wyników /koniec -->

                            @endif
                            


                            <!-- Pojedynczy wynik wyszukiwania /start -->
                            

                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $('ul.pagination').hide();
                    $(function() {
                        $('.infinite-scroll').jscroll({
                            autoTrigger: true,
                            loadingHtml: '<img class="center-block" src="{{asset("/images/gfx/loading.gif")}}" alt="Loading..." />',
                            padding: 0,
                            nextSelector: '.pagination li.active + li a',
                            contentSelector: 'div.infinite-scroll',
                            callback: function() {
                                $('ul.pagination').remove();
                            }
                        });
                    });
            
                    
                </script>

                </div>

            </div>

        </div>

    </div>
@endsection
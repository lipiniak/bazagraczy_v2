
@extends(Auth::user() ? 'layouts.app' : 'layouts.full')

@section('content')
<div id="info-page" class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                @if(!Auth::check())
                <div class="logreg-logo">
                        <a href="/"><img src="{{ asset('images/logo/logo-bg-small-full.png') }}" alt="BAZAGRACZY.PL Logo"></a>
                </div>
                @endif    
                <h1>Kontakt</h1>
                <p>Zapraszamy do kontaktu. Odpowiadamy na wszystkie przesyłane do nas wiadomości i staramy się robić to najszybciej, jak tylko jest to możliwe.</p>
                <form method='post' action='/kontakt/wyslij'>
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="form-label-custom" for="kontaktEmail">Twój email *</label>
                        <input type="text" class="form-control" id="kontaktEmail" name='email' value="{{old('email')}}">
                        
                        @if ($errors->has('email'))
                            <div class="error-feedback">
                                
                                {!! $errors->first('email') !!}<br>
                            </div>  
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label-custom" for="kontaktTrasc">Treść wiadomosći *</label>
                        <textarea class="form-control" id="kontaktTresc" rows="5" name='content'>{{old('content')}}</textarea>
                        
                        @if ($errors->has('content'))
                            <div class="error-feedback">
                                
                                {!! $errors->first('content') !!}<br>
                            </div>  
                        @endif
                    </div>
                    <div class="form-group">
                    {!! Recaptcha::render() !!}
                    @if ($errors->has('g-recaptcha-response'))
                            <div class="error-feedback">
                                
                                {!! $errors->first('g-recaptcha-response') !!}<br>
                            </div>  
                        @endif
                        </div>
                        <div class="text-right">
                                <hr class="separator">
                                                 <button type="submit" class="btn btn-primary">Wyślij wiadomość</button>
                                 </div>
                    
                </form>

            </div>
        </div>

    </div>
</div>
@endsection


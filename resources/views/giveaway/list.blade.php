@extends('layouts.app')

@section('content')


<div id="giveaway-list-page" class="content">

    <div class="container-fluid">


        <div class="row">
            <div class="col-12">
                <h1 class="mb30">Giveaway</h1>
            </div>
        </div>

        @if($giveaways->where('status',1)->isEmpty())
        <div class="row">
            <div class="col-12">
                <div class="alert alert-warning mb30">Brak aktywnych giveaway.</div>
            </div>
        </div>
        @else 
        <div class="row">
            <div class="col-12">
                <div class="card-deck">
                    @foreach($giveaways as $giveaway)
                        <div class="card">
                            <a href="{{route('giveaway.show',['id'=>$giveaway->id])}}">
                                @if($giveaway->main_img != '')
                                    <img src="{{ Storage::url($giveaway->main_img) }}" class="card-img-top" alt="Giveaway Image">
                                @else
                                    <img src="{{ asset('images/giveaway/giveaway-default.jpg') }}" class="card-img-top" alt="Giveaway Image">
                                @endif
                            </a>
                            <div class="card-body p10 pt20">
                                <h3 class="card-title">{{$giveaway->title}}</h3>
                                <p class="text-muted mb0"><small>{{date('d.m.Y H:i:s',strtotime($giveaway->ending_at))}}</small></p>
                            </div>
                            <div class="card-footer">
                                
                                @if($giveaway->participants()->where('user_id',Auth::user()->id)->Where('joined',1)->exists())
                                    <a href="{{route('giveaway.show',['id'=>$giveaway->id])}}" class="btn btn-success">Dołączono!</a>
                                @else
                                    <a href="{{route('giveaway.show',['id'=>$giveaway->id])}}" class="btn btn-primary">Dołącz do giveaway!</a>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif

        <div class="row">
            <div class="col-12">
                <div class="alert alert-info mb30">
                    <h4>Zostań sponsorem!</h4>
                    <p class="mb0">Jeżeli jesteś zainteresowany promocją swojej marki i chcesz ufundować nagrodę na giveaway, zajrzyj do działu <a href="#" class="highlight">współpraca</a> i skontaktuj się z nami. Zapraszamy!</p>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-12">
                <h1 class="mb30">Archiwalne giveaway</h1>
            </div>
        </div>

        @if($giveaways->where('status',3)->count() > 0)
        <div class="row">
            <div class="col-12">
                <div class="card-deck">
                    @foreach($giveaway->where('status',3) as $giveaway)
                    <!-- pojedynczy wynik / start -->
                    <div class="card">
                        <a href="{{route('giveaway.show',['id'=>$giveaway->id])}}">
                        @if($giveaway->main_img != '')
                            <img src="{{ Storage::url($giveaway->main_img) }}" class="card-img-top" alt="Giveaway Image">
                        @else
                            <img src="{{ asset('images/giveaway/giveaway-default.jpg') }}" class="card-img-top" alt="Giveaway Image">
                        @endif
                        </a>
                        <div class="card-body p10 pt20">
                            <h3 class="card-title">{{$giveaway->title}}</h3>
                            <p class="text-muted mb0"><small>{{date('d.m.Y H:i:s',strtotime($giveaway->ending_at))}}</small></p>
                        </div>
                        <div class="card-footer text-center">
                            <i class="fas fa-crown"></i> <a href="#" class="highlight">{nickWinnera}</a>
                        </div>
                    </div>
                    <!-- pojedynczy wynik / koniec -->
                    @endforeach
                    <!-- pojedynczy wynik / start -->
                    
                </div>
            </div>

            <!--<div class="col-12 mb30">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                        </li>
                        <li class="page-item">
                            <span class="page-link">
                                1
                                <span class="sr-only">(current)</span>
                            </span>
                        </li>
                        <li class="page-item"><a class="page-link highlight" href="#">2</a></li>
                        <li class="page-item"><a class="page-link highlight" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link highlight" href="#">Next</a>
                        </li>
                    </ul>
                </nav>
            </div>-->
        </div>
        @else 
            <div class="row">
                <div class="col-12">
                    <div class="alert alert-warning mb30">Brak archiwalnych giveaway.</div>
                </div>
            </div>
        @endif


    </div>

</div>

@endsection
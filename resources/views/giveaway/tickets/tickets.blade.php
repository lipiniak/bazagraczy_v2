@extends('layouts.app')

@section('content')
<div id="tickety" class="content">

    <div class="container-fluid">


        <div class="row">
            <div class="col-12 mb30">
                <h1 class="mb5">Twoje osiągnięcia</h1>
                <p><small>Osiągnięcia pozwalają na zapoznanie się z portalem.</small></p>
            </div>
        </div>


        <div class="row">
            <div class="col-12 mb30">
                <div class="alert alert-info text-center">
                    <h2 class="m20 text-center">PUNKTY ZA OSIĄGNIĘCIA<br>{{Auth::user()->tickets}}</h2>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-12 mb30">
                <h5 class="mb0">Osiągnięcia</h5>
                <p><small>Tickety przypisane na stałe do twojego profilu.</small></p>

                @if(!$userQuestsStatic->isEmpty())
                <table class="table table-hover table-dark">
                    <tbody>
                        
                        @foreach($userQuestsStatic as $userQuest)
                            <tr>
                                <td class="text-nowrap text-center align-middle"><span class="text-success"><i class="fas fa-check"></i></span></td>
                                <td class="align-middle" style="width:100%">{{$userQuest->title}}</td>
                                <td class="text-nowrap text-center align-middle">{{$userQuest->points}}</td>
                            </tr>
                            
                        @endforeach
                    </tbody>
                </table>

                <p class="text-right">Suma zdobytych punktów: {{$pointsStatic}}</p>
                @else
                <p class="text-info">Nie zdobyłeś żadnych osiągnięć.</p>
                @endif
               
                <p class="mb5"><small>ZDOBĄDŹ OSIĄGNIĘCIA</small></p>
                @if(!$questsStatic->isEmpty())
                <table class="table table-hover table-dark">
                    <tbody>
                        @foreach($questsStatic as $quest)
                        <tr>
                            <td class="text-nowrap text-center align-middle"><span class="text-danger"><i class="fas fa-times"></i></span></td>
                            <td class="align-middle" style="width:100%">{{$quest->title}}</td>
                            <td class="text-nowrap text-center align-middle">{{$quest->points}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @else
                <p class="text-info">Obecnie brak dodatkowych osiągnięc.</p>
                @endif

            </div>

        </div>
<!--
        <div class="row">
            <div class="col-12 mb30">
                <h5 class="mb0">TICKETY JEDNORAZOWE</h5>
                <p><small>Wszystkie aktywowane tickety jednorazowe zostaną zużyte podczas najbliższego giveaway, do którego dołączysz.</small></p>

                @if(!$userQuestsAdditional->isEmpty())
                    <table class="table table-hover table-dark">
                        <tbody>
                            @foreach($userQuestsAdditional as $userQuest)
                            <tr>
                                <td class="text-nowrap text-center align-middle"><span class="text-success"><i class="fas fa-check"></i></span></td>
                                <td class="align-middle" style="width:100%">{{$userQuest->title}}. <a href="#" class="highlight">Dowiedz się więcej.</a></td>
                                <td class="text-nowrap text-center align-middle">{{$userQuest->points}}<br><a class="btn btn-primary btn-sm">Aktywuj</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                
                    <p class="text-right">Aktywowane tickety: {{$pointsAdditional}}</p>
                @else 
                    <p class="text-info">Nie zdobyłeś żadnych ticketów dodatkowych.</p>
                @endif
                <p class="mb5"><small>ZDOBĄDŹ DODATKOWE TICKETY</small></p>
                @if(!$questsAdditional->isEmpty())
                <table class="table table-hover table-dark">
                    <tbody>

                        @foreach($questsAdditional as $quest)
                        <tr>
                            <td class="text-nowrap text-center align-middle"><span class="text-danger"><i class="fas fa-times"></i></span></td>
                            <td class="align-middle" style="width:100%">{{$quest->title}}</td>
                            <td class="text-nowrap text-center align-middle">{{$quest->points}}</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                @else
                <p class="text-info">Obecnie brak dodatkowych ticketów.</p>
                @endif

            </div>

        </div>
    -->
    </div>
</div>
@endsection
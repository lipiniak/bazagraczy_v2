@extends('layouts.app')

@section('content')
<script type="text/javascript" src="{{asset('js/html5gallery.js')}}"></script>
<div id="giveaway-page" class="content">

    <div class="container-fluid">

        <div class="row">

            <div class="col-12">
                <h1 class="mb30">{{$giveaway->title}}</h1>
            </div>

            <div class="col-12 mb30">
                Losowanie: <span class="text-nowrap">{{date('d.m.Y H:i:s',strtotime($giveaway->ending_at))}}</span>
            </div>

            <div class="col mb30">
                <p>{{$giveaway->description}}</p>    
            </div>

            <div class="col-lg-auto mb30">


                <!-- A wrapper DIV to center the Gallery -->
                @if($giveaway->carousel_img != NULL)
                <div style="text-align:center;" class="d-sm-none">
                    <!-- Define the Div for Gallery -->
                    <!-- 1. Add class html5gallery to the Div -->
                    <!-- 2. Define parameters with HTML5 data tags -->
                    <div style="display:none;margin:0 auto;" class="html5gallery" data-skin="horizontal" data-width="220" data-height="130" data-resizemode="fill">
                        <!-- Add images to Gallery -->
                        
                        @foreach(json_decode($giveaway->carousel_img) as $image)
                        <a href="{{ Storage::url($image) }}"><img src="{{ Storage::url($image) }}" alt=""></a>
                        @endforeach
                        
                       
                    </div>
                </div>
                @endif
                @if($giveaway->carousel_img != NULL)
                <!-- A wrapper DIV to center the Gallery -->
                <div style="text-align:center;" class="d-none d-sm-block d-lg-none">
                    <!-- Define the Div for Gallery -->
                    <!-- 1. Add class html5gallery to the Div -->
                    <!-- 2. Define parameters with HTML5 data tags -->
                    <div style="display:none;margin:0 auto;" class="html5gallery" data-skin="horizontal" data-width="470" data-height="278" data-resizemode="fill">
                            
                            @foreach(json_decode($giveaway->carousel_img) as $image)
                            <a href="{{ Storage::url($image) }}"><img src="{{ Storage::url($image) }}" alt=""></a>
                            @endforeach
                            
                        
                    </div>
                </div>
                @endif
                @if($giveaway->carousel_img != NULL)
                <!-- A wrapper DIV to center the Gallery -->
                <div style="text-align:center;" class="d-none d-lg-block">
                    <!-- Define the Div for Gallery -->
                    <!-- 1. Add class html5gallery to the Div -->
                    <!-- 2. Define parameters with HTML5 data tags -->
                    <div style="display:none;margin:0 auto;" class="html5gallery" data-skin="horizontal" data-width="560" data-height="331" data-resizemode="fill">
                        <!-- Add images to Gallery -->
                        
                        @foreach(json_decode($giveaway->carousel_img) as $image)
                        <a href="{{ Storage::url($image) }}"><img src="{{ Storage::url($image) }}" alt=""></a>
                        @endforeach
                        
                       
                    </div>
                </div>
                @endif

            </div>


            <div class="col-12 mb30">
                <h4>WARUNKI UDZIAŁU</h4> 
                <ul>
                    @foreach($giveaway->rules as $rule)
                        <li>{{$rule->rule_name}}  @if($giveaway->checkRule($rule->rule_key))<i class="fas fa-check-circle text-success"></i>@else<i class="fas fa-times-circle text-danger"></i>@endif</li>
                    
                    @endforeach
                </ul>
            </div>
            
            @if($giveaway->status == 1)
                @if($giveaway->participants()->where('user_id',Auth::user()->id)->Where('joined',1)->exists())
                
                    <div class="col-12 mb30 text-center">
                        <div class="alert alert-info">
                            <p class="text-success"><i class="fas fa-check fa-2x"></i><br>Dołączyłeś do giveawaya!</p>
                            <?php
                                $allParticipants = $giveaway->participants()->where('joined',1)->count();
                                $allTickets = $giveaway->participants->sum('tickets');
                                $myTickets = $giveaway->participants()->where("user_id",Auth::user()->id)->first()->pivot->tickets;
                            ?>

                            <p>Wszyscy uczestnicy: {{$allParticipants}}<br>
                                Wszystkie tickety: {{$allTickets}}<br>
                                Twoje tickety: {{$myTickets}}<br>Twoja szansa: {{round(($myTickets/$allTickets)*100,2)}}%</p>
                            <p>Zwiększ swoje szanse zdobywając <a href="#" class="highlight">więcej ticketów.</a></p>
                        </div>
                    </div>
                @else 
                    <div class="col-12 mb30 text-center">
                        <div class="alert alert-info">
                            <p>Twoje tickety: {{Auth::user()->tickets+Auth::user()->tmp_tickets}}<br> Zwiększ swoje szanse zdobywając <a href="#" class="highlight">więcej ticketów.</a></p>
                            @if($giveaway->joinAvalible)
                                <a href="{{route('giveaway.join',['id'=>$giveaway->id])}}"class="btn btn-primary btn-lg"><strong>DOŁĄCZ DO GIVEAWAY!</strong></a>
                            @else
                                <p class="mb5 text-danger">Nie spełniasz wszystkich warunków.</p>
                            @endif
                            
                            
                        </div>
                    </div>
                @endif
            @elseif($giveaway->status == 3)
             <div class="col-12 mb30 text-center">
                <div class="alert alert-info">
                    <p class="mt15"><i class="fas fa-crown fa-2x"></i><br>Zwycięzca: <a href="#" class="highlight">{Nickgracza}</a></p>
                </div>
            </div>
            @endif
        </div>

    </div>
    
</div>

@endsection
@extends('layouts.app')

@section('content')
<?php
$user = Auth::user();
?>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.11&appId=1769261916703776';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <div class="content">

            <div class="container-fluid">
                    <div class="row top-info-row">
                           

                            <div class="modal fade" id="noweZgloszenie" tabindex="-1" role="dialog" aria-labelledby="noweZgloszenieTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Nowe zgłoszenie</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <form style="width:100%" method='post' action='{{route("raport.new")}}'>
                                                {{ csrf_field() }}
                                                <div class="modal-body text-left">
                                                    <div class="form-group">
                                                        <label class="form-label-custom" for="noweZgloszenieTemat">Temat zgłoszenia</label>
                                                        <select class="form-control" id="noweZgloszenieTemat" name='type'>
                                                            <option value='new_game'>Dodanie nowej gry do bazy</option>
                                                            <option value='errors'>Problem z działaniem aplikacji</option>
                                                            <option value="new_feature">Sugestia zmiany/dodania funkcjonalności</option>
                                                        </select>
                                                        <label class="form-label-custom" for="profil-zglos">Treść zgłoszenia</label>
                                                        <textarea class="form-control" id="profil-zglos" rows="3" style="min-height:80px;" name='content'></textarea>
                                                        
                                                    </div> 
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary">Wyślij</button>
                                                </div>
                                            </form>
                                        </div>
                                    
                                </div>
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-12">
                            <h1 class="mb30">Kokpit</h1>
                        </div>
                    </div>
                    
                    
                    
                    <div class="kokpit-content-outer">

                        <div class="kokpit-content-left">

                            <div class="row">
                                <div class="col-12">
                                    <div class="kokpit-box">
                                        <h3>Aktualności</h3>
                                        <div class="kokpit-box-inner fb-hidden">

                                            <div class="fb-page" data-href="https://www.facebook.com/bazagraczy/" data-tabs="timeline" data-width="500" data-height="2000" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><blockquote cite="https://www.facebook.com/bazagraczy/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/bazagraczy/">BAZAGRACZY.pl</a></blockquote></div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="kokpit-content-right">
                            <?php $players_count = Stats::getPlayerMaxMetches();?>
                            @if($players_count['user'] != false)
                            <div class="row">
                                <div class="col-12">
                                    <div class="kokpit-box">
                                        <h3 class="mb30">Statystyki GRACZE</h3>
                                        <div class="kokpit-box-inner">
                                            <div class="box100">
                                                <div class="row">
                                                    <?php $procentage_player_win = Stats::getPlayerMaxProcentageWins();?>

                                                    @if($procentage_player_win['user'] != false)
                                                    <div class="col-12 col-sm-6 col-lg-4">
                                                        <div class="kokpit-top-list">
                                                            <p class="top-list-kryterium">WYGRANE MECZE</p>
                                                            <p class="top-list-wartosc">{{$procentage_player_win['procentage']}}%</p>
                                                            <a href="/profil/{{$procentage_player_win['user']->id}}">
                                                                @if($procentage_player_win['user']->avatar == 'no-avatar.jpg')
                                                                    <img src="{{asset('images/avatars/')}}/{{$procentage_player_win['user']->avatar}}" class="top-list-avatar" alt="{{$procentage_player_win['user']->login}}">
                                                                @else
                                                                    <img src="{{ Storage::url($procentage_player_win['user']->avatar) }}" class="top-list-avatar" alt="{{$procentage_player_win['user']->login}}">
                                                                @endif
                                                                <p class="top-list-name">{{$procentage_player_win['user']->login}}</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    
                                                    @if($players_count['user'] != false)
                                                    <div class="col-12 col-sm-6 col-lg-4">
                                                        <div class="kokpit-top-list">
                                                            <p class="top-list-kryterium">ROZEGRANE MECZE</p>
                                                            <p class="top-list-wartosc">{{$players_count['count']}}</p>
                                                            <a href="/profil/{{$players_count['user']->id}}">
                                                                @if($players_count['user']->avatar == 'no-avatar.jpg')
                                                                    <img src="{{asset('images/avatars/')}}/{{$players_count['user']->avatar}}" class="top-list-avatar" alt="{{$players_count['user']->login}}">
                                                                @else
                                                                    <img src="{{ Storage::url($players_count['user']->avatar) }}" class="top-list-avatar" alt="{{$players_count['user']->login}}">
                                                                @endif
                                                                <p class="top-list-name">{{$players_count['user']->login}}</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    <?php $procentage_players_cancel = Stats::getPlayerMaxProcentageCancel();?>
                                                    @if($procentage_players_cancel['user'] != false)
                                                    <div class="col-12 col-sm-6 col-lg-4">
                                                        <div class="kokpit-top-list">
                                                            <p class="top-list-kryterium text-danger">ANULOWANE MECZE</p>
                                                            <p class="top-list-wartosc">{{$procentage_players_cancel['procentage']}}%</p>
                                                            <a href="/profil/{{$procentage_players_cancel['user']->id}}">
                                                                @if($procentage_players_cancel['user']->avatar == 'no-avatar.jpg')
                                                                    <img src="{{asset('images/avatars/')}}/{{$procentage_players_cancel['user']->avatar}}" class="top-list-avatar" alt="{{$procentage_players_cancel['user']->login}}">
                                                                @else
                                                                    <img src="{{ Storage::url($procentage_players_cancel['user']->avatar) }}" class="top-list-avatar" alt="{{$procentage_players_cancel['user']->login}}">
                                                                @endif
                                                                <p class="top-list-name">{{$procentage_players_cancel['user']->login}}</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @if(Stats::getPlayerMaxProcentageWinsPerGame()['user'] != false || Stats::getPlayerMaxMetchesPerGame()['user']!=false || Stats::getPlayerMaxProcentageCancelPerGame()['user'] != false)
                                                    <div class="col-12 text-center mt30">
                                                        <p>{{Auth::user()->selectedGame()['game']}} <span class="badge badge-muted text-white">{{Auth::user()->selectedGame()['platform']}}</span></p>
                                                    </div>
                                                    @endif
                                                    <?php $procentage_player_win_game = Stats::getPlayerMaxProcentageWinsPerGame();?>

                                                    @if($procentage_player_win_game['user'] != false)
                                                    <div class="col-12 col-sm-6 col-lg-4">
                                                        <div class="kokpit-top-list">
                                                            <p class="top-list-kryterium">WYGRANE MECZE</p>
                                                            <p class="top-list-wartosc">{{$procentage_player_win_game['procentage']}}%</p>
                                                            <a href="/profil/{{$procentage_player_win_game['user']->id}}">
                                                                @if($procentage_player_win_game['user']->avatar == 'no-avatar.jpg')
                                                                    <img src="{{asset('images/avatars/')}}/{{$procentage_player_win_game['user']->avatar}}" class="top-list-avatar" alt="{{$procentage_player_win_game['user']->login}}">
                                                                @else
                                                                    <img src="{{ Storage::url($procentage_player_win_game['user']->avatar) }}" class="top-list-avatar" alt="{{$procentage_player_win_game['user']->login}}">
                                                                @endif
                                                                <p class="top-list-name">{{$procentage_player_win_game['user']->login}}</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    <?php $players_max_per_game = Stats::getPlayerMaxMetchesPerGame();?>
                                                    @if($players_max_per_game['user'] != false)
                                                    <div class="col-12 col-sm-6 col-lg-4">
                                                        <div class="kokpit-top-list">
                                                            <p class="top-list-kryterium">ROZEGRANE MECZE</p>
                                                            <p class="top-list-wartosc">{{$players_max_per_game['count']}}</p>
                                                            <a href="/profil/{{$players_max_per_game['user']->id}}">
                                                                @if($players_max_per_game['user']->avatar == 'no-avatar.jpg')
                                                                    <img src="{{asset('images/avatars/')}}/{{$players_max_per_game['user']->avatar}}" class="top-list-avatar" alt="{{$players_max_per_game['user']->login}}">
                                                                @else
                                                                    <img src="{{ Storage::url($players_max_per_game['user']->avatar) }}" class="top-list-avatar" alt="{{$players_max_per_game['user']->login}}">
                                                                @endif
                                                                <p class="top-list-name">{{$players_max_per_game['user']->login}}</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    <?php $procentage_players_cancel_game = Stats::getPlayerMaxProcentageCancelPerGame();?>
                                                    @if($procentage_players_cancel_game['user'] != false)
                                                    <div class="col-12 col-sm-6 col-lg-4">
                                                        <div class="kokpit-top-list">
                                                            <p class="top-list-kryterium text-danger">ANULOWANE MECZE</p>
                                                            <p class="top-list-wartosc">{{$procentage_players_cancel_game['procentage']}}%</p>
                                                            <a href="/profil/{{$procentage_players_cancel_game['user']->id}}">
                                                                @if($procentage_players_cancel_game['user']->avatar == 'no-avatar.jpg')
                                                                    <img src="{{asset('images/avatars/')}}/{{$procentage_players_cancel_game['user']->avatar}}" class="top-list-avatar" alt="{{$procentage_players_cancel_game['user']->login}}">
                                                                @else
                                                                    <img src="{{ Storage::url($procentage_players_cancel_game['user']->avatar) }}" class="top-list-avatar" alt="{{$procentage_players_cancel_game['user']->login}}">
                                                                @endif
                                                                <p class="top-list-name">{{$procentage_players_cancel_game['user']->login}}</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <?php $matches_played_clan = Stats::getClanMaxMatches();?>
                            @if($matches_played_clan['clan'] != false)
                            <div class="row">
                                <div class="col-12">
                                    <div class="kokpit-box">

                                        <h3 class="mb30">Statystyki KLANY</h3>
                                        <div class="kokpit-box-inner">
                                            <div class="box100">
                                                <div class="row">
                                                    <?php $procentage_winn = Stats::getClanMaxProcentageWins();?>
                                                    @if($procentage_winn['clan'] != false)
                                                    <div class="col-12 col-sm-6 col-lg-4">
                                                        <div class="kokpit-top-list">
                                                            <p class="top-list-kryterium">WYGRANE MECZE</p>
                                                            <p class="top-list-wartosc">{{$procentage_winn['procentage']}}%</p>
                                                            <a href="/klan/{{$procentage_winn['clan']->link}}">
                                                                @if($procentage_winn['clan']->logo == 'no-clan-logo.jpg')
                                                                    <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="top-list-avatar klan-def-border" alt="{{$procentage_winn['clan']->name}}">
                                                                @else 
                                                                    <img src="{{ Storage::url($procentage_winn['clan']->logo)}}" class="top-list-avatar klan-def-border" alt="{{$procentage_winn['clan']->name}}">
                                                                @endif
                                                                <p class="top-list-clantag"><small>[{{$procentage_winn['clan']->tag}}]</small></p>
                                                                <p class="top-list-name">{{$procentage_winn['clan']->name}}</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    
                                                    @if($matches_played_clan['clan'] != false)
                                                    <div class="col-12 col-sm-6 col-lg-4">
                                                        <div class="kokpit-top-list">
                                                            <p class="top-list-kryterium">ROZEGRANE MECZE</p>
                                                            <p class="top-list-wartosc">{{$matches_played_clan['count']}}</p>
                                                            <a href="/klan/{{$matches_played_clan['clan']->link}}">
                                                                @if($matches_played_clan['clan']->logo == 'no-clan-logo.jpg')
                                                                    <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="top-list-avatar klan-def-border" alt="{{$matches_played_clan['clan']->name}}">
                                                                @else 
                                                                    <img src="{{ Storage::url($matches_played_clan['clan']->logo)}}" class="top-list-avatar klan-def-border" alt="{{$matches_played_clan['clan']->name}}">
                                                                @endif
                                                                <p class="top-list-clantag"><small>[{{$matches_played_clan['clan']->tag}}]</small></p>
                                                                <p class="top-list-name">{{$matches_played_clan['clan']->name}}</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    <?php $procentage_cancel = Stats::getClanMaxProcentageCancel();?>
                                                    @if($procentage_cancel['clan'] != false)
                                                    <div class="col-12 col-sm-6 col-lg-4">
                                                        <div class="kokpit-top-list">
                                                            <p class="top-list-kryterium text-danger">ANULOWANE MECZE</p>
                                                            <p class="top-list-wartosc">{{$procentage_cancel['procentage']}}%</p>
                                                            <a href="/klan/{{$procentage_cancel['clan']->link}}">
                                                                @if($procentage_cancel['clan']->logo == 'no-clan-logo.jpg')
                                                                    <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="top-list-avatar klan-def-border" alt="{{$procentage_cancel['clan']->name}}">
                                                                @else 
                                                                    <img src="{{ Storage::url($procentage_cancel['clan']->logo)}}" class="top-list-avatar klan-def-border" alt="{{$procentage_cancel['clan']->name}}">
                                                                @endif
                                                                <p class="top-list-clantag"><small>[{{$procentage_cancel['clan']->tag}}]</small></p>
                                                                <p class="top-list-name">{{$procentage_cancel['clan']->name}}</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @if(Stats::getClanMaxProcentageWinsPerGame()['clan'] != false || Stats::getClanMaxMatchesPerGame()['clan']!=false || Stats::getClanMaxProcentageCancelPerGame()['clan'] != false)
                                                    <div class="col-12 text-center mt30">
                                                        <p>{{Auth::user()->selectedGame()['game']}} <span class="badge badge-muted text-white">{{Auth::user()->selectedGame()['platform']}}</span></p>
                                                    </div>
                                                    @endif
                                                    <?php $procentage_winn = Stats::getClanMaxProcentageWinsPerGame();?>
                                                    @if($procentage_winn['clan'] != false)
                                                    <div class="col-12 col-sm-6 col-lg-4">
                                                        <div class="kokpit-top-list">
                                                            <p class="top-list-kryterium">WYGRANE MECZE</p>
                                                            <p class="top-list-wartosc">{{$procentage_winn['procentage']}}%</p>
                                                            <a href="/klan/{{$procentage_winn['clan']->link}}">
                                                                @if($procentage_winn['clan']->logo == 'no-clan-logo.jpg')
                                                                    <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="top-list-avatar klan-def-border" alt="{{$procentage_winn['clan']->name}}">
                                                                @else 
                                                                    <img src="{{ Storage::url($procentage_winn['clan']->logo)}}" class="top-list-avatar klan-def-border" alt="{{$procentage_winn['clan']->name}}">
                                                                @endif
                                                                <p class="top-list-clantag"><small>[{{$procentage_winn['clan']->tag}}]</small></p>
                                                                <p class="top-list-name">{{$procentage_winn['clan']->name}}</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    <?php $matches_played_clan = Stats::getClanMaxMatchesPerGame();?>
                                                    @if($matches_played_clan['clan'] != false)
                                                    <div class="col-12 col-sm-6 col-lg-4">
                                                        <div class="kokpit-top-list">
                                                            <p class="top-list-kryterium">ROZEGRANE MECZE</p>
                                                            <p class="top-list-wartosc">{{$matches_played_clan['count']}}</p>
                                                            <a href="/klan/{{$matches_played_clan['clan']->link}}">
                                                                @if($matches_played_clan['clan']->logo == 'no-clan-logo.jpg')
                                                                    <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="top-list-avatar klan-def-border" alt="{{$matches_played_clan['clan']->name}}">
                                                                @else 
                                                                    <img src="{{ Storage::url($matches_played_clan['clan']->logo)}}" class="top-list-avatar klan-def-border" alt="{{$matches_played_clan['clan']->name}}">
                                                                @endif
                                                                <p class="top-list-clantag"><small>[{{$matches_played_clan['clan']->tag}}]</small></p>
                                                                <p class="top-list-name">{{$matches_played_clan['clan']->name}}</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    <?php $procentage_cancel = Stats::getClanMaxProcentageCancelPerGame();?>
                                                    @if($procentage_cancel['clan'] != false)
                                                    <div class="col-12 col-sm-6 col-lg-4">
                                                        <div class="kokpit-top-list">
                                                            <p class="top-list-kryterium text-danger">ANULOWANE MECZE</p>
                                                            <p class="top-list-wartosc">{{$procentage_cancel['procentage']}}%</p>
                                                            <a href="/klan/{{$procentage_cancel['clan']->link}}">
                                                                @if($procentage_cancel['clan']->logo == 'no-clan-logo.jpg')
                                                                    <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="top-list-avatar klan-def-border" alt="{{$procentage_cancel['clan']->name}}">
                                                                @else 
                                                                    <img src="{{ Storage::url($procentage_cancel['clan']->logo)}}" class="top-list-avatar klan-def-border" alt="{{$procentage_cancel['clan']->name}}">
                                                                @endif
                                                                <p class="top-list-clantag"><small>[{{$procentage_cancel['clan']->tag}}]</small></p>
                                                                <p class="top-list-name">{{$procentage_cancel['clan']->name}}</p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-12">
                                    <div class="kokpit-box">
                                        <h3 class="mb30">Statystyki BAZAGRACZY</h3>
                                        <div class="kokpit-box-inner">

                                            <div class="box100">
                                                <div class="row">
                                                    <div class="col-6 col-sm-6 col-lg-4">
                                                        <p><span class="text-muted"><small>Zarejestrowani gracze</small></span><br>{{$data['stats_registered']}}</p>
                                                    </div>
                                                    <div class="col-6 col-sm-6 col-lg-4">
                                                        <p><span class="text-muted"><small>Aktualnie online</small></span><br>{{$data['stats_online']}}</p>
                                                    </div>
                                                    <div class="col-6 col-sm-6 col-lg-4">
                                                        <p><span class="text-muted"><small>Zarejestrowane klany</small></span><br>{{$data['stats_total_clans']}}</p>
                                                    </div>
                                                    <div class="col-6 col-sm-6 col-lg-4">
                                                        <p><span class="text-muted"><small>Utworzone drużyny</small></span><br>{{$data['stats_total_teams']}}</p>
                                                    </div>
                                                    <div class="col-6 col-sm-6 col-lg-4">
                                                        <p><span class="text-muted"><small>Rozegrane klanówki</small></span><br>{{$data['stats_total_matches']}}</p>
                                                    </div>
                                                    <div class="col-6 col-sm-6 col-lg-4">
                                                        <p><span class="text-muted"><small>Gry</small></span><br>{{$data['stats_total_games']}}</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="clearfix"></div>

                </div>

    </div>
@endsection

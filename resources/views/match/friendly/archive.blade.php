@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.42/css/bootstrap-datetimepicker.css" />
<div id="wojny-klanowe" class="content">

    <div class="container-fluid">

        <div class="row">

            <div class="col-12">
                <h1 class="mb5">Klanówki archiwalne</h1>
                <p class="text-muted">{{$selectedGames['game']}} <span class="badge badge-muted">{{$selectedGames['platform']}}</span><p>
            </div>

            <div class="col-12">
                
                <!-- Wyszukiwarka /start -->
                <form action='{{route("match.friendly.archive.search")}}' method="POST">
                    {{ csrf_field() }}
                    <div class="wyszukiwarka-form-wrapper">
                        <div class="row">
                            <div class="col-12 col-lg-6 col-xl-6">
                                <div class="form-group mb5" style="width:100%">
                                    <label class="form-label-custom" for="wyszukiwarka-klan1">Nazwa klanu #1</label>
                                    <select class="form-control bgselect" style="width:100%" name='host_clan' id='host_clan_select'>
                                        <option  label=" " class="disabled-select-option" selected></option>
                                        @if(!$clans)
                                            <option  value='' class="disabled-select-option" selected>Brak klanów</option>
                                        @else
                                            @foreach($clans as $clan)
                                                <option value='{{$clan->id}}' @if(!empty($formData['host_clan']) && $formData['host_clan'] == $clan->id) selected @endif>{{$clan->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div> 
                            <div class="col-12 col-lg-6 col-xl-6">
                                <div class="form-group mb5" style="width:100%">
                                    <label class="form-label-custom" for="wyszukiwarka-klan2">Nazwa klanu #2</label>
                                    <select class="form-control bgselect" style="width:100%" name='enemy_clan' id='enemy_clan_select'>
                                        <option  label=" " class="disabled-select-option" selected></option>
                                        @if(!$clans)
                                            <option  value='' class="disabled-select-option" selected>Brak klanów</option>
                                        @else
                                            @foreach($clans as $clan)
                                            
                                            <option value='{{$clan->id}}' @if(!empty($formData['enemy_clan']) && $formData['enemy_clan'] == $clan->id) selected @endif>{{$clan->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div> 

                            <script>
                                $(document).ready(function () {
                                    
                                    $('#host_clan_select').select2({});
                                    $('#enemy_clan_select').select2({});
                                });
                            </script>
                            <div class="col-12 col-lg-6 col-xl-3">
                                <div class="form-group mb5" style="width:100%">
                                    <label class="form-label-custom" for="wyszukiwarka-dataod">Data od...</label>
                                    <input type="text" class="form-control" id="wyszukiwarka-dataod" name="date_from" autocomplete='off' value='@if(!empty($formData["date_from"])) {{$formData["date_from"]}} @endif' readonly>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6 col-xl-3">
                                <div class="form-group mb5" style="width:100%">
                                    <label class="form-label-custom" for="wyszukiwarka-dataod">Data do...</label>
                                    <input type="text" class="form-control" id="wyszukiwarka-datado" name='date_to' autocomplete='off'value='@if(!empty($formData["date_to"])) {{$formData["date_to"]}} @endif' readonly>
                                </div>
                            </div>
                            <script>
                                    $('select').on('select2:open', function (e) {
                                        $('.select2-search input').prop('focus',false);
                                    });

                                $(function () {
                                        $('#wyszukiwarka-dataod').datetimepicker(
                                            {   
                                                ignoreReadonly: true,
                                                format: 'DD.MM.YYYY',
                                                locale: 'pl',
                                                viewMode: 'days',
                                                icons: {
                                                    time: 'fa fa-clock-o',
                                                    date: 'fa fa-calendar',
                                                    up: 'fa fa-chevron-up',
                                                    down: 'fa fa-chevron-down',
                                                    //previous: 'glyphicon glyphicon-chevron-left',
                                                    previous: 'fa fa-chevron-left',
                                                    next: 'fa fa-chevron-right',
                                                    today: 'fa fa-crosshairs',
                                                    clear: 'fa fa-trash',
                                                    close: 'fa fa-remove',
                                                },
                                            }
                                        );
                                    });
                                    $(function () {
                                    $('#wyszukiwarka-datado').datetimepicker(
                                        {   
                                            ignoreReadonly: true,
                                            format: 'DD.MM.YYYY',
                                            locale: 'pl', 
                                            viewMode: 'days',
                                            icons: {
                                                time: 'fa fa-clock-o',
                                                date: 'fa fa-calendar',
                                                up: 'fa fa-chevron-up',
                                                down: 'fa fa-chevron-down',
                                                //previous: 'glyphicon glyphicon-chevron-left',
                                                previous: 'fa fa-chevron-left',
                                                next: 'fa fa-chevron-right',
                                                today: 'fa fa-crosshairs',
                                                clear: 'fa fa-trash',
                                                close: 'fa fa-remove',
                                            },
                                        }
                                    );
                                });
                                </script>
                            <div class="col-12 col-md-12 col-xl-6">
                                <div class="btn-group special mb5" role="group" style="margin-top:22px">
                                    <a href='{{route("match.friendly.show.archive")}}'class="btn btn-secondary">Resetuj</a>
                                    <input type='submit' class="btn btn-primary" value='Wyszukaj'>
                                </div>
                            </div> 
                        </div>
                    </div>
                </form>
                <!-- Wyszukiwarka /koniec -->
                @if(count($matches) == 0)
                    Brak wyników.
                @else
                <ul class="list-group lista-rogrywki-klanowe" style="list-style-type: none">
                    @foreach($matches as $match)
                    <!-- Wynik /start -->
                    <li class="mb25">
                        <div class="row table-row-border" >
                            <div class="col-12 col-sm-3 col-md-2 col-xl-1 table-cell-border text-center">
                                
                                @if($match->result == 1)
                                    @if($match->winner_id == $match->host_clan)
                                        <div class="center-result-icon">
                                            <div class="m0 rozgrywki-wynik rozgrywki-zwyciestwo" style="width: auto;">
                                                <i class="fas fa-crown fa-2x" data-toggle="tooltip" data-placement="top" title="Zwycięstwo"></i>
                                            </div>
                                        </div>
                                    @else
                                        <div class="center-result-icon">
                                            <div class="m0 rozgrywki-wynik rozgrywki-porazka" style="width: auto;">
                                                <i class="fas fa-tint fa-2x" data-toggle="tooltip" data-placement="top" title="Porażka"></i>
                                            </div>
                                        </div>
                                    @endif
                                @elseif($match->result == 3)
                                <div class="center-result-icon">
                                    <div class="m0 rozgrywki-wynik rozgrywki-remis" style="width: auto;">
                                        <i class="fas fa-shield-alt fa-2x" data-toggle="tooltip" data-placement="top" title="Remis"></i>
                                    </div>
                                </div>
                                @elseif($match->result == 4)
                                <div class="center-result-icon">
                                    <div class="m0 rozgrywki-wynik rozgrywki-anulowany" style="width: auto;">
                                        <i class="fas fa-ban fa-2x" data-toggle="tooltip" data-placement="top" title="Anulowany"></i>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="col-12 col-sm-9 col-md-10 col-xl-4 table-cell-border text-center">
                                <p class="m0"><a href="/klan/{{$match->hostClan->link}}" class="highlight">[{{$match->hostClan->tag}}] {{$match->hostClan->name}}</a><br><span class="">{{$match->hostTeam->name}}</span></p>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-xl-2 table-cell-border text-center">
                                <p class="m0"><span class="color-info">{{date('d.m.Y',strtotime($match->match_date))}}</span><br><a href="{{route('match.friendly.show',['id'=>$match->id])}}" class="btn btn-secondary btn-sm">Szczegóły</a></p>
                            </div>
                            <div class="col-12 col-sm-9 col-md-10 col-xl-4 table-cell-border text-center">
                                <p class="m0"><a href="/klan/{{$match->enemyClan->link}}" class="highlight">[{{$match->enemyClan->tag}}] {{$match->enemyClan->name}}</a><br><span class="">{{$match->enemyTeam->name}}</span></p>
                            </div>
                            <div class="col-12 col-sm-3 col-md-2 col-xl-1 table-cell-border text-center">
                                @if($match->result == 1)
                                    @if($match->winner_id == $match->enemy_clan)
                                    <div class="center-result-icon">
                                        <div class="m0 rozgrywki-wynik rozgrywki-zwyciestwo" style="width: auto;">
                                            <i class="fas fa-crown fa-2x" data-toggle="tooltip" data-placement="top" title="Zwycięstwo"></i>
                                        </div>
                                    </div>
                                    @else
                                    <div class="center-result-icon">
                                        <div class="m0 rozgrywki-wynik rozgrywki-porazka" style="width: auto;">
                                            <i class="fas fa-tint fa-2x" data-toggle="tooltip" data-placement="top" title="Porażka"></i>
                                        </div>
                                    </div>
                                    @endif
                                @elseif($match->result == 3)
                                <div class="center-result-icon">
                                    <div class="m0 rozgrywki-wynik rozgrywki-remis" style="width: auto;">
                                        <i class="fas fa-shield-alt fa-2x" data-toggle="tooltip" data-placement="top" title="Remis"></i>
                                    </div>
                                </div>
                                @elseif($match->result == 4)
                                <div class="center-result-icon">
                                    <div class="m0 rozgrywki-wynik rozgrywki-anulowany" style="width: auto;">
                                        <i class="fas fa-ban fa-2x" data-toggle="tooltip" data-placement="top" title="Anulowany"></i>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </li>
                    <!-- Wynik /koniec -->
                    @endforeach
                    
                </ul>
                @endif
            </div>

        </div>

    </div>

</div>
<script src="{{ asset('js/moment.js') }}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('js/pl.js') }}"></script>
@endsection
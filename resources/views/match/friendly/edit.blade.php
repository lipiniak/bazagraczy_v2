@extends('layouts.app')

@section('content')


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" />
        
<div id="wojny-klanowe" class="content">

                    <div class="container-fluid">

                        <div class="row">

                            <div class="col-12">
                                <h1 class="mb5">Edycja klanówki</h1>
                                <p class="text-muted">{{$selectedGame['game']}} <span class="badge badge-muted">{{$selectedGame['platform']}}</span><p>
                            </div>
                            @if(false)
                            <div class="col-12">
                                <div class="alert alert-info">
                                    <h4>Nie posiadasz wystarczających uprawnień!</h4>
                                    <ul class="mb0">
                                        <li>Klanówki mogą organizować tylko klany posiadające utworzoną drużynę.</li>
                                        <li>Do drużyny musi być przypisana platforma i tytuł gry, której klanówka dotyczy.</li>
                                        <li>Musisz być Liderem klanu lub Kapitanem drużyny.</li>
                                    </ul>
                                </div>
                            </div>
                            @elseif($noEdit)
                            <div class="col-12">
                                <div class="alert alert-warning">
                                    <h4>Edycja tymczasowo niedostępna!</h4>
                                    <p class="mb0">Edycja ustawień meczu może być niedostępna ponieważ:</p>
                                    <ul class="mb5">
                                        <li>W danym meczu istnieją aktywne prośby innych klanów o dołączenie do rozgrywki. Przed edycją danych należy wszystkie te prośby odrzucić.</li>
                                        <li>W aktualnej chwili jeden z klanów jest w trakcie składania prośby o dołączenie do meczu (przegląda podsumowanie).</li>
                                    </ul>
                                    <a href="{{route('match.friendly.show',['id'=>$match->id])}}" class="btn btn-secondary btn-sm mr5 mb3">Wróć do meczu</a>
                                </div>
                            </div>
                            @else
                            @if($match->status == 1)
                             <div class="col-12">
                                <div class="alert alert-warning">
                                    <h4>Uwaga!</h4>
                                    <p class="mb0">Aktywacja edycji ustawień meczowych uniemożliwia klanom dołączenie do meczu, dlatego edycja jest ograniczona czasowo. Wszelkie zmiany powinny być zapisane przed automatycznym zamknięciem strony edycji.<br><strong>Pozostało <span id='countdown'></span></strong></p>
                                    <?php
                                    
                                    $enddate = strtotime(date('Y-m-d H:i:s', strtotime("$match->edit_at")));
                                    $todaydate = strtotime(date('Y-m-d H:i:s'));

                                    $remaining  = $enddate - $todaydate;
                                    ?>
                                    <script>
                                            var initialTime = <?php echo $remaining;?>

                                            var seconds = initialTime;
                                            function timer() {
                                                var days        = Math.floor(seconds/24/60/60);
                                                var hoursLeft   = Math.floor((seconds) - (days*86400));
                                                var hours       = Math.floor(hoursLeft/3600);
                                                var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
                                                var minutes     = Math.floor(minutesLeft/60);
                                                var remainingSeconds = seconds % 60;
                                                if (remainingSeconds < 10) {
                                                    remainingSeconds = "0" + remainingSeconds; 
                                                }
                                                document.getElementById('countdown').innerHTML = minutes + "m " + remainingSeconds+ "s";
                                                if (seconds == 0) {
                                                    clearInterval(countdownTimer);
                                                    window.location.replace("{{route('match.friendly.change.edit',['id'=>$match->id])}}")
                                                } else {
                                                    seconds--;
                                                }
                                            }
                                            var countdownTimer = setInterval('timer()', 1000);
                                    </script>
                                </div>
                            </div>
                            @endif
                            <div class="col-12">
                                <form style="width: 100%;" method="POST" action="{{route('match.friendly.update')}}">
                                {{ csrf_field() }}
                                <input type="hidden" name="game_id" value='{{$match->game_id}}'>
                                <input type="hidden" name="platform_id" value='{{$match->platform_id}}'>
                                <input type="hidden" name="match_id" value='{{$match->id}}'>
                                    <div class="row">
                                        <div class="form-group col-12 col-lg-6">
                                            <label class="form-label-custom">Klan *</label>
                                            <select class="form-control bgselect" style="width:100%" name='clan_id' id='clan_id'>

                                                    <option></option>
                                                @foreach($clans as $clan)
                                                   
                                                            <option value='{{$clan->id}}' @if($clan->id == $match->host_clan) selected @endif>{{$clan->name}}</option>
                                                   
                                                @endforeach
                                               
                                            </select>
                                            <script>
                                                    $(document).ready(function () {
                                                        var clan_id = $('#clan_id');
                                                        var team_id = $('#team_id');
                                                        var members = $('#members');
        
        
                                                        var clan_id = clan_id.select2({
                                                            placeholder: "",
                                                        });
                                                        var team_id = team_id.select2({
                                                            placeholder: "",
                                                        });
                                                        var members = members.select2({
                                                            placeholder: "",
                                                            tags: true
                                                        });
        
                                                        $('select').on('select2:open', function (e) {
                                                            $('.select2-search input').prop('focus',false);
                                                        });
        
                                                        clan_id.on('change',function(){
                                                            var value= $('#clan_id option:selected').attr('value');
                                                            console.log(value);
                                                            if(value == 0) {
                                                                $('#team_id').empty();
                                                            }
        
                                                            var url = "/helper/teams/"+value;
        
                                                            $.ajax({
                                                                type: "GET",
                                                                url: url,
                                                                success: function(data) {
                                                                    $('#team_id').html('<option></option>');
                                                                    $('#members').html('');
                                                                    team_id.select2({data:data,placeholder: "",});
                                                                }
                                                            });
        
                                                        });
        
                                                        team_id.on('change',function() {
                                                            var value= $('#team_id option:selected').attr('value');
                                                            console.log(value);
                                                            if(value == 0) {
                                                                $('#members').empty();
                                                            }
        
                                                            var url = "/helper/members/"+value;
        
                                                            $.ajax({
                                                                type: "GET",
                                                                url: url,
                                                                success: function(data) {
                                                                    $('#members').html('');
                                                                    members.select2({data:data,placeholder: "",});
                                                                }
                                                            });
                                                        });
                                                        
        
                                                    });
                                                        
                                                      
                                        
                                                    </script>
                                            @if ($errors->has('clan_id'))
                                                <div class="error-feedback">
                                                    {!! $errors->first('clan_id') !!}<br>
                                                </div>
                                            @endif
                                            
                                        </div>
                                        <div class="form-group col-12 col-lg-6">
                                            <label class="form-label-custom">Drużyna *</label>
                                            <select class="form-control bgselect" style="width:100%" id="team_id" name='team_id'>
                                                    <option></option>
                                                @foreach($teams as $team)
                                                    <option value='{{$team["id"]}}' @if($team['id'] == $match->host_team) selected @endif>{{$team['text']}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('team_id'))
                                                <div class="error-feedback">
                                                    {!! $errors->first('team_id') !!}<br>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group col-12 col-lg-6">
                                            <label class="form-label-custom">Ilość graczy w drużynie *</label>
                                            <input type="text" class="form-control" name='players_count' value='{{$match->players_count}}'>
                                            @if ($errors->has('players_count'))
                                                <div class="error-feedback">
                                                    {!! $errors->first('players_count') !!}<br>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group col-12 col-lg-6">
                                            
                                            <label class="form-label-custom">Skład drużyny *</label>
                                            <?php
                                                    $selectedMembers = array();
                                                   
                                                    foreach($match->hostMembers() as $member){
                                                        $selectedMembers[] = $member->id;
                                                    } 
                                                   
                                                   
                                            ?>
                                            <select class="bgselect" style="width:100%" name="members[]" id='members' multiple="multiple">
                                                
                                                @foreach($members as $member) 
                                                <option value='{{$member["id"]}}' 
                                                                                     @if(in_array($member['id'],$selectedMembers)) 
                                                                                        selected 
                                                                                    @endif 
                                                                                >{{$member['text']}}</option>
                                                @endforeach
                                                
                                            </select>
                                            @if ($errors->has('members'))
                                                <div class="error-feedback">
                                                    {!! $errors->first('members') !!}<br>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group col-12 col-lg-6">
                                            <label class="form-label-custom">Data * </label>
                                            <input type="text" class="form-control" name='match_date' id='match_date' value="{{date('d.m.Y',strtotime($match->match_date))}}" autocomplete="off" readonly>
                                            <script>
                                                $(function () {
                                                        $('#match_date').datetimepicker(
                                                            {format: 'DD.MM.YYYY',
                                                                locale: 'pl',
                                                                ignoreReadonly: true,
                                                                useCurrent: false,
                                                                minDate: moment().millisecond(0).second(0).minute(0).hour(0),
                                                                viewMode: 'days',
                                                                icons: {
                                                                time: 'fa fa-clock-o',
                                                                date: 'fa fa-calendar',
                                                                up: 'fa fa-chevron-up',
                                                                down: 'fa fa-chevron-down',
                                                                //previous: 'glyphicon glyphicon-chevron-left',
                                                                previous: 'fa fa-chevron-left',
                                                                next: 'fa fa-chevron-right',
                                                                today: 'fa fa-crosshairs',
                                                                clear: 'fa fa-trash',
                                                                close: 'fa fa-remove',
                                                                
                                                            },
                                                            }
                                                        );
                                                    });
                                                    $(function () {
                                                        $('#match_time').datetimepicker(
                                                            {format: 'LT',
                                                            ignoreReadonly: true,
                                                                locale: 'pl',
                                                                icons: {
                                                                time: 'fa fa-clock-o',
                                                                date: 'fa fa-calendar',
                                                                up: 'fa fa-chevron-up',
                                                                down: 'fa fa-chevron-down',
                                                                //previous: 'glyphicon glyphicon-chevron-left',
                                                                previous: 'fa fa-chevron-left',
                                                                next: 'fa fa-chevron-right',
                                                                today: 'fa fa-crosshairs',
                                                                clear: 'fa fa-trash',
                                                                close: 'fa fa-remove',
                                                                
                                                            },
                                                            }
                                                        );
                                                    });
                                                </script>
                                          @if ($errors->has('match_date'))
                                                <div class="error-feedback">
                                                    {!! $errors->first('match_date') !!}<br>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group col-12 col-lg-6">
                                            <label class="form-label-custom">Godzina *</label>
                                            <input type="text" class="form-control" name='match_time' id='match_time' value='{{$match->match_time}}' autocomplete="off" readonly>
                                            @if ($errors->has('match_time'))
                                                <div class="error-feedback">
                                                    {!! $errors->first('match_time') !!}<br>
                                                </div>
                                            @endif
                                        </div>
                                        <!-- ///Nie usuwać///
                                        <div class="col-12">
                                            <hr class="separator">
                                            <p class="mb5">Ustawienia rozgrywki</p>
                                            <p class="text-muted"><small>Dodaj niezbędne informacje odnośnie planowanej rozgrywki.</small></p>

                                            <div class="row mb30">
                                                <div class="col-12 col-lg-6 add-setings-input-pr0">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="min-width: 75px;">Nazwa:</span>
                                                        </div>
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-12 col-lg-6 add-setings-input-pl0">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="min-width: 75px;">Wartość:</span>
                                                        </div>
                                                        <input type="text" class="form-control">
                                                        <div class="input-group-append">
                                                            <button class="btn btn-danger" type="button" id="button-addon2">×</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div>
                                                <button type="button" class="btn btn-secondary">Dodaj następne</button>
                                            </div>

                                        </div>
                                        -->
                                        <div class="form-group col-12">
                                            <hr class="separator">
                                            <label for="new-clan-news-text" class="form-label-custom">Dodatkowe ustawienia rozgrywki</label>
                                            <textarea class="form-control" id="new-clan-news-text" rows="3" style="min-height:80px;" name='description'>{{$match->description}}</textarea>
                                            <p class="text-muted mb0"><small>Dodaj niezbędne informacje odnośnie planowanej rozgrywki.</small></p>
                                            @if ($errors->has('description'))
                                                <div class="error-feedback">
                                                    {!! $errors->first('description') !!}<br>
                                                </div>
                                            @endif
                                            
                                        </div>
                                        <div class="col-12">
                                            <div class="text-right">
                                                <a href="{{route('match.friendly.change.edit',['id'=>$match->id])}}" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Anuluj</a>
                                                <button type="submit" class="btn btn-primary">Opublikuj</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            @endif
                        </div>

                    </div>

                </div>
                <script src="{{ asset('js/moment.js') }}"></script>
        <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
        <script src="{{ asset('js/pl.js') }}"></script>
@endsection
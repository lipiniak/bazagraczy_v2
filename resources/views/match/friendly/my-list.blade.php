@extends('layouts.app')

@section('content')

<div id="wojny-klanowe" class="content">

        <div class="container-fluid" style="padding-bottom:0px!important">

            <div class="row">
                <div class="col-12">
                    <h1 class="mb5">Moje klanówki</h1>
                    <p class="text-muted">Wszystkie</p>
                </div>
            </div>
        </div>
            
                    <div id="profile-menu" class="pn-ProductNav_Wrapper mb30">
                            <nav id="pnProductNav" class="pn-ProductNav dragscroll" data-overflowing="right">
                                    <div id="pnProductNavContents" class="pn-ProductNav_Contents pn-ProductNav_Contents-no-transition" style="transform: none;">
                                <a href="{{route('match.friendly.show.open')}}" class="pn-ProductNav_Link" @if($type==1)aria-selected="true"@endif>@if(Auth::user()->OpenFriendlyMatchesAction())<span class="badge badge-danger menu-badge ml3 clan-shield" style="min-width: 15px; min-height: 15px;"><strong><i class="fas fa-exclamation-circle"></i></strong></span>@else<span class="badge badge-danger menu-badge ml3 clan-shield" style="min-width: 15px; min-height: 15px; display:none;"><strong><i class="fas fa-exclamation-circle"></i></strong></span>@endif &nbsp;Otwarte ({{$matchesCount[0]}})</a>
                                <a href="{{route('match.friendly.show.active')}}" class="pn-ProductNav_Link" @if($type==2)aria-selected="true"@endif>@if(Auth::user()->ActiveFriendlyMatchesAction())<span class="badge badge-danger menu-badge ml3 clan-shield" style="min-width: 15px; min-height: 15px;"><strong><i class="fas fa-exclamation-circle"></i></strong></span>@else<span class="badge badge-danger menu-badge ml3 clan-shield" style="min-width: 15px; min-height: 15px; display:none;"><strong><i class="fas fa-exclamation-circle"></i></strong></span>@endif &nbsp;Aktywne ({{$matchesCount[1]}})</a>
                                <a href="{{route('match.friendly.show.closed')}}" class="pn-ProductNav_Link" @if($type==3)aria-selected="true"@endif>@if(Auth::user()->ClosedFriendlyMatchesAction())<span class="badge badge-danger menu-badge ml3 clan-shield" style="min-width: 15px; min-height: 15px;"><strong><i class="fas fa-exclamation-circle"></i></strong></span>@else<span class="badge badge-danger menu-badge ml3 clan-shield" style="min-width: 15px; min-height: 15px; display:none;"><strong><i class="fas fa-exclamation-circle"></i></strong></span>@endif &nbsp;Zakończone ({{$matchesCount[2]}})</a>
                                
                                <span id="pnIndicator" class="pn-ProductNav_Indicator" style="transform: translateX(0px) scaleX(0.794844); background-color: rgb(134, 113, 0);"></span>
                                
                            </div>
                        </nav>
                        <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
                                <i class="fas fa-chevron-left"></i>
                            </button>
                            <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right" type="button">
                                <i class="fas fa-chevron-right"></i>
                            </button>
                            <hr class="separator hr-pagemenu">
                    </div>
                    <script src="{{ asset('js/horizontal-menu-scroller.js') }}"></script>
                
                    <div class="container-fluid">         
            <div class="row">
                <div class="col-12 mecze-list-small">

                    <!-- Wyszukiwarka /start -->
                    <form class="input-group mb15">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Wyszukaj..." aria-label="Wyszukaj..." aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-secondary" type="button">Szukaj</button>
                            </div>
                        </div>
                    </form>
                    <!-- Wyszukiwarka /koniec -->
                    @if(count($matches) == 0)
                    <!-- Wynik /start -->
                    <div class="row">
                        <div class="col-12">
                            <p>Brak wyników.</p>
                        </div>
                    </div>
                    <!-- Wynik /koniec -->
                    @else
                    <!-- Wynik /start -->
                    @foreach($matches as $match)
                    <div class="row table-row-border mb30">
                        <div class="col-12 col-sm-3 col-md-2 col-xl-1 table-cell-border wynik-bg">
                            <label class="form-label-custom text-muted">Id meczu</label>
                            <p class="mb5">#{{$match->id}}</p>
                            
                            <a href="{{route('match.friendly.show',['id'=>$match->id])}}" class="btn btn-secondary btn-sm mr3 mb3" data-toggle="tooltip" data-placement="top" title="Szczegóły">@if($match->isHost() && $match->host_need_action == 1)<span class="badge badge-danger menu-badge clan-shield"  style="min-width: 15px; height: 15px;"><strong><i class="fas fa-exclamation-circle"></i></strong></span>@elseif($match->isEnemy() && $match->enemy_need_action == 1) <span class="badge badge-danger menu-badge clan-shield" style="min-width: 15px; height: 15px;"><strong><i class="fas fa-exclamation-circle"></i></strong></span> @endif <i class="far fa-eye"></i></a>
                            @if($match->isHost() || $match->isEnemy())
                            @if($type == 1)
                            <a href="{{route('match.friendly.edit',['id'=>$match->id])}}" class="btn btn-secondary btn-sm mr3 mb3" data-toggle="tooltip" data-placement="top" title="Edytuj"><i class="fas fa-edit"></i></a>
                            <span data-toggle="modal" data-target="#usunMeczId1">
                            <a class="btn btn-secondary btn-sm mr3 mb3" data-toggle="tooltip"  data-placement="top" title="Usuń"><i class="far fa-trash-alt"></i></a>
                            </span>
                            <div class="modal fade" id="usunMeczId1" role="dialog" aria-labelledby="usunMeczId1Title" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Uwaga!</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-left">
                                            <p class="mb10">Czy na pewno chcesz usunąć mecz?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                                            <a href='{{route("match.friendly.delete",["id"=>$match->id])}}' class="btn btn-danger" > Usuń </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @if($type == 3) 
                            @endif
                            @endif
                        </div>
                        <div class="col-12 col-sm-3 col-md-2 col-xl-1 table-cell-border wynik-bg">
                            <label class="form-label-custom text-muted">Typ</label>
                            <p class="mb5">Towarzyski</p>
                        </div>
                        <div class="col-12 col-sm-6 col-md-8 col-xl-4 table-cell-border wynik-bg">
                            <label class="form-label-custom text-muted">Gospodarz</label>
                            <p class="m5"><a href="/klan/{{$match->hostClan->link}}" class="highlight">[{{$match->hostClan->tag}}] {{$match->hostClan->name}}</a><br><span class="">{{$match->hostTeam->name}}</span></p>
                            @if($type == 3) 
                                @if($match->result == 1)
                                    @if($match->winner_id == $match->host_clan)
                                    <div class="lista-wynik-meczu wygrana"><i class="fas fa-crown"></i> ZWYCIĘSTWO</div>
                                    @else
                                    <div class="lista-wynik-meczu przegrana"><i class="fas fa-tint"></i> PORAŻKA</div>
                                    @endif

                                @elseif($match->result == 3)
                                    <div class="lista-wynik-meczu remis"><i class="fas fa-shield-alt"></i> REMIS</div>
                                @elseif($match->result == 4)
                                    <div class="lista-wynik-meczu anulowany"><i class="fas fa-ban"></i> ANULOWANY</div>
                                @else 
                                    <div class="lista-wynik-meczu text-muted">OCZEKIWANIE NA WYNIK</div>
                                @endif
                            @endif
                        </div>
                        <div class="col-12 col-sm-6 col-md-8 col-xl-4 table-cell-border wynik-bg">
                            <label class="form-label-custom text-muted">Gość</label>
                            @if($type == 1)
                            <div class="alert alert-info" role="alert">
                                Oczekiwanie na przeciwnika...
                            </div>
                            @elseif($type == 2) 
                            
                            <p class="m5"><a href="/klan/{{$match->enemyClan->link}}" class="highlight">[{{$match->enemyClan->tag}}] {{$match->enemyClan->name}}</a><br><span class="">{{$match->enemyTeam->name}}</span></p>
                            @elseif($type == 3)
                            <p class="m5"><a href="/klan/{{$match->enemyClan->link}}" class="highlight">[{{$match->enemyClan->tag}}] {{$match->enemyClan->name}}</a><br><span class="">{{$match->enemyTeam->name}}</span></p>
                                @if($match->result == 1)
                                    @if($match->winner_id == $match->enemy_clan)
                                    <div class="lista-wynik-meczu wygrana"><i class="fas fa-crown"></i> ZWYCIĘSTWO</div>
                                    @else
                                    <div class="lista-wynik-meczu przegrana"><i class="fas fa-tint"></i> PORAŻKA</div>
                                    @endif
                                @elseif($match->result == 3)
                                    <div class="lista-wynik-meczu remis"><i class="fas fa-shield-alt"></i> REMIS</div>
                                @elseif($match->result == 4)
                                    <div class="lista-wynik-meczu anulowany"><i class="fas fa-ban"></i> ANULOWANY</div>
                                @else 
                                    <div class="lista-wynik-meczu text-muted">OCZEKIWANIE NA WYNIK</div>
                                @endif
                            @endif
                        </div>
                        <div class="col-12 col-sm-3 col-md-2 col-xl-1 table-cell-border wynik-bg">
                            <label class="form-label-custom text-muted">Data</label>
                            <p class="m0">{{date('d.m.Y',strtotime($match->match_date))}}<br>{{date('H:i',strtotime($match->match_time))}}</p>
                        </div>
                        <div class="col-12 col-sm-3 col-md-2 col-xl-1 table-cell-border wynik-bg">
                            <label class="form-label-custom text-muted">Status</label>
                            @if($type == 1)
                            <p class="m0">Otwarty</p>
                            @elseif($type == 2)
                            <p class="m0">Aktywny</p>
                            @elseif($type == 3)
                            <p class="m0">Zakończony</p>
                            @endif
                        </div>
                    </div>
                    <!-- Wynik /koniec -->
                    @endforeach
                    @endif
                </div>
            </div>

        </div>

    </div>
@endsection
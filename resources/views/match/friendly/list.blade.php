@extends('layouts.app')

@section('content')

                <div id="wojny-klanowe" class="content">

<div class="container-fluid">

    <div class="row">

        <div class="col-12">
            <h1 class="mb5">Aktualne klanówki</h1>

            <p class="text-muted"><a class="text-muted-link" data-toggle="modal" data-target="#InfoZawartosc">{{$selectedGame['game']}} <span class="badge badge-muted">{{$selectedGame['platform']}}</span></a><p>
                <div class="modal fade" id="InfoZawartosc" tabindex="-1" role="dialog" aria-labelledby="InfoZawartoscTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Info</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body text-left">
                                    <div class="twoja-notatka text-muted">
                                        <p class="mb15">Zawartość aktualnie przeglądanej strony, jej treść oraz wyniki, zawężona jest do treści związanych z <span class="text-white">{{$selectedGame['game']}}</span> <span class="badge badge-white">{{$selectedGame['platform']}}</span></p>
                                        <p class="mb0">Aby zmienić, wybierz inną grę w prawym górnym rogu aplikacji.</p>
                                    </div>
                                </div>
                            </div>
                        
                    </div>
                </div>

        </div>
        @if($selectedGame['game_id'] == null)
            <div class="alert alert-info" role="alert">
                    Wybierz tytuł gry oraz platformę, aby wyświetlić zawartość tablicy.
            </div>
        @else
        <div class="col-12">
            <a href="{{route('match.friendly.create')}}" class="btn btn-secondary mb15">Stwórz klanówkę</a>
        </div>

        <!-- Wyszukiwarka /start -->
        <div class="col-12">
            <form class="input-group mb15" method="POST" action="{{route('match.friendly.list.search')}}">
                    {{ csrf_field() }}
                <div class="input-group">
                    <input name='searchText' type="text" class="form-control" placeholder="Wyszukaj..." aria-label="Wyszukaj..." aria-describedby="basic-addon2" value="@if(isset($formData['searchText'])){{$formData['searchText']}}@endif">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="submit">Szukaj</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- Wyszukiwarka /koniec -->

        <div class="col-12">

            <div class="row">
                
                @if($data['matches']->isEmpty())
                <div class='col-12'>
                <p> Brak meczy towarzyskich. </p>
                </div>
                @else
                <!-- Ogłoszenie /start -->
                @foreach($data['matches'] as $match)
                <div class="col-12 col-xl-6">
                    <div class="alert alert-info p15 pt30" style="overflow: hidden;">
                        <div class="row">
                            
                            <div class="col-12">
                                <div class="clanwar-teamlogo-outer">
                                    <p class="text-muted mt-15 mb0"><small>Id: {{$match->id}}</small></p>
                                    <div class="avatar-wrapper">
                                        <a href="/klan/{{$match->hostClan->link}}">
                                        @if($match->hostClan->logo == 'no-clan-logo.jpg')
                                    <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="menu-avatar img-fluid klan-def-border" alt="Logo klanowe">
                                    @else 
                                    <img src="{{ Storage::url($match->hostClan->logo)}}" class="img-fluid klan-def-border" alt="Logo klanu">
                                    @endif
                                    
                                        </a>
                                    </div>
                                </div>

                                <div class="clanwar-teamname text-center">
                                <?php
                                
                                
                                ?>
                                    <h3 class="mb5"><a href="/klan/{{$match->hostClan->link}}" class="highlight">[{{$match->hostClan->tag}}] {{$match->hostClan->name}}</a></h3>
                                    <p class="mb5">{{$match->hostTeam->name}}</p>
                                    <p class="text-center color-info">{{date('d.m.Y',strtotime($match->match_date))}}<br>godz. {{date('H:i',strtotime($match->match_time))}}</p>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="clanwar-buttons-outer">
                                    <table style="width: 100%; height: 100%;">
                                        <tbody style="width: 100%; height: 100%;">
                                            <tr style="width: 100%; height: 100%;">
                                                <td class="align-middle" style="width: 100%; height: 100%;">
                                                    <p class="text-center"><a href="{{route('match.friendly.show',['id'=>$match->id])}}" class="btn btn-primary btn-sm">Otwórz stronę meczu</a></p>
                                                    <p class="text-center"><button class="btn btn-secondary btn-sm" type="button" data-toggle="collapse" data-target="#collapseID{{$match->id}}" aria-expanded="true" aria-controls="collapseID{{$match->id}}">Szczegóły</button></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="accordion mb-15" id="accordionID{{$match->id}}" style="width:100%">
                                <div class="card">
                                    <div id="collapseID{{$match->id}}" class="collapse" aria-labelledby="TitleID{{$match->id}}" data-parent="#wojny-klanowe">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                                    <p class="ustawienia-info">Tytuł gry</p>
                                                    <p class="wartosc-info">{{$match->game->name}}</p>
                                                </div>

                                                <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                                    <p class="ustawienia-info">Platforma</p>
                                                    <p class="wartosc-info">{{$match->platform->short_name}}</p>
                                                </div>

                                                <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                                    <p class="ustawienia-info">Data</p>
                                                    <p class="wartosc-info">{{date('d.m.Y',strtotime($match->match_date))}}</p>
                                                </div>

                                                <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                                    <p class="ustawienia-info">Godzina</p>
                                                    <p class="wartosc-info">{{date('H:i',strtotime($match->match_time))}}</p>
                                                </div>

                                                <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                                    <p class="ustawienia-info">Gracze</p>
                                                    <p class="wartosc-info">{{$match->players_count}} vs {{$match->players_count}}</p>
                                                </div>

                                                <div class="col-12 table-cell-border">
                                                    <p class="ustawienia-info">Dodatkowe informacje</p>
                                                    <p class="wartosc-info">{!!nl2br(e($match->description))!!}</p>
                                                </div>

                                                <div class="col-12 table-cell-border">
                                                    <p class="ustawienia-info">Skład drużyny</p>
                                                    <p class="wartosc-info">
                                                        @foreach($match->hostMembers() as $member)
                                                        <a href="/profil/{{$member->id}}" class="btn btn-secondary btn-sm m2 mb2">{{$member->login}}</a>
                                                        @endforeach
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="alert alert-bginfo mt15 mb0" style="overflow: hidden;">

                                                <p style="border-bottom: 1px solid rgba(255, 255, 255, 0.15); margin-bottom: 5px;">Osoby kontaktowe:</p>
                                                <div class="row">
                                                    <div class="col-12 text-center pr0 pt5">
                                                        <label class="form-label-custom mb-3" style="width: 100%">Kapitan drużyny</label>
                                                        <a href="/profil/{{$match->hostTeam->leader->id}}" class="btn btn-secondary btn-sm m2 mb2">{{$match->hostTeam->leader->login}}</a>
                                                        <label class="form-label-custom mb-3" style="width: 100%">Lider klanu</label>
                                                        <a href="/profil/{{$match->hostClan->leader->id}}" class="btn btn-secondary btn-sm m2 mb2">{{$match->hostClan->leader->login}}</a>
                                                    </div>
                                                </div>
                                                <p class="text-center m0 mt5" style="border-top: 1px solid rgba(255, 255, 255, 0.15);"><small>Jeżeli informacje dotyczące rozgrywki są wystarczające, zaakceptuj mecz. <span class="text-nowrap">W przeciwnym</span> razie zwróć się do gospodarza meczu o ich uzupełnienie.</small></p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach
                <!-- Ogłoszenie /koniec -->
                @endif
                
            </div>

        </div>
        @endif
    </div>

</div>

</div>

@endsection
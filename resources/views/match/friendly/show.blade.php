@extends('layouts.app')

@section('content')
@if (Session::has('showModal'))
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#wynikMeczuId1').modal('show');
        });
    </script>
@endif
<div id="wojny-klanowe" class="content">
                    <div class="container-fluid">





                        <div class="row mb30">
                            <div class="col-12">
                                <!-- tuta sprawdzamy czy jest liderem klanu i kapitanem gdy status jest otwarty -->
                                @if($match->isHost() && $match->status == 1)
                                <div class="alert alert-info">
                                    <p class="mb5"><i class="fas fa-info-circle"></i> Możesz edytować ustawienia meczu, lub go usunąć. Pojawienie się przeciwnika wyłączy możliwość moderacji meczu. Brak przeciwnika do ustalonej daty meczu spowoduje automatyczne usunięcie meczu.</p>
                                    <a href="{{route('match.friendly.edit',['id'=>$match->id])}}" class="btn btn-secondary btn-sm mr5 mb3">Edytuj mecz</a>
                                    <!-- usuwanie to czas do meczu odliczanie countdown -->
                                    <a href="#" class="btn btn-danger btn-sm mr5 mb3" data-toggle="modal" data-target="#usunMeczId1">Usuń <span id='countdown'></span></a>
                                    <?php
                                    
                                    $enddate = strtotime(date('Y-m-d H:i:s', strtotime("$match->match_date $match->match_time")));
                                    $todaydate = strtotime(date('Y-m-d H:i:s'));

                                    $remaining  = $enddate - $todaydate;
                                    ?>
                                    <script>
                                            var initialTime = <?php echo $remaining;?>

                                            var seconds = initialTime;
                                            function timer() {
                                                var days        = Math.floor(seconds/24/60/60);
                                                var hoursLeft   = Math.floor((seconds) - (days*86400));
                                                var hours       = Math.floor(hoursLeft/3600);
                                                var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
                                                var minutes     = Math.floor(minutesLeft/60);
                                                var remainingSeconds = seconds % 60;
                                                if (remainingSeconds < 10) {
                                                    remainingSeconds = "0" + remainingSeconds; 
                                                }
                                                document.getElementById('countdown').innerHTML = days + ":" + hours + ":" + minutes + ":" + remainingSeconds+ "";
                                                if (seconds == 0) {
                                                    clearInterval(countdownTimer);
                                                    window.location.replace("{{route('match.friendly.delete',['id'=>$match->id])}}")
                                                } else {
                                                    seconds--;
                                                }
                                            }
                                            var countdownTimer = setInterval('timer()', 1000);
                                    </script>
                                </div>
                                
                                <div class="modal fade" id="usunMeczId1" role="dialog" aria-labelledby="usunMeczId1Title" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Uwaga!</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body text-left">
                                                <p class="mb10">Czy na pewno chcesz usunąć mecz?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                                                <a href='{{route("match.friendly.delete",["id"=>$match->id])}}' class="btn btn-danger" > Usuń </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <!-- pojawia sie po przejscu z podsumowania lider klanu i kapitan za kazdym wejsciem na strone gdy mieli zgloszenie -->
                                @if($match->isAplicant())
                                    <!--Wyswietlic liste aplikacji do meczu per lider klanu i per lider drużyny-->
                                    @foreach($match->applications as $aplication)
                                        @if($aplication->clan->leader_id == Auth::user()->id)
                                        <div class="alert alert-info">
                                            <p class="mb5"><i class="fas fa-info-circle"></i> Prośba o dołączenie do meczu drużyny {{$aplication->team->name}} została wysłana i oczekuje na akceptację.</p>
                                            <a href="{{route('match.friendly.aplication.cancel',['id'=>$aplication->id,'match_id' => $match->id])}}" class="btn btn-secondary btn-sm mr5 mb3">Anuluj</a> <!-- usuwa aplikacje do meczu -->
                                        </div>
                                        @elseif($aplication->team->leader_id == Auth::user()->id) 
                                        <div class="alert alert-info">
                                            <p class="mb5"><i class="fas fa-info-circle"></i> Prośba o dołączenie do meczu drużyny {{$aplication->team->name}} została wysłana i oczekuje na akceptację.</p>
                                            <a href="{{route('match.friendly.aplication.cancel',['id'=>$aplication->id,'match_id' => $match->id])}}" class="btn btn-secondary btn-sm mr5 mb3">Anuluj</a> <!-- usuwa aplikacje do meczu -->
                                        </div>
                                        @endif
                                        
                                    @endforeach
                                    
                                   
                                @endif
                                <!-- aplikacje sprawdzenie czy zgloszenie istnieje za kazdym razem -->
                                @if($match->isHost())
                                @if(count($match->applications) > 0)
                                    @foreach($match->applications as $appliaction)
                                        <div class="alert alert-warning">
                                            <p class="mb5"><i class="fas fa-exclamation-circle"></i> Drużyna <small><strong><a href="">[{{$appliaction->clan->tag}}] {{$appliaction->team->name}}</a></strong></small> chce dołączyć do meczu.</p>
                                            <a href="{{route('match.friendly.accept',['id'=>$appliaction->id])}}" class="btn btn-success btn-sm mr5 mb3">Akceptuj</a>
                                            <a href="{{route('match.friendly.aplication.cancel',['id'=>$appliaction->id,'match_id'=>$match->id])}}" class="btn btn-danger btn-sm mr5 mb3">Odrzuć</a>
                                        </div>
                                    @endforeach
                                @endif
                                @endif
                                <!-- wysiwetla sie kapitaomi liderom kalnow przy statusie aktywny -->
                                @if(($match->isHost() || $match->isEnemy()) && $match->status == 2)
                                <div class="alert alert-info">
                                    <p class="mb0"><i class="fas fa-info-circle"></i> Po zakończeniu meczu należy przesłać jego wynik.</p>
                                </div>
                                @endif
                                @if($match->status > 1)
                                    <!-- zolty przicisk 12 godzin widozny gdy status jest zakonoczny i widoczny dla kapitanow i liderow obu druzyn  -->
                                
                                @if((($match->isHost() && $match->host_result_count > 0) || ($match->isEnemy() && $match->enemy_result_count > 0)) && $match->status > 1 && $match->result == 0)
                                    <!-- Nie wyswietla sie osoba ktore wysłały juz wynik -->
                                    @if(!$match->hasResult() )

                                    <form method='post' action="{{route('match.friendly.send.result')}}">
                                        {{ csrf_field() }}
                                        <input type='hidden' name="match_id" value='{{$match->id}}'>
                                        <div class="alert alert-warning">
                                            <p class="mb5"><i class="fas fa-exclamation-circle"></i> Wyślij wynik meczu. Brak wyniku spowoduje anulowanie meczu.</p>
                                            <a href="#" class="btn btn-primary btn-sm mr5 mb3" data-toggle="modal" data-target="#wynikMeczuId1">Wyślij wynik (<span id='resultCountdown'></span>)</a>
                                            <?php
                                            
                                            $enddate = strtotime(date('Y-m-d H:i:s', strtotime("$match->match_date $match->match_time +3 hours")));
                                            $todaydate = strtotime(date('Y-m-d H:i:s'));

                                            $remaining  = $enddate - $todaydate;
                                            ?>
                                            <script>
                                                    var initialTime = <?php echo $remaining;?>

                                                    var seconds = initialTime;
                                                    function timer() {
                                                        var days        = Math.floor(seconds/24/60/60);
                                                        var hoursLeft   = Math.floor((seconds) - (days*86400));
                                                        var hours       = Math.floor(hoursLeft/3600);
                                                        var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
                                                        var minutes     = Math.floor(minutesLeft/60);
                                                        var remainingSeconds = seconds % 60;
                                                        if (remainingSeconds < 10) {
                                                            remainingSeconds = "0" + remainingSeconds; 
                                                        }
                                                        document.getElementById('resultCountdown').innerHTML = days + ":" + hours + ":" + minutes + ":" + remainingSeconds+ "";
                                                        if (seconds == 0) {
                                                            clearInterval(countdownTimer);
                                                            window.location.replace("{{route('match.friendly.cancel',['id'=>$match->id])}}")
                                                        } else {
                                                            seconds--;
                                                        }
                                                    }
                                                    var countdownTimer = setInterval('timer()', 1000);
                                            </script>
                                        </div>  
                                        <div class="modal fade" id="wynikMeczuId1" role="dialog" aria-labelledby="wynikMeczuId1Title" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Wynik meczu</h5>
                                                    </div>
                                                    <div class="modal-body text-left">
                                                        <div class="row">
                                                            
                                                            <div class="col-12">
                                                                <div class="row">
                                                                    <div class="form-group col-12"><p class="text-muted">
                                                                            Pozostało prób:
                                                                            @if($match->isHost())
                                                                                {{$match->host_result_count}}
                                                                            @elseif($match->isEnemy())
                                                                                {{$match->enemy_result_count}}
                                                                            @endif</p>
                                                                        <label class="form-label-custom">Kto wygrał mecz?</label>
                                                                        
                                                                        <script>
                                                                            var members = $('#members');


                                                                            var clan_id = clan_id.select2();
                                                                            
                                                                        </script>
                                                                        <select class="form-control bgselect" style="width:100%" name='result' id='send-result'>
                                                                            <option value='1'>[{{$match->hostClan->tag}}] {{$match->hostClan->name}} ({{$match->hostTeam->name}})</option>
                                                                            <option value='2'>[{{$match->enemyClan->tag}}] {{$match->enemyClan->name}} ({{$match->enemyTeam->name}})</option>
                                                                            <option value='3'>REMIS</option>
                                                                            <option value='4'>MECZ ANULOWANY</option>
                                                                        </select>
                                                                        @if($errors->has('result'))
                                                                        <div class="error-feedback">
                                                                            Uzupełnij pole<br>
                                                                        </div>
                                                                        @endif
                                                                        <script>
                                                                                $("#send-result").select2({ dropdownCssClass : "results-send",minimumResultsForSearch: Infinity });
                                                                                $('#send-result').on('select2:open', function (e) {
                                                                                  
                                                                                    $('.results-send .select2-search input').prop('focus',false);
                                                                                });
                                                                        </script>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                                                        <button type="submit" class="btn btn-primary">Prześlij</button><!-- sprawdza czy wynik istnieje, sprawdzenie czy jest kapitanem badz liderem -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    
                                    @endif

                                @endif

                                @if((($match->isHost() && $match->host_result_count == 0) || ($match->isEnemy() && $match->enemy_result_count == 0)) && $match->status == 2) 
                                <div class="alert alert-info">
                                        <p class="mb0"><i class="fas fa-info-circle"></i> Próby przesłania wyniku zostały wykorzystane. Oczekuj na wynik od przeciwnika.</p>
                                    </div>
                                @endif
                                <!--
                                    jesli osoba wysylajaca wynik zglosi druzyne przeciwna to status meczu jest ustawione zwyciasca jest druzyna przeciwna do zgaszajacego
                                    wynik, potwierdzony nie potwierdzony, 
                                    wygrala osoba ktora uzupelnia wynik i status nie potweirzdzony

                                    wynik i status niepotwierzdzony to przeciwnik widzi komuniakt 

                                    czas na reakcje 12 godzin. 

                                -->
                                @if($match->result == 0)
                                    @if($match->isEnemy() && $match->enemySendResult() != false )
                                    <div class="alert alert-info">
                                        <p class="mb0"><i class="fas fa-info-circle"></i> Wynik został przesłany i oczekuje na akceptację przez przeciwnika. Brak reakcji przeciwnika w czasie <span id='resultEnemyCountdown'></span> spowoduje automatyczną akceptację wyniku.</p>
                                    </div> 
                                    @endif
                                @endif
                                @if($match->isEnemy())
                                    @if($match->hostSendResult() == 1)
                                    <div class="alert alert-warning">
                                        <p class="mb5"><i class="fas fa-exclamation-circle"></i> Przeciwnik przesłał wynik meczu, w którym oznaczył <strong>SIEBIE</strong> jako zwycięzcę. Brak reakcji spowoduje automatyczną akceptację.</p>
                                        <a href="{{route('match.friendly.accept.result',['match_id'=>$match->id])}}" class="btn btn-success btn-sm mr5 mb3">Akceptuj (<span id='resultEnemyCountdown'></span>)</a><a href="{{route('match.friendly.reject.result',['match_id'=>$match->id])}}" class="btn btn-danger btn-sm mr5 mb3">Odrzuć</a>
                                    </div>
                                    @endif

                                    @if($match->hostSendResult() == 3)
                                    <div class="alert alert-warning">
                                        <p class="mb5"><i class="fas fa-exclamation-circle"></i> Przeciwnik przesłał wynik meczu, w którym oznaczył wynik meczu jako <strong>REMIS</strong>. Jeżeli potwierdzasz tą informację, zaakceptuj wynik. Brak reakcji spowoduje automatyczną akceptację.</p>
                                        <a href="{{route('match.friendly.accept.result',['match_id'=>$match->id])}}" class="btn btn-success btn-sm mr5 mb3">Akceptuj (<span id='resultEnemyCountdown'></span>)</a><a href="{{route('match.friendly.reject.result',['match_id'=>$match->id])}}" class="btn btn-danger btn-sm mr5 mb3">Odrzuć</a>
                                    </div>
                                    @endif
                                    @if($match->hostSendResult() == 4)
                                    <div class="alert alert-warning">
                                        <p class="mb5"><i class="fas fa-exclamation-circle"></i> Przeciwnik przesłał wynik meczu, w którym oznaczył wynik meczu jako <strong>MECZ ANULOWANY</strong>. Jeżeli potwierdzasz tą informację, zaakceptuj wynik. Brak reakcji spowoduje automatyczną akceptację.</p>
                                        <a href="{{route('match.friendly.accept.result',['match_id'=>$match->id])}}" class="btn btn-success btn-sm mr5 mb3">Akceptuj (<span id='resultEnemyCountdown'></span>)</a><a href="{{route('match.friendly.reject.result',['match_id'=>$match->id])}}" class="btn btn-danger btn-sm mr5 mb3">Odrzuć</a>
                                    </div>
                                    @endif
                                @endif
                                <?php
                                if($match->hostSendResult() > 0 || $match->enemySendResult() > 0) {
                                    $expire = strtotime(date('Y-m-d H:i:s', strtotime($match->matchResult->expire_at)));
                                    $todaydate = strtotime(date('Y-m-d H:i:s'));
                                
                                $remaining  = $expire - $todaydate;
                                

                                
                                ?>
                                <script>
                                    var initialTime = <?php echo $remaining;?>

                                    var seconds = initialTime;
                                    function timer() {
                                        var days        = Math.floor(seconds/24/60/60);
                                        var hoursLeft   = Math.floor((seconds) - (days*86400));
                                        var hours       = Math.floor(hoursLeft/3600);
                                        var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
                                        var minutes     = Math.floor(minutesLeft/60);
                                        var remainingSeconds = seconds % 60;
                                        if (remainingSeconds < 10) {
                                            remainingSeconds = "0" + remainingSeconds; 
                                        }
                                        document.getElementById('resultEnemyCountdown').innerHTML = days + ":" + hours + ":" + minutes + ":" + remainingSeconds+ "";
                                        if (seconds == 0) {
                                            clearInterval(countdownTimer);
                                            // przekierowanie na akceptacje obecnego wyniku
                                            window.location.replace("{{route('match.friendly.accept.result',['match_id'=>$match->id])}}")
                                        } else {
                                            seconds--;
                                        }
                                    }
                                    var countdownTimer = setInterval('timer()', 1000);
                            </script>
                            <?php
                        } else {
                            $remaining = 0;
                        } 
                            ?>

                                @if($match->isHost() && $match->hostSendResult() != false)
                                <div class="alert alert-info">
                                    <p class="mb0"><i class="fas fa-info-circle"></i> Wynik został przesłany i oczekuje na akceptację przez przeciwnika. Brak reakcji przeciwnika w czasie <span id='resultEnemyCountdown'></span> spowoduje automatyczną akceptację wyniku.</p>
                                </div> 
                                @endif
                                
                                @if($match->isHost())
                                    @if($match->enemySendResult() == 2)
                                    <div class="alert alert-warning">
                                        <p class="mb5"><i class="fas fa-exclamation-circle"></i> Przeciwnik przesłał wynik meczu, w którym oznaczył <strong>SIEBIE</strong> jako zwycięzcę. Brak reakcji spowoduje automatyczną akceptację.</p>
                                        <a href="{{route('match.friendly.accept.result',['match_id'=>$match->id])}}" class="btn btn-success btn-sm mr5 mb3">Akceptuj (<span id='resultEnemyCountdown'></span>)</a><a href="{{route('match.friendly.reject.result',['match_id'=>$match->id])}}" class="btn btn-danger btn-sm mr5 mb3">Odrzuć</a>
                                    </div>
                                    @endif

                                    @if($match->enemySendResult() == 3)
                                    <div class="alert alert-warning">
                                        <p class="mb5"><i class="fas fa-exclamation-circle"></i> Przeciwnik przesłał wynik meczu, w którym oznaczył wynik meczu jako <strong>REMIS</strong>. Jeżeli potwierdzasz tą informację, zaakceptuj wynik. Brak reakcji spowoduje automatyczną akceptację.</p>
                                        <a href="{{route('match.friendly.accept.result',['match_id'=>$match->id])}}" class="btn btn-success btn-sm mr5 mb3">Akceptuj (<span id='resultEnemyCountdown'></span>)</a><a href="{{route('match.friendly.reject.result',['match_id'=>$match->id])}}" class="btn btn-danger btn-sm mr5 mb3">Odrzuć</a>
                                    </div>
                                    @endif
                                    @if($match->enemySendResult() == 4)
                                    <div class="alert alert-warning">
                                        <p class="mb5"><i class="fas fa-exclamation-circle"></i> Przeciwnik przesłał wynik meczu, w którym oznaczył wynik meczu jako <strong>MECZ ANULOWANY</strong>. Jeżeli potwierdzasz tą informację, zaakceptuj wynik. Brak reakcji spowoduje automatyczną akceptację.</p>
                                        <a href="{{route('match.friendly.accept.result',['match_id'=>$match->id])}}" class="btn btn-success btn-sm mr5 mb3">Akceptuj (<span id='resultEnemyCountdown'></span>)</a><a href="{{route('match.friendly.reject.result',['match_id'=>$match->id])}}" class="btn btn-danger btn-sm mr5 mb3">Odrzuć</a>
                                    </div>
                                    @endif
                                    <?php
                                if($match->hostSendResult() > 0 || $match->enemySendResult() > 0) {
                                    $expire = strtotime(date('Y-m-d H:i:s', strtotime($match->matchResult->expire_at)));
                                    $todaydate = strtotime(date('Y-m-d H:i:s'));
                                
                                $remaining  = $expire - $todaydate;
                                

                                
                                ?>
                                <script>
                                    var initialTime = <?php echo $remaining;?>

                                    var seconds = initialTime;
                                    function timer() {
                                        var days        = Math.floor(seconds/24/60/60);
                                        var hoursLeft   = Math.floor((seconds) - (days*86400));
                                        var hours       = Math.floor(hoursLeft/3600);
                                        var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
                                        var minutes     = Math.floor(minutesLeft/60);
                                        var remainingSeconds = seconds % 60;
                                        if (remainingSeconds < 10) {
                                            remainingSeconds = "0" + remainingSeconds; 
                                        }
                                        document.getElementById('resultEnemyCountdown').innerHTML = days + ":" + hours + ":" + minutes + ":" + remainingSeconds+ "";
                                        if (seconds == 0) {
                                            clearInterval(countdownTimer);
                                            // przekierowanie na akceptacje obecnego wyniku
                                            window.location.replace("{{route('match.friendly.accept.result',['match_id'=>$match->id])}}")
                                        } else {
                                            seconds--;
                                        }
                                    }
                                    var countdownTimer = setInterval('timer()', 1000);
                                    </script>
                                    <?php
                                } else {
                                    $remaining = 0;
                                } 
                                    ?>
                                @endif
                                @endif
                            </div>
                        </div>





                        <div class="row mb30">
                            <div class="col-12">
                                <h1 class="text-center mb0">MECZ TOWARZYSKI</h1>
                                <p class="text-muted text-center mb0">#{{$match->id}}</p>
                                <p class="text-center">Status: <span class="color-info">
                                @if($match->status == 1)
                                Otwarty 
                                @elseif($match->status == 2) Aktywny 
                                @elseif($match->status == 3) Zakończony
                                @endif
                                <!-- status zakonczony jest gdy minie termin meczu trzeba napisac crona-->
                                </span></p>
                            </div>
                        </div>






                        <div class="row ml0 mr0 mb30">
                            <div class="col-12 pl0">
                                <h4 class="mb15">Szczegóły rozgrywki</h4>
                            </div>

                            <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                <p class="ustawienia-info">Data</p>
                                <p class="wartosc-info color-info" style="color:#17a2b8 !important">{{date('d.m.Y',strtotime($match->match_date))}}</p>
                            </div>

                            <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                <p class="ustawienia-info">Godzina</p>
                                <p class="wartosc-info" style="color:#17a2b8 !important">{{date('H:i',strtotime($match->match_time))}}</p>
                            </div>

                            <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                <p class="ustawienia-info">Tytuł gry</p>
                                <p class="wartosc-info">{{$match->game->name}}</p>
                            </div>

                            <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                <p class="ustawienia-info">Platforma</p>
                                <p class="wartosc-info">{{$match->platform->short_name}}</p>
                            </div>

                            <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                <p class="ustawienia-info">Gracze</p>
                                <p class="wartosc-info">{{$match->players_count}} vs {{$match->players_count}}</p>
                            </div>

                            <div class="col-12 table-cell-border">
                                <p class="ustawienia-info">Dodatkowe ustawienia rozgrywki</p>
                                <p class="wartosc-info">{!!nl2br(e($match->description))!!}</p>
                            </div>
                        </div>




                        @if($match->status == 1)
                        <div class="row mb30">
                            <div class="col-12 col-xl-5">
                                <div class="row team-home">
                                    <div class="col-12">
                                        <p class="home-away-header">Gospodarz</p>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div style="width:100%; height: 100%">
                                                <div class="avatar-wrapper mb15">
                                                <a href="/klan/{{$match->hostClan->link}}">
                                                    @if($match->hostClan->logo == 'no-clan-logo.jpg')
                                                <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="menu-avatar img-fluid klan-def-border" alt="Logo klanowe">
                                                @else 
                                                <img src="{{ Storage::url($match->hostClan->logo)}}" class="img-fluid klan-def-border" alt="Logo klanu">
                                                @endif
                                                </a>
                                                </div>
                                                <div class="home-away-detiles">
                                                    <table class="home-away-detiles-table">
                                                        <tbody style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td class="align-middle" style="width: 100%">
                                                                    <h4 class="mb5">
                                                                        <a href="/klan/{{$match->hostClan->link}}" class="highlight">[{{$match->hostClan->tag}}] {{$match->hostClan->name}}</a>
                                                                    </h4>
                                                                    <p class="mb0">{{$match->hostTeam->name}}</p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>              
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="table-cell-border">
                                            <p class="ustawienia-info">Skład drużyny</p>
                                            <p class="wartosc-info">
                                                @foreach($match->hostMembers() as $member)
                                                    <a href="/profil/{{$member->id}}" class="btn btn-secondary btn-sm m2 mb2">{{$member->login}}</a>
                                                @endforeach                                                
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-xl-2 text-center">
                                <table class="home-away-detiles-versus">
                                    <tbody style="width: 100%">
                                        <tr style="width: 100%">
                                            <td class="align-middle" style="width: 100%">
                                                <p class="mt20 mb20">VS.</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>  
                            </div>

                            <div class="col-12 col-xl-5">
                                <div class="row team-away">
                                    <div class="col-12">
                                        <p class="home-away-header">Gość</p>
                                    </div> 
                                    <!-- nie spełniajacy warunku -->

                                    <div class="col-12">
                                        <div class="row">
                                            @if($match->isAplicant())
                                            <div class="col-12">
                                            <div class="alert alert-info">
                                                    <p class="mb5"><i class="fas fa-info-circle"></i> Prośba o dołączenie do meczu została wysłana i oczekuje na akceptację.</p>
                                                    <a href="{{route('match.friendly.aplication.cancel',['id'=>$match->getAplicationId(),'match_id'=>$match->id])}}" class="btn btn-secondary btn-sm mr5 mb3">Anuluj</a> <!-- usuwa aplikacje do meczu -->
                                                </div>
                                            </div>
                                            @elseif($match->isHost())
                                            <div class="col-12">
                                            <div class="alert alert-info">
                                                    <p class="mb5"><i class="fas fa-info-circle"></i> Oczekiwanie na przeciwnika</p>
                                                </div>
                                            </div>
                                            @elseif($match->isGuest())
                                            <div style="width:100%; height: 100%">
                                                    <div class="avatar-wrapper mb15">
                                                        <!-- <a href="#">
                                                            <img src="assets/images/clanlogo/no-clan-logo.jpg" class="img-fluid klan-def-border" alt="Logo klanu">
                                                        </a> -->
                                                        <span class="team-unknown">?</span>
                                                    </div>
                                                    <div class="home-away-detiles">
                                                        <div class="col-12">
                                                            <form class="text-left" style="width: 100%;" method='post' action='{{route("match.friendly.join")}}'>
                                                            {{ csrf_field() }}
                                                            <input type='hidden' name='match_id' value="{{$match->id}}">
                                                                <div class="row ml-30 mr-30">
                                                                    <div class="form-group col-12 mb0">
                                                                        <label class="form-label-custom">Klan *</label>
                                                                        <select class="form-control bgselect" style="width:100%" name='clan_id' id='clan_id'>
                                                                            

                                                                        <option></option>
                                                                        @foreach($clans as $clan)
                                                                        
                                                                                    <option value='{{$clan->id}}' @if($clan->id == old('clan_id')) selected @endif>{{$clan->name}}</option>
                                                                                
                                                                        @endforeach
                                                                    
                                                                            
                                                                              
                                                                        </select>
                                                                        @if ($errors->has('clan_id'))
                                                                            <div class="error-feedback">
                                                                                {!! $errors->first('clan_id') !!}<br>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                    <script>
                                                                            $(document).ready(function () {
                                                                                var clan_id = $('#clan_id');
                                                                                var team_id = $('#team_id');
                                                                                var members = $('#members');
                                
                                
                                                                                var clan_id = clan_id.select2({
                                                                                    placeholder: "",
                                                                                });
                                                                                var team_id = team_id.select2({
                                                                                    placeholder: "",
                                                                                });
                                                                                var members = members.select2({
                                                                                    placeholder: "",
                                                                                    tags: true
                                                                                });
                                
                                                                                $('select').on('select2:open', function (e) {
                                                                                    $('.select2-search input').prop('focus',false);
                                                                                });
                                
                                                                                clan_id.on('change',function(){
                                                                                    var value= $('#clan_id option:selected').attr('value');
                                                                                    console.log(value);
                                                                                    if(value == 0) {
                                                                                        $('#team_id').empty();
                                                                                    }
                                
                                                                                    var url = "/helper/teams/"+value;
                                
                                                                                    $.ajax({
                                                                                        type: "GET",
                                                                                        url: url,
                                                                                        success: function(data) {
                                                                                            $('#team_id').html('<option></option>');
                                                                                            $('#members').html('');
                                                                                            team_id.select2({data:data,placeholder: "",});
                                                                                        }
                                                                                    });
                                
                                                                                });
                                
                                                                                team_id.on('change',function() {
                                                                                    var value= $('#team_id option:selected').attr('value');
                                                                                    console.log(value);
                                                                                    if(value == 0) {
                                                                                        $('#members').empty();
                                                                                    }
                                
                                                                                    var url = "/helper/members/"+value;
                                
                                                                                    $.ajax({
                                                                                        type: "GET",
                                                                                        url: url,
                                                                                        success: function(data) {
                                                                                            $('#members').html('');
                                                                                            members.select2({data:data,placeholder: "",});
                                                                                        }
                                                                                    });
                                                                                });
                                                                                
                                
                                                                            });
                                                                                
                                                                              
                                                                
                                                                            </script>
                                                                    <div class="form-group col-12 mb0">
                                                                        <label class="form-label-custom text-left">Drużyna *</label>
                                                                        @if(session()->has('teams'))
                                                <?php $teams = session('teams');
                                                    $team_id = old('team_id');
                                                ?>
                                            @endif
                                                                        <select class="form-control bgselect" style="width:100%" name='team_id' id='team_id'>
                                                                            <option></option>
                                                                                @if(session()->has('teams'))
                                                                                @foreach($teams as $team)
                                
                                                                                <option value='{{$team["id"]}}' @if($team['id'] == $team_id) selected @endif>{{$team['text']}}</option>
                                                                                @endforeach
                                                                                @endif
                                                                            
                                                                        </select>
                                                                        @if ($errors->has('team_id'))
                                                                            <div class="error-feedback">
                                                                                {!! $errors->first('team_id') !!}<br>
                                                                            </div>
                                                                        @endif
                                                                    </div>
                                                                    <div class="form-group col-12 mb0">
                                                                        <label class="form-label-custom text-left">Skład drużyny *</label>
                                                                        @if(session()->has('members'))
                                                <?php $members = session('members');
                                                
                                                ?>
                                            @endif
                                                                        <select class="bgselect" style="width:100%" name="members[]" multiple="multiple" id='members'>
                                                                                @if(session()->has('members'))
                                                                                @foreach($members as $member) 
                                                                                <option value='{{$member["id"]}}' @if(old('members') != NULL)
                                                                                                                     @if(in_array($member['id'],old('members'))) 
                                                                                                                        selected 
                                                                                                                    @endif 
                                                                                                                @endif>{{$member['text']}}</option>
                                                                                @endforeach
                                                                                @endif
                                                                            
                                                                        </select>
                                                                        @if ($errors->has('members'))
                                                                            <div class="error-feedback">
                                                                                {!! $errors->first('members') !!}<br>
                                                                            </div>
                                                                        @endif
                                                                        
                                                                    </div>
                                                                    <div class="col-12">
                                                                        <div class="text-right">
                                                                            <button type="submit" class="btn btn-primary mt15">Dołącz do meczu</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>        
                                                    </div>
                                                </div>
                                            @else 
                                            
                                            <div class="col-12">
                                                    <div class="alert alert-info">
                                                            <p class="mb5"><i class="fas fa-info-circle"></i> Oczekiwanie na przeciwnika</p>

                                                        <ul class="text-muted mb0">
                                                            
                                                            <li><small>Klanówki mogą organizować tylko klany posiadające utworzoną drużynę.</small></li>
                                                            <li><small>Do drużyny musi być przypisana platforma i tytuł gry, której klanówka dotyczy.</small></li>
                                                            <li><small>Musisz być Liderem klanu lub Kapitanem drużyny.</small></li>
                                                        
                                                        </ul>
                                                    </div>
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @else
                        
                        
                        <div class="row mb30">
                            <div class="col-12 col-lg-5">
                                <div class="row team-home">
                                    <div class="col-12">
                                        <p class="home-away-header">Gospodarz</p>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div style="width:100%; height: 100%">
                                                <div class="avatar-wrapper mb15">
                                                    <a href="/klan/{{$match->hostClan->link}}">
                                                    @if($match->hostClan->logo == 'no-clan-logo.jpg')
                                                        <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="menu-avatar img-fluid klan-def-border" alt="Logo klanowe">
                                                    @else 
                                                        <img src="{{ Storage::url($match->hostClan->logo)}}" class="img-fluid klan-def-border" alt="Logo klanu">
                                                    @endif
                                                    </a>
                                                </div>
                                                <div class="home-away-detiles">
                                                    <table class="home-away-detiles-table">
                                                        <tbody style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td class="align-middle" style="width: 100%">
                                                                        <h4 class="mb5">
                                                                                <a href="/klan/{{$match->hostClan->link}}" class="highlight">[{{$match->hostClan->tag}}] {{$match->hostClan->name}}</a>
                                                                            </h4>
                                                                            <p class="mb0">{{$match->hostTeam->name}}</p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>              
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="table-cell-border">
                                            <p class="ustawienia-info">Skład drużyny</p>
                                            <p class="wartosc-info">
                                                 @foreach($match->hostMembers() as $member)
                                                    <a href="/profil/{{$member->id}}" class="btn btn-secondary btn-sm m2 mb2">{{$member->login}}</a>
                                                @endforeach 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-lg-2 text-center">
                                <table class="home-away-detiles-versus">
                                    <tbody style="width: 100%">
                                        <tr style="width: 100%">
                                            <td class="align-middle" style="width: 100%">
                                                <p class="mt20 mb20">VS.</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>  
                            </div>

                            <div class="col-12 col-lg-5">
                                <div class="row team-away">
                                    <div class="col-12">
                                        <p class="home-away-header">Gość</p>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div style="width:100%; height: 100%">
                                                <div class="avatar-wrapper mb15">
                                                        <a href="/klan/{{$match->enemyClan->link}}">
                                                            @if($match->enemyClan->logo == 'no-clan-logo.jpg')
                                                                <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="menu-avatar img-fluid klan-def-border" alt="Logo klanowe">
                                                            @else 
                                                                <img src="{{ Storage::url($match->enemyClan->logo)}}" class="img-fluid klan-def-border" alt="Logo klanu">
                                                            @endif
                                                            </a>
                                                </div>
                                                <div class="home-away-detiles">
                                                    <table class="home-away-detiles-table">
                                                        <tbody style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td class="align-middle" style="width: 100%">
                                                                        <h4 class="mb5">
                                                                                <a href="/klan/{{$match->enemyClan->link}}" class="highlight">[{{$match->enemyClan->tag}}] {{$match->enemyClan->name}}</a>
                                                                            </h4>
                                                                            <p class="mb0">{{$match->enemyTeam->name}}</p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>              
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="table-cell-border">
                                            <p class="ustawienia-info">Skład drużyny</p>
                                            <p class="wartosc-info">
                                                @foreach($match->enemyMembers() as $member)
                                                    <a href="/profil/{{$member->id}}" class="btn btn-secondary btn-sm m2 mb2">{{$member->login}}</a>
                                                @endforeach 
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        @endif
                        @if($match->status == 2)
                        <div class="row mb30">
                            <div class="col-12">

                                <hr class="separator mt-15 mb50">
                                @if($match->result == 0)
                                <!-- Oczekiwanie na wynik /start kiedy staus jest zamkniety i brak potwierdzonego wyniku -->
                                <h2 class="text-center"><strong>WYNIK MECZU</strong></h2>
                                <p class="text-center text-muted mb0">Oczekiwanie na wynik...</p>
                                <!-- Oczekiwanie na wynik /koniec -->
                                @endif
                            </div>
                        </div>
                        @endif
                        @if($match->status == 3)
                        <div class="row mb30">
                            <div class="col-12">

                                <hr class="separator mt-15 mb50">
                                @if($match->result == 0)
                                <!-- Oczekiwanie na wynik /start kiedy staus jest zamkniety i brak potwierdzonego wyniku -->
                                <h2 class="text-center"><strong>WYNIK MECZU</strong></h2>
                                <p class="text-center text-muted mb0">Oczekiwanie na wynik...</p>
                                <!-- Oczekiwanie na wynik /koniec -->
                                @endif
                                <!-- Wygrana /start w momencie potwierdzenia wyniku i zalezy od wyniku -->
                                @if($match->result == 1)
                                <h2 class="text-center"><strong>MECZ WYGRYWA</strong></h2>
                                @if($match->winner_id == $match->host_clan)
                                <div class="match-cup-winner-outer">
                                    <div class="avatar-wrapper">
                                        <a href="/klan/{{$match->hostClan->link}}">
                                            @if($match->hostClan->logo == 'no-clan-logo.jpg')
                                                <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="menu-avatar img-fluid klan-def-border" alt="Logo klanowe">
                                            @else 
                                                <img src="{{ Storage::url($match->hostClan->logo)}}" class="img-fluid klan-def-border" alt="Logo klanu">
                                            @endif
                                            </a>
                                    </div>
                                    <div class="match-cup-winner">
                                        <p class="text-center mb0"><a href="/klan/{{$match->hostClan->link}}" class="highlight">{{$match->hostClan->name}}</a></p>
                                        <br>
                                        <p class="text-center mb0">{{$match->hostTeam->name}}</p>
                                    </div>
                                </div>
                                @else 
                                <div class="match-cup-winner-outer">
                                    <div class="avatar-wrapper">
                                        <a href="/klan/{{$match->enemyClan->link}}">
                                            @if($match->enemyClan->logo == 'no-clan-logo.jpg')
                                                <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="menu-avatar img-fluid klan-def-border" alt="Logo klanowe">
                                            @else 
                                                <img src="{{ Storage::url($match->enemyClan->logo)}}" class="img-fluid klan-def-border" alt="Logo klanu">
                                            @endif
                                            </a>
                                    </div>
                                    <div class="match-cup-winner">
                                        <p class="text-center mb0"><a href="/klan/{{$match->enemyClan->link}}" class="highlight">{{$match->enemyClan->name}}</a></p>
                                        <br>
                                        <p class="text-center mb0">{{$match->enemyTeam->name}}</p>
                                    </div>
                                </div>
                                @endif
                                @endif
                                <!-- Wygrana /koniec -->
                                @if($match->result == 3)
                                <!-- Remis /start -->
                                <h2 class="text-center"><strong>WYNIK MECZU</strong></h2>
                                <div class="match-cup-draw-outer">
                                    <div class="match-cup-winner">
                                        <p class="text-center mb0">REMIS</p>
                                    </div>
                                </div>
                                @endif
                                <!-- Remis /koniec -->
                                @if($match->result == 4)
                                <!-- Anulowany /start -->
                                <h2 class="text-center"><strong>WYNIK MECZU</strong></h2>
                                <div class="match-cup-cancel-outer">
                                    <div class="match-cup-winner">
                                        <p class="text-center mb0">MECZ ANULOWANY</p>
                                    </div>
                                </div>
                                <!-- Anulowany /koniec -->
                                @endif
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
@endsection
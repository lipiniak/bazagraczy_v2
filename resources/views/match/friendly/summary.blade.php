@extends('layouts.app')

@section('content')
<div id="wojny-klanowe" class="content">
                    <div class="container-fluid">

                        

                        <div class="row mb30">
                            <div class="col-12">
                                <h1 class="text-center mb0">MECZ TOWARZYSKI</h1>
                                <p class="text-muted text-center mb0">#{{$match->id}}</p>
                                <p class="text-center">PODSUMOWANIE (<span id='countdown'></span>)</p>
                            </div>
                        </div>



                        <div class="row ml0 mr0 mb30">
                            <div class="col-12 pl0">
                                <h4 class="mb15">Szczegóły rozgrywki</h4>
                            </div>

                            <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                <p class="ustawienia-info">Data</p>
                                <p class="wartosc-info color-info" style="color:#17a2b8 !important">{{date('d.m.Y',strtotime($match->match_date))}}</p>
                            </div>

                            <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                <p class="ustawienia-info">Godzina</p>
                                <p class="wartosc-info" style="color:#17a2b8 !important">{{date('H:i',strtotime($match->match_time))}}</p>
                            </div>

                            <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                <p class="ustawienia-info">Tytuł gry</p>
                                <p class="wartosc-info">{{$match->game->name}}</p>
                            </div>

                            <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                <p class="ustawienia-info">Platforma</p>
                                <p class="wartosc-info">{{$match->platform->short_name}}</p>
                            </div>

                            <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                <p class="ustawienia-info">Gracze</p>
                                <p class="wartosc-info">{{$match->players_count}} vs {{$match->players_count}}</p>
                            </div>

                            <div class="col-12 table-cell-border">
                                <p class="ustawienia-info">Dodatkowe ustawienia rozgrywki</p>
                                <p class="wartosc-info">{!!nl2br(e($match->description))!!}</p>
                            </div>
                        </div>



                        <div class="row ml0 mr0 mb30">
                            <div class="col-12 pl0">
                                <h4 class="mb15">Gospodarz</h4>
                            </div>

                            <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                <p class="ustawienia-info">Klan</p>
                                <p class="wartosc-info">[{{$match->hostClan->tag}}] {{$match->hostClan->name}}</p>
                            </div>

                            <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                <p class="ustawienia-info">Drużyna</p>
                                <p class="wartosc-info">{{$match->hostTeam->name}}</p>
                            </div>

                            <div class="col-12 col-md-12 col-xl-4 table-cell-border">
                                <p class="ustawienia-info">Zawodnicy</p>
                                <p class="wartosc-info">
                                    <?php
                                    $m = array();
                                    foreach($match->hostMembers() as $member)
                                        $m[] = $member->login;
                                    
                                    echo implode(', ',$m);
                                        ?>

                               
                                </p>
                            </div>
                        </div>



                        <div class="row ml0 mr0 mb30">
                            <div class="col-12 pl0">
                                <h4 class="mb15">Gość</h4>
                            </div>

                            <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                <p class="ustawienia-info">Klan</p>
                                <p class="wartosc-info">[{{$enemy['clan']->tag}}] {{$enemy['clan']->name}}</p>
                            </div>

                            <div class="col-12 col-md-6 col-xl-4 table-cell-border">
                                <p class="ustawienia-info">Drużyna</p>
                                <p class="wartosc-info">{{$enemy['team']->name}}</p>
                            </div>

                            <div class="col-12 col-md-12 col-xl-4 table-cell-border">
                                <p class="ustawienia-info">Zawodnicy</p>
                                <p class="wartosc-info">
                                        <?php
                                        $m = array();
                                        foreach($enemy['members'] as $member)
                                            $m[] = $member->login;
                                        
                                        echo implode(', ',$m);
                                            ?>
    
                                </p>
                            </div>
                        </div>

                        <form action="{{route('match.friendly.apply')}}" method="post" id='apply-form'>
                            {{ csrf_field() }}
                            <input type='hidden' name='match_id' value='{{$match->id}}'>
                            <input type='hidden' name='clan_id' value='{{$enemy["clan"]->id}}'>
                            <input type='hidden' name='team_id' value='{{$enemy["team"]->id}}'>
                            <input type='hidden' name='members' value='{{$members}}'>
                        </form>   
                        <div class="row">
                            <div class="col-12 text-right">
                                <a href="{{route('match.friendly.show',['id'=>$match->id])}}" class="btn btn-secondary ml3 mb3">Anuluj <span id='countdown1'></span></a>
                                <?php
                                    
                                    $enddate = strtotime(date('Y-m-d H:i:s', strtotime("+1 minutes 59 seconds")));
                                    $todaydate = strtotime(date('Y-m-d H:i:s'));

                                    $remaining  = $enddate - $todaydate;
                                    ?>
                                    <script>
                                            var initialTime = <?php echo $remaining;?>

                                            var seconds = initialTime;
                                            function timer() {
                                                var days        = Math.floor(seconds/24/60/60);
                                                var hoursLeft   = Math.floor((seconds) - (days*86400));
                                                var hours       = Math.floor(hoursLeft/3600);
                                                var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
                                                var minutes     = Math.floor(minutesLeft/60);
                                                var remainingSeconds = seconds % 60;
                                                if (remainingSeconds < 10) {
                                                    remainingSeconds = "0" + remainingSeconds; 
                                                }
                                                document.getElementById('countdown').innerHTML = minutes + ":" + remainingSeconds+ "";
                                                document.getElementById('countdown1').innerHTML = minutes + ":" + remainingSeconds+ "";
                                                if (seconds == 0) {
                                                    clearInterval(countdownTimer);
                                                    window.location.replace("{{route('match.friendly.show',['id'=>$match->id])}}")
                                                } else {
                                                    seconds--;
                                                }

                                            }
                                            var countdownTimer = setInterval('timer()', 1000);
                                    </script>
                                <a href="#" class="btn btn-primary ml3 mb3" onclick="event.preventDefault();
                                document.getElementById('apply-form').submit();">Akceptuj mecz</a>
                            </div>
                        </div>



                    </div>
                </div>
@endsection
@extends('layouts.app')

@section('content')


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.42/css/bootstrap-datetimepicker.css" />
        
<div id="wojny-klanowe" class="content">

                    <div class="container-fluid">

                        <div class="row">

                            <div class="col-12">
                                <h1 class="mb5">Stwórz klanówkę</h1>
                                <p class="text-muted">{{$selectedGame['game']}} <span class="badge badge-muted">{{$selectedGame['platform']}}</span><p>
                            </div>
                            @if($noPermission)
                            <div class="col-12">
                                <div class="alert alert-info">
                                    <h4>Nie posiadasz wystarczających uprawnień!</h4>
                                    <ul class="mb0">
                                        <li>Klanówki mogą organizować tylko klany posiadające utworzoną drużynę.</li>
                                        <li>Do drużyny musi być przypisana platforma i tytuł gry, której klanówka dotyczy.</li>
                                        <li>Musisz być Liderem klanu lub Kapitanem drużyny.</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-12 alert alert-warning">
                                <div class="row">
                                    <div class="col-12 mb15">
                                        Próbujesz utworzyć klanówkę dla {{$selectedGame['game']}} {{$selectedGame['platform']}}
                                    </div>
                                    <div class="col-12">
                                        <small><strong>GRY DLA KTRÓRYCH MOŻESZ STWORZYĆ KLANÓWKĘ:</strong></small>
                                    </div>
                                    <?php $games = (Auth::user()->UserMatchCreateGames());?>
                                    @if(empty($games))
                                    <div class="col-12">
                                        Brak <a href="{{route('clan.create')}}" class="highlight"><i class="fas fa-plus-circle"></i> Stwórz klan</a>
                                    </div>
                                    @else 
                                    <div class="col-12">
                                        @foreach($games as $game)
                                            {{$game['game']}} {{$game['platform']}} <a href="#" class="highlight"><i class="fas fa-chevron-circle-right"></i> Przełącz</a><br>
                                        @endforeach
                                        
                                    </div>
                                    @endif
                                </div>
                            </div>
                            @else
                            @if($noEdit)
                            <div class="col-12">
                                <div class="alert alert-warning">
                                    <h4>Edycja tymczasowo niedostępna!</h4>
                                    <p class="mb0">Edycja ustawień meczu może być niedostępna ponieważ:</p>
                                    <ul class="mb5">
                                        <li>W danym meczu istnieją aktywne prośby innych klanów o dołączenie do rozgrywki. Przed edycją danych należy wszystkie te prośby odrzucić.</li>
                                        <li>W aktualnej chwili jeden z klanów jest w trakcie składania prośby o dołączenie do meczu (przegląda podsumowanie).</li>
                                    </ul>
                                    <a href="#" class="btn btn-secondary btn-sm mr5 mb3">Wróć do meczu</a>
                                </div>
                            </div>
                            
                             <div class="col-12">
                                <div class="alert alert-warning">
                                    <h4>Uwaga!</h4>
                                    <p class="mb0">Aktywacja edycji ustawień meczowych uniemożliwia klanom dołączenie do meczu, dlatego edycja jest ograniczona czasowo. Wszelkie zmiany powinny być zapisane przed automatycznym zamknięciem strony edycji.<br><strong>Pozostało mm:ss</strong></p>
                                </div>
                            </div>
                            @endif
                            <div class="col-12">
                                <form style="width: 100%;" method="POST" action="{{route('match.friendly.save')}}">
                                {{ csrf_field() }}
                                <input type="text" autofocus="autofocus" style="display:none" />
                                <input type="hidden" name="game_id" value='{{$selectedGame["game_id"]}}'>
                                <input type="hidden" name="platform_id" value='{{$selectedGame["platform_id"]}}'>
                                    <div class="row">
                                        <div class="form-group col-12 col-lg-6">
                                                
                                            <label class="form-label-custom">Klan *</label>
                                            <select class="form-control bgselect" style="width:100%" name='clan_id' id='clan_id' >
                                                    <option></option>
                                                @foreach($clans as $clan)
                                                    
                                                            <option value='{{$clan->id}}' @if($clan->id == old('clan_id')) selected @endif>{{$clan->name}}</option>
                                                    
                                                @endforeach
                                               
                                            </select>
                                            <script>
                                            $(document).ready(function () {
                                                var clan_id = $('#clan_id');
                                                var team_id = $('#team_id');
                                                var members = $('#members');


                                                var clan_id = clan_id.select2({
                                                    placeholder: "",
                                                });
                                                var team_id = team_id.select2({
                                                    placeholder: "",
                                                });
                                                var members = members.select2({
                                                    placeholder: "",
                                                    tags: true
                                                });

                                                $('select').on('select2:open', function (e) {
                                                    $('.select2-search input').prop('focus',false);
                                                });

                                                clan_id.on('change',function(){
                                                    var value= $('#clan_id option:selected').attr('value');
                                                    console.log(value);
                                                    if(value == 0) {
                                                        $('#team_id').empty();
                                                    }

                                                    var url = "/helper/teams/"+value;

                                                    $.ajax({
                                                        type: "GET",
                                                        url: url,
                                                        success: function(data) {
                                                            $('#team_id').html('<option></option>');
                                                            $('#members').html('');
                                                            team_id.select2({data:data,placeholder: "",});
                                                        }
                                                    });

                                                });

                                                team_id.on('change',function() {
                                                    var value= $('#team_id option:selected').attr('value');
                                                    console.log(value);
                                                    if(value == 0) {
                                                        $('#members').empty();
                                                    }

                                                    var url = "/helper/members/"+value;

                                                    $.ajax({
                                                        type: "GET",
                                                        url: url,
                                                        success: function(data) {
                                                            $('#members').html('');
                                                            members.select2({data:data,placeholder: "",});
                                                        }
                                                    });
                                                });
                                                

                                            });
                                                
                                              
                                
                                            </script>
                                            @if ($errors->has('clan_id'))
                                                <div class="error-feedback">
                                                    {!! $errors->first('clan_id') !!}<br>
                                                </div>
                                            @endif
                                            
                                        </div>
                                        <div class="form-group col-12 col-lg-6">
                                            <label class="form-label-custom">Drużyna *</label>
                                            @if(session()->has('teams'))
                                                <?php $teams = session('teams');
                                                    $team_id = old('team_id');
                                                ?>
                                            @endif
                                            <select class="form-control bgselect" style="width:100%" id="team_id" name='team_id'>
                                                    <option></option>
                                                @if(session()->has('teams'))
                                                @foreach($teams as $team)

                                                <option value='{{$team["id"]}}' @if($team['id'] == $team_id) selected @endif>{{$team['text']}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                            @if ($errors->has('team_id'))
                                                <div class="error-feedback">
                                                    {!! $errors->first('team_id') !!}<br>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group col-12 col-lg-6">
                                            <label class="form-label-custom">Ilość graczy w drużynie *</label>
                                            <input type="text" class="form-control" name='players_count' value='{{old("players_count")}}' autocomplete="off">
                                            @if ($errors->has('players_count'))
                                                <div class="error-feedback">
                                                    {!! $errors->first('players_count') !!}<br>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group col-12 col-lg-6">
                                            <label class="form-label-custom">Skład drużyny *</label>
                                            
                                            @if(session()->has('members'))
                                                <?php $members = session('members');
                                                
                                                ?>
                                            @endif
                                            <select class="bgselect" style="width:100%" name="members[]" id='members' multiple="multiple">
                                                    
                                                @if(session()->has('members'))
                                                @foreach($members as $member) 
                                                <option value='{{$member["id"]}}' @if(old('members') != NULL)
                                                                                     @if(in_array($member['id'],old('members'))) 
                                                                                        selected 
                                                                                    @endif 
                                                                                @endif>{{$member['text']}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                            @if ($errors->has('members'))
                                                <div class="error-feedback">
                                                    {!! $errors->first('members') !!}<br>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group col-12 col-lg-6">
                                            <label class="form-label-custom">Data * </label>
                                            <input type="text" class="form-control" name='match_date' id='match_date' value='{{old("match_date")}}' autocomplete="off" readonly>
                                            <script>
                                                $(function () {
                                                        $('#match_date').datetimepicker(
                                                            {   
                                                                
                                                                format: 'DD.MM.YYYY',
                                                                locale: 'pl',
                                                                minDate: moment().millisecond(0).second(0).minute(0).hour(0),
                                                                ignoreReadonly: true,
                                                                viewMode: 'days',
                                                                icons: {
                                                                    time: 'fa fa-clock-o',
                                                                    date: 'fa fa-calendar',
                                                                    up: 'fa fa-chevron-up',
                                                                    down: 'fa fa-chevron-down',
                                                                    //previous: 'glyphicon glyphicon-chevron-left',
                                                                    previous: 'fa fa-chevron-left',
                                                                    next: 'fa fa-chevron-right',
                                                                    today: 'fa fa-crosshairs',
                                                                    clear: 'fa fa-trash',
                                                                    close: 'fa fa-remove',
                                                                },
                                                            }
                                                        );
                                                    });
                                                    $(function () {
                                                        $('#match_time').datetimepicker(
                                                            { format: 'LT',
                                                            ignoreReadonly: true,
                                                                locale: 'pl',
                                                                icons: {
                                                                time: 'fa fa-clock-o',
                                                                date: 'fa fa-calendar',
                                                                up: 'fa fa-chevron-up',
                                                                down: 'fa fa-chevron-down',
                                                                //previous: 'glyphicon glyphicon-chevron-left',
                                                                previous: 'fa fa-chevron-left',
                                                                next: 'fa fa-chevron-right',
                                                                today: 'fa fa-crosshairs',
                                                                clear: 'fa fa-trash',
                                                                close: 'fa fa-remove',
                                                                
                                                            },
                                                            }
                                                        );
                                                    });
                                                </script>
                                          @if ($errors->has('match_date'))
                                                <div class="error-feedback">
                                                    {!! $errors->first('match_date') !!}<br>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group col-12 col-lg-6">
                                            <label class="form-label-custom">Godzina *</label>
                                            <input type="text" class="form-control" name='match_time' id='match_time' value='{{old("match_time")}}' autocomplete="off" readonly>
                                            @if ($errors->has('match_time'))
                                                <div class="error-feedback">
                                                    {!! $errors->first('match_time') !!}<br>
                                                </div>
                                            @endif
                                        </div>
                                        <!-- ///Nie usuwać///
                                        <div class="col-12">
                                            <hr class="separator">
                                            <p class="mb5">Ustawienia rozgrywki</p>
                                            <p class="text-muted"><small>Dodaj niezbędne informacje odnośnie planowanej rozgrywki.</small></p>

                                            <div class="row mb30">
                                                <div class="col-12 col-md-6 add-setings-input-pr0">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="min-width: 75px;">Nazwa:</span>
                                                        </div>
                                                        <input type="text" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-12 col-md-6 add-setings-input-pl0">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" style="min-width: 75px;">Wartość:</span>
                                                        </div>
                                                        <input type="text" class="form-control">
                                                        <div class="input-group-append">
                                                            <button class="btn btn-danger" type="button" id="button-addon2">×</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div>
                                                <button type="button" class="btn btn-secondary">Dodaj następne</button>
                                            </div>

                                        </div>
                                        -->
                                        <div class="form-group col-12">
                                            <hr class="separator">
                                            <label for="new-clan-news-text" class="form-label-custom">Dodatkowe ustawienia rozgrywki</label>
                                            <textarea class="form-control" id="new-clan-news-text" rows="3" style="min-height:80px;" name='description'>{{old('description')}}</textarea>
                                            <p class="text-muted mb0"><small>Dodaj niezbędne informacje odnośnie planowanej rozgrywki.</small></p>
                                            @if ($errors->has('description'))
                                                <div class="error-feedback">
                                                    {!! $errors->first('description') !!}<br>
                                                </div>
                                            @endif
                                            
                                        </div>
                                        <div class="col-12">
                                            <div class="text-right">
                                                <a href="{{route('match.friendly.list')}}" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Anuluj</a>
                                                <button type="submit" class="btn btn-primary">Opublikuj</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            @endif
                        </div>

                    </div>

                </div>
                <script src="{{ asset('js/moment.js') }}"></script>
        <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
        <script src="{{ asset('js/pl.js') }}"></script>
@endsection
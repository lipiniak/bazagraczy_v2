@extends('layouts.app')

@section('content')
<div id="klan-edycja" class="content">
    @if($type == 'edit')
    <div class="container-fluid nawigacja">
            <div class="row">
                <div class="col-12 mb5">
                    KLANY
                    <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.list')}}">Klany i członkostwa</a></span>
                    <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.clan',['link'=>$clan->link])}}">{{$clan->name}}</a></span>
                    <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="/klan/{{$clan->id}}/edytuj">Edycja</a></span>
                </div>
                <div class="col-12">
                    <span class="text-nowrap"><i class="fas fa-chevron-circle-left"></i> <a href="{{route('clan.search')}}">Wyszukiwarka klanów</a></span>
                </div>
            </div>
        </div>
    @else
    <div class="container-fluid nawigacja">
            <div class="row">
                <div class="col-12 mb5">
                    KLANY
                    <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.list')}}">Klany i członkostwa</a></span>
                    <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.create')}}">Stwórz klan</a></span>
                </div>
                <div class="col-12">
                    <span class="text-nowrap"><i class="fas fa-chevron-circle-left"></i> <a href="{{route('clan.search')}}">Wyszukiwarka klanów</a></span>
                </div>
            </div>
        </div>
    @endif
    <div class="container-fluid">


        <div class="row">
            <div class="col-12">
                <h1 class="mb30"> {{$title}}</h1>
            </div>
        </div>


        <div class="row">
            @if($type == 'edit')
            {{ Form::model($clan, ['route' => ['clan.post.edit'],'files'=> true],['autocomplete'=>'off']) }}
            {{Form::hidden('id',null,['class' => 'form-control', 'id'=>'klan-nazwa'])}}
            @else 
            {{ Form::model($clan, ['route' => ['clan.post.create'],'files'=> true],['autocomplete'=>'off']) }}
            @endif
                

                <div class="col-12 mb30">
                    <h3>Podstawowe informacje</h3>
                    
                    <div class="form-group">
                        <label class="form-label-custom" for="klan-nazwa">Nazwa *</label>
                        {{Form::text('name',null,['class' => 'form-control', 'id'=>'klan-nazwa'])}}
                        @if ($errors->has('name'))
                            <div class="error-feedback">
                                {!! $errors->first('name') !!}<br>
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label-custom" for="klan-tag">Tag *</label>
                        {{Form::text('tag',null,['class' => 'form-control', 'id'=>'klan-taga'])}}
                        @if ($errors->has('tag'))
                            <div class="error-feedback">
                                {!! $errors->first('tag') !!}<br>
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="form-label-custom" for="klan-tag">Opis</label>
                        {{Form::textarea('description',null,['class' => 'form-control', 'id'=>'klan-opis'])}}
                        @if ($errors->has('description'))
                            <div class="error-feedback">
                                {{ $errors->first('description') }}<br>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="col-12 mb30" id="profil-avatar">
                    <h3>Logo</h3>
                    <div class="avatar-wrapper mb15">
                        <div class="avatar-inner">
                            @if($type =='edit')
                                @if($clan->logo == 'no-clan-logo.jpg')
                                <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="menu-avatar img-fluid klan-def-border" alt="Logo klanowe">
                                @else 
                                <img src="{{ Storage::url($clan->logo)}}" class="img-fluid klan-def-border" alt="Logo klanu">
                                @endif  
                            @else
                            <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="menu-avatar img-fluid klan-def-border" alt="Logo klanowe">
                            @endif                       
                        </div>
                    </div>
                    @if($type=='edit' && $clan->logo != 'no-clan-logo.jpg')
                    <div class="del-btn">
                        <a class="btn btn-secondary" data-toggle="modal" data-target="#deleteLogo">Usuń</a>
                    </div>
                    <!-- MODAL / Start -->
                    <div class="modal fade" id="deleteLogo" tabindex="-1" role="dialog" aria-labelledby="deleteLogoTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="deleteAvatarTitle">Uwaga!</h5>
                                </div>
                                <div class="modal-body text-center">
                                    Czy na pewno chcesz usunąć logo klanu?
                                </div>
                                <div class="modal-footer">
                                    <a href='/klan/{{$clan->id}}/avatar/usun' class="btn btn-primary">Tak</a>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Nie</button>                  
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!-- MODAL / Koniec -->
                    <div class="form-group">
                        <div class="custom-file">
                            
                            {{Form::file('avatar',['class'=>'custom-file-input','id'=>'avatarFile'])}}
                            <label class="custom-file-label" for="avatarFile">Wybierz plik...</label>
                            
                            @if ($errors->has('avatar'))
                            <div class="error-feedback">
                                    {{ $errors->first('avatar') }}<br>
                            </div>
                            @endif
                            <script>
                                    $("input[type=file]").change(function () {
                                        var fieldVal = $(this).val();
                                      
                                        // Change the node's value by removing the fake path (Chrome)
                                        fieldVal = fieldVal.replace("C:\\fakepath\\", "");
                                          
                                        if (fieldVal != undefined || fieldVal != "") {
                                          $(this).next(".custom-file-label").attr('data-content', fieldVal);
                                          $(this).next(".custom-file-label").text(fieldVal);
                                        }
                                      
                                      });
                            </script>
                            <small id="avatarHelp" class="form-text text-muted">Uwaga: W celu uzyskania odpowiedniej jakości loga zaleca się wybór obrazka o kwadratowym kształcie.</small>
                        </div>
                    </div>
                </div>

                <div class="col-12 mb30">
                    <h3>Ustawienia</h3>
                    <div class="custom-control custom-checkbox my-1 mr-sm-2 mr0">
                        
                        {{Form::checkbox('recruitment',null,null,['class'=>'custom-control-input','id'=>'otwarcie-rekrutacji'])}}
                        <label class="custom-control-label" for="otwarcie-rekrutacji">Otwórz rekrutację</label>
                        
                        <small class="form-text text-muted mb5">Otwarcie rekrutacji pozwoli graczom na wyszukanie klanu poprzez wyszukiwarkę z zaznaczoną opcją "Otwarta rekrutacja" oraz umożliwi im dołączenie do tego klanu.</small>
                        <div class="form-group">
                            <label class="form-label-custom" for="info-rekrutacja">Info o rekrutacji</label>
                            
                            {{Form::textarea('recruitment_info',null,['class' => 'form-control', 'id'=>'info-rekrutacja','rows'=>'4','style'=>'min-height: 102px;'])}}
                            @if ($errors->has('recruitment_info'))
                            <div class="error-feedback">
                                {{ $errors->first('recruitment_info') }}<br>
                            </div>
                        @endif
                        </div> 
                    </div>
                </div>

                <div class="col-12">
                    <hr class="separator">
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-primary">Zapisz</button>
                    </div>
                </div>
                @if($type=='edit')
                <div class="col-12 mt50">
                    <div class="form-group text-center">
                        <a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#usunKlanID1">Usuń klan</a>
                    </div>
                    <div class="modal fade" id="usunKlanID1" tabindex="-1" role="dialog" aria-labelledby="usunKlanID1Title" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Uwaga!</h5>
                                </div>
                                <div class="modal-body text-center">
                                    <h4>Nazwa klanu</h4>
                                    <p class="mb10">Czy na pewno chcesz usunąć klan?</p>
                                </div>
                                <div class="modal-footer">
                                    <a href='#' class="btn btn-primary" onClick="event.preventDefault();
                                document.getElementById('deleteform').submit();">Tak</a>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Nie</button>
                                    <form></form>
                                    <form id='deleteform' action='/klan/usun' method='post'>
                                    {{ csrf_field() }}
                                    <input type='hidden' name='clan_id' value='{{$clan->id}}'>
                                    </form>                  
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            {{ Form::close() }}
        </div>


    </div>
</div>

@endsection


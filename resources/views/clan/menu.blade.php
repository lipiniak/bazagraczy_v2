<div id="profile-menu" class="pn-ProductNav_Wrapper mb30">
        <nav id="pnProductNav" class="pn-ProductNav dragscroll" data-overflowing="none">
            <div id="pnProductNavContents" class="pn-ProductNav_Contents pn-ProductNav_Contents-no-transition" style="transform: none;">
                
                <a href="{{route('clan.clan',['link'=>$clan->link])}}" class="pn-ProductNav_Link" @if($page == 'about')aria-selected="true"@endif>O klanie</a>
                
                <a href="{{route('clan.clan.members',['link'=>$clan->link])}}" class="pn-ProductNav_Link" @if($page == 'members')aria-selected="true"@endif>Członkowie @if($clan->leader_id == Auth::user()->id) @if($clan->aplicants()->count() > 0) <span class="badge badge-danger menu-badge">{{$clan->aplicants()->count()}}</span>@endif @endif</a>
                
                <a href="{{route('clan.clan.teams',['link'=>$clan->link])}}" class="pn-ProductNav_Link" @if($page == 'teams')aria-selected="true"@endif>Drużyny</a>
                
                <a href="{{route('clan.clan.matches',['link'=>$clan->link])}}" class="pn-ProductNav_Link" @if($page == 'matches')aria-selected="true"@endif>Rozgrywki</a>
                
                <span id="pnIndicator" class="pn-ProductNav_Indicator" style="transform: translateX(0px) scaleX(0.827031); background-color: rgb(255, 193, 7);"></span>
            </div>
        </nav>
        
        <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
            <i class="fas fa-chevron-left"></i>
        </button>
        <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right" type="button">
            <i class="fas fa-chevron-right"></i>
        </button>
        <hr class="separator hr-pagemenu">

        <script src="{{ asset('js/horizontal-menu-scroller.js') }}"></script>
    </div>
@extends('layouts.app')

@section('content')

@if (Session::has('fail'))
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#teamCreate').modal('show');
        });
    </script>
@endif
@if (Session::has('edit'))
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#EdytujDruzyne{{Session::get("edit")}}').modal('show');
        });
    </script>
@endif

<div id="klan" class="content">
        <div class="container-fluid nawigacja">
                <div class="row">
                    <div class="col-12 mb5">
                        KLANY
                        <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.list')}}">Klany i członkostwa</a></span>
                        <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.clan',['link'=>$clan->link])}}">{{$clan->name}}</a></span>
                        <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.clan.teams',['link'=>$clan->link])}}">Drużyny</a></span>
                    </div>
                    <div class="col-12">
                        <span class="text-nowrap"><i class="fas fa-chevron-circle-left"></i> <a href="{{route('clan.search')}}">Wyszukiwarka klanów</a></span>
                    </div>
                </div>
            </div>
        @include('clan.menu')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb50">
                <h2>{{$clan->name}} [{{$clan->tag}}]</h2>
                @if($clan->teams()->count() == 0)
                    <div class="alert alert-warning mb30 mt30">Ten klan nie posiada utworzonej drużyny. Aby uczestniczyć w klanówkach, należy stworzyć drużynę.</div>
                @endif
                @if($clan->teams()->count() > 0 || Auth::user()->id == $clan->leader_id)
                    <div class="row">
                        <div class="col-12">
                            @if(Auth::user()->id == $clan->leader_id)
                                <a class="btn btn-primary mb15" data-toggle="modal" data-target="#teamCreate">Stwórz nową drużynę</a>
                            @endif
                            <div class="modal fade" id="teamCreate" role="dialog" aria-labelledby="StworzDruzyneTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                        {!! Form::open(['route' => ['clan.team.create']]) !!} 
                                            <input type="hidden" name="clan_id" value="{{$clan->id}}">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Tworzenie nowej drużyny</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                        <div class="modal-body text-left">
                                            <div class="form-group">
                                                <label class="form-label-custom">Klan</label>
                                                <input type="text" class="form-control" value="{{$clan->name}}" readonly>
                                            </div>
                                            <div class="form-group" style="width:100%">
                                                <label class="form-label-custom">Tytuł gry *</label>
                                                {{Form::select('game_id',$data['games'],null,['class'=> 'form-control bgselect','id'=>'game-title','style'=>'width:100%'])}}
                                                
                                                @if ($errors->has('game_id'))
                                                <div class="error-feedback">
                                                        {{ $errors->first('game_id') }}<br>
                                                    
                                                </div>
                                                @endif
                                            </div>
                                            <script>
                                                    $('#game-title').select2({
                                                        
                                                      }).on('change',function() {
                                                          
                                                          var value= $('#game-title option:selected').attr('value');
                
                                                          if(value == 0) {
                                                            $('#wyszukiwarka-platforma').empty();
                                                            $("#wyszukiwarka-platforma").select2();
                                                          }
    
                                                          var url = "/helper/platforms/"+value;
                                                            $.ajax({
                                                                type: "GET",
                                                                url: url,
                                                                success: function(data)
                                                                {
                                                                    
                                                                    $('#wyszukiwarka-platforma').empty();
                                                                    $("#wyszukiwarka-platforma").select2({
                                                                        data: data
                                                                    });
                                                                }
                                                            });
                                                          
                                                    });
                
                                                    </script>
                                            <div class="form-group" style="width:100%">
                                                <label class="form-label-custom">Platforma *</label>
                                                @if(session()->has('platform'))
                                                <?php $city = session('platform');?>
                                                @endif
                                                <?php 
                                                if(Session::has('teamData')) {
                                                    $teamData = Session::get('teamData');
                                                    
                                                } else 
                                                    $teamData['platform'][0] = '';

                                                
                                                ?>
                                                {{Form::select('platform_id',$teamData['platform'],null,['class'=> 'form-control','id'=>'wyszukiwarka-platforma','style'=>'width:100%'])}}
                                                <script>
                                                        $("#wyszukiwarka-platforma").select2();
                                                </script>
                                                @if ($errors->has('platform_id'))
                                                <div class="error-feedback">
                                                        {{ $errors->first('platform_id') }}<br>
                                                    
                                                </div>
                                                @endif
                                                
                                            </div>
                                            <div class="form-group" style="width:100%">
                                                <label class="form-label-custom">Skład drużyny *</label>
                                                <script>
                                                        $(document).ready(function () {
                                                            $('#members').select2();
                                                        });
                                                </script>
                                                <select id='members' style="width:100%" name="members[]" multiple="multiple">
                                                    @if(!empty(old('members')))
                                                    <?php $mem = array();
                                                    print_r(old('members'));?>
                                                        @foreach(old('members') as $mems)
                                                        <?php $mem[$mems] = $mems ;?>
                                                        
                                                    @endforeach
                                                    @endif
                                                    @foreach($clan->members as $member) 
                                                    <option value="{{$member->id}}" @if(isset($mem[$member->id])) selected @endif>{{$member->login}}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('members'))
                                                <div class="error-feedback">
                                                        {{ $errors->first('members') }}<br>
                                                    
                                                </div>
                                                @endif
                                            </div>
                                            <div class="form-group" style="width:100%">
                                                <label class="form-label-custom">Lider drużyny *</label>
                                                <script>
                                                        $(document).ready(function () {
                                                            $('#leader').select2();
                                                        });
                                                </script>
                                                <select name='leader_id' class="bgselect next_leader" id="leader" style="width:100%">
                                                        <option value="0"></option>
                                                        @foreach($clan->members as $memberr)
                                                        
                                                        <option value="{{$memberr->id}}" @if(old('leader_id') == $memberr->id) selected @endif>{{$memberr->login}}</option>
                                                        
                                                        @endforeach
                                                        
                                                    </select>
                                                    @if ($errors->has('leader_id'))
                                                    <div class="error-feedback">
                                                            {{ $errors->first('leader_id') }}<br>
                                                        
                                                    </div>
                                                    @endif
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label-custom">Nazwa drużyny *</label>
                                                <input type="text" name='name' class="form-control" value="{{old('name')}}">
                                                @if ($errors->has('name'))
                                                    <div class="error-feedback">
                                                            {!!$errors->first('name') !!}<br>
                                                        
                                                    </div>
                                                    @endif
                                                
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Anuluj</button>
                                            <button type="submit" class="btn btn-primary">Zapisz</button>
                                        </div>
                                    </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                        @foreach($clan->teams as $team)
                    <div class="col-12 col-xl-6 mb30">
                        
                        <div class="druzyna">
                            <h4>{{$team->name}}</h4>
                            <hr class="separator">
                            <p class="mb5"><span class="text-muted">Gra: </span>{{$team->game->name}}</p>
                            <p class="mb5"><span class="text-muted">Platforma:</span> <span class="badge badge-white">{{$team->platform->short_name}}</span></p>
                            <p class="mb5"><span class="text-muted">Kapitan:</span> @if($team->leader_id != 0)<span class="inline-status {{$team->leader->getStatus()}}"></span> <a href="/profil/{{$team->leader->id}}">{{$team->leader->login}}</a>@else Brak @endif</p>
                            <p class="mt15 mb5">Skład drużyny:</p>
                            @foreach($team->members as $member)
                            <div class="czlonek-druzyny">
                                <div class="row">
                                    <div class="col">
                                        <span class="inline-status {{$member->getStatus()}}"></span> <a href="/profil/{{$member->id}}">{{$member->login}}</a>
                                    </div>
                                    <div class="col-auto">
                                        @if(Auth::user()->id != $member->id)
                                        <a href="/wiadomosci/{{$member->id}}"><i class="fas fa-comments"></i></a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endforeach

                            
                            @if(Auth::user()->id == $clan->leader_id)
                            <div class="text-right">
                                <a class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#EdytujDruzyne{{$team->id}}">Edytuj</a>
                                <div class="modal fade" id="EdytujDruzyne{{$team->id}}" role="dialog" aria-labelledby="EdytujDruzyneIDTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                            {!! Form::open(['route' => ['clan.team.update']]) !!} 
                                            <input type="hidden" name="clan_id" value="{{$clan->id}}">
                                            <input type="hidden" name="team_id" value="{{$team->id}}">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Edycja drużyny</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body text-left">
                                                    <div class="form-group">
                                                        <label class="form-label-custom">Klan</label>
                                                        <input type="text" class="form-control" value="{{$clan->name}}" readonly>
                                                    </div>
                                                    <div class="form-group" style="width:100%">
                                                        <label class="form-label-custom">Tytuł gry *</label>
                                                        
                                                        <select class="form-control bgselect" name="game_id-edit-{{$team->id}}" style="width:100%" id='game_edit_{{$team->id}}'>
                                                            @foreach($data['games_edit'] as $game) 
                                                                @if ($errors->any())
                                                                    <option value='{{$game['id']}}' @if(isset($teamData['game_id-edit-'.$team->id])) @if($game['id']==$teamData['game_id-edit-'.$team->id]) selected @endif @endif>{{$game['text']}}</option>
                                                                @else
                                                                    <option value='{{$game['id']}}' @if($game['id']==$team->game->id) selected @endif>{{$game['text']}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                        <script>
                                                                $('#game_edit_{{$team->id}}').select2({
                                                                    
                                                                  }).on('change',function() {
                                                                      
                                                                      var value= $('#game_edit_{{$team->id}} option:selected').attr('value');
                            
                                                                      if(value == 0) {
                                                                        $('#platform_edit_{{$team->id}}').empty();
                                                                        $("#platform_edit_{{$team->id}}").select2();
                                                                      }
                
                                                                      var url = "/helper/platforms/"+value;
                                                                        $.ajax({
                                                                            type: "GET",
                                                                            url: url,
                                                                            success: function(data)
                                                                            {
                                                                                
                                                                                $('#platform_edit_{{$team->id}}').empty();
                                                                                $("#platform_edit_{{$team->id}}").select2({
                                                                                    data: data
                                                                                });
                                                                            }
                                                                        });
                                                                      
                                                                });
                            
                                                                </script>
                                                                @if ($errors->has('game_id-edit-'.$team->id))
                                                                <div class="error-feedback">
                                                                        {!!$errors->first('game_id-edit-'.$team->id) !!}<br>
                                                                    
                                                                </div>
                                                                @endif
                                                    </div>
                                                    <div class="form-group" style="width:100%">
                                                        <label class="form-label-custom">Platforma *</label>
                                                        <select class="form-control bgselect" style="width:100%" name="platform_id-edit-{{$team->id}}" id="platform_edit_{{$team->id}}">
                                                                @foreach($data['platforms_edit'] as $game) 
                                                                @if ($errors->any())
                                                                    <option value='{{$game['id']}}' @if(isset($teamData['platform_id-edit-'.$team->id]))@if($game['id']==$teamData['platform_id-edit-'.$team->id]) selected @endif @endif>{{$game['text']}}</option>
                                                                @else
                                                                    <option value='{{$game['id']}}' @if($game['id']==$team->platform->id) selected @endif>{{$game['text']}}</option>
                                                                @endif
                                                            @endforeach   
                                                        </select>
                                                        <script>
                                                                $("#platform_edit_{{$team->id}}").select2();
                                                        </script>
                                                        @if ($errors->has('platform_id-edit-'.$team->id))
                                                        <div class="error-feedback">
                                                                {!!$errors->first('platform_id-edit-'.$team->id) !!}<br>
                                                            
                                                        </div>
                                                        @endif
                                                    </div>
                                                    <div class="form-group" style="width:100%">
                                                        <label class="form-label-custom">Skład drużyny *</label>
                                                        <script>
                                                                $(document).ready(function () {
                                                                    $('#members_edit_{{$team->id}}').select2();
                                                                });
                                                        </script>
                                                        <?php $thisteam[$team->id] = array();
                                                            
                                                            ?>
                                                            @foreach($team->members as $member)
                                                            <?php 
                                                                $thisteam[$team->id][] = $member->id;
                                                            ?>
                                                            @endforeach
                                                            
                                                        <select class="bgselect" style="width:100%" name="members-edit-{{$team->id}}[]" multiple="multiple" id='members_edit_{{$team->id}}'>
                                                            
                                                            @foreach($clan->members as $members)
                                                            @if($errors->any())
                                                                <option value="{{$members->id}}" @if(isset($teamData['members-edit-'.$team->id])) @if(in_array($members->id, $teamData['members-edit-'.$team->id])) selected @endif @endif>{{$members->login}}</option>
                                                            @else
                                                                <option value="{{$members->id}}" @if(in_array($members->id, $thisteam[$team->id])) selected @endif>{{$members->login}}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('members-edit-'.$team->id))
                                                        <div class="error-feedback">
                                                                {!!$errors->first('members-edit-'.$team->id) !!}<br>
                                                            
                                                        </div>
                                                        @endif
                                                    </div>
                                                    <div class="form-group" style="width:100%">
                                                        <label class="form-label-custom">Kapitan drużyny *</label>
                                                        <script>
                                                                $(document).ready(function () {
                                                                    $('#leader_edit_{{$team->id}}').select2();
                                                                });
                                                        </script>
                                                        <select class="bgselect" style="width:100%" name="leader_id-edit-{{$team->id}}" id='leader_edit_{{$team->id}}'>
                                                            <option></option>

                                                            @foreach($clan->members as $member)
                                                                @if ($errors->any())
                                                                    <option value="{{$member->id}}" @if(isset($teamData['leader_id-edit-'.$team->id])) @if($teamData['leader_id-edit-'.$team->id] != 0) @if($member->id == $teamData['leader_id-edit-'.$team->id]) selected @endif @endif @endif>{{$member->login}}</option>
                                                                @else 
                                                                    <option value="{{$member->id}}" @if($team->leader_id != 0) @if($member->id == $team->leader->id) selected @endif @endif>{{$member->login}}</option>
                                                                @endif
                                                            @endforeach
                                                            
                                                        </select>
                                                        @if ($errors->has('leader_id-edit-'.$team->id))
                                                        <div class="error-feedback">
                                                                {!!$errors->first('leader_id-edit-'.$team->id) !!}<br>
                                                            
                                                        </div>
                                                        @endif
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-label-custom">Nazwa drużyny *</label>
                                                        <input name='name-edit-{{$team->id}}' type="text" class="form-control" value="@if ($errors->any())@if(isset($teamData['name-edit-'.$team->id])){{$teamData['name-edit-'.$team->id]}} @endif @else {{$team->name}} @endif">
                                                        
                                                        @if ($errors->has('name-edit-'.$team->id))
                                                        <div class="error-feedback">
                                                                {!!$errors->first('name-edit-'.$team->id) !!}<br>
                                                            
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Anuluj</button>
                                                    <button type="submit" class="btn btn-primary">Zapisz</button>
                                                </div>
                                            </div>
                                        {!!Form::close()!!}
                                    </div>
                                </div>

                                <a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#usunDruzyne{{$team->id}}">Usuń drużynę</a>
                                <div class="modal fade" id="usunDruzyne{{$team->id}}" tabindex="-1" role="dialog" aria-labelledby="usunDruzyneID1Title" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deletePostId1Title">Uwaga!</h5>
                                            </div>
                                            <div class="modal-body text-center">
                                                <h4>{{$team->name}}</h4>
                                                <p class="text-muted">{{$team->game->name}}<span class="badge badge-muted">{{$team->platform->short_name}}</span></p>
                                                <p class="mb10">Czy na pewno chcesz usunąć drużynę?</p>
                                            </div>
                                            <form method="post" action="/klan/druzyna/usun" id="deleteTeam{{$team->id}}">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="team_id" value="{{$team->id}}">
                                            </form>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" onclick="event.preventDefault();
                                                document.getElementById('deleteTeam{{$team->id}}').submit();">Tak</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Nie</button>                  
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                            @endif
                        </div>
                        
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
            
        </div>
    </div>
</div>

@endsection
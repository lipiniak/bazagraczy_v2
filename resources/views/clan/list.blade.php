@extends('layouts.app')

@section('content')
<script>
        
        $(document).ready(function () {
            $('.next_leader').select2();
        });
       
    </script>
<div id="klan-zarzadzanie" class="content">
    <div class="container-fluid" id="profil">


        <div class="row mb50">
            <div class="col-12">
                <h1 class="mb30">Moje klany i członkostwa</h1>
            </div>
            @if(empty($clans))
            <div class="col-12">
                <div class="alert alert-info text-center" role="alert">
                    Nie należysz do żadnego klanu
                </div>
            </div>
            @endif
            @foreach($clans as $clan)
                <div class="col-12 col-xl-6">
                    <div class="czlonkostwo-wrapper">
                        <div class="logo-klanu-wrapper">
                            <a href="/klan/{{$clan->link}}">
                                <div class="logo-klanu">
                                    @if($clan->logo == 'no-clan-logo.jpg')
                                    <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="menu-avatar img-fluid klan-def-border" alt="Logo klanowe">
                                    @else 
                                    <img src="{{ Storage::url($clan->logo)}}" class="img-fluid klan-def-border" alt="Logo klanu">
                                    @endif

                                </div>
                            </a>
                        </div>
                        <div class="czlonkostwo-info">
                            <h3 class="mb0"><a href="/klan/{{$clan->link}}">{{$clan->name}}</a></h3>
                            <p class="mb0"><small>Pozycja: @if($clan->leader_id == Auth::user()->id) Lider @else Członek @endif</small></p>
                            <p class="mb0"><small>Członkowie: {{$clan->members()->count()}}</small></p>
                            <p class="mb5"><small class="text-muted">Ostatnia aktywność: {{$clan->updated_at->format('d.m.Y')}}</small></p>
                            <a href="/klan/{{$clan->link}}" class="btn btn-secondary btn-sm">Zobacz</a>
                            @if($clan->leader_id == Auth::user()->id)
                            <a href="/klan/{{$clan->id}}/czlonkowie" class="btn btn-secondary btn-sm">Członkowie @if($clan->aplicants()->count() > 0) <span class="badge badge-danger menu-badge">{{$clan->aplicants()->count()}}</span>@endif</a>
                            <a href="/klan/{{$clan->link}}/druzyny" class="btn btn-secondary btn-sm">Drużyny</a>
                            <a href="/klan/{{$clan->id}}/edytuj" class="btn btn-secondary btn-sm">Edytuj klan</a>
                            @endif
                            <a href="#" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#opuscKlanID{{$clan->id}}">Opuść</a>
                            <div class="modal fade" id="opuscKlanID{{$clan->id}}" role="dialog" aria-labelledby="opuscKlanID1Title" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Uwaga!</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="post" action='/klan/{{$clan->id}}/oposc' id='deleteForm{{Auth::user()->id}}'>
                                            {{ csrf_field() }}
                                            <input type="hidden" name='user_id' value="{{Auth::user()->id}}">
                                            <input type="hidden" name='type' value="leave">
                                        @if(Auth::user()->id == $clan->leader_id)
                                        <div class="modal-body text-left">
                                            
                                            @if(count($clan->members) == 1)
                                            <p class="mb10">Jesteś ostatnim członkiem klanu. Opuszczenie klanu spowoduje całkowiete usunięcie klanu. Proces ten jest nieodwracalny.</p>
                                            @else 
                                            <p class="mb10">Jesteś liderem klanu. Przed opuszczeniem klanu musisz przekazać dowództwo.</p>
                                            <div class="form-group text-left" style="width:100%">
                                                <label class="form-label-custom">Wybierz nowego lidera</label>
                                                <select name='next_id' class="bgselect next_leader" style="width:100%">
                                                    <option value="0"></option>
                                                    @foreach($clan->members as $member)
                                                    @if($member->id != Auth::user()->id)
                                                    <option value="{{$member->id}}">{{$member->login}}</option>
                                                    @endif
                                                    @endforeach
                                                    
                                                </select>
                                                
                                            </div>
                                            @endif
                                        </div>
                                        @else
                                        <div class="modal-body text-left">
                                            <p class="mb10">Czy na pewno chcesz opóścić klan?</p>
                                        </div>
                                        @endif
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                                            <button type="submit" class="btn btn-danger">Opuść</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            @endforeach

            

        </div>


        <div class="row">
            <div class="col-12">
                <h1 class="mb15">Stwórz nowy klan</h1>
            </div>
            <div class="col-12">
                <a href="{{route('clan.create')}}" class="btn btn-primary">Rozpocznij</a>
            </div>
        </div>


    </div>
</div>
@endsection


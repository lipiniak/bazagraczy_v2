@extends('layouts.app')

@section('content')
<script>
        
        $(document).ready(function () {
            $('.next_leader').select2();
        });
       
    </script>

    @if (Session::has('fail'))
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#opuscKlan{{Session::get("fail")}}').modal('show');
        });
    </script>
@endif
<div id="klan-czlonkowie" class="content">
        <div class="container-fluid nawigacja">
                <div class="row">
                    <div class="col-12 mb5">
                        KLANY
                        <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.list')}}">Klany i członkostwa</a></span>
                        <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.clan',['link'=>$clan->link])}}">{{$clan->name}}</a></span>
                        <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="/klan/{{$clan->id}}/czlonkowie">Zarządzenie członkami</a></span>
                    </div>
                    <div class="col-12">
                        <span class="text-nowrap"><i class="fas fa-chevron-circle-left"></i> <a href="{{route('clan.search')}}">Wyszukiwarka klanów</a></span>
                    </div>
                </div>
            </div>
                    <div class="container-fluid">


                        <div class="row">
                            <div class="col-12">
                                <h1 class="mb30">{{$clan->name}}</h1>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-12 mb30">
                                <h3>Członkostwa do zatwierdzenia</h3>
                                <div class="row">
                                    <div class="col-12">
                                        <ul class="clear-list">
                                            @if(empty($aplicants))
                                            <div class="alert alert-info text-center" role="alert">
                                                Brak nowych zgłoszeń
                                            </div>
                                            @else
                                            @foreach($aplicants as $aplicant)
                                            <!-- Pojedynczy wynik /start -->
                                            <li class="lista-graczy">
                                                <div class="pojedynczy-wynik">
                                                    <div class="avatar-wrapper">

                                                        <a href="/profil/{{$aplicant->id}}">
                                                            @if($aplicant->avatar == 'no-avatar.jpg')
                                                            <img src="{{asset('images/avatars/')}}/{{$aplicant->avatar}}" class="menu-avatar" alt="Avatar">
                                                        @else
                                                            <img src="{{ Storage::url($aplicant->avatar) }}" class="menu-avatar" alt="Avatar">
                                                        @endif
                                                    </a>
                                                    </div>
                                                    <div class="gracz-info">
                                                        <a href="#">
                                                            <h4 class="mb0"><span>{{$aplicant->login}}</span></h4>
                                                            <i>#{{$aplicant->id}}</i>
                                                        </a>
                                                    </div>
                                                    <div class="gracz-akcje">
                                                        <a class="btn btn-primary akcja" onclick="event.preventDefault();
                                                        document.getElementById('user-{{$aplicant->id}}-accept').submit();">Akceptuj</a>
                                                        <a class="btn btn-danger akcja" onclick="event.preventDefault();
                                                        document.getElementById('user-{{$aplicant->id}}-reject').submit();">Odrzuć</a>
                                                    </div>
                                                    <form id="user-{{$aplicant->id}}-accept" action="/klan/{{$clan->id}}/akceptuj" method="POST" style="display: none;">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="user_id" value='{{$aplicant->id}}'>
                                                    </form>
                                                    <form id="user-{{$aplicant->id}}-reject" action="/klan/{{$clan->id}}/odrzuc" method="POST" style="display: none;">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="user_id" value='{{$aplicant->id}}'>
                                                    </form>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </li>
                                            <!-- Pojedynczy wynik /koniec -->
                                            @endforeach
                                            @endif

                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 mb30" id="profil-avatar">
                                <h3>Aktualni członkowie</h3>
                                <div class="row">
                                    <div class="col-12">
                                        <input type="text" id="gamesInput" onkeyup="FiltrowanieListy()" placeholder="Wyszukaj...">
                                        <ul id="gamesUL">

                                            <!-- Pojedynczy wynik /start -->
                                            @foreach($members as $member)
                                            <li class="lista-graczy">
                                                <div class="pojedynczy-wynik">
                                                    <div class="avatar-wrapper">
                                                            <a href="/profil/{{$member->id}}">
                                                                @if($member->avatar == 'no-avatar.jpg')
                                                                <img src="{{asset('images/avatars/')}}/{{$member->avatar}}" class="menu-avatar" alt="Avatar">
                                                            @else
                                                                <img src="{{ Storage::url($member->avatar) }}" class="menu-avatar" alt="Avatar">
                                                            @endif
                                                        </a>
                                                    </div>
                                                    <div class="gracz-info">
                                                        <a href="#">
                                                            <h4 class="mb0"><span>{{$member->login}}</span></h4>
                                                            <i>#{{$member->id}}</i>
                                                        </a>
                                                    </div>
                                                    <div class="gracz-akcje">
                                                        @if($member->id == Auth::user()->id)
                                                        <a class="btn btn-danger akcja" data-toggle="modal" data-target="#opuscKlan{{$member->id}}">Opuść klan</a>
                                                        @else
                                                        <a class="btn btn-secondary akcja" data-toggle="modal" data-target="#przekazDowodztwo{{$member->id}}">Przekaż dowództwo</a>
                                                        <a class="btn btn-danger akcja" data-toggle="modal" data-target="#opuscKlan{{$member->id}}">Usuń</a>
                                                        @endif
                                                        <div class="modal fade" id="opuscKlan{{$member->id}}" role="dialog" aria-labelledby="opuscKlanID1Title" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                                <div class="modal-content">
                                                                    
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title">Uwaga!</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form method="post" action='/klan/{{$clan->id}}/oposc' id='deleteForm{{$member->id}}'>
                                                                    {{ csrf_field() }}
                                                                    <input type="hidden" name='user_id' value="{{$member->id}}">
                                                                    <input type="hidden" name='type' value="remove">
                                                                    @if($member->id == $clan->leader_id)
                                                                    <div class="modal-body text-left">    
                                                                        @if(count($clan->members) == 1)
                                                                        <p class="mb10">Jesteś ostatnim członkiem klanu. Opuszczenie klanu spowoduje całkowiete usunięcie klanu. Proces ten jest nieodwracalny.</p>
                                                                        @else 
                                                                        <p class="mb10">Jesteś liderem klanu. Przed opuszczeniem klanu musisz przekazać dowództwo.</p>
                                                                        <div class="form-group text-left" style="width:100%">
                                                                            <label class="form-label-custom">Wybierz nowego lidera</label>
                                                                            <select name='next_id' class="bgselect next_leader" style="width:100%">
                                                                                <option value="0"></option>
                                                                                @foreach($clan->members as $memberr)
                                                                                @if($memberr->id != $member->id )
                                                                                <option value="{{$memberr->id}}">{{$memberr->login}}</option>
                                                                                @endif
                                                                                @endforeach
                                                                                
                                                                            </select>
                                                                            @if ($errors->has('next_id'))
                                                                                <div class="error-feedback">
                                                                                    {{ $errors->first('next_id') }}<br>
                                                                                </div>
                                                                            @endif
                                                                        </div>
                                                                        @endif
                                                                    </div>
                                                                    @else
                                                                    <div class="modal-body text-left">
                                                                        <p class="mb10">Czy na pewno chcesz usunąć gracza?</p>
                                                                    </div>
                                                                    @endif
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                                                                        
                                                                        <button type="button" class="btn btn-danger" onclick="event.preventDefault();
                                                                        document.getElementById('deleteForm{{$member->id}}').submit();">@if($member->id == Auth::user()->id) Opuść klan @else Usuń @endif</button>
                                                                    </div>
                                                                </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        
                                                        <div class="modal fade" id="przekazDowodztwo{{$member->id}}" role="dialog" aria-labelledby="przekazDowodztwoTitle" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title">Uwaga!</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form method="post" action='/klan/{{$clan->id}}/przekaz' id='przekazDowodztwoForm{{$member->id}}'>
                                                                        {{ csrf_field() }}
                                                                        <input type="hidden" name='user_id' value="{{$member->id}}">
                                                                        <input type="hidden" name='type' value="pass">
                                                                    <div class="modal-body text-left">
                                                                        <p class="mb10">Zamierzasz przekazać dowództwo graczowi.</p>
                                                                        <h3>{{$member->login}}</h3>
                                                                        <p class="mb10 text-muted"><small>Po przekazaniu dowództwa utracisz możliwość zarządzania klanem oraz utracisz wszelkie prawa lidera. Proces ten jest odwracalny jedynie po ponownym przekazaniu dowództwa przez aktualnego lidera.</small></p>
                                                                        
                                                                    </div>
                                                                    </form>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                                                                        <button type="button" class="btn btn-danger" onclick="event.preventDefault();
                                                                        document.getElementById('przekazDowodztwoForm{{$member->id}}').submit();">Przekaż</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </li>
                                            <!-- Pojedynczy wynik /koniec -->
                                            @endforeach
                                            

                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>

@endsection


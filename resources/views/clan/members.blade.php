@extends('layouts.app')

@section('content')

@if (Session::has('fail'))
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#teamCreate').modal('show');
        });
    </script>
@endif
@if (Session::has('edit'))
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#EdytujDruzyne{{Session::get("edit")}}').modal('show');
        });
    </script>
@endif
<div id="klan" class="content">
        <div class="container-fluid nawigacja">
                <div class="row">
                    <div class="col-12 mb5">
                        KLANY
                        <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.list')}}">Klany i członkostwa</a></span>
                        <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.clan',['link'=>$clan->link])}}">{{$clan->name}}</a></span>
                        <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.clan.members',['link'=>$clan->link])}}">Członkowie</a></span>
                    </div>
                    <div class="col-12">
                        <span class="text-nowrap"><i class="fas fa-chevron-circle-left"></i> <a href="{{route('clan.search')}}">Wyszukiwarka klanów</a></span>
                    </div>
                </div>
            </div>
    @include('clan.menu')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 mb50">
                <h2>{{$clan->name}} [{{$clan->tag}}]</h2>
                <p class="mb30">Wszyscy członkowie: {{$clan->members()->count()}}</p>
                
                @if($clan->leader_id == Auth::user()->id)
                <a href="/klan/{{$clan->id}}/czlonkowie" class="mt-25 mb30 btn btn-secondary btn-sm">Zarządzaj członkami @if($clan->aplicants()->count() > 0) <span class="badge badge-danger menu-badge">{{$clan->aplicants()->count()}}</span>@endif</a>
                @endif
                <div class="row">
                    @foreach($clan->members as $member)
                    <div class="col-12 col-lg-6 col-xl-4">
                        <div class="czlonkostwo-wrapper">
                            <div class="logo-klanu-wrapper">
                                <a href="/profil/{{$member->id}}">
                                    <div class="logo-klanu">
                                            
                                            
                                                @if($member->avatar == 'no-avatar.jpg')
                                                <img src="{{asset('images/avatars/')}}/{{$member->avatar}}" class="menu-avatar" alt="Avatar">
                                            @else
                                                <img src="{{ Storage::url($member->avatar) }}" class="menu-avatar" alt="Avatar">
                                            
                                            @endif
                                    </div>
                                </a>
                            </div>
                            <div class="czlonkostwo-info mt0">
                                <h3 class="mb0"><a href="/profil/{{$member->id}}">{{$member->login}}</a></h3>
                                @if($member->getStatus() == 'online')
                                <p class="mb0"><small><span class="inline-status online"></span> Online</small></p>
                                @elseif($member->getStatus() == 'offline')
                                <p class="mb0"><small><span class="inline-status"></span> Offline</small></p>
                                @else
                                <p class="mb0"><small><span class="inline-status away"></span> Zaraz Wracam</small></p>
                                @endif
                                <p class="mb0"><small class="text-muted">Dołączył: {{$member->pivot->updated_at->format('d.m.Y')}}</small></p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
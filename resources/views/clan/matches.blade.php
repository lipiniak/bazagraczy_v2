@extends('layouts.app')

@section('content')

@if (Session::has('fail'))
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#teamCreate').modal('show');
        });
    </script>
@endif
@if (Session::has('edit'))
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#EdytujDruzyne{{Session::get("edit")}}').modal('show');
        });
    </script>
@endif
<script>
        function FiltrowanieListyMeczy(type) {
            // Declare variables
            var input, filter, ul, li, a, i;
            input = type;
            
            filter = type;

            ul = document.getElementById("matcheslist");
            li = ul.getElementsByTagName('li');
    
            // Loop through all list items, and hide those who don't match the search query
            for (i = 0; i < li.length; i++) {
                a = li[i].getElementsByTagName("span")[0];
                if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    li[i].style.display = "";
                } else {
                    li[i].style.display = "none";
                }
            }
        }
    </script>
<div id="klan" class="content">
        <div class="container-fluid nawigacja">
            <div class="row">
                <div class="col-12 mb5">
                    KLANY
                    <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.list')}}">Klany i członkostwa</a></span>
                    <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.clan',['link'=>$clan->link])}}">{{$clan->name}}</a></span>
                    <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.clan.matches',['link'=>$clan->link])}}">Rozgrywki</a></span>
                </div>
                <div class="col-12">
                    <span class="text-nowrap"><i class="fas fa-chevron-circle-left"></i> <a href="{{route('clan.search')}}">Wyszukiwarka klanów</a></span>
                </div>
            </div>
        </div>
        @include('clan.menu')
    <div class="container-fluid">
        <div class="row">
                <div class="col-12 mb50">
                    <h2>{{$clan->name}} [{{$clan->tag}}]</h2>
                    <?php
                        $win = 0;
                        $los = 0;
                        $tie = 0;
                        $cancel = 0;
                        $wait = 0;
                        foreach($clan->allMatches() as $match) {
                            if($match->result == 4)
                                $cancel++;
                            if($match->result == 3)
                                $tie++;
                            if($match->result == 1) {
                                if($match->winner_id == $clan->id) {
                                    $win++;
                                } else {
                                    $los++;
                                }

                            }
                            if($match->result == 0) {
                                $wait++;
                            }
                            
                        }

                    ?>
                    <div class="mt15 mb10">
                        <a href="#" onclick="FiltrowanieListyMeczy('')" class="btn btn-secondary btn-sm mr0 mb5">Wszystkie: {{$clan->allMatches()->count()}}</a>
                        <a href="#" onclick="FiltrowanieListyMeczy('ZWYCIĘSTWO')" class="btn btn-secondary btn-sm mr0 mb5">Zwycięstwa: {{$win}}</a>
                        <a href="#" onclick="FiltrowanieListyMeczy('PORAŻKA')" class="btn btn-secondary btn-sm mr0 mb5">Porażki: {{$los}}</a>
                        <a href="#" onclick="FiltrowanieListyMeczy('REMIS')" class="btn btn-secondary btn-sm mr0 mb5">Remisy: {{$tie}}</a>
                        <a href="#" onclick="FiltrowanieListyMeczy('OCZEKIWANIE NA WYNIK')" class="btn btn-secondary btn-sm mr0 mb5">Oczekujące: {{$wait}}</a>
                        <a href="#" onclick="FiltrowanieListyMeczy('ANULOWANY')" class="btn btn-secondary btn-sm mr0 mb5">Anulowane: {{$cancel}}</a>
                    </div>
                    <div class="row">

                        <div class="col-12">
                            <ul class="lista-rogrywki-klanowe" id='matcheslist'>
                                @foreach($clan->allMatches() as $match)
                                <li>
                                    <div class="row" style="align-items: center">
                                        <div class="cleardiv">
                                            @if($match->result == 1)
                                                @if($match->winner_id == $clan->id)

                                                    <div class="col-auto rozgrywki-wynik rozgrywki-zwyciestwo">
                                                        <span class="d-none">ZWYCIĘSTWO</span>
                                                        <i class="fas fa-crown fa-2x" data-toggle="tooltip" data-placement="top" title="" data-original-title="Zwycięstwo"></i>
                                                    </div>
                                                @else
                                                    <div class="col-auto rozgrywki-wynik rozgrywki-porazka">
                                                            <span class="d-none">PORAŻKA</span>
                                                        <i class="fas fa-tint fa-2x" data-toggle="tooltip" data-placement="top" title="" data-original-title="Porażka"></i>
                                                    </div>
                                                @endif
                                            @elseif($match->result == 3)
                                            <div class="col-auto rozgrywki-wynik rozgrywki-remis">
                                                    <span class="d-none">REMIS</span>
                                                    <i class="fas fa-shield-alt fa-2x" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remis"></i>
                                                </div>
                                            @elseif($match->result == 4)
                                            <div class="col-auto rozgrywki-wynik rozgrywki-anulowany">
                                                    <span class="d-none">ANULOWANY</span>
                                                    <i class="fas fa-ban fa-2x" data-toggle="tooltip" data-placement="top" title="" data-original-title="Anulowany"></i>
                                                </div>
                                            @else
                                            <div class="col-auto rozgrywki-wynik rozgrywki-remis" style="color:#91979A!important; background-color:transparent!important; border-color:transparent!important;" >
                                                    <span class="d-none">OCZEKIWANIE NA WYNIK</span>
                                                    <i class="fas fa-clock fa-2x" data-toggle="tooltip" data-placement="top" title="" data-original-title="Oczekiwanie na wynik"></i>
                                                </div>
                                            @endif
                                        </div>
                                        
                                        <div class="col-6 col-lg-auto rozgrywki-data">
                                            {{date('d.m.Y',strtotime($match->match_date))}}
                                            
                                        </div>
                                        <div class="col-12 col-lg rozgrywki-przeciwnik">
                                            @if($match->host_clan == $clan->id)
                                            @if($match->enemy_clan != NULL)
                                            <a href="{{route('clan.clan',['link'=>$match->enemyClan->link])}}">[{{{$match->enemyClan->tag}}}] {{$match->enemyClan->name}}</a> <p class="mb0 mt-5"><small class="text-muted">{{$match->game->name}} <span class="badge badge-muted">{{$match->platform->name}}</span></small></p>
                                            @else 
                                            Oczekiwanie na przeciwnika <p class="mb0 mt-5"><small class="text-muted">{{$match->game->name}} <span class="badge badge-muted">{{$match->platform->name}}</span></small></p>
                                            @endif
                                            @else 
                                            <a href="{{route('clan.clan',['link'=>$match->hostClan->link])}}">[{{{$match->hostClan->tag}}}] {{$match->hostClan->name}}</a> <p class="mb0 mt-5"><small class="text-muted">{{$match->game->name}} <span class="badge badge-muted">{{$match->platform->name}}</span></small></p>
                                            @endif
                                            <div class="rozgrywki-szczegoly">
                                                <a href="{{route('match.friendly.show',['id'=>$match->id])}}" class="btn btn-secondary btn-sm">Zobacz</a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </li>
                                @endforeach
                                
                            </ul>
                        </div>

                    </div>
                </div>

        </div>
    </div>
</div>

@endsection
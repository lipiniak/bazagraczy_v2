@extends('layouts.app')

@section('content')

@if (Session::has('fail'))
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#teamCreate').modal('show');
        });
    </script>
@endif
@if (Session::has('edit'))
    <script type="text/javascript">
        $(window).on('load',function(){
            $('#EdytujDruzyne{{Session::get("edit")}}').modal('show');
        });
    </script>
@endif
@if(Auth::user()->id == $clan->leader_id)
<script>
    $('#top-alerts').addClass('lider-show');
</script>
@else
<script>
    $('#top-alerts .lider-only').closest('.alert').css('display','none');
    
</script>
@endif
<div id="klan" class="content">
        <div class="container-fluid nawigacja">
                <div class="row">
                    <div class="col-12 mb5">
                        KLANY
                        <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.list')}}">Klany i członkostwa</a></span>
                        <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.clan',['link'=>$clan->link])}}">{{$clan->name}}</a></span>
                        <span class="text-nowrap"><i class="fas fa-chevron-right"></i> <a href="{{route('clan.clan',['link'=>$clan->link])}}">O klanie</a></span>
                    </div>
                    <div class="col-12">
                        <span class="text-nowrap"><i class="fas fa-chevron-circle-left"></i> <a href="{{route('clan.search')}}">Wyszukiwarka klanów</a></span>
                    </div>
                </div>
            </div>
        @include('clan.menu')
    <div class="container-fluid">
        <div class="row">


            <div class="col-12 col-xl-6 mb50">

                <div class="avatar-wrapper">
                    <div class="avatar">
                        @if($clan->logo == 'no-clan-logo.jpg')
                        <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="menu-avatar img-fluid klan-def-border" alt="Logo klanowe">
                        @else 
                        <img src="{{ Storage::url($clan->logo)}}" class="img-fluid klan-def-border" alt="Logo klanu">
                        @endif

                    </div>
                </div>

                <div class="profil-akcje-wrapper">
                    <div>
                        <h1 class="mb10">{{$clan->name}} [{{$clan->tag}}]</h1>
                    </div>
                    <div class="akcje-obcy">
                    @if($clan->recruitment == 1) 
                        @if($clan->allMembers()->where('users_id',Auth::user()->id)->get()->isEmpty())
                        <a class="btn btn-primary" data-toggle="modal" data-target="#dolaczDoKlanuID1">Dołącz do klanu</a>
                        <div class="modal fade" id="dolaczDoKlanuID1" tabindex="-1" role="dialog" aria-labelledby="dolaczDoKlanuID1Title" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Dołącz do klanu</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body text-left">
                                        <p class="mb10">Przed dołączeniem do klanu, jego lider będzie musiał zaakceptować twoje zgłoszenie.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                                        <a href='/klan/{{$clan->id}}/dolacz' class="btn btn-primary">Wyślij zgłoszenie</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @elseif($clan->aplicants()->where('users_id',Auth::user()->id)->get()->count()  == 1)
                        <a class="btn btn-primary disabled" data-toggle="modal" data-target="#dolaczDoKlanuID1" disabled>Oczekuje na akceptację</a>
                        @endif
                    @endif
                        @if(Auth::user()->id == $clan->leader_id)
                        <a href="/klan/{{$clan->id}}/czlonkowie" class="btn btn-secondary">Członkowie @if($clan->aplicants()->count() > 0) <span class="badge badge-danger menu-badge">{{$clan->aplicants()->count()}}</span>@endif</a>
                        <a href="/klan/{{$clan->id}}/edytuj" class="btn btn-secondary">Edytuj</a>
                        @endif
                        @if(Auth::user()->id != $clan->leader_id)
                        <a class="btn btn-secondary" data-toggle="modal" data-target="#profilZglos">Zgłoś</a>
                        <div class="modal fade" id="profilZglos" tabindex="-1" role="dialog" aria-labelledby="profilZglosTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <form style="width:100%" method='post' action='/zglos/klan'>
                                    {{ csrf_field() }}
                                    
                                <input type='hidden' name='clan_id' value="{{$clan->id}}">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Zgłoszenie profilu gracza</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-left">
                                            <div class="form-group">
                                                
                                                <p class="text-muted"><small>Opisz w paru słowach czego dotyczy problem.</small></p>
                                                <label class="form-label-custom" for="profil-zglos">Treść zgłoszenia</label>
                                                <textarea class="form-control" name='report_klan_{{$clan->id}}'id="profil-zglos" rows="3" style="min-height:80px;"></textarea>
                                                
                                                @if ($errors->has('report_klan_'.$clan->id))
                                            <div class="error-feedback">
                                                {!! $errors->first('report_klan_'.$clan->id) !!}<br>
                                            </div>  
                                            @endif
                                            </div> 
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary">Wyślij</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @endif
                        <div class="modal fade" id="profilZablokuj" tabindex="-1" role="dialog" aria-labelledby="profilZablokujTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="profilZablokujTitle">Uwaga!</h5>
                                    </div>
                                    <div class="modal-body text-center">
                                        <p class="mb10">Czy na pewno chcesz dodać</p> <p class="mb10"><strong>NazwaGracza</strong></p> <p class="mb0">do zablokowanych?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary">Tak</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Nie</button>                  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="mb0"><small class="text-muted">Data utworzenia: <span class="text-nowrap">{{$clan->created_at->format('d.m.Y')}}</span></small></p>
                    <p><small class="text-muted">Ostatnia aktywność: <span class="text-nowrap">{{$clan->updated_at->format('d.m.Y, H:i')}}</span></small></p>
                    <div class="row">
                        <div class="col-12">
                            <div class="czlonkostwo-wrapper mb20">
                                <div class="logo-klanu-wrapper">
                                    <a href="/profil/{{$clan->leader->id}}">
                                        <div class="logo-klanu">
                                            @if($clan->leader->avatar == 'no-avatar.jpg')
                                                <img src="{{asset('images/avatars/')}}/{{$clan->leader->avatar}}" class="menu-avatar" alt="Avatar">
                                            @else
                                                <img src="{{ Storage::url($clan->leader->avatar) }}" class="menu-avatar" alt="Avatar">
                                            @endif
                                        </div>
                                    </a>
                                </div>
                                <div class="czlonkostwo-info text-left">
                                    <p class="mb0">Lider</p>
                                    <h5 class="mb5"><span class="inline-status {{$clan->leader->getStatus()}}"></span> <a href="/profil/{{$clan->leader->id}}">{{$clan->leader->login}}</a></h5>
                                    @if(Auth::user()->id != $clan->leader_id)
                                    <a href="#" onclick='showConversation({{$clan->leader->id}},"{{$clan->leader->login}}")' class="btn btn-secondary btn-sm">Wyślij wiadomość</a>
                                    @endif
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    @if($clan->recruitment == 1)
                    <div class="row">
                        <div class="col-12">
                            <p class="mb5 text-left"><span class="color-info"><i class="fas fa-exclamation-circle"></i> Ten klan poszukuje graczy</span></p>
                            @if(!empty($clan->recruitment_info))
                            <div class="twoja-notatka text-left">
                                <h6 class="text-muted">Info</h6>
                                <p class="mb0">{!!nl2br(e($clan->recruitment_info))!!}</p>
                            </div>
                            @endif
                        </div>
                    </div>
                    @endif
                </div>

                <div class="clearfix"></div>

            </div>

            @if(!empty($clan->description))
            <div class="col-12 mb50">
                <h3 class="h-border-bottom">Opis klanu</h3>
                <div class="row">
                    <div class="col-12">
                        <p>{!!nl2br(e($clan->description))!!}</p>
                    </div>
                </div>
            </div>
            @endif


            @if($clan->posts()->count() > 0 || Auth::user()->id == $clan->leader_id)
            <div class="col-12 mb50">
                <h3 class="h-border-bottom">Aktualności</h3>
                @if(Auth::user()->id == $clan->leader_id)
                <div class="row">
                    <div class="col-12">
                        <div class="clannews-outer">
                            
                            {{ Form::open(['route' => 'clan.post'],['autocomplete'=>'off']) }}
                                <input type="hidden" name="clan_id" value="{{$clan->id}}">
                                <div class="form-group">
                                    <label for="new-clan-news-text" class="form-label-custom">Treść *</label>
                                    <textarea name="content" class="form-control" id="new-clan-news-text" rows="3" style="min-height:80px;"></textarea>
                                    @if ($errors->has('content'))
                                        <div class="error-feedback">
                                            {{ $errors->first('content') }}<br>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group text-right mb0">
                                    <button type="submit" class="btn btn-primary">Opublikuj</button>
                                </div>
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
                @endif
                @foreach($clan->posts()->orderBy('created_at','DESC')->get() as $post)
                <div class="row">
                    <div class="col-12">

                        <p class="mb5 text-muted"><small>Dodano: {{$post->created_at->format('d.m.Y, H:i')}}</small></p>
                        <div class="twoja-notatka klan-news">
                            <p>{!!nl2br(e($post->content))!!}</p>
                            @if(Auth::user()->id == $clan->leader_id)
                            <div class="text-right">

                                <hr class="separator">

                                <a class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#EditPost{{$post->id}}">Edytuj</a>
                                <div class="modal fade" id="EditPost{{$post->id}}" tabindex="-1" role="dialog" aria-labelledby="EditPostIDTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <form style="width: 100%;" method="POST" action="{{route('clan.post.post.edit')}}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="post_id" value="{{$post->id}}">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Edycja posta</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body text-left">
                                                    <div class="form-group">
                                                        <label class="form-label-custom" for="new-clan-news-text-edit">Treść *</label>
                                                        <textarea class="form-control" name="editPost{{$post->id}}"id="new-clan-news-text-edit" rows="3" style="min-height:80px;">{{$post->content}}</textarea>
                                                        @if ($errors->has('editPost'.$post->id))
                                                            <div class="error-feedback">
                                                                {{ $errors->first('editPost'.$post->id) }}<br>
                                                            </div>
                                                        @endif
                                                    </div> 
                                                </div>
                                                
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Anuluj</button>
                                                    <button type="submit" class="btn btn-primary">Zapisz</button>
                                                </div>
                                                
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                
                                <a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deletePost{{$post->id}}">Usuń</a>
                                <div class="modal fade" id="deletePost{{$post->id}}" tabindex="-1" role="dialog" aria-labelledby="deletePost{{$post->id}}" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Uwaga!</h5>
                                            </div>
                                            <div class="modal-body text-center">
                                                <p class="mb10">Czy na pewno chcesz usunąć posta?</p>
                                            </div>
                                            <div class="modal-footer">
                                                    
                                                    
                                                <button type="button" class="btn btn-primary" onclick="event.preventDefault();
                                                document.getElementById('deletePost-{{$post->id}}').submit();">Tak</button>
                                                <form id="deletePost-{{$post->id}}" action="{{ route('clan.post.delete') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" value="{{$post->id}}" name="post_id">
                                                </form>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Nie</button>                  
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endif

        </div>
    </div>
</div>

@endsection
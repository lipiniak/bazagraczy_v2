@extends('layouts.app')

@section('content')
<script>
        
    $(document).ready(function () {
        $('#wyszukiwarka-tytul').select2();
    });
    $(document).ready(function () {
        $('#wyszukiwarka-platforma').select2();
    });
    $(document).ready(function () {
        $('#wyszukiwarka-wojewodztwo').select2();
    });
    $(document).ready(function () {
        $('#wyszukiwarka-miasto').select2();
    });
    $(document).ready(function () {
        $('#wyszukiwarka-wiek').select2();
    });
    $(document).ready(function () {
        $('#wyszukiwarka-plec').select2();
    });
    $(document).ready(function () {
        $('#wyszukiwarka-filtry').select2();
    });
</script>
<div id="wyszukiwarka" class="content">

    <div class="container-fluid">

        <div class="row">

            <div class="col-12">

                    {{ Form::open(['route' => 'clan.do.search'],['autocomplete'=>'off']) }}
                    <h1 class="mb15">Wyszukiwarka klanów</h1>
                    <input type='hidden' name='game' value='{{$user_select["game_id"]}}'>
                    <input type='hidden' name='platform' value='{{$user_select["platform_id"]}}'>
                    <div class="wyszukiwarka-form-wrapper">
                        <div class="row">
                            <!--
                            <div class="col-12 col-lg-6 col-xl-4">
                                <div class="form-group mb5" style="width:100%">
                                    <label class="form-label-custom" for="wyszukiwarka-tytul">Tytuł gry</label>
                                    {{Form::select('game',$data['games'],null,['class'=> 'form-control','id'=>'wyszukiwarka-tytul','style'=>'width:100%'])}}
                                        <script>
                                                $('#wyszukiwarka-tytul').select2({
                                                    
                                                  }).on('change',function() {
                                                      
                                                      var value= $('#wyszukiwarka-tytul option:selected').attr('value');
            
                                                      if(value == 0) {
                                                        $('#wyszukiwarka-platforma').empty();
                                                        $("#wyszukiwarka-platforma").select2();
                                                      }

                                                      var url = "/helper/platforms/"+value;
                                                        $.ajax({
                                                            type: "GET",
                                                            url: url,
                                                            success: function(data)
                                                            {
                                                                
                                                                $('#wyszukiwarka-platforma').empty();
                                                                $("#wyszukiwarka-platforma").select2({
                                                                    data: data
                                                                });
                                                            }
                                                        });
                                                      
                                                });
            
                                                </script>
                                </div>
                            </div> 
                            <div class="col-12 col-lg-6 col-xl-4">
                                <div class="form-group mb5" style="width:100%">
                                    <label class="form-label-custom" for="wyszukiwarka-platforma">Platforma</label>
                                    @if(session()->has('platform'))
                                        <?php $city = session('platform');?>
                                        @endif
                                        {{Form::select('platform',$data['platform'],null,['class'=> 'form-control','id'=>'wyszukiwarka-platforma','style'=>'width:100%'])}}
                                        <script>
                                                $("#wyszukiwarka-platforma").select2();
                                        </script>
                                </div>
                            </div> -->

                            <div class="col-12 col-lg-6 col-xl-4">
                                    <div class="form-group mb5" style="width:100%">
                                        <label class="form-label-custom">Nazwa <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Wyszukuje podaną frazę wśród nazw i tagów klanowych."><i class="fas fa-question-circle"></i></a></label>
                                        <input type="text" class="form-control"  id="wyszukiwarka-fraza" name="keyword" maxlength="25" value="@if(isset($formData['keyword'])) {{$formData['keyword']}} @endif">
                                    </div>
                                </div>
                            <div class="col-12 col-lg-6 col-xl-4">
                                <div class="form-group mb5" style="width:100%">
                                    <label class="form-label-custom" for="wyszukiwarka-filtry">Filtry</label>
                                    <select name='filters[]' class="form-control js-example-basic-multiple" id="wyszukiwarka-filtry" name="filtry[]" multiple="multiple" style="width:100%">
                                        @if(isset($formData['filters']))
                                        @foreach($formData['filters'] as $filter)
                                            <?php $filters[$filter] = $filter ;?>
                                            
                                        @endforeach
                                            <option value="1" <?php echo isset($filters[1]) ? 'selected' : null; ?>>Otwarta rekrutacja</option>
                                            
                                        @else
                                            <option value="1">Otwarta rekrutacja</option>
                                        @endif
                                        
                                        
                                    </select>
                                </div>
                            </div> 

                            
                            
                            <div class="col-12 col-md-12 col-xl-4">
                                <div class="btn-group special mb5" role="group" style="margin-top:22px">
                                    <a href='/klan/szukaj'class="btn btn-secondary">Resetuj</a>
                                    <button type='submit' class="btn btn-primary" @if($user_select['game_id'] == 0) disabled @endif>Wyszukaj</a>
                                </div>
                            </div> 
                        </div>
                    </div>
                </form>

                <div class="row">
                    <div class="col-12">
                        @if(empty($clans))    
                        <!-- Brak wyników /start -->
                        <p style="clear:both">Brak wyników spełniających wybrane kryteria. Spróbuj ponownie.</p>
                        <!-- Brak wyników /koniec -->
                        @else
                        <!-- Pojedynczy wynik wyszukiwania /start -->
                        @foreach($clans as $clan)
                        <div class="wynik-wrapper">
                            <div class="avatar-wrapper">
                                <a href='/klan/{{$clan->link}}'>
                                @if($clan->logo == 'no-clan-logo.jpg')
                                    <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="menu-avatar img-fluid klan-def-border" alt="Logo klanowe">
                                    @else 
                                    <img src="{{ Storage::url($clan->logo)}}" class="img-fluid klan-def-border" alt="Logo klanu">
                                    @endif
                                </a>
                            </div>
                            <div class="nazwa-klanu">
                                [{{$clan->tag}}] {{$clan->name}}
                            </div>
                            <div class="gracz-info">
                                <div class="btn-group special" role="group" aria-label="...">
                                    <a href='/klan/{{$clan->link}}'class="btn btn-secondary">Zobacz klan</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <!-- Pojedynczy wynik wyszukiwania /koniec -->
                        @endif
                    </div>
                </div>



            </div>

        </div>

    </div>

</div>
@endsection


<li class="user-result-wrapper @if($user->id == Auth::user()->id) owner @endif {{$user->lobbyStatus(Auth::user()->id)}}" id='user-{{$user->id}}'>
    <div class="user-result-inner">
        <div id="_user_{{$user->id}}_status" class="status-color {{$user->getStatus()}}"></div>
        <div class="user-result-avatar">
                @if($user->avatar == 'no-avatar.jpg')
                <img src="{{asset('images/avatars/')}}/{{$user->avatar}}" class="menu-avatar" alt="Avatar">
            @else
                <img src="{{ Storage::url($user->avatar) }}" class="menu-avatar" alt="Avatar">
            @endif
        </div>
        <p class="mb0"><span>{{$user->login}}</span></p>
        <p class="user-description">
            @if($user->lobbyInfo($user->selectedGame()['game_id'],$user->selectedGame()['platform_id'])==NULL)
                brak info
            @else 
                {{$user->lobbyInfo($user->selectedGame()['game_id'],$user->selectedGame()['platform_id'])}}
            @endif
        </p>
        @if($user->id != Auth::user()->id)
        <div class="dropdown" style="display: inline-block;">
            <a class="btn btn-secondary btn-sm dropdown-toggle m0 mr3 mb3" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Oznacz</a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="#" onclick='markStatus({{$user->id}},"like")'>Lubię</a>
                <a class="dropdown-item" href="#" onclick='markStatus({{$user->id}},"dislike")'>Nie lubię</a>
                <a class="dropdown-item" href="#" onclick='markStatus({{$user->id}},"neutral")'>Usuń oznaczenie</a>
            </div>
        </div>
        @endif
        <a href='/profil/{{$user->id}}'class="btn btn-secondary btn-sm mr3 mb3">Profil</a>

        <?php
            $note = $user->note()->where('user_id',Auth::user()->id)->get();
        ?>
        @if($user->id != Auth::user()->id)
            @if(!empty($note[0]))
            <a class="btn btn-secondary btn-sm mr3 mb3" data-toggle="modal" data-target="#IdGraczaNotatka">Notatka</a>
            <div class="modal fade" id="IdGraczaNotatka" tabindex="-1" role="dialog" aria-labelledby="IdGraczaNotatkaTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <form>
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">{{$user->id}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body text-left">
                                <div class="twoja-notatka">
                                    <h6 class="text-muted">Notatka</h6>
                                    <p class="mb0">{!! nl2br(e($note[0]->note))!!}</p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endif
            <a class="btn btn-secondary btn-sm mr3 mb3" data-toggle="modal" data-target="#RozmowaUser001" onclick='showConversation({{$user->id}},"{{$user->login}}")'>Rozmowa</a>
        @endif
        <div class="clearfix"></div>
    </div>
</li>
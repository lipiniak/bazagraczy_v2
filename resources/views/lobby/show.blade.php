@extends('layouts.app')

@section('content')
<script>
            $(window).on('load', function() {
                var user_id = {{Auth::user()->id}};
                
                $.ajax({url: '/poczekalnia/join/'+user_id, success: function(result){
                
                }})
            });

            $(window).on('beforeunload',function() {
                
                $.ajax({url: '/poczekalnia/leave/'+user_id, success: function(result){
                
                }})
            });

            var game_id = {{$selectedGames['game_id']}};
            var platform_id = {{$selectedGames['platform_id']}};
            var user_id = {{Auth::user()->id}};
            
            var channel = Echo.join('lobby-'+game_id+'-'+platform_id)
            .here((users) => {
                
                $.each(users,function(index){
                    $.ajax({url: '/poczekalnia/user/'+users[index].id, success: function(result){
                        $('#poczekalnia').find('#user-'+user_id).remove();
                        if(users[index].status == 'szukam') {
                            if(users[index].id == user_id) {
                                $("#szukam").addClass('active')
                                $('#WaitingUL').prepend(result);
                            } else {
                                $('#WaitingUL').append(result);
                            }    
                        }
                        if(users[index].status == 'gram') {
                            if(users[index].id == user_id) {
                                $("#gram").addClass('active')
                                $('#InGameUL').prepend(result);
                            } else {
                                $('#InGameUL').append(result);
                            }
                        }
                        if(users[index].status == 'afk') {
                            if(users[index].id == user_id) {
                                $("#afk").addClass('active')
                                $('#AfkUL').prepend(result);
                            } else {
                                $('#AfkUL').append(result);
                            }
                                
                        }     
                    }})
                })
            })
            .joining((user) => {
                $.ajax({url: '/poczekalnia/user/'+user.id, success: function(result){
                    $('#WaitingUL').append(result);
                }})
            })
            .leaving((user) => {
                $('#user-'+user.id).remove();
                $.ajax({url: '/poczekalnia/set/szukam/'+user.id, success: function(result){
                
                }})
                
                
            }).listenForWhisper('change_status', (e) => {
                switch(e.status) {
                    case 'szukam':
                        var element = $('#user-'+e.user_id);
                        
                        element.detach().appendTo($('#WaitingUL'));
                    break;
                    case 'gram':
                        var element = $('#user-'+e.user_id);
                        element.detach().appendTo($('#InGameUL'));
                    break;
                    case 'afk':
                        var element = $('#user-'+e.user_id);
                        element.detach().appendTo($('#AfkUL'));
                    break;
                }
            }).listen('userJoinLobby', (e) => {
                //
            });;
    
            
            
        
        function changeStatus(value) {
            
            var status = value;
            switch(status) {
                case 'szukam':
                    var element = $('#user-'+user_id);
                    element.detach().prependTo($('#WaitingUL'));
                break;
                case 'gram':
                    var element = $('#user-'+user_id);
                    element.detach().prependTo($('#InGameUL'));
                break;
                case 'afk':
                    var element = $('#user-'+user_id);
                    element.detach().prependTo($('#AfkUL'));
                break;
            }
            
            $.ajax({url: '/poczekalnia/set/'+status+'/'+user_id, success: function(result){
                
            }})

            channel.whisper('change_status', {
                status: status,
                user_id: user_id
            })
        }

        function markStatus(user_id,status) {
            var userElement = $('#user-'+user_id);

            userElement.removeClass('like');
            userElement.removeClass('dislike');
            if(status != 'neutral') {
                userElement.addClass(status);
            }
        
            $.ajax({url: '/poczekalnia/mark/'+status+'/'+user_id, success: function(result){
                
            }})
            
                
        }
</script>
<div id="poczekalnia" class="content">

        <div class="container-fluid">

            <div class="row">

                <div class="col-12">
                    <h1 class="mb5">Poczekalnia</h1>
                    <p class="text-muted"><a class="text-muted-link" data-toggle="modal" data-target="#InfoZawartosc">{{$selectedGames['game']}} <span class="badge badge-muted">{{$selectedGames['platform']}}</span></a><p>
                    <div class="modal fade" id="InfoZawartosc" tabindex="-1" role="dialog" aria-labelledby="InfoZawartoscTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Info</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body text-left">
                                        <div class="twoja-notatka text-muted">
                                            <p class="mb15">Zawartość aktualnie przeglądanej strony, jej treść oraz wyniki, zawężona jest do treści związanych z <span class="text-white">{{$selectedGames['game']}}</span> <span class="badge badge-white">{{$selectedGames['platform']}}</span></p>
                                            <p class="mb0">Aby zmienić, wybierz inną grę w prawym górnym rogu aplikacji.</p>
                                        </div>
                                    </div>
                                </div>
                            
                        </div>
                    </div>
                </div>

                <div class="col-12 text-center mb30">
                    <a class="btn btn-primary btn-sm mb5" data-toggle="modal" data-target="#dodajInfo"><strong>USTAW INFO</strong></a>
                    <div class="modal fade" id="dodajInfo" role="dialog" aria-labelledby="dodajInfoTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            
                            <div class="modal-content">
                                <form method="POST" action="{{route('lobby.set.info')}}">
                                {{ csrf_field() }} 
                                    <div class="modal-header">
                                        <h5 class="modal-title">Ustaw info</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body text-left">
                                        <p class="mb10 text-muted">W kilku słowach podaj swoje wymagania lub preferencje, co do rozgrywki, lub osób, z którymi masz zamiar grać.</p>
                                        <div class="form-group text-left" style="width:100%">
                                            <input type='hidden' name='game_id' value="{{$selectedGames['game_id']}}">
                                            <input type='hidden' name='platform_id' value="{{$selectedGames['platform_id']}}">
                                            <input type="text" name="info" class="form-control" id="" placeholder="Info....">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                                        <button type="submit" class="btn btn-primary">Zapisz</button>
                                    </div>
                                </form>
                                <script>

                                </script>
                            </div>
                        
                        </div>
                    </div>
                    <p class="user-swoj-opis">
                            @if(Auth::user()->lobbyInfo(Auth::user()->selectedGame()['game_id'],Auth::user()->selectedGame()['platform_id']) == NULL)
                            brak info
                        @else 
                            {{Auth::user()->lobbyInfo(Auth::user()->selectedGame()['game_id'],Auth::user()->selectedGame()['platform_id'])}}
                        @endif
                    </p>
                    <p class="mb5 text-muted"><small><strong>WYBIERZ STATUS</strong></small></p>
                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                        <label class="btn btn-primary btn-sm"  id="szukam"  onclick="changeStatus('szukam')">
                            <input type="radio" name="options" autocomplete="off" value='szukam'> <strong>SZUKAM</strong>
                        </label>
                        <label class="btn btn-primary btn-sm"  onclick="changeStatus('gram')" id="gram">
                            <input type="radio" name="options"  autocomplete="off" value='gram'> <strong>GRAM</strong>
                        </label>
                        <label class="btn btn-primary btn-sm" onclick="changeStatus('afk')" id="afk">
                            <input type="radio" name="options" autocomplete="off" value='afk "'> <strong>AFK</strong>
                        </label>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-12 col-xl-6 mb30">
                    <h5 class="section-header">Szukam do gry</h5>
                    <div class="alert alert-info">
                        <div class="row">
                            <div class="col-12">
                                <input type="text" id="WaitingInput" onkeyup="FilterWaiting()" placeholder="Filtruj...">
                                <ul id="WaitingUL">

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-xl-6 mb30">
                    <h5 class="section-header">W grze</h5>
                    <div class="alert alert-info">
                        <div class="row">
                            <div class="col-12">
                                <input type="text" id="InGameInput" onkeyup="FilterInGame()" placeholder="Filtruj...">
                                <ul id="InGameUL">

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 mb30">

                    <h5 class="section-header">Afk</h5>
                    <div class="alert alert-info">
                        <div class="row">
                            <div class="col-12">
                                <ul class="clearUL" id='AfkUL'>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

@endsection
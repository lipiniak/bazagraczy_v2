@extends('admin.layouts.admin')
@section('page_title', 'Podgląd posta')
@section('page_subtitle', 'wraz z komentarzem.')

@section('content')
          <div class="row">
            <div class="col-xs-6">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Post id: {{$post->id}}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <b>Id</b>
                    <p>{{$post->id}}</p>

                    <b>Gra</b>
                    <p>{{$post->game->name}}</p>

                    <b>Platforma</b>
                    <p>{{$post->platform->name}}</p>
                    
                    <b>Autor</b>
                    <p><a href="{{route('admin.user.show',['id'=>$post->poster->id])}}">{{$post->poster->login}}</a></p>
                    

                    <b>Treść</b>
                    <p>{{$post->text}}</p>

                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
              <!-- /.box -->
            </div>
            <div class="col-xs-6">
                    <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Komentarze</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        
                          @if($post->comments->count() > 0)
                          <div class="box-footer box-comments" id='comments-{{$post->id}}'>
                              @foreach($post->comments as $comment)
                              <div class="box-comment">
                                  
                                  @if($comment->user->avatar == 'no-avatar.jpg')
                                      <img class="profile-user-img img-responsive img-circle" src="{{asset('images/avatars/')}}/{{$comment->user->avatar}}" alt="User profile picture">
                                  @else
                                      <img class="profile-user-img img-responsive img-circle" src="{{ Storage::url($comment->user->avatar) }}" alt="User profile picture">  
                                  @endif
                  
                                  <div class="comment-text">
                                      <span class="username">
                                          {{$comment->user->login}}
                                          <span class="text-muted pull-right">{{date('d.m.Y',strtotime($comment->created_at))}}</span>
                                      </span><!-- /.username -->
                                  {{$comment->text}}
                                  </div>
                                  
                              </div>
                              @endforeach
                          </div>
                          @endif
                          
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    <!-- /.box -->
                  </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        
      <script>
            $(document).ready( function () {
                $('#example1').DataTable();
            } );
          </script>
@endsection
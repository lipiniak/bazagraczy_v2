@extends('admin.layouts.admin')
@section('page_title', 'Posty')
@section('page_subtitle', 'Posty na tablicach')


@section('content')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">


  

          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Lista postów</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>Id</th>
                      <th>Nazwa Gry</th>
                      <th>Nazwa Platformy</th>
                      <th>Autor</th>
                      <th>Ilość komentarzy</th>
                      <th>Data</th>
                      <th>Akcje</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                    <tr>
                      <td>{{$post->id}}</td>
                      <td>{{$post->game->name}}
                      </td>
                      <td>{{$post->platform->name}}</td>
                      <td>{{$post->user->login}}</td>
                      <td>{{$post->comments->count()}}</td>
                      <td>{{$post->created_at}}</td>
                      <td>
                        <div class="btn-group">
                          <a href='{{route("admin.post.show",["id"=>$post->id])}}' class="btn btn-primary"><i class="fa fa-eye"></i></a>
                          <a href='{{route("admin.post.edit",["id"=>$post->id])}}' class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#permission-remove-{{$post->id}}">
                              <i class="fa fa-remove"></i>
                          </button>
                          <div class="modal modal-danger fade" id="permission-remove-{{$post->id}}" style="display: none;">
                                  <div class="modal-dialog">
                                      <div class="modal-content">
                                      <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">×</span></button>
                                          <h4 class="modal-title">Usuwanie uprawnienia</h4>
                                      </div>
                                      <div class="modal-body">
                                          <p>Czy na pewno chcesz usunąć to uprawnienie? Jest to nieodwracalne i uprawnienie będzie usunięte ze wszystkich powiązanych ról.</p>
                                      </div>
                                      <div class="modal-footer">
                                          <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Nie</button>
                                          <a href='{{route("admin.post.delete",["id"=>$post->id])}}' class="btn btn-outline">Tak</a>
                                      </div>
                                      </div>
                                      <!-- /.modal-content -->
                                  </div>
                                  <!-- /.modal-dialog -->
                              </div>
                          </div>
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Id</th>
                        <th>Nazwa Gry</th>
                        <th>Nazwa Platformy</th>
                        <th>Autor</th>
                        <th>Ilość komentarzy</th>
                        <th>Data</th>
                        <th>Akcje</th>
                    </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
              <!-- /.box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
       
      <script>
            $(document).ready( function () {
                $('#example1').DataTable({
                  "order": [[ 5, "desc" ]]
                });
            } );
          </script>
@endsection
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        

        @foreach(Navigation::getMenu(3) as $item)
            @if(empty($item['childrens']))
                <li>
                    <a href="{{route($item['route'])}}">
                        <i class="fa fa-{{$item['icon']}}"></i> 
                        <span>{{$item['name']}}</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                </li>
            @else
                <li class="treeview">
                    <a href="{{route($item['route'])}}">
                        <i class="fa fa-{{$item['icon']}}"></i> 
                        <span>{{$item['name']}}</span>
                        <span class="pull-right-container">
                        <span class="label label-primary pull-right"></span>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @foreach($item['childrens'] as $children)
                            <li><a href="{{route($item['route'])}}"><i class="fa fa-{{$children['icon']}}"></i> {{$children['name']}}</a></li>
                        @endforeach
                    </ul>
                </li>
            @endif
        @endforeach
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
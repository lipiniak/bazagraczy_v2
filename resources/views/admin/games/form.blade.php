<div class="form-group">
    {!! Form::label('name', 'Nazwa:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('description', 'Opis:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('img', 'Obrazek:') !!}
    {!! Form::text('img', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('feeds', 'Feeds:') !!}
    
    <select type='text' id='feeds' multiple='multiple' class="form-control" name='feeds[]'>
        @if(isset($game)) 
        <?php 

            $feeds = json_decode($game->feeds,true);
        ?>
        @if(!empty($feeds))
            @foreach(json_decode($game->feeds,true) as $feed)
                <option value='{{$feed["name"]}}' selected>{{$feed['name']}}</option>
            @endforeach
            @endif
        @endif
    </select>
</div>
<div class="form-group">
    {!! Form::label('platforms', 'Platforma:') !!}
    <select name='platforms[]' class="form-control" multiple="multiple" id='editPlatforms'>
        @foreach($platformsAll as $platform)
            @if(isset($game))
            @foreach($game->platforms as $item)
            <option value="{{$platform->id}}" @if($item->id == $platform->id) selected @endif>{{$platform->short_name}}</option>
            @endforeach
            @else
            <option value="{{$platform->id}}" >{{$platform->short_name}}</option>
            @endif
        @endforeach
    </select>
    
    <script>
        $('#editPlatforms').select2({
            tags: true
        });
        $('#feeds').select2({
            tags: true
        });
    </script>
</div>

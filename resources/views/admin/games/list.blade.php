@extends('admin.layouts.admin')

@section('page_title', 'Gry')
@section('page_subtitle', 'Lista gier')

@section('content')

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Lista gier</h3>
        <div class='pull-right box-tools'>
        <a href="{{route('admin.game.create')}}" class='btn btn-success btn-sm'><i class="fa fa-plus"></i></a>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-hover">
          <thead>
          <tr>
                  <th>Id</th>
                  <th>Nazwa</th>
                  <th>Platformy</th>
                  <th>Data</th>
                  <th>Opcje</th>
          </tr>
          </thead>
          <tbody>
              
          @foreach($gierki as $game)
          <tr>
            <td>{{$game->id}}</td>
            <td>{{$game->name}}
            </td>
            
            <td>
                @foreach($game->platforms as $platform)
                  {{$platform->short_name}}, 
                @endforeach
            </td>
            <td>{{$game->created_at}}</td>
            <td> 
                <div class="btn-group">
                  <a href="{{route('admin.game.show',['id'=>$game->id])}}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                  <a href="{{route('admin.game.edit', ['id'=>$game->id])}}" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                </div>
              </td>
          </tr>
          @endforeach
          </tbody>
          <tfoot>
          <tr>
              <th>Id</th>
              <th>Nazwa</th>
              <th>Platformy</th>
              <th>Data</th>
              <th>Opcje</th>
          </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

      <script>
            $(document).ready( function () {
                $('#example1').DataTable();
            } );
          </script>
@endsection
@extends('admin.layouts.admin')
@section('page_title', 'Gry')
@section('page_subtitle', 'Dodaj nową grę')
@section('content')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
<script src="{{ asset('js/select2.full.js') }}"></script>        

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Dodaj nową grę</h3>
        <div class='pull-right box-tools'>
              <a href="{{route('admin.game.list')}}" class='btn btn-sm btn-primary'><i class="fa fa-reply"></i></a>
          </div>
      </div>
      <!-- /.box-header -->
      
      <div class="box-body">
              {!! Form::open(['action' => 'Admin\GamesController@saveGame','method'=>'POST']) !!}
                  @include('admin.games.form', ['submitButtonText' => 'Dodaj grę','platforms'=>$platforms])
            
      </div>
      <div class="box-footer">
          {!! Form::submit('Dodaj grę', ['class' => 'btn btn-lg btn-success']) !!}
          {!! Form::close() !!}
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

@endsection
@extends('admin.layouts.admin')
@section('page_title', 'Gry')
@section('page_subtitle', 'Podgląd gry')
@section('content')

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Dane gry</h3>
        <div class='pull-right box-tools'>
            <a href="{{route('admin.game.edit',['id'=>$game->id])}}" class='btn btn-success btn-sm'><i class="fa fa-pencil"></i></a>
              <a href="{{route('admin.game.list')}}" class='btn btn-sm btn-primary'><i class="fa fa-reply"></i></a>
          </div>
      </div>
      <!-- /.box-header -->
      
      <div class="box-body">
          @foreach($game->toArray() as $key => $item)
              <p><strong>{{$key}}</strong> : {{$item}}</p>
          @endforeach
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

@endsection
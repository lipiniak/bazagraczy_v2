@extends('admin.layouts.admin')
@section('page_title', 'Gry')
@section('page_subtitle', 'Scrapowanie gier')
@section('content')

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Scrapowanie gier z portalu gry-online.pl</h3>
        <div class='pull-right box-tools'>
           
          </div>
      </div>
      <!-- /.box-header -->
      
      <div class="box-body">
        Aktualna ilość gier: {{$gameCount}}
        
        
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

@endsection
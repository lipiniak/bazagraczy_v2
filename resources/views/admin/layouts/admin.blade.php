<!DOCTYPE html>
<html>
    <head>
        @include('admin.layouts.includes.header')
    </head>

    <body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                @include('admin.layouts.includes.topbar')
            </header>

            <aside class="main-sidebar">
                @include('admin.layouts.includes.menu')
            </aside>

            <div class="content-wrapper">
                @include('admin.layouts.includes.content')
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                @include('admin.layouts.includes.footer')
            </footer>
        </div>
        @include('admin.layouts.includes.sidebar')

        <!-- jQuery UI 1.11.4 -->
        <script src="{{ asset('admin/components/jquery-ui/jquery-ui.min.js') }}"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
          $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.7 -->
        <script src="{{ asset('admin/components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <!-- Morris.js charts -->
        <script src="{{ asset('admin/components/raphael/raphael.min.js') }}"></script>
        <script src="{{ asset('admin/components/morris.js/morris.min.js') }}"></script>
        <!-- Sparkline -->
        <script src="{{ asset('admin/components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
        <!-- jQuery Knob Chart -->
        <script src="{{ asset('admin/components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
        <!-- daterangepicker -->
        <script src="{{ asset('admin/components/moment/min/moment.min.js') }}"></script>
        <script src="{{ asset('admin/components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
        <!-- datepicker -->
        <script src="{{ asset('admin/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
        <!-- Slimscroll -->
        <script src="{{ asset('admin/components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <!-- FastClick -->
        <script src="{{ asset('admin/components/fastclick/lib/fastclick.js') }}"></script>
        <!--Datatables-->
        
        <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>         
        <!-- AdminLTE App -->
        <script src="{{ asset('admin/js/adminlte.min.js') }}"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->

    </body>
</html>



</body>
</html>

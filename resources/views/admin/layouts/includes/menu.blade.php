<section class="sidebar">
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENU</li>
            @include(config('laravel-menu.views.bootstrap-items'), ['items' => $mainMenu->roots()])
    
        </ul>
    </section>
    
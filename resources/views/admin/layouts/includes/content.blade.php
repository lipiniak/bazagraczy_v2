<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        @yield('page_title')
        <small>@yield('page_subtitle')</small>
    </h1>


     <!--Menu::get('mainMenu')->crumbMenu()->asOl(['class'=>'breadcrumb'])-->

</section>

    <!-- Alerts -->

    <!-- Main content -->
    @if(Session::has('success'))
        @include('admin.layouts.includes.alerts.success')
    @endif
    @if(Session::has('error'))
        @include('admin.layouts.includes.alerts.error')
    @endif
    @if(Session::has('warning'))
        @include('admin.layouts.includes.alerts.warning')
    @endif
    @if(Session::has('prmimary'))
        @include('admin.layouts.includes.alerts.primary')
    @endif


    <section class="content">
        @yield('content')
    </section>
    <!-- /.content -->

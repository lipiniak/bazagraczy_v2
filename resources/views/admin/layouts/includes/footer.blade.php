<div class="pull-right hidden-xs">
    <b>Wersja</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2018-{{date('Y')}} <a href="https://playershub.net">BAZAGRACZY.pl</a>.</strong>
    Wszelkie prawa zastrzeżone.
    
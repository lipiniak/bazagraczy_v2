@extends('admin.layouts.admin')
@section('page_title', 'Zadanie')
@section('page_subtitle', 'Dodaj nowe zadanie')
@section('content')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
<script src="{{ asset('js/select2.full.js') }}"></script>        

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Stwórz nowy giveaway</h3>
        <div class='pull-right box-tools'>
              <a href="{{route('admin.giveaway.list')}}" class='btn btn-sm btn-primary'><i class="fa fa-reply"></i></a>
          </div>
      </div>
      <!-- /.box-header -->
      
      <div class="box-body">
              {!! Form::open(['action' => 'Admin\GiveawayController@store','method'=>'POST','enctype'=>"multipart/form-data"]) !!}
              <div class="form-group">
                    {!! Form::label('title', 'Tytuł:') !!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description', 'Opis konkursu:') !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('main_img', 'Główny obrazek:') !!}
                            {!! Form::file('main_img', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('ending_at', 'Data zakończenia konkursu:') !!}
                            {!! Form::date('ending_at', \Carbon\Carbon::now()); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('ending_at', 'Godzina zakończenia konkursu:') !!}
                            <input type="time" name='ending_time'>
                        </div>
                        

                        <div class="form-group">
                            {!! Form::label('status', 'Status :') !!}<br>
                            <div class="checkbox">
                                <label>
                                        {!!Form::checkbox('status', '1'); !!}
                                        Aktywny
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                        {!!Form::checkbox('status', '0'); !!}
                                        Niekatywny
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('carousel_img', 'Karuzela obrazek 1:') !!}
                            {!! Form::file('carousel_img[]', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('carousel_img', 'Karuzela obrazek 2:') !!}
                            {!! Form::file('carousel_img[]', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('carousel_img', 'Karuzela obrazek 3:') !!}
                            {!! Form::file('carousel_img[]', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('carousel_img', 'Karuzela obrazek 4:') !!}
                            {!! Form::file('carousel_img[]', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('carousel_img', 'Karuzela obrazek 5:') !!}
                            {!! Form::file('carousel_img[]', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {!! Form::label('type', 'Wybierz wymagania :') !!}<br>
                    
                        @foreach($rules as $rule) 
                            <div class="checkbox">
                                <label>
                                    {!!Form::checkbox('rules[]', $rule->id); !!}
                                    {{$rule->rule_name}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>   
      </div>
      <div class="box-footer">
          {!! Form::submit('Dodaj zadanie', ['class' => 'btn btn-lg btn-success']) !!}
          {!! Form::close() !!}
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

@endsection
@extends('admin.layouts.admin')
@section('page_title', 'Podgląd Giveawaya')
@section('page_subtitle', $quest->title)

@section('content')
          <div class="row">
            <div class="col-xs-6">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Giveaway id: {{$quest->id}}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <b>Id</b>
                    <p>{{$quest->id}}</p>

                    <b>Tytuł zadania</b>
                    <p>{{$quest->title}}</p>

                    <b>Opis giveawaya</b>
                    <p>{{$quest->description}}</p>
                    
                    

                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
              <!-- /.box -->
            </div>
            <div class="col-xs-6">
                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Wymagania:</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                      @foreach($quest->rules as $rules) 
                            {{$rules->rule_name}}<br>
                      @endforeach
                      
                  </div>
                  <!-- /.box-body -->
                </div>
            </div>
                <div class="col-xs-6">
                    <div class="box">
                      <div class="box-header">
                        <h3 class="box-title">Uczestnicy:</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                          @foreach($quest->participants as $user) 
                                {{$user->login}}|{{$user->pivot->tickets}}|{{$user->pivot->rules}}<br>
                                
                          @endforeach
                          
                      </div>
                      <!-- /.box-body -->
                    </div>
                </div>
          </div>
          <!-- /.row -->
        
      <script>
            $(document).ready( function () {
                $('#example1').DataTable();
            } );
          </script>
@endsection
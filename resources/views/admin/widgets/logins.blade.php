
    <div class="col-md-6">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Dzienny ilość logowań</h3>
    
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <div class="btn-group">
                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-wrench"></i></button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-12">
                    <p class="text-center">
                      <strong>Dzienna ilosć logowań</strong>
                    </p>
    
                    <div class="chart">
                      <div id='loginsAppendChart'></div>
                    </div>
                    <!-- /.chart-responsive -->
                  </div>
                  <!-- /.col -->
    
                </div>
                <!-- /.row -->
              </div>
              <!-- ./box-body -->
              
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
          </div>
        
        
    <script>
        generateChartSum();
    
        function generateChartSum() {
            var url = '{{route("admin.stats.logins.append")}}';
        
            var options = {
                title: {
                text: ''
                },
                chart: {
    
                    zoomType: 'x',
    
                    renderTo: 'loginsAppendChart',
                    type: 'line',
        
                },
                yAxis: {
                    title: {
                        text: 'Liczba logowań'
                    },
                },
                legend: {
                    enabled: false
                },
                xAxis: {
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    },
                },
                series: [{}],
                plotOptions: {
                column: {
                    pointWidth: 20,
                    dataLabels: {
                    enabled: true
                    },
                    enableMouseTracking: false
                }
                },
            };
            console.log('json');
        
            $.getJSON(url, function (data) {
                
                var categories = [],
                points = [];
        
                $.each(data, function(i, el) {
                categories.push(el.name);
                points.push(parseFloat(el.value));
                });
                options.xAxis.categories = categories;
                options.series[0].name = 'Liczba';
                options.series[0].data = points;
                var chart = new Highcharts.Chart(options);
            })
            }
    </script>
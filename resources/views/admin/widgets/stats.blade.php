<!-- Small boxes (Stat box) -->

<div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-aqua">
        <div class="inner">
            <h3>{{$stats['users_online']}}</h3>

            <p>Użytkowników online</p>
        </div>
        <div class="icon">
            <i class="ion ion-person"></i>
        </div>
        <a href="{{route('admin.user.list')}}" class="small-box-footer">Więcej <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
        <div class="inner">
            <h3>{{$stats['users_registerd']}}</h3>

            <p>Zarejestrowanych użytkowników</p>
        </div>
        <div class="icon">
            <i class="ion ion-person-add"></i>
        </div>
        <a href="{{route('admin.user.list')}}" class="small-box-footer">Więcej <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
        <div class="inner">
            <h3>{{$stats['clans_registerd']}}</h3>

            <p>Zarejestrowanych klanów</p>
        </div>
        <div class="icon">
            <i class="ion ion-beer"></i>
        </div>
        <a href="{{route('admin.user.list')}}" class="small-box-footer">Więcej <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-red">
        <div class="inner">
            <h3>{{$stats['matches_played']}}</h3>

            <p>Rozegranych meczy</p>
        </div>
        <div class="icon">
            <i class="ion ion-bonfire"></i>
        </div>
        <a href="{{route('admin.user.list')}}" class="small-box-footer">Więcej <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    </div>
    <!-- Small boxes (Stat box) -->

    <div class="row">
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-purple">
        <div class="inner">
            <h3>{{$stats['new_tickets']}}</h3>

            <p>Nowe zgłoszenia</p>
        </div>
        <div class="icon">
            <i class="ion ion-folder"></i>
        </div>
        <a href="{{route('admin.tickets.list')}}" class="small-box-footer">Więcej <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-navy">
        <div class="inner">
            <h3>{{$stats['posts']}}</h3>

            <p>Liczba postów</p>
        </div>
        <div class="icon">
            <i class="ion ion-document"></i>
        </div>
        <a href="{{route('admin.post.list')}}" class="small-box-footer">Więcej <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-teal">
        <div class="inner">
            <h3>{{$stats['comments']}}</h3>

            <p>Liczba komentarzy</p>
        </div>
        <div class="icon">
            <i class="ion ion-bookmark"></i>
        </div>
        <a href="{{route('admin.user.list')}}" class="small-box-footer">Więcej <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-gray">
        <div class="inner">
            <h3>{{$stats['messages']}}</h3>

            <p>Liczba wiadomości</p>
        </div>
        <div class="icon">
            <i class="ion ion-chatboxes"></i>
        </div>
        <a href="{{route('admin.user.list')}}" class="small-box-footer">Więcej <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <!-- ./col -->
</div>
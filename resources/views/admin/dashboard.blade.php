@extends('admin.layouts.admin')
@section('page_title', 'Dashboard')
@section('page_subtitle', 'Centrum dowodzenia.')

@section('content')

        @include('admin.widgets.stats')
        
        <div class="row">
            @include('admin.widgets.users')
            @include('admin.widgets.logins')
            @include('admin.widgets.posts')
            @include('admin.widgets.comments')
        </div>

        <div class="row">
            <section class="col-lg-7 connectedSortable">

            
               

            </section>
        
            <section class="col-lg-5 connectedSortable">
            
            
                <div class="box-body no-padding">
                    <!--The calendar -->
                    <div id="calendar" style="width: 100%"></div>
                </div>
            <!-- /.box-body -->
            <div class="box-footer text-black">
                <div class="row">
                    <div class='col-12'>
                        <div id='usersAppendChart'></div>
                    </div>
                <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
        </section>
        <!-- right col -->
        </div>

@endsection
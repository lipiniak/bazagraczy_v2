@extends('admin.layouts.admin')
@section('page_title', 'Podgląd posta')
@section('page_subtitle', 'wraz z komentarzem.')

@section('content')
          <div class="row">
            <div class="col-xs-9">
              <div class="box @if($ticket->status == 1) box-success @elseif($ticket->status == 2) box-danger @endif">
                <div class="box-header">
                    
                    <h3 class="box-title">@if($ticket->status == 1)<span class="label label-success">w toku</span> @elseif($ticket->status == 2)<span class="label label-danger">zamknięte</span> @endif Zgłoszenie numer: {{$ticket->id}}</h3>
                    <div class="box-tools pull-right">
                        @if($ticket->status <= 1)
                        <button type="button" data-toggle="modal" data-target="#permission-remove-{{$ticket->id}}" class="btn btn-xs btn-danger">Zamknij</button>
                        <div class="modal modal-danger fade" id="permission-remove-{{$ticket->id}}" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span></button>
                                        <h4 class="modal-title">Zamknięcie zgłoszenia</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Czy na pewno chcesz zamknąć to zgłoszenie?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Nie</button>
                                        <a href='{{route("admin.tickets.close",["id"=>$ticket->id])}}' class="btn btn-outline">Tak</a>
                                    </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                        </div>
                        @else 
                        <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#permission-open-{{$ticket->id}}">
                               Otwórz
                            </button>
                            <div class="modal modal-success fade" id="permission-open-{{$ticket->id}}" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span></button>
                                            <h4 class="modal-title">Otwórz zgłoszenie</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Czy na pewno chcesz otowrzyć to zgłoszenie?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Nie</button>
                                            <a href='{{route("admin.tickets.open",["id"=>$ticket->id])}}' class="btn btn-outline">Tak</a>
                                        </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                            
                        @endif
                        
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="btn-group">
                            
                    </div><br>
                    <b>Numer zgłoszenia</b>
                    <p>{{$ticket->id}}</p>
                    
                    <b>Typ zgłoszenia</b>
                    <p>{{$ticket->type}}</p>

                    <b>Autor zgłoszenia</b>
                    <p><a href="{{route('admin.user.show',['id'=>$ticket->user->id])}}">{{$ticket->user->login}}</a></p>

                    <b>Treść</b>
                    <p>{{$ticket->message}}</p>
                    
                    <b>Dane</b>
                    <p>{{$ticket->data}}</p>
                </div>
                <!-- /.box-body -->
              </div>
              
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            
                            <li class="active"><a href="#author" data-toggle="tab">Podgląd autora</a></li>
                            @if($ticket->getData()['type'] == 'user')
                                <?php $user = $ticket->getData()['content'];?>
                                <li><a href="#user" data-toggle="tab">Podgląd zgłoszonego</a></li>
                            @endif
                            @if($ticket->getData()['type'] == 'clan')
                                <?php $clan = $ticket->getData()['content'];?>
                                <li><a href="#clan" data-toggle="tab">Podgląd klanu</a></li>
                            @endif
                        </ul>
                        <div class="tab-content">
                                @include('admin.tickets.tabs.author')
                            @if($ticket->getData()['type'] == 'user')
                                @include('admin.tickets.tabs.user')
                            @endif
                            @if($ticket->getData()['type'] == 'clan')
                                @include('admin.tickets.tabs.clan')
                            @endif
                        </div>
                        <!-- /.tab-content -->
                    </div>
                
              <!-- /.box -->
              <!-- /.box -->
            </div>
            @if($ticket->status != 2)
            @include('admin.tickets.tabs.conversation')
            @endif 
          </div>
          <div class="row">
            
                  
            <!-- /.col -->
          </div>
          <!-- /.row -->
        
      <script>
            $(document).ready( function () {
                $('#example1').DataTable();
            } );
          </script>
@endsection
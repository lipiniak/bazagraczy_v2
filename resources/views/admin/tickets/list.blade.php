@extends('admin.layouts.admin')


@section('page_title', 'Zgłoszenia')
@section('page_subtitle', 'Lista zgłoszeń.')


@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#new" data-toggle="tab">Nowe</a></li>
                <li><a href="#open" data-toggle="tab">W toku</a></li>
                <li><a href="#closed" data-toggle="tab">Zamknięte</a></li>
                

            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="new">
                    <h3>Nowe zgłoszenia</h3>
                    <table id="newTickets" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                                <th>Id</th>
                                <th>Typ</th>
                                <th>Wiadomość</th>
                                <th>Autor</th>
                                <th>Data</th>
                                <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tickets->where('status',0) as $ticket)
                        <tr>
                            <td>{{$ticket->id}}</td>
                            <td>{{$ticket->type}}
                            </td>
                            <td>{{$ticket->message}}</td>
                            <td>{{$ticket->user->login}}</td>
                            <td>{{$ticket->created_at}}</td>
                            <td style="align-content: center; width: 120px;">
                                <div class="btn-group">
                                    <a href='{{route("admin.tickets.show",["id"=>$ticket->id])}}' class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                    
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#permission-remove-{{$ticket->id}}">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                    <div class="modal modal-danger fade" id="permission-remove-{{$ticket->id}}" style="display: none;">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span></button>
                                                    <h4 class="modal-title">Zamknięcie zgłoszenia</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Czy na pewno chcesz zamknąć to zgłoszenie?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Nie</button>
                                                    <a href='{{route("admin.tickets.close",["id"=>$ticket->id])}}' class="btn btn-outline">Tak</a>
                                                </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Typ</th>
                            <th>Wiadomość</th>
                            <th>Autor</th>
                            <th>Data</th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                        
                </div>
                <div class="tab-pane" id="open">
                        <h3>Otwarte zgłoszenia</h3>
                        <table id="openTickets" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                        <th>Id</th>
                                        <th>Typ</th>
                                        <th>Wiadomość</th>
                                        <th>Autor</th>
                                        <th>Data</th>
                                        <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($tickets->where('status',1) as $ticket)
                                <tr>
                                  <td>{{$ticket->id}}</td>
                                  <td>{{$ticket->type}}
                                  </td>
                                  <td>{{$ticket->message}}</td>
                                  <td>{{$ticket->user->login}}</td>
                                  <td>{{$ticket->created_at}}</td>
                                  <td style="align-content: center; width: 120px;">
                                      <div class="btn-group">
                                          <a href='{{route("admin.tickets.show",["id"=>$ticket->id])}}' class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#permission-remove-{{$ticket->id}}">
                                              <i class="fa fa-remove"></i>
                                          </button>
                                          <div class="modal modal-danger fade" id="permission-remove-{{$ticket->id}}" style="display: none;">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span></button>
                                                        <h4 class="modal-title">Zamknięcie zgłoszenia</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Czy na pewno chcesz zamknąć to zgłoszenie?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Nie</button>
                                                        <a href='{{route("admin.tickets.close",["id"=>$ticket->id])}}' class="btn btn-outline">Tak</a>
                                                    </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                        </div>
                                      </div>
                                  </td>
                                </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Typ</th>
                                    <th>Wiadomość</th>
                                    <th>Autor</th>
                                    <th>Data</th>
                                    <th></th>
                                </tr>
                                </tfoot>
                              </table>
                            
                    </div>
                    <div class="tab-pane" id="closed">
                            <h3>Zamknięte zgłoszenia</h3>
                            <table id="closedTickets" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                            <th>Id</th>
                                            <th>Typ</th>
                                            <th>Wiadomość</th>
                                            <th>Autor</th>
                                            <th>Data</th>
                                            <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tickets->where('status',2) as $ticket)
                                    <tr>
                                      <td>{{$ticket->id}}</td>
                                      <td>{{$ticket->type}}
                                      </td>
                                      <td>{{$ticket->message}}</td>
                                      <td>{{$ticket->user->login}}</td>
                                      <td>{{$ticket->created_at}}</td>
                                      <td style="align-content: center; width: 120px;">
                                          <div class="btn-group">
                                              <a href='{{route("admin.tickets.show",["id"=>$ticket->id])}}' class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#permission-remove-{{$ticket->id}}">
                                                    <i class="fa fa-folder-open-o"></i>
                                                </button>
                                                <div class="modal modal-success fade" id="permission-remove-{{$ticket->id}}" style="display: none;">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                                <h4 class="modal-title">Otwórz zgłoszenie</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>Czy na pewno chcesz otowrzyć to zgłoszenie?</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Nie</button>
                                                                <a href='{{route("admin.tickets.open",["id"=>$ticket->id])}}' class="btn btn-outline">Tak</a>
                                                            </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                </div>
                                              
                                          </div>
                                      </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Typ</th>
                                        <th>Wiadomość</th>
                                        <th>Autor</th>
                                        <th>Data</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                  </table>
                                
                        </div>
                
            </div>
            
            </div>
    </div>
        <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
</div>
         
<script>
  $(document).ready( function () {
      $('#newTickets').DataTable();
      $('#openTickets').DataTable();
      $('#closedTickets').DataTable();

  } );
</script>
@endsection
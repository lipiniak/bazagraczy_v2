<div class="col-md-3">
    <div class="box box-primary direct-chat direct-chat-primary">
            <div class="box-header with-border">
                    <h3 class="box-title">Rozmowa ze zgłaszającym</h3>
  
                    
                  </div>
        <div class="box-body">
            <!-- Conversations are loaded here -->
            <div class="direct-chat-messages rozmowa" id='rozmowa'>

                @foreach($messages as $msg)
                <?php $lastId = $msg->id;?>
                    <div class="direct-chat-msg @if($msg->sender->id == 0) right @endif" data-id='{{$msg->id}}' id='{{$msg->id}}'>
                        <div class="direct-chat-info clearfix">
                            <span class="direct-chat-name @if($msg->sender->id == 0) pull-right @else pull-left @endif">{{$msg->sender->login}}</span>
                            <span class="direct-chat-timestamp @if($msg->sender->id == 0) pull-left @else pull-right @endif"><span value='{{strtotime($msg->created_at)}}' class='time-from'></span></span>
                        </div>
                        @if($msg->sender->avatar == 'no-avatar.jpg')
                            <img class="direct-chat-img" src="{{asset('images/avatars/')}}/{{$msg->sender->avatar}}">
                        @else
                            <img class="direct-chat-img" src="{{ Storage::url($msg->sender->avatar) }}">
                        @endif
                        <div class="direct-chat-text">
                            {!!$msg->message!!}
                        </div>
                    
                    </div>
                @endforeach

               
                <div class="direct-chat-msg newMessage message" data-id='0' id='0' style='display: none;'>
                        <div class="direct-chat-info clearfix">
                            <span class="direct-chat-name"></span>
                            <span class="direct-chat-timestamp"><span value='' class='time-from'></span></span>
                        </div>
                        
                        <img class="direct-chat-img" src="">
                        
                        <div class="direct-chat-text">

                        </div>
                    
                    </div>
            </div>
        </div>
        <div class="box-footer" style="">
            <form id='message-send'>
                {{ csrf_field() }}
                <input type='hidden' name='reciver_id' value='{{$ticket->user->id}}'>
                <div class="input-group">
                
                <textarea class="napisz-wiadomosc form-control" name='message' rows="1" placeholder="Napisz wiadomość..." id='message'></textarea>
                
                <span class="input-group-btn">
                        <a onCLick='sendForm()' class="btn btn-primary btn-flat textarea-send-btn">Wyślij</a>
                        
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
            <script>
    
                var login = 'BAZAGRACZY.pl';
                var userId = 0;
    
                var lastId = {{isset($lastId) ? $lastId : 0}};
    
                var offset = 1;
                var reciver_id = {{$ticket->user->id}};
                var message = $('.newMessage');
                var newMessage;
                var conversationDiv = $('.rozmowa');
                var messengerSound = false;
                document.getElementById(lastId).scrollIntoView();
    
                $('.tresc').each(function(i, obj) {
                    $(obj).html(anchorme($(obj).html(),{attributes:[{name:'target',value:'blank'}]}));
                });
    
                $(function(){
                    autosize($('textarea'));
                    $("textarea").on("keydown",function(e){
                        if(!e.shiftKey && e.keyCode===13){
                            e.preventDefault();
                            var form = $('#message-send');
    
                            var url = '/admin/tickets/wyslij';
                            
                            if(!$('textarea').val()) {
                                e.preventDefault()
                            } else {
                                $.ajax({
                                    method: "POST",
                                    data: form.serialize(),
                                    url: url,
                                })
                                .done(function( data ) {
        
                                });
                                
                                $('textarea').val('');
                                $('textarea').css('height','36px');
                            }
                            
                        }
                    });
                });
    
                function sendForm() {
                    if(!$('textarea').val()) {
                        e.preventDefault()
                    } else {
                        var form = $('#message-send');
    
                        var url = '/admin/tickets/wyslij';
                        
                        $.ajax({
                            method: "POST",
                            data: form.serialize(),
                            url: url,
                        })
                        .done(function( data ) {
    
                        });
                        
                        $('textarea').val('');
                        $('textarea').css('height','36px');
                    }
                    
                }
    
                    Echo.private('conversation-{!!$channelUsers[0]!!}-{!!$channelUsers[1]!!}')
                    
                    .whisper('typing', {
                        name: login+' Pisze...'
                    })
                    .listenForWhisper('typing', (e) => {
                        console.log(e.name);
                    })
                    //.joining(...)
                    //.leaving(...)
                    .listen('newMessage', (e) => {
                        
                        console.log(e);
                    
                        var n = Date.now();
                        newMessage = message.clone();
                        
                        newMessage.attr('id',e.data.id)
                        newMessage.attr('data-id',e.data.id)
                        newMessage.find('.direct-chat-text').html(anchorme(e.data.message,{attributes:[{name:'target',value:'blank'}]}));
                        newMessage.find('.direct-chat-name').html(e.user['login']);
                        newMessage.find('.direct-chat-img').attr("src",e.user['avatar']);
                        newMessage.find('.time-from').attr('value',n/1000);
                        newMessage.find('.time-from').html('1 sek. temu');
                        newMessage.toggleClass('newMessage');
                        if(e.user['id'] == userId)
                            newMessage.toggleClass('messageowner');
    
                        newMessage.appendTo(conversationDiv);
                        newMessage.show();
    
                        timeToText(newMessage.find('.time-from'));
                        if(messengerSound) {
                            if(!document.hasFocus()) {
                                console.log('audio without focus ');
                                audio.play();
                            }
                        }
                        
                        $('#no-messages').remove();
                        
                        document.getElementById(e.data.id).scrollIntoView();
                        
                        if(userId == e.user['id']) {
                            var element = $('#conversation-'+e.reciverId);
                            
                        } else {
                            
                            var element = $('#conversation-'+e.user['id']);
                            
                        }
                        
                        //console.log(element);
    
                        if(element.length  > 0) {
                            element.remove();
                            console.log('mamy element');
                            newMessageInfo = true;
    
                            element.removeClass('nowa-wiadomosc');      
    
                            $('#conversations').prepend(element);
                            
                            var count = 0;
    
                            $.each($('.conversation'), function(key,value) {
                                if($(value).hasClass('nowa-wiadomosc')) {
                                    count++;
                                } 
                            });
                            
    
                            if(count == 0) {
                                $('#messages-count').hide();
                            } else {
                                $('#messages-count').show();
                                $('#messages-count').html(count);
                            }
                        } else {
                            
                                $('.no-messages').remove();
                                //console.log('nie mamy elementu');
                                var element = $('.newConversation');
                                newMessageInfo = true;
                                
                                element.removeClass('nowa-wiadomosc');      
    
                                element.find('.nick-rozmowcy').html(e.reciverLogin);
                                element.find('.menu-avatar').attr("src",e.reciverAvatar);
                                element.attr("id",'conversation-'+e.reciverId);
                                element.find('.usun-rozmowce a').attr("onclick",'removeConversation('+e.reciverId+')');
                                element.find('.adress').attr("href",'/wiadomosci/'+e.reciverId);
                                element.show();
                                element.toggleClass('newConversation');
    
    
                                $('#conversations').prepend(element);
                                var count = 0;
                                $.each($('.conversation'), function(key,value) {
                                    if($(value).hasClass('nowa-wiadomosc')) {
                                        count++;
                                    } 
                                });
                                //console.log('unreaded message count:'+count);
                                if(count == 0) {
                                    $('#messages-count').hide();
                                } else {
                                    $('#messages-count').show();
                                    $('#messages-count').html(count);
                                }
                            
                            
                        }
                        
    
                        // mark as seen
                        if(e.user['id'] != userId) {
                            var url = '/wiadomosci/przeczytaj/'+e.data.id;
                                
                            $.ajax({
                                method: "GET",
                                url: url,
                            })
                            .done(function( data ) {
    
                            });
                        }
                        
                    });
                    
                 
    
                    function isScrolledIntoView(elem){
                        var $elem = $(elem);
                        var $window = $(window);
                    
                        var docViewTop = $window.scrollTop();
                        var docViewBottom = docViewTop + $window.height();
                    
                        var elemTop = $elem.offset().top;
                        var elemBottom = elemTop + $elem.height();
                    
                        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
                    }
    
            </script>
            <script src="{{ asset('js/textarea-autosize.js') }}"></script>
            <script>
                
    
                    const ELEMENT_CLASS = '.time-from';
                    const DATE_ATTR_NAME = 'value';
        
                    const TEN_SECONDS_MILLIS = 10 * 1000;
                    const MINUTE_MILLIS = 60 * 1000;
                    const HOUR_MILLIS = 60 * MINUTE_MILLIS;
                    const DAY_MILLIS = 24 * HOUR_MILLIS;
        
                    const UNDER_MINUTE_REFRESH_MILLIS = TEN_SECONDS_MILLIS; // every 10 seconds
                    const UNDER_HOUR_REFRESH_MILLIS = MINUTE_MILLIS; // every minute
                    const UNDER_DAY_REFRESH_MILLIS = HOUR_MILLIS; // every hour
                    const OVER_DAY_REFRESH_MILLIS = DAY_MILLIS; // every day
        
        
                    function getRandomTimeoutId() {
                    return Math.floor(Math.random() * (1e16));
                    }
        
                    function timeToText(element, timeoutId) {
                    const date = parseInt(element.attr(DATE_ATTR_NAME)) * 1000;
                    //const millis = (Date.now() - date);
                    const now = Date.now();
                    const millis = date > now ? 1 : (now - date);
                    
                    if (timeoutId) {
                        window.clearTimeout(timeoutId);
                    }
                    const tId = getRandomTimeoutId();
        
                    let text, refreshRate;
        
                    if (millis < MINUTE_MILLIS) {
                        refreshRate = UNDER_MINUTE_REFRESH_MILLIS;
                        text = underMinuteText(millis);
                    } else if (millis < HOUR_MILLIS) {
                        refreshRate = UNDER_HOUR_REFRESH_MILLIS;
                        text = underHourText(millis);
                    } else if (millis < DAY_MILLIS) {
                        refreshRate = UNDER_DAY_REFRESH_MILLIS;
                        text = underDayText(millis);
                    } else {
                        refreshRate = OVER_DAY_REFRESH_MILLIS;
                        text = overDayText(millis,date);
                    }
        
                    setTimeout(function() {
                        timeToText(element, tId);
                    }, refreshRate);
        
                    element.text(text);
                    }
        
                    function underMinuteText(millis) {
                    const tensOfSeconds = Math.floor(millis / TEN_SECONDS_MILLIS);
                    if (tensOfSeconds < 1) {
                        return `${Math.floor(millis / 1000)} sek. temu`;
                    }
                    return `${tensOfSeconds * 10} sek. temu`;
                    }
        
                    function underHourText(millis) {
                    const minutes = Math.floor(millis / MINUTE_MILLIS);
                    return `${minutes} min. temu`;
                    }
        
                    function underDayText(millis) {
                    const hours = Math.floor(millis / HOUR_MILLIS);
                    return `${hours} godz. temu`;
                    }
        
                    function overDayText(millis,date) {
                    const days = Math.floor(millis / DAY_MILLIS);
                    if (days === 1) {
                        return "1 dzień temu";
                    } else {
                        if(days > 3) {
                            var date = new Date(date);
                            
                            return date.customFormat( "#DD#.#MM#.#YYYY#" ) 
                        } else {
                            return `${days} dni temu`;
                        }
                    }
                    }
        
                    $(document).ready(function() {
                    $(ELEMENT_CLASS).each(function(i, element) {
                        timeToText($(element));
                    });
                    });
    
            </script>
        
    
@extends('admin.layouts.admin')
@section('page_title', 'Zadanie')
@section('page_subtitle', 'Dodaj nowe zadanie')
@section('content')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
<script src="{{ asset('js/select2.full.js') }}"></script>        

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Dodaj nowe zadanie</h3>
        <div class='pull-right box-tools'>
              <a href="{{route('admin.quest.list')}}" class='btn btn-sm btn-primary'><i class="fa fa-reply"></i></a>
          </div>
      </div>
      <!-- /.box-header -->
      
      <div class="box-body">
              {!! Form::open(['action' => 'Admin\QuestController@store','method'=>'POST']) !!}
              <div class="form-group">
                    {!! Form::label('quest_key', 'Quest key:') !!}
                    {!! Form::text('quest_key', null, ['class' => 'form-control']) !!}
                </div>
            <div class="form-group">
                    {!! Form::label('title', 'Treść zadania:') !!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                </div>
            <div class="form-group">
                    {!! Form::label('points', 'Punkty:') !!}
                    {!! Form::text('points', null, ['class' => 'form-control']) !!}
                </div>
            <div class="form-group">
                    {!! Form::label('type', 'Typ zadania :') !!}
                    {!! Form::text('type', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                        {!! Form::label('status', 'Status :') !!}
                        {!! Form::text('status', null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                            {!! Form::label('config', 'Konfig JSON:') !!}
                            {!! Form::textarea('config', null, ['class' => 'form-control']) !!}
                        </div>
      </div>
      <div class="box-footer">
          {!! Form::submit('Dodaj zadanie', ['class' => 'btn btn-lg btn-success']) !!}
          {!! Form::close() !!}
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

@endsection
@extends('admin.layouts.admin')
@section('page_title', 'Podgląd usera')
@section('page_subtitle', $user->login)

@section('content')
<div class="row">
    <div class="col-md-3">

        <!-- Profile Image -->
        <div class="box box-primary">
        <div class="box-body box-profile">
            
            @if($user->avatar == 'no-avatar.jpg')
                <img class="profile-user-img img-responsive img-circle" src="{{asset('images/avatars/')}}/{{$user->avatar}}" alt="User profile picture">
            @else
                <img class="profile-user-img img-responsive img-circle" src="{{ Storage::url($user->avatar) }}" alt="User profile picture">  
            @endif
            <h3 class="profile-username text-center">{{$user->login}}</h3>

            <p class="text-muted text-center">{{$user->email}}</p>
            <p class="text-muted text-center">{{$user->provider}}</p>

            <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                    <b>Id</b> <a class="pull-right">#{{$user->id}}</a>
                </li>
                <li class="list-group-item">
                    <b>Data ostatniego logowania</b> <a class="pull-right">{{date('d.m.Y, H:i:s',strtotime($user->login_at))}}</a>
                </li>

                <li class="list-group-item">
                    <b>Czas spędzony online</b> <a class="pull-right">{{format_time($user->online_time)}}</a>
                </li>
                <li class="list-group-item">
                    <b>Data dołączenia</b> <a class="pull-right">{{date('d.m.Y, H:i:s',strtotime($user->created_at))}}</a>
                </li>
                <li class="list-group-item">
                    <b>Aktywny</b> <a class="pull-right">@if($user->activated) Tak @else Nie @endif</a>
                </li>
                <li class="list-group-item">
                    <b>Postów</b> <a class="pull-right">{{$user->post->count()}}</a>
                </li>
                
            </ul>
            
            <a href="{{route('profile.user',['login'=>$user->id])}}" class="btn btn-primary btn-block"><b>Przejdź do profilu na portalu</b></a>
        </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- About Me Box -->
        <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Podstawowe informacje</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <strong><i class="fa fa-book margin-r-5"></i> Dane osobowe</strong>

            <p class="text-muted">
            {{$user->name}} {{$user->surname}} 
            </p>
            
            @if($user->birth_date != '')
            <p class="text-muted">
                {{$user->birth_date}}
                {{$user->age()}}
            </p>
            @endif
            
            <p class="text-muted">
                {{$user->sex()}}
            </p>
            <hr>

            <strong><i class="fa fa-map-marker margin-r-5"></i> Lokalizacja</strong>

            <p class="text-muted">
                    {{$user->city()}}, {{$user->voivode()}} 
                </p>

            <hr>

            <strong><i class="fa fa-pencil margin-r-5"></i> Platformy</strong>

            <p>
            <span class="label label-grey bg-gray">{{$user->steam}}</span>
            <span class="label label-primary">{{$user->psn}}</span>
            <span class="label label-success">{{$user->xboxlive}}</span>
            <span class="label label-teal bg-teal">{{$user->origin}}</span>
            <span class="label label-warning">{{$user->uplay}}</span>
            <span class="label label-primary bg-navy">{{$user->battlenet}}</span>
            <span class="label label-warning">{{$user->epicgames}}</span>
            </p>

            <hr>
            
        </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
        <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#activity" data-toggle="tab">Aktywność</a></li>
            <li><a href="#library" data-toggle="tab">Biblioteka gier</a></li>
            <li><a href="#followers" data-toggle="tab">Znajomi</a></li>
            <li><a href="#clans" data-toggle="tab">Klany</a></li>
            <li><a href="#avatar" data-toggle="tab">Avatar</a></li>
            <li><a href="#notes" data-toggle="tab">Notatki</a></li>
            <li><a href="#ustawienia" data-toggle="tab">Ustawienia</a></li>
            <li><a href="#ustawienia" data-toggle="tab">Edycja danych</a></li>

        </ul>
        <div class="tab-content">

           @include('admin.users.tabs.activity') 
           @include('admin.users.tabs.games') 
           @include('admin.users.tabs.followers') 
           @include('admin.users.tabs.clans') 
           @include('admin.users.tabs.notes') 
           
        </div>
        <!-- /.tab-content -->
        </div>
        <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
    </div>
@endsection
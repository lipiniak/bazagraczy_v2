<div class="tab-pane" id="followers">
        <h3>Obserwuje</h3>
        <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th>Login</th>
                        <th>E-mail</th>
                        <th>Akcja</th>    
                    </tr>
                </thead>
                <tbody>
                    @foreach($user->myfollowers as $follower)    
                        
                            <tr>
                                <td>{{$follower->login}}</td>
                                <td>{{$follower->email}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger"><i class="fa fa-remove"></i></button>
                                    </div>
                                </td>
                            </tr>
                        
                    @endforeach
                    
                </tbody>
            </table>
        </div>    
        <h3>Obserwowany przez</h3>
        <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th>Login</th>
                        <th>E-mail</th>
                        <th>Akcja</th>    
                    </tr>
                </thead>
                <tbody>
                    @foreach($user->followers as $follower)    
                        
                            <tr>
                                <td>{{$follower->login}}</td>
                                <td>{{$follower->email}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger"><i class="fa fa-remove"></i></button>
                                    </div>
                                </td>
                            </tr>
                        
                    @endforeach
                    
                </tbody>
            </table>
        </div>    
    
    </div>
<div class="tab-pane" id="clans">
        <h3>Klany</h3>
        <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th>Nazwa</th>
                        <th>TAG</th>
                        <th>Typ członkostwa</th>
                        <th>Ostatnia aktywność</th>
                        <th>Akcja</th>    
                    </tr>
                </thead>
                <tbody>
                    @foreach($user->clans as $clan)    
                        
                            <tr>
                                <td>{{$clan->name}}</td>
                                <td>{{$clan->tag}}</td>
                                <td>@if($clan->leader_id == $user->id) Lider @else Członek @endif</td>
                                <td>{{$clan->updated_at->format('d.m.Y, H:i')}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                        <button type="button" class="btn btn-danger"><i class="fa fa-remove"></i></button>
                                    </div>
                                </td>
                            </tr>
                        
                    @endforeach
                    
                </tbody>
            </table>
        </div>    
    
    </div>
<div class="active tab-pane" id="activity">
    <h3>Aktywność</h3>
    @foreach($user->post as $post)
        <div class="post">
            <div class="user-block">
                @if($user->avatar == 'no-avatar.jpg')
                    <img class="profile-user-img img-responsive img-circle" src="{{asset('images/avatars/')}}/{{$user->avatar}}" alt="User profile picture">
                @else
                    <img class="profile-user-img img-responsive img-circle" src="{{ Storage::url($user->avatar) }}" alt="User profile picture">  
                @endif
                <span class="username">
                    <b>{{$post->game->name}} [{{$post->platform->name}}]</b>
                    <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                </span>
            <span class="description">{{date('d.m.Y',strtotime($post->created_at))}}</span>
            </div>
            <!-- /.user-block -->
            <p>
            {{$post->text}}
            </p>
            <ul class="list-inline">
            <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Zgłoś post</a></li>
            <li><a href="#" class="link-black text-sm"><i class="fa fa-pencil margin-r-5"></i> Edytuj post</a>
            <li><a href="#" class="link-black text-sm"><i class="fa fa-remove margin-r-5"></i> Usuń post</a>
                
            </li>
            <li class="pull-right">
                <a data-toggle="collapse" data-parent="#accordion" href="#comments-{{$post->id}}" aria-expanded="false" class="link-black text-sm "><i class="fa fa-comments-o margin-r-5"></i> Komentarzy ({{$post->comments->count()}})
                </a></li>
            </ul>
            @if($post->comments->count() > 0)
            <div class="box-footer box-comments collapse" id='comments-{{$post->id}}' aria-expanded="false" style="height: 0px;">
                @foreach($post->comments as $comment)
                <div class="box-comment">
                    
                    @if($comment->user->avatar == 'no-avatar.jpg')
                        <img class="profile-user-img img-responsive img-circle" src="{{asset('images/avatars/')}}/{{$comment->user->avatar}}" alt="User profile picture">
                    @else
                        <img class="profile-user-img img-responsive img-circle" src="{{ Storage::url($comment->user->avatar) }}" alt="User profile picture">  
                    @endif
    
                    <div class="comment-text">
                        <span class="username">
                            {{$comment->user->login}}
                            <span class="text-muted pull-right">{{date('d.m.Y',strtotime($comment->created_at))}}</span>
                        </span><!-- /.username -->
                    {{$comment->text}}
                    </div>
                    
                </div>
                @endforeach
            </div>
            @endif
        </div>
    @endforeach
        
</div>
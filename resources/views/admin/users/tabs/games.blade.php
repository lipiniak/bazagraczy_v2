<div class="tab-pane" id="library">
    <h3>Biblioteka gier</h3>
    @if($user->games->isEmpty())
            <p>brak gier w bibliotece</p>
    @else
    <div class="table-responsive">
        <table class="table no-margin">
            <thead>
                <tr>
                    <th>Nazwa</th>
                    <th>Platforma</th>
                    <th>Akcja</th>    
                </tr>
            </thead>
            <tbody>
                @foreach($user->games as $game)    
                    @foreach($game->userPlatforms()->where('user_id', $user->id)->get() as $item)
                        <tr>
                            <td>{{$game->name}}</td>
                            <td>{{$item->name}}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-danger"><i class="fa fa-remove"></i></button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                @endforeach
                
            </tbody>
        </table>
    </div>    
    @endif
</div>
<div class="tab-pane" id="notes">
        <h3>Notatki od</h3>
        @if($user->mynote->isEmpty())
            <p>brak notek</p>
        @else
        <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th>Data stowrzenia</th>
                        
                        <th>Komu</th>
                        <th>Notka</th>
                        <th>Akcja</th>    
                    </tr>
                </thead>
                <tbody>
                    @foreach($user->mynote as $note)    
                        
                            <tr>
                                <td>{{$note->created_at}}</td>
                                
                                <td>{{$note->for->login}}</td>
                                <td>{{$note->note}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger"><i class="fa fa-remove"></i></button>
                                    </div>
                                </td>
                            </tr>
                        
                    @endforeach
                    
                </tbody>
            </table>
        </div>  
        @endif
        <h3>Notatki dla</h3>
        @if($user->note->isEmpty())
            <p>brak notek</p>
        @else
        <div class="table-responsive">
            <table class="table no-margin">
                <thead>
                    <tr>
                        <th>Data stowrzenia</th>
                        <th>Kto</th>
                        
                        <th>Notka</th>
                        <th>Akcja</th>    
                    </tr>
                </thead>
                <tbody>
                    @foreach($user->note as $note)    
                        
                            <tr>
                                <td>{{$note->created_at}}</td>
                                <td>{{$note->from->login}}</td>
                                
                                <td>{{$note->note}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-danger"><i class="fa fa-remove"></i></button>
                                    </div>
                                </td>
                            </tr>
                        
                    @endforeach
                    
                </tbody>
            </table>
        </div>    
        @endif
    </div>
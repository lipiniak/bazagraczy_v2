@extends('admin.layouts.admin')

@section('page_title', 'Użytkownicy')
@section('page_subtitle', 'Lista użytkowników')

@section('content')

<div class="row">
  <div class="col-xs-12">
    
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Lista użytkowników</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="users" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>Id</th>
            <th>Login</th>
            <th>Adres E-mail</th>
            <th>Dostawca</th>
            <th>Data dołączenia</th>
            <th>Ostatnie logowanie</th>
            <th>Akcje</th>
          </tr>
          </thead>
          <tbody>
              @foreach($users as $user)
          <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->login}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->provider}}</td>
            <td>{{date('d.m.Y, H:i:s',strtotime($user->created_at))}}</td>
            <td>{{date('d.m.Y, H:i:s',strtotime($user->login_at))}}</td>
            <td>
                <div class="btn-group">
                  <a href='{{route("admin.user.show",["id"=>$user->id])}}' type="button" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                  <button type="button" class="btn btn-warning"><i class="fa fa-pencil"></i></button>
                  <button type="button" class="btn btn-danger"><i class="fa fa-remove"></i></button>
                </div>
              </td>
          </tr>
              @endforeach
          </tbody>
          <tfoot>
          <tr>
                  <th>Id</th>
                  <th>Login</th>
                  <th>Adres E-mail</th>
                  <th>Dostawca</th>
                  <th>Data dołączenia</th>
                  <th>Ostatnie logowanie</th>
                  <th>Akcje</th>
          </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<script>
    $(document).ready( function () {
        $('#users').DataTable();
    } );
</script>        

@endsection
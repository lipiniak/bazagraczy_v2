@extends('layouts.app')

@section('content')
<div id="profil" class="content ">
        @if(Auth::user()->id == $user->id)
        @include('profile.menu')
        @endif
        @if ($errors->has('note'))
      
        <script type="text/javascript">
            $(window).on('load',function(){
                $('#profilNotatka').modal('show');
            });
        </script>
        @endif
        <div class="container-fluid">

            <div class="row">
                <?php
                    $note = $user->note()->where('user_id',Auth::user()->id)->get();
                ?>
                <div class="col-12 col-xl-6 mb50">
                    <div class="avatar-wrapper user_{{$user->id}}">
                        <div class="avatar">
                            @if($user->avatar == 'no-avatar.jpg')
                                <img src="{{asset('images/avatars/')}}/{{$user->avatar}}" class="menu-avatar" alt="Avatar">
                            @else
                                <img src="{{ Storage::url($user->avatar) }}" class="menu-avatar" alt="Avatar">
                            @endif
                            <span class="status {{$user->getStatus()}}"></span>
                        </div>
                    </div>
                    <div class="profil-akcje-wrapper">
                        <div>
                            <h1 class="mb5 tekst-linia">{{$user->login}}</h1>
                            <p class="text-muted mb5">#{{$user->id}} 
                                @if(Auth::user()->id == $user->id)
                                <a href="#"  class="btn btn-secondary btn-sm btn-logout" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                       <small> Wyloguj się </small>
                                </a>
                            </p>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                </form>
                                @endif
                        
                        </div>
                        
                        @if(Auth::user()->id != $user->id)
                        <div class="akcje-obcy">
                            @if($user->followers()->find(Auth::user()->id))
                                <a href='{{route("profile.unfollow",["id"=> $user->id])}}' class="btn btn-secondary">Nie obserwuj</a>
                            @else
                                <a href='{{route("profile.follow",["id"=> $user->id])}}' class="btn btn-secondary">Obserwuj</a>
                            @endif
                            <a href="#" onclick='showConversation({{$user->id}},"{{$user->login}}")' class="btn btn-secondary">Rozmowa</a>
                            <a class="btn btn-secondary" data-toggle="modal" data-target="#profilNotatka">Notatka</a>
                                        <div class="modal fade" id="profilNotatka" tabindex="-1" role="dialog" aria-labelledby="profilNotatkaTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <form method="post" action='/profil/{{$user->id}}/notatka/dodaj'>
                                                    {{ csrf_field() }}
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Dodaj notatkę</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body text-left">
                                                            <div class="form-group">
                                                                <p class="text-muted"><small>Zanotuj swoje uwagi odnośnie danego gracza, tak aby w przyszłości pamiętać ważne informacje na jego temat. Notatki są prywatne i widoczne tylko dla ciebie.</small></p>
                                                                <label class="form-label-custom" for="profil-notatka">Treść notatki</label>
                                                                <textarea class="form-control" name='note' id="profil-notatka" rows="3" style="min-height:80px;">@if(!empty($note[0])){{ $note[0]->note}}@endif</textarea>
                                                                @if ($errors->has('note'))
                                                                <div class="error-feedback">
                                                                    {{ $errors->first('note') }}<br>
                                                                </div>
                                                                @endif
                                                            </div> 
                                                        </div>
                                                        <div class="modal-footer">
                                                            <a href='/profil/{{$user->id}}/notatka/usun' class="btn btn-link">Usuń notatkę</a>
                                                            <input type="submit" class="btn btn-primary" value='Zapisz'>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        @if($errors->has('report_profile_'.$user->id))
                                   
                                        <script type="text/javascript">
                                            $(window).on('load',function(){
                                                $('#profilZglos').modal('show');
                                            });
                                        </script>
                                        @endif
                                        <a class="btn btn-secondary" data-toggle="modal" data-target="#profilZglos">Zgłoś</a>
                                        <div class="modal fade" id="profilZglos" tabindex="-1" role="dialog" aria-labelledby="profilZglosTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <form style="width:100%" method='post' action='/zglos/profil'>
                                                    {{ csrf_field() }}
                                                    
                                                <input type='hidden' name='user_id' value="{{$user->id}}">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">Zgłoszenie profilu gracza</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body text-left">
                                                            <div class="form-group">
                                                                
                                                                <p class="text-muted"><small>Opisz w paru słowach czego dotyczy problem.</small></p>
                                                                <label class="form-label-custom" for="profil-zglos">Treść zgłoszenia</label>
                                                                <textarea class="form-control" name='report_profil_{{$user->id}}'id="profil-zglos" rows="3" style="min-height:80px;"></textarea>
                                                                
                                                                @if ($errors->has('report_profil_'.$user->id))
                                                            <div class="error-feedback">
                                                                {!! $errors->first('report_profil_'.$user->id) !!}<br>
                                                            </div>  
                                                            @endif
                                                            </div> 
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary">Wyślij</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        @if($user->blocked()->find(Auth::user()->id))
                                        <a href="{{route('profile.unblock',['id'=>$user->id])}}" class="btn btn-secondary">Odblokuj</a>
                                        @else
                                        <a class="btn btn-secondary" data-toggle="modal" data-target="#profilZablokuj">Zablokuj</a>
                                        <div class="modal fade" id="profilZablokuj" tabindex="-1" role="dialog" aria-labelledby="profilZablokujTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="profilZablokujTitle">Uwaga!</h5>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <p class="mb10">Czy na pewno chcesz dodać</p> <p class="mb10"><strong>{{$user->login}}</strong></p> <p class="mb0">do zablokowanych?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="{{route('profile.block',['id'=>$user->id])}}" class="btn btn-primary">Tak</a>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Nie</button>                  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                       
                            
                        </div>
                        @endif
                        @if(isset($user->login_at))<p class="mb0"><small class="text-muted">Ostatnio zalogowany <span class="text-nowrap">{{$user->login_at->format('d.m.Y, H:i')}}</span></small></p>@endif
                        <?php $selectedGame = $user->selectedGame();?>
                        <p class="mb0"><small class="text-muted">Obecnie: <span>{{$selectedGame['game']}} <span class="badge badge-info badge-muted">{{$selectedGame['platform']}}</span></small></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                
                <div class="col-12 col-xl-6 mb50">
                    @if($user->settings()->first()->lfc == 1)
                    <p><span class="color-info"><i class="fas fa-exclamation-circle"></i> Ten gracz poszukuje klanu</span></p>
                    @endif
                    @if(!empty($note[0]))
                    <div class="twoja-notatka">
                        <h6 class="text-muted">Notatka</h6>
                        <p class="mb0">{!! nl2br(e($note[0]->note))!!}</p>
                    </div>
                    @endif
                </div>
                
                
                @if($user->name != '' || $user->surname != '' || $user->voivodeship != 0 || $user->city != 0 || $user->sex != 0 || $user->www != '' || $user->show_age == '1')
                <div class="col-12 col-xl-6 mb50">
                    <h3 class="h-border-bottom">Informacje</h3>
                    @if($user->name != '')
                    <div class="row">
                        <div class="col-md-auto info-label">
                            Imię:
                        </div>
                        <div class="col info text-linia">
                            {{$user->name}}
                        </div>
                    </div>
                    @endif
                    @if($user->surname != '')
                    <div class="row">
                        <div class="col-md-auto info-label">
                            Nazwisko:   
                        </div>
                        <div class="col info text-linia">
                            {{$user->surname}}
                        </div>
                    </div>
                    @endif
                    @if($user->voivodeship != 0)
                    <div class="row">
                        <div class="col-md-auto info-label">
                            Województwo:
                        </div>
                        <div class="col info">
                                {{$user->voivode()}}
                        </div>
                    </div>
                    @endif
                    @if($user->city != 0)
                    <div class="row">
                        <div class="col-md-auto info-label">
                            Miasto:
                        </div>
                        <div class="col info">
                            {{$user->city()}}
                        </div>
                    </div>
                    @endif
                    @if($user->birth_date != '')
                    @if($user->show_age == '1')
                    <div class="row">
                        <div class="col-md-auto info-label">
                            Wiek:
                        </div>
                        <div class="col info">
                            {{$user->age()}}
                        </div>
                    </div>
                    @endif
                    @endif
                    @if($user->sex != null)
                    <div class="row">
                        <div class="col-md-auto info-label">
                            Płeć:
                        </div>
                        <div class="col info">
                            {{$user->sex()}}
                        </div>
                    </div>
                    @endif
                    @if($user->www != '')
                    <div class="row">
                        <div class="col-md-auto info-label">
                            WWW:
                        </div>
                        <div class="col info">
                                <?php
                                $url = $user->www;
                                if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
                                    $url = "http://" . $url;
                                }
                                ?>
                                    <a href="{{ $url }}" target="_blank">{{ preg_replace('#^https?://#', '', $user->www) }}</a>
                            
                        </div>
                    </div>
                    @endif
                    
                </div>
                @endif
                @if($user->description != '')
                <div class="col-12 col-xl-6 mb50">
                    <h3 class="h-border-bottom">Opis</h3>
                    <div class="row">
                        <div class="col-12">
                            <p>{!! nl2br(e($user->description)) !!}</p>
                        </div>
                    </div>
                </div>
                @endif

                
                @if(!empty($user->games->all()) || !empty($user->tags->first()))
                <div class="col-12 col-xl-6 mb50">
                    
                    <h3 class="h-border-bottom">Biblioteka gier</h3>
                    <div class="row mb30">
                        <div class="col-12">
                                @if(!empty($user->games->all()))
                            <input type="text" id="gamesInput" onkeyup="FiltrowanieListy()" placeholder="Wyszukaj...">
                            <ul id="gamesUL">
                                <?php $user->games = $user->games->unique('name');?>
                                @foreach($user->games as $game)
                                    @foreach($game->userPlatforms()->where('user_id', $user->id)->get() as $item)
                                        <li class="game-list-item"><span>{{$game->name}} <span class="badge badge-info">{{$item->short_name}}</span></span></li>
                                    @endforeach
                                @endforeach
                            </ul>
                            @else   
                                <p class="mb0">Biblioteka tego gracza jest pusta.</p>
                            @endif
                        </div>
                    </div>
                    
                    @if(!empty($user->tags->first()))
                    <div class="row">
                        <div class="col-12">
                            <h5>Tagi</h5>
                            @foreach($user->tags as $tag)
                                <span class="badge badge-info p5 mr5 mb5">{{$tag->tag}}</span>
                            @endforeach
                        </div>
                    </div>
                    @endif
                </div>
                @endif
                @if($user->psn != '' || $user->xboxlive != '' || $user->steam != '' || $user->origin != '' || $user->uplay != '' || $user->battlenet != '' || $user->epicgames != '' || $user->gog != '')
                <div class="col-12 col-xl-6 mb50">
                        <h3 class="h-border-bottom">Platformy</h3>
                        @if($user->psn != '')
                        <div class="row">
                            <div class="col-md-auto info-label">
                                PSN:
                            </div>
                            <div class="col info text-linia">
                                {{$user->psn}}
                            </div>
                        </div>
                        @endif
                        @if($user->xboxlive != '')
                        <div class="row">
                            <div class="col-md-auto info-label">
                                Xbox Live:   
                            </div>
                            <div class="col info text-linia">
                                {{$user->xboxlive}}
                            </div>
                        </div>
                        @endif
                        @if($user->steam != '')
                        <div class="row">
                            <div class="col-md-auto info-label">
                                Steam:
                            </div>
                            <div class="col info">
                                {{$user->steam}}
                            </div>
                        </div>
                        @endif
                        @if($user->origin != '')
                        <div class="row">
                            <div class="col-md-auto info-label">
                                Origin:
                            </div>
                            <div class="col info">
                                {{$user->origin}}
                            </div>
                        </div>
                        @endif
                        @if($user->uplay != '')
                        <div class="row">
                            <div class="col-md-auto info-label">
                                Uplay:
                            </div>
                            <div class="col info">
                                {{$user->uplay}}
                            </div>
                        </div>
                        @endif
                        @if($user->battlenet != '')
                        <div class="row">
                            <div class="col-md-auto info-label">
                                Battle.net:
                            </div>
                            <div class="col info">
                                {{$user->battlenet}}
                            </div>
                        </div>
                        @endif
                        @if($user->epicgames != '')
                        <div class="row">
                            <div class="col-md-auto info-label">
                                EPIC Games:
                            </div>
                            <div class="col info">
                                {{$user->epicgames}}
                            </div>
                        </div>
                        @endif
                        @if($user->gog != '')
                        <div class="row">
                            <div class="col-md-auto info-label">
                                GOG.com:
                            </div>
                            <div class="col info">
                                {{$user->gog}}
                            </div>
                        </div>
                        @endif
                    </div>
                    @endif
                    @if($user->clans()->count() > 0)
                    <div class="col-12 col-xl-6 mb50">
    
                        <h3 class="h-border-bottom">Członkostwa</h3>
    
                        <div class="row">
                            @foreach($user->clans as $clan) 
                            <div class="col-12">
                                <div class="czlonkostwo-wrapper">
                                    <div class="logo-klanu-wrapper">
                                            <a href="/klan/{{$clan->link}}">
                                                <div class="logo-klanu">
                                                    @if($clan->logo == 'no-clan-logo.jpg')
                                                    <img src="{{asset('images/clanlogo/')}}/no-clan-logo.jpg" class="menu-avatar img-fluid klan-def-border" alt="Logo klanowe">
                                                    @else 
                                                    <img src="{{ Storage::url($clan->logo)}}" class="img-fluid klan-def-border" alt="Logo klanu">
                                                    @endif
                
                                                </div>
                                            </a>
                                    </div>
                                    <div class="czlonkostwo-info">
                                        <h3 class="mb0"><a href="/klan/{{$clan->link}}">{{$clan->name}}</a></h3>
                                        <p class="text-muted mb0"> @if($clan->leader_id == $user->id) Lider @else Członek @endif</p>
                                        <p class="text-muted mb0"><small>{{$clan->members()->count()}} członków</small></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            @endforeach
                            
    
                        </div>
    
                    </div>
                    @endif

            </div>

        </div>


    </div>
@endsection

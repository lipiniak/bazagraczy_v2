@extends('layouts.app')

@section('content')
<div id="gracze-zarzadzanie" class="content">

        <div class="container-fluid small-page">

            <div class="row">

                <div class="col-12">

                    <h1 class="mb15">Obserwowani gracze</h1>

                    <div class="row">
                        <div class="col-12">
                            <input type="text" id="gamesInput" onkeyup="FiltrowanieListy()" placeholder="Wyszukaj...">
                            <ul id="gamesUL">

                                <!-- Pojedynczy wynik /start -->
                                @if (!empty($user->myfollowers->first()))
                                    @foreach($user->myfollowers as $follower)
                                    <li class="lista-graczy">
                                        <div class="pojedynczy-wynik">
                                            <div class="avatar-wrapper">
                                                    <a href="/profil/{{$follower->id}}">
                                                    @if($follower->avatar == 'no-avatar.jpg')
                                                    <img src="{{asset('images/avatars/')}}/{{$follower->avatar}}" class="menu-avatar" alt="Avatar">
                                                @else
                                                    <img src="{{ Storage::url($follower->avatar) }}" class="menu-avatar" alt="Avatar">
                                                @endif
                                                    </a>
                                            </div>
                                            <div class="gracz-info">
                                                <a href='{{route("profile.user",["id"=> $follower->id])}}'>
                                                    <span>{{$follower->login}}
                                                        <i>#{{$follower->id}}</i>
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="gracz-akcje">
                                                <a href="{{route('messages.conversation',['id'=> $follower->id])}}" class="btn btn-secondary akcja">Rozmowa</a>
                                                <a href='{{route("profile.unfollow",["id"=> $follower->id])}}' class="btn btn-secondary akcja">Nie obserwuj</a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </li>
                                    @endforeach
                                @else 
                                    Nikogo nie obserwujesz.
                                @endif
                                <!-- Pojedynczy wynik /koniec -->

                                


                            </ul>
                        </div>
                    </div>



                </div>

            </div>

        </div>

    </div>
@endsection
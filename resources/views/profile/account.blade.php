@extends('layouts.app')

@section('content')
<div id="profil-konto" class="content">

        @if(Auth::user()->id == $user->id)
        @include('profile.menu')
        @endif



        <div class="container-fluid small-page">

            <div class="row">

                <div class="col-12">

                    <h1 class="mb30">Edycja danych logowania</h1>
                    @if($user->provider == 'bazagraczy')
                    <h4>Zmień hasło</h4>
                    <form method='get' action="{{route('profile.account.password')}}" autocomplete="off">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label-custom">Aktualne hasło *</label>
                                    <input class="form-control" name="password_old" type="password" value="" autocomplete="off" autofill='off'>
                                    @if ($errors->has('password_old'))
                                    <div class="error-feedback">
                                            {{ $errors->first('password_old') }}<br>
                                    </div>
                                    @endif
                                    <small class="form-text text-muted">
                                        <span>Po udanej zmianie hasła, zostaniesz automatycznie wylogowany.</span>
                                    </small>
                                </div>
                                <div class="form-group">
                                    <label class="form-label-custom">Nowe hasło *</label>
                                    <input class="form-control" name="password" type="password" value="" autocomplete="off" autofill='off'>
                                    @if ($errors->has('password'))
                                    <div class="error-feedback">
                                            {{ $errors->first('password') }}<br>
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="form-label-custom">Nowe hasło (powtórz) *</label>
                                    <input class="form-control" name="password_confirmation" type="password" value="" autocomplete="off" autofill='off'>
                                    @if ($errors->has('password_confirmation'))
                                    <div class="error-feedback">
                                        {{ $errors->first('password_confirmation') }}<br>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-12">
                                <hr class="separator">
                                <div class="form-group text-right">
                                    <button type="submit" class="btn btn-primary">Zapisz</button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <h4>Zmień email</h4>
                    <form method='get' action="{{route('profile.account.email')}}" autocomplete="off">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <p  class="mt-5"><span class="text-muted">{{$user->email}}</span></p>
                                </div>
                                <div class="form-group">
                                    <label class="form-label-custom">Nowy adres email *</label>
                                    <input class="form-control" name="email" type="text" value="{{old('email')}}" autocomplete="off" autofill='off'>
                                    @if ($errors->has('email'))
                                    <div class="error-feedback">
                                        {{ $errors->first('email') }}<br>
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="form-label-custom">Nowy adres email (powtórz) *</label>
                                    <input class="form-control" name="email_confirmation" type="text" value="{{old('email_confirmation')}}" autocomplete="off" autofill='off'>
                                    @if ($errors->has('email_confirmation'))
                                    <div class="error-feedback">
                                            {{ $errors->first('email_confirmation') }}<br>
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="form-label-custom">Aktualne hasło *</label>
                                    <input class="form-control" name="password" type="password" value="" autocomplete="off" autofill='off' readonly  
                                    onfocus="this.removeAttribute('readonly');">
                                    @if ($errors->has('password'))
                                    <div class="error-feedback">
                                            {{ $errors->first('password') }}<br>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-12">
                                <hr class="separator">
                                <div class="form-group text-right">
                                    <button type="submit" class="btn btn-primary">Zapisz</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    @else
                    <div class="row">
                        <div class="col-12">
                            <div class="alert alert-info" role="alert">
                                Ten profil został stworzony przy użyciu jednego z serwisów społecznościowych i jest z nim powiązany. Edycja danych logowania nie jest możliwa.
                            </div>
                        </div>
                    </div>
                    @endif
                </div>

            </div>

        </div>

    </div>
@endsection
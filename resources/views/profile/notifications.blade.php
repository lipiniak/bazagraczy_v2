@extends('layouts.app')

@section('content')
<div id="profil-ustawienia" class="content">

        @if(Auth::user()->id == $user->id)
        @include('profile.menu')
        @endif

        <div class="container-fluid">

            <div class="row">

                <div class="col-12">
                    <h1>Powiadomienia</h1>
                    @if(count($user->notifications) > 0)
                    <a href="/profil/powiadomienia/przeczytaj" class="btn btn-secondary btn-sm mr3 mb3">Oznacz jako przeczytane</a> <a href="/profil/powiadomienia/usun/wszystkie" class="btn btn-secondary btn-sm mr3 mb3">Usuń wszystkie</a>
                    
                        @foreach($user->notifications as $notification)
                        
                        <div class="powiadomienie-outer @if($notification->read_at == NULL) nowe @endif">
                            <div class="dropdown show">
                                <a class="btn btn-link dropdown-toggle" href="#" role="button" id="powiadomienieID1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-ellipsis-h"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="powiadomienieID1">
                                    <a href="#"  class="dropdown-item" onclick="event.preventDefault();
                                        document.getElementById('delete{{$notification->id}}').submit();">
                                        Usuń powiadomienie
                                    </a>
                                    <form id="delete{{$notification->id}}" action="/profil/powiadomienia/usun" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                            <input type="hidden" name='notification_id' value="{{$notification->id}}">
                                    </form>
                                    @if($notification->read_at == NULL) 
                                    <a href="#"  class="dropdown-item" onclick="event.preventDefault();
                                        document.getElementById('read{{$notification->id}}').submit();">
                                        Oznacz jako przeczytane
                                    </a>
                                    <form id="read{{$notification->id}}" action="/profil/powiadomienia/oznacz" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                            <input type="hidden" name='notification_id' value="{{$notification->id}}">
                                    </form>
                                    
                                    @else
                                    <a href="#"  class="dropdown-item" onclick="event.preventDefault();
                                        document.getElementById('unread{{$notification->id}}').submit();">
                                        Oznacz jako nieprzeczytane
                                    </a>
                                    <form id="unread{{$notification->id}}" action="/profil/powiadomienia/odznacz" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                            <input type="hidden" name='notification_id' value="{{$notification->id}}">
                                    </form>
                                    @endif
                                </div>
                            </div>
                            
                            <p class="text-muted mb5"><small><span value='{{strtotime($notification->created_at)}}' class='time-from'></span></small></p>
                            <p class="mb0">{!!$notification->data['data']!!}</p>
                        </div>
                        @endforeach
                    @else
                        <br>Brak powiadomień
                    @endif
                    

                </div>

            </div>

        </div>

    </div>
    <script>
            function redirect(element,e) {
                // ajax do php z notification id i url
                console.log(element);
                
                var data = $(element).data();
                var token = $('meta[name="csrf-token"]').attr('content');
                var redirect = data.href;
                var notificationid = data.notificationid;

                $.post( "/profil/powiadomienia/przekieruj", { _token: token, notificationid: notificationid, redirect: redirect } );
                
                location.href = redirect;
            }

            const ELEMENT_CLASS = '.time-from';
            const DATE_ATTR_NAME = 'value';

            const TEN_SECONDS_MILLIS = 10 * 1000;
            const MINUTE_MILLIS = 60 * 1000;
            const HOUR_MILLIS = 60 * MINUTE_MILLIS;
            const DAY_MILLIS = 24 * HOUR_MILLIS;

            const UNDER_MINUTE_REFRESH_MILLIS = TEN_SECONDS_MILLIS; // every 10 seconds
            const UNDER_HOUR_REFRESH_MILLIS = MINUTE_MILLIS; // every minute
            const UNDER_DAY_REFRESH_MILLIS = HOUR_MILLIS; // every hour
            const OVER_DAY_REFRESH_MILLIS = DAY_MILLIS; // every day


            function getRandomTimeoutId() {
            return Math.floor(Math.random() * (1e16));
            }

            function timeToText(element, timeoutId) {
            const date = parseInt(element.attr(DATE_ATTR_NAME)) * 1000;
            //const millis = (Date.now() - date);
            const now = Date.now();
            const millis = date > now ? 1 : (now - date);
            
            if (timeoutId) {
                window.clearTimeout(timeoutId);
            }
            const tId = getRandomTimeoutId();

            let text, refreshRate;

            if (millis < MINUTE_MILLIS) {
                refreshRate = UNDER_MINUTE_REFRESH_MILLIS;
                text = underMinuteText(millis);
            } else if (millis < HOUR_MILLIS) {
                refreshRate = UNDER_HOUR_REFRESH_MILLIS;
                text = underHourText(millis);
            } else if (millis < DAY_MILLIS) {
                refreshRate = UNDER_DAY_REFRESH_MILLIS;
                text = underDayText(millis);
            } else {
                refreshRate = OVER_DAY_REFRESH_MILLIS;
                text = overDayText(millis);
            }

            setTimeout(function() {
                timeToText(element, tId);
            }, refreshRate);

            element.text(text);
            }

            function underMinuteText(millis) {
            const tensOfSeconds = Math.floor(millis / TEN_SECONDS_MILLIS);
            if (tensOfSeconds < 1) {
                return `${Math.floor(millis / 1000)} sek. temu`;
            }
            return `${tensOfSeconds * 10} sek. temu`;
            }

            function underHourText(millis) {
            const minutes = Math.floor(millis / MINUTE_MILLIS);
            return `${minutes} min. temu`;
            }

            function underDayText(millis) {
            const hours = Math.floor(millis / HOUR_MILLIS);
            return `${hours} godz. temu`;
            }

            function overDayText(millis) {
            const days = Math.floor(millis / DAY_MILLIS);
            if (days === 1) {
                return "1 dzień temu";
            } else {
                return `${days} dni temu`;
            }
            }

            $(document).ready(function() {
            $(ELEMENT_CLASS).each(function(i, element) {
                timeToText($(element));
            });
            });

    </script>
@endsection
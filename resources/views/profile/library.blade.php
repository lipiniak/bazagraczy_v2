@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" />
        
        <div id="profil-edycja" class="content">

                @if(Auth::user()->id == $user->id)
                @include('profile.menu')
                @endif

                <script>
                        

                    $(document).ready(function () {
                        $('#biblioteka-platform').select2({
                            placeholder: "Wybierz...",
                            dropdownCssClass : "platformal",
                        });
                    });
                    
                    
                </script>


                <div class="container-fluid small-page">

                    <div class="row">

                        <div class="col-12 mb30">
                                <div class="alert-info p15 pt20" style="border-radius: 3px;">
                                    <h4>Dodaj do biblioteki</h4>
                                    <div class="biblioteka-dodaj">

                                        <form method='post' action='{{route("profile.library.add")}}'>
                                            {{ csrf_field() }}
                                            <div class="title">
                                                <div class="form-group" style="width:100%">
                                                    <label for="topbar-title" class="sr-only">Tytuł gry</label>
                                                    <select name="game" class="form-control" id="biblioteka-title" style="width:100%">
                                                        <option></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="platform">
                                                <div class="form-group" style="width:100%">
                                                    <label for="topbar-platform" class="sr-only">Platforma</label>
                                                    <select name="platform"class="form-control" id="biblioteka-platform" style="width:100%" data-minimum-results-for-search="Infinity">
                                                            <option></option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="dodaj-button">
                                                <button class="btn btn-primary">Dodaj</button>
                                            </div>
                                            
                                                    <script>
                                                            $.ajax({
                                                                url: "/helper/games",
                                                                type: "GET",
                                                                }).then(function (response) {
                                                                $("#biblioteka-title").select2({
                                                                    placeholder: "Wybierz...",
                                                                    dropdownCssClass : "gral",
                                                                    data: response['results']
                                                                }).on('change',function() {
                                                                    
                                                                    var value= $('#biblioteka-title option:selected').attr('value');
                            
                                                                    var url = "/helper/platforms/"+value;
                                                
                                                                        $.ajax({
                                                                            type: "GET",
                                                                            url: url,
                                                                            success: function(data)
                                                                            {
                                                                                
                                                                                $('#biblioteka-platform').empty();
                                                                                $("#biblioteka-platform").select2({
                                                                                    placeholder: "Wybierz...",
                                                                                    dropdownCssClass : "platformal",
                                                                                    data: data
                                                                                });
                                                                            }
                                                                        });
                                                                    
                                                                });;  
                                                                });
                                                                $('#biblioteka-platform').on('select2:open', function (e) {
                                
                                                                $('.platformal .select2-search input').prop('focus',false);
                                                            });
                                                                $('#biblioteka-title').on('select2:open', function (e) {
                                
                                                                    $('.gral .select2-search input').prop('focus',false);
                                                            });
                                                            </script>
                                            @if ($errors->has('game') || $errors->has('platform'))
                                            <div class="error-feedback mt-10">
                                                    {{ $errors->first('game') }}<br>{{ $errors->first('platform') }}<br>
                                                <br>
                                            </div>
                                            @endif
                                        </form>

                                    </div>
                                </div>
                        </div>

                        <div class="col-12">
                            <h4>Twoja biblioteka</h4>
                            <input type="text" id="gamesInput" onkeyup="FiltrowanieListy()" placeholder="Wyszukaj...">
                            @if (!empty($user->games->first()))
                            <ul id="gamesUL">
                                <?php $user->games = $user->games->unique('name');?>
                                @foreach($user->games as $game)
                                @foreach($game->userPlatforms()->where('user_id', $user->id)->get() as $item)
                                <!-- Pojedynczy wynik /start -->
                                <li class="game-list-item biblioteka"><span>{{$game->name}} <span class="badge badge-info"> {{$item->short_name}}</span></span><button type="button" class="usun-gre" data-toggle="modal" data-target="#deleteGameId{{$game->id}}{{$item->id}}">&times;</button></li>
                                <div class="modal fade" id="deleteGameId{{$game->id}}{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteGameId1Title" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deleteGameId1Title">Uwaga!</h5>
                                            </div>
                                            <div class="modal-body text-center">
                                                <p class="mb10">Czy na pewno chcesz usunąć</p> <p class="mb10"><strong>{{$game->name}} <span class="badge badge-info">{{$item->short_name}}</span></strong></p> <p class="mb0">ze swojej biblioteki?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="{{route('profile.library.remove',['game_id'=>$game->id,'platform_id'=>$item->id])}}" class="btn btn-primary">Tak</a>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Nie</button>                  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Pojedynczy wynik /koniec -->
                                @endforeach
                                @endforeach
                                

                            </ul>
                            @else
                            <p class="text-muted"><small>Twoja biblioteka gier jest pusta.</small></p>
                            @endif
                        </div>

                    </div>

                </div>

            </div>
        <script src="{{ asset('js/moment.js') }}"></script>
        <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
        <script src="{{ asset('js/pl.js') }}"></script>
@endsection
@extends('layouts.app')

@section('content')
<div id="profil-avatar" class="content">
        @if(Auth::user()->id == $user->id)
        @include('profile.menu')
        @endif

    <div class="container-fluid xsmall-page">

        <div class="row">

            <div class="col-12">

                <div class="avatar-wrapper">
                    <div class="avatar-inner">

                            @if($user->avatar == 'no-avatar.jpg')
                                <img src="{{asset('images/avatars/')}}/{{$user->avatar}}" class="menu-avatar img-fluid" alt="Avatar">
                            @else
                                <img src="{{ Storage::url($user->avatar) }}" class="menu-avatar img-fluid" alt="Avatar">
                            @endif
                    </div>
                </div>
                
                <div class="del-btn">
                    @if($user->avatar != 'no-avatar.jpg')
                        <a class="btn btn-secondary" data-toggle="modal" data-target="#deleteAvatar">Usuń</a>
                    @endif
                </div>
                
                <!-- MODAL / Start -->
                <div class="modal fade" id="deleteAvatar" tabindex="-1" role="dialog" aria-labelledby="deleteAvatarTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="deleteAvatarTitle">Uwaga!</h5>
                            </div>
                            <div class="modal-body text-center">
                                Czy na pewno chcesz usunąć avatar?
                            </div>
                            <div class="modal-footer">
                                <a href="{{route('profile.avatar.delete')}}" class="btn btn-primary" >Tak</a>
                                <button class="btn btn-secondary" data-dismiss="modal">Nie</button>                  
                            </div>
                        </div>
                    </div>
                </div>
                <!-- MODAL / Koniec -->

                {{ Form::model($user, ['route' => ['profile.avatar.change'],'files'=> true]) }}
                    <div class="form-group">
                        <div class="custom-file">
                            
                            {{Form::file('avatar',['class'=>'custom-file-input','id'=>'avatarFile'])}}
                            <label class="custom-file-label" for="avatarFile">Wybierz plik...</label>
                            @if ($errors->has('avatar'))
                            <div class="error-feedback">
                                    {{ $errors->first('avatar') }}<br>
                            </div>
                            @endif
                            <script>
                                    $("input[type=file]").change(function () {
                                        var fieldVal = $(this).val();
                                      
                                        // Change the node's value by removing the fake path (Chrome)
                                        fieldVal = fieldVal.replace("C:\\fakepath\\", "");
                                          
                                        if (fieldVal != undefined || fieldVal != "") {
                                          $(this).next(".custom-file-label").attr('data-content', fieldVal);
                                          $(this).next(".custom-file-label").text(fieldVal);
                                        }
                                      
                                      });
                            </script>
                            <small id="avatarHelp" class="form-text text-muted">W celu uzyskania odpowiedniej jakości avatara zaleca się wybór obrazka o kwadratowym kształcie. Maksymalny rozmiar pliku to 500kb. w formacie .jpg, .gif, lub .png.</small>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-primary">Zapisz</button>
                    </div>
                </form>

            </div>

        </div>

    </div>

</div>
@endsection
@extends('layouts.app')

@section('content')
<div id="gracze-zarzadzanie" class="content">

        <div class="container-fluid small-page">

            <div class="row">

                <div class="col-12">

                    <h1 class="mb15">Zablokowani gracze</h1>

                    <div class="row">
                        <div class="col-12">
                            <input type="text" id="gamesInput" onkeyup="FiltrowanieListy()" placeholder="Wyszukaj...">
                            <ul id="gamesUL">

                                <!-- Pojedynczy wynik /start -->
                                @if (!empty($user->myblocked->first()))
                                    @foreach($user->myblocked as $blocked)
                                    <li class="lista-graczy">
                                        <div class="pojedynczy-wynik">
                                            <div class="avatar-wrapper">
                                                    <a href="/profil/{{$blocked->id}}">
                                                    @if($blocked->avatar == 'no-avatar.jpg')
                                                    <img src="{{asset('images/avatars/')}}/{{$blocked->avatar}}" class="menu-avatar" alt="Avatar">
                                                @else
                                                    <img src="{{ Storage::url($blocked->avatar) }}" class="menu-avatar" alt="Avatar">
                                                @endif
                                                    </a>
                                            </div>
                                            <div class="gracz-info">
                                                <a href='{{route("profile.user",["id"=> $blocked->id])}}'>
                                                    <span>{{$blocked->login}}
                                                        <i>#{{$blocked->id}}</i>
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="gracz-akcje">
                                                <a href='{{route("profile.unblock",["id"=> $blocked->id])}}' class="btn btn-secondary akcja">Odblokuj</a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </li>
                                    @endforeach
                                @else 
                                    Nikogo nie zablokowałeś.
                                @endif
                                <!-- Pojedynczy wynik /koniec -->

                                


                            </ul>
                        </div>
                    </div>



                </div>

            </div>

        </div>

    </div>
@endsection
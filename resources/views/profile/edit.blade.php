@extends('layouts.app')

@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" />
        
        <div id="profil-edycja" class="content">
                @if(Auth::user()->id == $user->id)
                @include('profile.menu')
                @endif

            
            <div class="container-fluid small-page">

                {{ Form::model($user, ['route' => ['profile.update']],['autocomplete'=>'off']) }}

                    <div class="row">

                        <div class="col-12 col-lg-6">
                            <h4>Podstawowe informacje</h4>
                            <div class="form-group">
                                <label class="form-label-custom" for="profil-nazwa">Nazwa *</label>
                                
                                {{Form::text('login',null,['class' => 'form-control', 'id'=>'profil-nazwa'])}}
                                @if ($errors->has('login'))
                                <div class="error-feedback">
                                        {!! $errors->first('login') !!}<br>
                                    
                                </div>
                                @endif
                                
                            </div>
                            <div class="form-group" style="width:100%">
                                <label class="form-label-custom" for="profil-edycja-plec">Płeć *</label>
                                {{Form::select('sex', [''=>'','meale' => 'Mężczyzna', 'female' => 'Kobieta'],null,['class'=> 'form-control','id'=>'profil-edycja-plec','style'=>'width:100%'])}}
                                <script>
                                    $('#profil-edycja-plec').select2({
                                        placeholder: "Wybierz...",
                                        dropdownCssClass : "plec",
                                      });
                                      $('#profil-edycja-plec').on('select2:open', function (e) {
                                          
                                        $('.plec .select2-search input').prop('focus',false);
                                    });
                                    
                                    </script>
                                @if ($errors->has('sex'))
                                <div class="error-feedback">
                                        {{ $errors->first('sex') }}<br>
                                    
                                </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="form-label-custom" for="profil-imie">Imię</label>
                                
                                {{Form::text('name',null,['class' => 'form-control', 'id'=>'profil-imie'])}}
                                @if ($errors->has('name'))
                                <div class="error-feedback">
                                        {{ $errors->first('name') }}<br>
                                    
                                </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="form-label-custom" for="profil-nazwisko">Nazwisko</label>
                                {{Form::text('surname',null,['class' => 'form-control', 'id'=>'profil-nazwisko'])}}
                                @if ($errors->has('surname'))
                                <div class="error-feedback">
                                        {{ $errors->first('surname') }}<br>
                                    
                                </div>
                                @endif                                       
                            </div>
                            <div class="form-group" style="width:100%">
                                <label class="form-label-custom" for="profil-edycja-wojewodztwo">Województwo</label>
                                {{Form::select('voivodeship',$void,null,['class'=> 'form-control','id'=>'profil-edycja-wojewodztwo','style'=>'width:100%'])}}
                                <script>
                                    $('#profil-edycja-wojewodztwo').on('select2:open', function (e) {
                                          
                                        $('.wojewodztwo .select2-search input').prop('focus',false);
                                    });
                                    $('#profil-edycja-wojewodztwo').select2({ dropdownCssClass : "wojewodztwo", }
                                    ).on('change',function() {
                                          
                                          var value= $('#profil-edycja-wojewodztwo option:selected').attr('value');

                                          var url = "/helper/cities/"+value;
                    
                                            $.ajax({
                                                type: "GET",
                                                url: url,
                                                success: function(data)
                                                {
                                                    
                                                    $('#profil-edycja-miasto').empty();
                                                    $("#profil-edycja-miasto").select2({
                                                        data: data['results'],
                                                        dropdownCssClass : "miasto"
                                                    });
                                                }
                                            });
                                          
                                    });

                                    </script>


                                    
                            </div>
                            <div class="form-group" style="width:100%">
                                <label class="form-label-custom" for="profil-edycja-miasto">Miasto</label>
                                @if(session()->has('city'))
                                    <?php $city = session('city');?>
                                @endif
                                {{Form::select('city',$city,null,['class'=> 'form-control','id'=>'profil-edycja-miasto','style'=>'width:100%','autocomplete'=>"off"])}}
                                
                                <script>
                                        

                                        $("#profil-edycja-miasto").select2({ dropdownCssClass : "miasto" });
                                        $('#profil-edycja-miasto').on('select2:open', function (e) {
                                          
                                            $('.miasto .select2-search input').prop('focus',false);
                                        });
                                </script>
                            </div>
                            <div class="form-group">
                                <label class="form-label-custom" for="profil-dataur">Data ur. *</label>
                                <?php
                                    if($user->birth_date == NULL)
                                        $date = null;
                                    else
                                        $date = date('d.m.Y',strtotime($user->birth_date));
                                ?>
                                {{Form::text('birth_date',$date, ['class' => 'form-control readonly-datepicker','id' => 'form-dataur','autocomplete'=>'off','readonly'])}}
                                <script>
                                $(function () {
                                        $('#form-dataur').datetimepicker(
                                            {format: 'DD.MM.YYYY',
                                                locale: 'pl',
                                                useCurrent: false,
                                                ignoreReadonly: true,
                                                minDate : '1900/01/01',
                                                maxDate: moment(),
                                                viewMode: 'years',
                                                icons: {
                                                time: 'fa fa-clock-o',
                                                date: 'fa fa-calendar',
                                                up: 'fa fa-chevron-up',
                                                down: 'fa fa-chevron-down',
                                                //previous: 'glyphicon glyphicon-chevron-left',
                                                previous: 'fa fa-chevron-left',
                                                next: 'fa fa-chevron-right',
                                                today: 'fa fa-crosshairs',
                                                clear: 'fa fa-trash',
                                                close: 'fa fa-remove',
                                                
                                            },
                                            }
                                        );
                                    });
                                </script>
                                @if ($errors->has('birth_date'))
                                <div class="error-feedback">
                                        {{ $errors->first('birth_date') }}<br>
                                    
                                </div>
                                @endif
                            </div>
                            <div class="form-group custom-control custom-checkbox">
                                <?php
                                if($user->show_age == 0)
                                    $checked = false;
                                if($user->show_age == 1)
                                    $checked = true;
                                if($user->show_age == 2) 
                                    $checked = true;
                                ?>
                                {{Form::checkbox('show_age',null,$checked,['class'=>'custom-control-input','id'=>'pokaz-wiek'])}}
                                <label class="custom-control-label" for="pokaz-wiek">Pokaż wiek na stronie</label>
                            </div>
                            <div class="form-group">
                                <label class="form-label-custom" for="osobie">Opis</label>
                                {{Form::textarea('description',null, ['class' => 'form-control','id' => 'osobie','style'=>'min-height: 100px'])}}
                                
                                @if ($errors->has('description'))
                                <div class="error-feedback">
                                        {{ $errors->first('description') }}<br>
                                    
                                </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="form-label-custom" for="profil-www">Adres WWW</label>
                                
                                {{Form::text('www',null,['class' => 'form-control', 'id'=>'profil-www'])}}
                                @if ($errors->has('www'))
                                <div class="error-feedback">
                                        {{ $errors->first('www') }}<br>
                                    
                                </div>
                                @endif
                                
                            </div>
                        </div>

                        <div class="col-12 col-lg-6">
                            <h4>Platformy</h4>
                            <div class="form-group">
                                <label class="form-label-custom" for="profil-steam">Steam</label>
                                
                                {{Form::text('steam',null,['class' => 'form-control', 'id'=>'profil-steam','placeholder'=>'Twój nick'])}}
                                @if ($errors->has('steam'))
                                <div class="error-feedback">
                                        {{ $errors->first('steam') }}<br>
                                    
                                </div>
                                @endif
                                
                            </div>
                            <div class="form-group">
                                <label class="form-label-custom" for="profil-origin">Origin</label>
                                
                                {{Form::text('origin',null,['class' => 'form-control', 'id'=>'profil-origin','placeholder'=>'Twój nick'])}}
                                @if ($errors->has('origin'))
                                <div class="error-feedback">
                                        {{ $errors->first('origin') }}<br>
                                    
                                </div>
                                @endif
                                
                            </div>
                            <div class="form-group">
                                <label class="form-label-custom" for="profil-origin">PSN</label>
                                
                                {{Form::text('psn',null,['class' => 'form-control', 'id'=>'profil-psn','placeholder'=>'Twój nick'])}}
                                @if ($errors->has('psn'))
                                <div class="error-feedback">
                                        {{ $errors->first('psn') }}<br>
                                    
                                </div>
                                @endif
                                
                            </div>
                            <div class="form-group">
                                <label class="form-label-custom" for="profil-origin">XBOX Live</label>
                                
                                {{Form::text('xboxlive',null,['class' => 'form-control', 'id'=>'profil-xboxlive','placeholder'=>'Twój nick'])}}
                                @if ($errors->has('xboxlive'))
                                <div class="error-feedback">
                                        {{ $errors->first('xboxlive') }}<br>
                                    
                                </div>
                                @endif
                                
                            </div>
                            <div class="form-group">
                                <label class="form-label-custom" for="profil-uplay">Uplay</label>
                                {{Form::text('uplay',null,['class' => 'form-control', 'id'=>'profil-uplay','placeholder'=>'Twój nick'])}}
                                @if ($errors->has('uplay'))
                                <div class="error-feedback">
                                        {{ $errors->first('uplay') }}<br>
                                    
                                </div>
                                @endif
                                
                            </div>
                            <div class="form-group">
                                <label class="form-label-custom" for="profil-battlenet">Battle.net</label>
                                
                                {{Form::text('battlenet',null,['class' => 'form-control', 'id'=>'profil-battlenet','placeholder'=>'Twój nick'])}}
                                @if ($errors->has('battlenet'))
                                <div class="error-feedback">
                                        {{ $errors->first('battlenet') }}<br>
                                    
                                </div>
                                @endif
                                
                            </div>
                            <div class="form-group">
                                <label class="form-label-custom" for="profil-epic">Epic Games</label>
                                {{Form::text('epicgames',null,['class' => 'form-control', 'id'=>'profil-epic','placeholder'=>'Twój nick'])}}
                                @if ($errors->has('epicgames'))
                                <div class="error-feedback">
                                        {{ $errors->first('epicgames') }}<br>
                                    
                                </div>
                                @endif
                                
                            </div>
                            <div class="form-group">
                                <label class="form-label-custom" for="profil-gog">GOG.com</label>
                                {{Form::text('gog',null,['class' => 'form-control', 'id'=>'profil-gog','placeholder'=>'Twój nick'])}}
                                @if ($errors->has('gog'))
                                <div class="error-feedback">
                                        {{ $errors->first('gog') }}<br>
                                    
                                </div>
                                @endif
                                
                            </div>
                            <h4>Tagi</h4>
                            <p class="text-muted"><small>Otaguj swój profil w taki sposób, aby dostarczyć innym graczom istotnych informacji np. o twojej randze w danej grze.</small></p>
                            
                            <select name="tags[]" class='form-control' id='profil-tagi' style='width:100%' multiple="multiple">
                                @if(empty(old('tags')))
                                    @foreach($user->tags as $tag) 
                                        <option selected="selected" value='{{$tag->tag}}'>{{$tag->tag}}</option>
                                    @endforeach
                                @else
                                    @foreach(old('tags') as $tag)
                                        <option selected="selected" value='{{$tag}}'>{{$tag}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <script>
                                $('#profil-tagi').select2({
                                    tags: true
                                });

                            </script>
                        </div>

                        <div class="col-12">
                            <hr class="separator">
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-primary">Zapisz</button>
                            </div>
                        </div>

                    </div>

                {{ Form::close() }}

            </div>

        </div>
        <script src="{{ asset('js/moment.js') }}"></script>
        <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
        <script src="{{ asset('js/pl.js') }}"></script>
@endsection
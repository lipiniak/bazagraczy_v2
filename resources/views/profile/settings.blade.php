@extends('layouts.app')

@section('content')
<div id="profil-ustawienia" class="content">

        @if(Auth::user()->id == $user->id)
        @include('profile.menu')
        @endif

        <script>
                $(document).ready(function () {
                    $('#ustawienia-szukam-klanu').select2({
                        placeholder: "Wybierz...",
                        minimumResultsForSearch: Infinity
                    }).on('change', function(){
                        var value= $('#ustawienia-szukam-klanu option:selected').attr('value');

                        var url = "/profil/ustawienia/klan/"+value;
                    
                            $.ajax({
                                type: "GET",
                                url: url,
                                success: function(data)
                                {
                                    
                                }
                            });
                    });
                    $('#ustawienia-dzwiek-notyfikacji').select2({
                        placeholder: "Wybierz...",
                        minimumResultsForSearch: Infinity
                    }).on('change', function(){
                        var value= $('#ustawienia-dzwiek-notyfikacji option:selected').attr('value');

                        var url = "/profil/ustawienia/powiadomienia/"+value;
                    
                            $.ajax({
                                type: "GET",
                                url: url,
                                success: function(data)
                                {
                                    
                                }
                            });
                    });
                    $('#ustawienia-dzwiek-wiadomosci').select2({
                        placeholder: "Wybierz...",
                        minimumResultsForSearch: Infinity
                    }).on('change', function(){
                        var value= $('#ustawienia-dzwiek-wiadomosci option:selected').attr('value');

                        var url = "/profil/ustawienia/wiadomosci/"+value;
                    
                            $.ajax({
                                type: "GET",
                                url: url,
                                success: function(data)
                                {
                                    
                                }
                            });
                    });
                    $('#notify_all_games').select2({
                        placeholder: "Wybierz...",
                        minimumResultsForSearch: Infinity
                    }).on('change', function(){
                        var value= $('#notify_all_games option:selected').attr('value');

                        if(value == 2) {
                            $('#selected_games').show();
                        } else 
                            $('#selected_games').hide();

                        var url = "/profil/ustawienia/notify_all_games/"+value;
                    
                            $.ajax({
                                type: "GET",
                                url: url,
                                success: function(data)
                                {
                                    
                                }
                            });
                    });
                    $('#select_notify_games').select2({
                        placeholder: "Wybierz...",
                        minimumResultsForSearch: Infinity
                    }).on("select2:select select2:unselect", function (e) {

                        //this returns all the selected item
                        var items= $(this).val();       
                        console.log(items);
                        //Gets the last selected item
                        var lastSelectedItem = e.params.data.id;
                        console.log(lastSelectedItem);

                        var url = "/profil/ustawienia/select_games/"+lastSelectedItem;
                    
                        $.ajax({
                            type: "GET",
                            url: url,
                            success: function(data)
                            {
                                
                            }
                        });    
                    });
                    
                    
                    
                    
                });
        </script>
        <div class="container-fluid small-page">

            <div class="row">

                <div class="col-12 mb30">
                    
                    <h4 class="mb15">Ustawienia profilowe</h4>

                    <div class="ustawienia-wrapper">
                        <div class="row" style="position: relative;">
                            <div class="col-12 col-md-7 col-lg-8">
                                <p class="mb0">Szukam klanu</p>
                                <p class="mb5 text-muted"><small>Zaznaczenie tej opcji spowoduje wyświetlenie się stosownej informacji na twojej karcie profilowej oraz wpłynie na wyniki wyszukiwania przez innych graczy.</small></p>
                            </div>
                            <div class="col-12 col-md-5 col-lg-4">
                                <div class="form-group" style="width:100%">
                                    <div class="ustawienia-opcje fr">
                                        <label for="ustawienia-szukam-klanu" class="sr-only">Szukam klanu</label>
                                        <select class="form-control ustawienia-opcje" id="ustawienia-szukam-klanu" data-minimum-results-for-search="Infinity">
                                            <option></option>
                                            <option value='0' @if($user->settings()->first()->lfc == 0) selected @endif>Nie</option>
                                            <option value='1' @if($user->settings()->first()->lfc == 1) selected @endif>Tak</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mb30">
                    <h4 class="mb15">Ustawienia powiadomień</h4>
                    <div class="ustawienia-wrapper">
                        <div class="row" style="position: relative;">
                                <div class="col-12 col-md-7 col-lg-8">
                                    <p class="mb0">Dźwięk powiadomień</p>
                                    <p class="mb5 text-muted"><small>Włącz/wyłącz dźwięki powiadomień.</small></p>
                                </div>
                                <div class="col-12 col-md-5 col-lg-4">
                                    <div class="form-group" style="width:100%">
                                        <div class="ustawienia-opcje fr">
                                            <label for="ustawienia-dzwiek-notyfikacji" class="sr-only">Szukam klanu</label>
                                            <select class="form-control ustawienia-opcje" id="ustawienia-dzwiek-notyfikacji" data-minimum-results-for-search="Infinity">
                                                <option></option>
                                                <option value='0' @if($user->settings()->first()->notification_sound == 0) selected @endif>Nie</option>
                                                <option value='1' @if($user->settings()->first()->notification_sound == 1) selected @endif>Tak</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="ustawienia-wrapper">
                            <div class="row" style="position: relative;">
                                    <div class="col-12 col-md-7 col-lg-8">
                                        <p class="mb0">Dźwięk wiadomości</p>
                                        <p class="mb5 text-muted"><small>Włącz/wyłącz dźwięk wiadomości.</small></p>
                                    </div>
                                    <div class="col-12 col-md-5 col-lg-4">
                                        <div class="form-group" style="width:100%">
                                            <div class="ustawienia-opcje fr">
                                                <label for="ustawienia-dzwiek-wiadomosci" class="sr-only">Szukam klanu</label>
                                                <select class="form-control ustawienia-opcje" id="ustawienia-dzwiek-wiadomosci" data-minimum-results-for-search="Infinity">
                                                    <option></option>
                                                    <option value='0' @if($user->settings()->first()->messenger_sound == 0) selected @endif>Nie</option>
                                                    <option value='1' @if($user->settings()->first()->messenger_sound == 1) selected @endif>Tak</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                        </div>
                    
                        <div class="ustawienia-wrapper">
                                <div class="row" style="position: relative;">
                                        <div class="col-12 col-md-7 col-lg-8">
                                            <p class="mb0">Powiadomienia z tablic</p>
                                            <p class="mb5"><small><span class="text-muted">Dotyczy gier znajdujących się w</span> <a href='/profil/biblioteka' class="highlight">twojej bibliotece.</a></small></p>
                                        </div>
                                        <div class="col-12 col-md-5 col-lg-4">
                                            <div class="form-group" style="width:100%">
                                                <div class="ustawienia-opcje fr">
                                                    <label for="ustawienia-dzwiek-wiadomosci" class="sr-only">Otrzymywanie powiadomień</label>
                                                    <select class="form-control ustawienia-opcje" id="notify_all_games" data-minimum-results-for-search="Infinity">
                                                        <option></option>
                                                        <option value='0' @if($user->settings()->first()->notify_all_library_games == 0) selected @endif>Nie</option>
                                                        <option value='1' @if($user->settings()->first()->notify_all_library_games == 1) selected @endif>Wszystkie</option>
                                                        <option value='2' @if($user->settings()->first()->notify_all_library_games == 2) selected @endif>Wybrane</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                            </div>
                            <div class="ustawienia-wrapper" id='selected_games' @if($user->settings()->first()->notify_all_library_games != 2)style="display:none;"@endif>
                                    <div class="row" style="position: relative;">
                                            <div class="col-12">
                                                <p class="mb0">Wybrane tytuły</p>
                                                <p class="mb5"><small><span class="text-muted">Ogranicz powiadomienia do wybranych tytułów z</span> <a href='/profil/biblioteka' class="highlight">twojej biblioteki.</a></small></p>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group" style="width:100%">
                                                    <div class="ustawienia-opcje fr">
                                                        <label for="ustawienia-dzwiek-wiadomosci" class="sr-only">Otrzymywanie powiadomień</label>
                                                        <?php 
                                                        if($user->settings()->first()->selected_notify_games == '')
                                                            $selectedGames = array();
                                                        else {
                                                            $selectedGames = json_decode($user->settings()->first()->selected_notify_games,true);
                                                        }
                                                        
                                                        
                                                        ?>
                                                        <select class="form-control ustawienia-opcje" id="select_notify_games" data-minimum-results-for-search="Infinity" style='width:100%' multiple="multiple">
                                                            <option></option>
                                                            @foreach($user->games as $game) 
                                                                @foreach($game->userPlatforms()->where('user_id', $user->id)->get() as $item)
                                                                    <option value='{{$game->id}}-{{$item->id}}' @if(array_search(($game->id.'-'.$item->id),$selectedGames) !== false) selected @endif>{{$game->name}} [{{$item->short_name}}]</option>
                                                                @endforeach
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                </div>        
                </div>
            </div>
        </div>
    </div>
@endsection
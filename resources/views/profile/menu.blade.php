<div id="profile-menu" class="pn-ProductNav_Wrapper mb30">
        <nav id="pnProductNav" class="pn-ProductNav dragscroll" data-overflowing="right">
            <div id="pnProductNavContents" class="pn-ProductNav_Contents pn-ProductNav_Contents-no-transition" style="transform: none;">
                    <?php
                    $notifications = count(Auth::user()->unreadNotifications);
                    ?>
                    
                    @foreach(Navigation::getMenu(2) as $item)
                        @if($item['route'] == 'profile.show')
                            <a href="{{route($item['route'])}}/{{Auth::user()->id}}" class="pn-ProductNav_Link" @if($page == $item['route'])aria-selected="true"@endif>{!!$item['name']!!}</a>
                        @else
                            <a href="{{route($item['route'])}}" class="pn-ProductNav_Link" @if($page == $item['route'])aria-selected="true"@endif>{!!$item['name']!!}@if($item['route'] == 'profile.notifications.show')
                                    &nbsp;<?php
                                    $notifications = count(Auth::user()->unreadNotifications);
                                    ?>
                                    @if($notifications > 0)
                                    
                                    <span class="badge badge-danger menu-badge" >
                                        <span class='notification-count'>{{$notifications}}</span>
                                    </span>
                                    
                                    @else
                                    
                                    <span class="badge badge-danger menu-badge" style='display:none;' id='notification_show'>
                                            <span class='notification-count'></span>
                                    </span>
                                    
                                    @endif
                                
                                @endif</a>
                        @endif

                    @endforeach
                <span id="pnIndicator" class="pn-ProductNav_Indicator" style="transform: translateX(0px) scaleX(0.794844); background-color: rgb(134, 113, 0);"></span>
            </div>
        </nav>
        <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
            <i class="fas fa-chevron-left"></i>
        </button>
        <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right" type="button">
            <i class="fas fa-chevron-right"></i>
        </button>
        <hr class="separator hr-pagemenu">
    </div>
    <!-- JS Horizontal Menu Scroller -->
    <script src="{{ asset('js/horizontal-menu-scroller.js') }}"></script>
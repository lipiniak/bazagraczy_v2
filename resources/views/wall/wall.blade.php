@extends('layouts.app')

@section('content')
<div id="znajdz-do-gry" class="content">

        <div class="container-fluid">

            <div class="row">

                <div class="col-12">
                    <h1>Tablica</h1>
                    <p class="text-muted"><a class="text-muted-link" data-toggle="modal" data-target="#InfoZawartosc">{{$selectedGame['game']}} <span class="badge badge-muted">{{$selectedGame['platform']}}</span></a><p>
                        <div class="modal fade" id="InfoZawartosc" tabindex="-1" role="dialog" aria-labelledby="InfoZawartoscTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Info</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-left">
                                            <div class="twoja-notatka text-muted">
                                                <p class="mb15">Zawartość aktualnie przeglądanej strony, jej treść oraz wyniki, zawężona jest do treści związanych z <span class="text-white">{{$selectedGame['game']}}</span> <span class="badge badge-white">{{$selectedGame['platform']}}</span></p>
                                                <p class="mb0">Aby zmienić, wybierz inną grę w prawym górnym rogu aplikacji.</p>
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                        </div>
                    @if(Auth::user()->checkWallNotifications())
                    <p class="text-muted mt-15"><small>Powiadomienia z tej tablicy są <strong>wyłączone</strong>. Aby je włączyć zmień <a href='/profil/ustawienia' class="highlight">ustawienia.</a></small></p>
                    @endif
                    @if($selectedGame['game_id'] == null)
                        <div class="alert alert-info" role="alert">
                                Wybierz tytuł gry oraz platformę, aby wyświetlić zawartość tablicy.
                        </div>
                    @else 
                        
                        <div class="alert alert-info"  id="tworzenie-posta">
                            <h5>Utwórz post na tablicy</h5>
                            <form method='post' action='tablica/nowy' id="newPost">
                                {{ csrf_field() }}
                                <input type='hidden' name='game_id' value="{{$selectedGame['game_id']}}">
                                <input type='hidden' name='platform_id' value="{{$selectedGame['platform_id']}}">
                                <div class="textarea-wrapper">
                                    <textarea name='text' class="napisz-wiadomosc form-control" rows="1" placeholder="Treść posta..." value="{{old('text')}}">{{old('text')}}</textarea>
                                    <a onCLick='sendForm(this)'  class="btn btn-link textarea-send-btn"><i class="fas fa-paper-plane"></i></a> 
                                </div>
                                @if ($errors->has('text'))
                                <div class="error-feedback">
                                    
                                    {!! $errors->first('text') !!}<br>
                                </div>  
                                @endif
                            </form>
                        </div>
                    @endif
                    
                </div>

                <div class="col-12">
                    @foreach($wall as $post)
                    <div class="post-wrapper" id='post-{{$post->id}}'>
                        <script>
                            $(document).ready(function () {
                                $(".post-{{$post->id}}").click(function () {
                                    $("#post-{{$post->id}}").toggleClass("editon");
                                });
                            });
                        </script>
                        <div class="post">
                            <div class="autor-posta">
                                <a href='/profil/{{$post->poster->id}}'>
                                    @if($post->poster->avatar == 'no-avatar.jpg')
                                    <img src="{{asset('images/avatars/')}}/{{$post->poster->avatar}}" class="menu-avatar" alt="Avatar">
                                @else
                                    <img src="{{ Storage::url($post->poster->avatar) }}" class="menu-avatar" alt="Avatar">
                                @endif
                                </a>
                            </div>
                            <div class="post-akcje">
                                <div class="dropdown show">
                                    <a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuPostId1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuPostId1">
                                        <a href='/tablica/post/{{$post->id}}' class="dropdown-item">Zobacz post</a>
                                        @if($post->userFollow())
                                            <a href='/tablica/post/{{$post->id}}/follow' class="dropdown-item">Włącz powiadomienia</a>
                                        @else
                                            <a href='/tablica/post/{{$post->id}}/unfollow' class="dropdown-item">Wyłącz powiadomienia</a>
                                            
                                        @endif

                                        
                                        
                                        @if(Auth::user()->id == $post->poster->id || Auth::user()->role_id == 2)
                                        <a class="dropdown-item post-{{$post->id}}">Edytuj post</a>
                                        <a class="dropdown-item" data-toggle="modal" data-target="#deletePostId{{$post->id}}">Usuń post</a>
                                        @else
                                        <a class="dropdown-item" data-toggle="modal" data-target="#reportPostId{{$post->id}}">Zgłoś post</a>
                                        @endif
                                    </div>
                                    <div class="modal fade" id="deletePostId{{$post->id}}" tabindex="-1" role="dialog" aria-labelledby="deletePostId1" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deletePostId1Title">Uwaga!</h5>
                                                </div>
                                                <div class="modal-body text-center">
                                                    <p class="mb10">Czy na pewno chcesz usunąć posta?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href='/tablica/post/{{$post->id}}/usun' class="btn btn-primary">Tak</a>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Nie</button>                  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    @if($errors->has('report_post_'.$post->id))
                                   
                                    <script type="text/javascript">
                                        $(window).on('load',function(){
                                            $('#reportPostId{{$post->id}}').modal('show');
                                        });
                                    </script>
                                    @endif
                                    <div class="modal fade" id="reportPostId{{$post->id}}" tabindex="-1" role="dialog" aria-labelledby="reportPostId{{$post->id}}" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <form style="width:100%" method='post' action='/zglos/post'>
                                                {{ csrf_field() }}
                                                <input type='hidden' name="post_id" value='{{$post->id}}'>
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="reportPostId{{$post->id}}Title">Zgłoszenie posta</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-left">
                                                        <div class="form-group">
                                                            <p class="text-muted"><small>Opisz w paru słowach, czego dotyczy problem.</small></p>
                                                            <label class="form-label-custom">Treść zgłoszenia</label>

                                                            <textarea class="form-control" name='report_post_{{$post->id}}'rows="3" style="min-height:80px;"></textarea>
                                                            @if ($errors->has('report_post_'.$post->id))
                                                            <div class="error-feedback">
                                                                {!! $errors->first('report_post_'.$post->id) !!}<br>
                                                            </div>  
                                                            @endif
                                                        </div> 
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary">Wyślij</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <p class="rozmowa-autor"><a class="author" href="/profil/{{$post->poster->id}}">{{$post->poster->login}}</a> <small class="text-muted"><span value='{{strtotime($post->created_at)}}' class='time-from'></span></small></p>
                            <p class="m0 tresc-posta">{!! nl2br(e($post->text))!!}</p>
                            <div class="edycja-posta">
                                    <form method="POST" action="/tablica/post/{{$post->id}}/edit">
                                        {{ csrf_field() }}
                                        <input type='hidden' name='post_id' value="{{$post->id}}">
                                        <div class="textarea-wrapper">
                                            <textarea name='post-edit-{{$post->id}}'class="form-control" rows="4">{!! $post->text!!}</textarea>
                                        </div>
                                        @if ($errors->has('post-edit-{{$post->id}}'))
                                        <div class="error-feedback">
                                            {!! $errors->first('post-edit-{{$post->id}}') !!}<br>
                                        </div>  
                                        @endif

                                        <div class="text-right mt5">
                                            <a href="#" class="btn-link btn-sm post-{{$post->id}}">Anuluj</a><button type='submit' class="btn-primary btn-sm">Zapisz zmiany</button>
                                        </div>
                                    </form>
                                </div>
                        </div>
                        @foreach($post->comments as $comment)
                        <div class="komentarz" id='post-{{$post->id}}-comment-{{$comment->id}}'>
                            <script>
                                $(document).ready(function () {
                                    $(".post-{{$post->id}}-comment-{{$comment->id}}").click(function () {
                                        $("#post-{{$post->id}}-comment-{{$comment->id}}").toggleClass("editon");
                                    });
                                });
                            </script>
                            <div class="autor-komentarza">
                                    <a href='/profil/{{$comment->comentator->id}}'>
                                    @if($comment->comentator->avatar == 'no-avatar.jpg')
                                    <img src="{{asset('images/avatars/')}}/{{$comment->comentator->avatar}}" class="menu-avatar" alt="Avatar">
                                @else
                                    <img src="{{ Storage::url($comment->comentator->avatar) }}" class="menu-avatar" alt="Avatar">
                                @endif
                                    </a>
                            </div>
                            <div class="tresc-komentarza">
                                <p class="rozmowa-autor"><a class='author' href="/profil/{{$comment->comentator->id}}">{{$comment->comentator->login}}</a> <small class="text-muted"><span value='{{strtotime($comment->created_at)}}' class='time-from'></span></small></p>
                                <p class="m0 tekst-komentarza">{!! $comment->text!!}</p>
                                <div class="edycja-komentarza">
                                        <form method="POST" action="/tablica/komentarz/{{$post->id}}/edit">
                                            {{ csrf_field() }}
                                            <input type='hidden' name='post_id' value="{{$post->id}}">
                                            <input type='hidden' name='comment_id' value="{{$comment->id}}">
                                            <div class="textarea-wrapper">
                                                <textarea name="post-{{$post->id}}-comment-{{$comment->id}}" class=" form-control" rows="4">{!! $comment->text !!}</textarea>
                                            </div>
                                            @if ($errors->has('post-{{$post->id}}-comment-{{$comment->id}}'))
                                        <div class="error-feedback">
                                            {!! $errors->first('post-{{$post->id}}-comment-{{$comment->id}}') !!}<br>
                                        </div>  
                                        @endif 
                                            <div class="text-right mt5">
                                                <a href="#" class="btn-link btn-sm post-{{$post->id}}-comment-{{$comment->id}}">Anuluj</a><button type='submit' class="btn-primary btn-sm">Zapisz zmiany</button>
                                            </div>
                                        </form>
                                    </div>
                                <div class="komentarz-akcje">
                                    <div class="dropdown show">
                                        <a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuKomentarzId2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-h"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuKomentarzId2">
                                            @if(Auth::user()->id == $comment->comentator->id || Auth::user()->role_id == 2)
                                            <a class="dropdown-item post-{{$post->id}}-comment-{{$comment->id}}">Edytuj komentarz</a>
                                            <a class="dropdown-item" data-toggle="modal" data-target="#deleteKomentarzId{{$comment->id}}">Usuń komentarz</a>
                                            @else
                                            <a class="dropdown-item" data-toggle="modal" data-target="#reportKomentarzId{{$comment->id}}">Zgłoś komentarz</a>
                                            @endif
                                        </div>
                                        <div class="modal fade" id="deleteKomentarzId{{$comment->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteKomentarzId2" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="deleteKomentarzId{{$comment->id}}Title">Uwaga!</h5>
                                                    </div>
                                                    <div class="modal-body text-center">
                                                        <p class="mb10">Czy na pewno chcesz usunąć komentarz?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href='/tablica/komentarz/{{$comment->id}}/usun' class="btn btn-primary">Tak</a>
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Nie</button>                  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if($errors->has('report_komentarz_'.$comment->id))
                                   
                                    <script type="text/javascript">
                                        $(window).on('load',function(){
                                            $('#reportKomentarzId{{$comment->id}}').modal('show');
                                        });
                                    </script>
                                    @endif
                                        <div class="modal fade" id="reportKomentarzId{{$comment->id}}" tabindex="-1" role="dialog" aria-labelledby="reportKomentarzId{{$comment->id}}" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <form style="width:100%" method='post' action='/zglos/komentarz'>
                                                        {{ csrf_field() }}
                                                        
                                                    <input type='hidden' name='post_id' value="{{$post->id}}">
                                                    <input type='hidden' name='comment_id' value="{{$comment->id}}">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="reportKomentarzId{{$comment->id}}Title">Zgłoszenie komentarza</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body text-left">
                                                            <div class="form-group">
                                                                <p class="text-muted"><small>Opisz w paru słowach, czego dotyczy problem.</small></p>
                                                                <label class="form-label-custom">Treść zgłoszenia</label>
                                                                <textarea class="form-control" name='report_komentarz_{{$comment->id}}' rows="3" style="min-height:80px;"></textarea>
                                                                @if ($errors->has('report_komentarz_'.$comment->id))
                                                            <div class="error-feedback">
                                                                {!! $errors->first('report_komentarz_'.$comment->id) !!}<br>
                                                            </div>  
                                                            @endif
                                                            </div> 
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-primary">Wyślij</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div class="komentarz">
                            <div class="autor-komentarza">
                                    @if(Auth::user()->avatar == 'no-avatar.jpg')
                                    <img src="{{asset('images/avatars/')}}/{{Auth::user()->avatar}}" class="menu-avatar" alt="Avatar">
                                @else
                                    <img src="{{ Storage::url(Auth::user()->avatar) }}" class="menu-avatar" alt="Avatar">
                                @endif
                            </div>
                            <form class="pb15" method="post" action='tablica/nowy/komentarz' id='sendcomment-id-{{$post->id}}'>
                                {{ csrf_field() }}
                                <input type='hidden' name='post_id' value="{{$post->id}}">
                                <div class="textarea-wrapper">
                                    <textarea name='comment-{{$post->id}}'class="napisz-wiadomosc form-control" rows="1" placeholder="Odpowiedz...">{{old('comment-'.$post->id)}}</textarea>
                                    <a onCLick='sendComment({{$post->id}})'  class="btn btn-link textarea-send-btn"><i class="fas fa-paper-plane"></i></a> 
                                </div>
                                @if ($errors->has('post-'.$post->id))
                                <div class="error-feedback">
                                    
                                    {!! $errors->first('post-'.$post->id) !!}<br>
                                </div>  
                                @endif
                                
                            </form>
                        </div>
                    </div>
                    @endforeach
                    @if(count($wall)==0 and $selectedGame['game_id'] != NULL) 
                    Brak postów na tablicy.
                    @endif
                </div>

            </div>

        </div>


    </div>
    
    <script src="{{ asset('js/textarea-autosize.js') }}"></script>

    @if(Session::has('post_id'))
    <script>
        document.getElementById("post-{{session('post_id')}}").scrollIntoView();
    </script>
    @endif
    <script>
            

            const ELEMENT_CLASS = '.time-from';
            const DATE_ATTR_NAME = 'value';

            const TEN_SECONDS_MILLIS = 10 * 1000;
            const MINUTE_MILLIS = 60 * 1000;
            const HOUR_MILLIS = 60 * MINUTE_MILLIS;
            const DAY_MILLIS = 24 * HOUR_MILLIS;

            const UNDER_MINUTE_REFRESH_MILLIS = TEN_SECONDS_MILLIS; // every 10 seconds
            const UNDER_HOUR_REFRESH_MILLIS = MINUTE_MILLIS; // every minute
            const UNDER_DAY_REFRESH_MILLIS = HOUR_MILLIS; // every hour
            const OVER_DAY_REFRESH_MILLIS = DAY_MILLIS; // every day


            function getRandomTimeoutId() {
            return Math.floor(Math.random() * (1e16));
            }

            function timeToText(element, timeoutId) {
            const date = parseInt(element.attr(DATE_ATTR_NAME)) * 1000;
            //const millis = (Date.now() - date);
            const now = Date.now();
            const millis = date > now ? 1 : (now - date);
            
            if (timeoutId) {
                window.clearTimeout(timeoutId);
            }
            const tId = getRandomTimeoutId();

            let text, refreshRate;

            if (millis < MINUTE_MILLIS) {
                refreshRate = UNDER_MINUTE_REFRESH_MILLIS;
                text = underMinuteText(millis);
            } else if (millis < HOUR_MILLIS) {
                refreshRate = UNDER_HOUR_REFRESH_MILLIS;
                text = underHourText(millis);
            } else if (millis < DAY_MILLIS) {
                refreshRate = UNDER_DAY_REFRESH_MILLIS;
                text = underDayText(millis);
            } else {
                refreshRate = OVER_DAY_REFRESH_MILLIS;
                text = overDayText(millis,date);
            }

            setTimeout(function() {
                timeToText(element, tId);
            }, refreshRate);

            element.text(text);
            }

            function underMinuteText(millis) {
            const tensOfSeconds = Math.floor(millis / TEN_SECONDS_MILLIS);
            if (tensOfSeconds < 1) {
                return `${Math.floor(millis / 1000)} sek. temu`;
            }
            return `${tensOfSeconds * 10} sek. temu`;
            }

            function underHourText(millis) {
            const minutes = Math.floor(millis / MINUTE_MILLIS);
            return `${minutes} min. temu`;
            }

            function underDayText(millis) {
            const hours = Math.floor(millis / HOUR_MILLIS);
            return `${hours} godz. temu`;
            }

            function overDayText(millis,date) {
            const days = Math.floor(millis / DAY_MILLIS);

            if (days === 1) {
                return "1 dzień temu";
            } else {
                if(days > 3) {
                    var date = new Date(date);
                    
                    return date.customFormat( "#DD#.#MM#.#YYYY#" ) 
                } else {
                    return `${days} dni temu`;
                }
                
            }
            }

            $(document).ready(function() {
            $(ELEMENT_CLASS).each(function(i, element) {
                timeToText($(element));
            });
            });

            
            var test = true;

            $(function(){
                autosize($('textarea'));
                $("textarea").on("keydown",function(e){
                    if(!e.shiftKey && e.keyCode===13){

                    e.preventDefault();
                    var form = $($(this)[0].form);
                    if(test){
                        if(!$(this).val()) {
                            e.preventDefault()
                        } else {
                            test = false;
                            form.submit();
                        }
                    }
                    
                    }
                });
                });

                function sendForm(element) {
                    
                    var form = $('#newPost');
                    
                    if(test) {
                        if(form.find('textarea').val()) {
                            test = false;                      
                            form.submit();
                        }
                    }
                }
                
                function sendComment(element) {
                    
                    var form = $('#sendcomment-id-'+element);
                    
                    if(test) {
                        if(form.find('textarea').val()) {
                            test = false;
                            form.submit();
                        }
                    }
                    
                }
    </script>
@endsection
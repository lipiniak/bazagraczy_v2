@extends('layouts.app')

@section('content')
<div id="znajdz-do-gry" class="content">

        <div class="container-fluid">

            <div class="row">

                <div class="col-12">
                    <h1>Podgląd posta</h1>
                        @if(!$post) 
                        Nie ma takiego postu.
                        @else
                    
                    <div class="post-wrapper" id='post-{{$post->id}}'>
                        <script>
                                $(document).ready(function () {
                                    $(".post-{{$post->id}}").click(function () {
                                        $("#post-{{$post->id}}").toggleClass("editon");
                                    });
                                });
                            </script>
                        <div class="post">
                            <div class="autor-posta">
                                <a href='/profil/{{$post->poster->id}}'>
                                    @if($post->poster->avatar == 'no-avatar.jpg')
                                    <img src="{{asset('images/avatars/')}}/{{$post->poster->avatar}}" class="menu-avatar" alt="Avatar">
                                @else
                                    <img src="{{ Storage::url($post->poster->avatar) }}" class="menu-avatar" alt="Avatar">
                                @endif
                                </a>
                            </div>
                            <div class="post-akcje">
                                <div class="dropdown show">
                                    <a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuPostId1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuPostId1">
                                            @if($post->userFollow())
                                            <a href='/tablica/post/{{$post->id}}/follow' class="dropdown-item">Włącz powiadomienia</a>
                                        @else
                                            <a href='/tablica/post/{{$post->id}}/unfollow' class="dropdown-item">Wyłącz powiadomienia</a>
                                            
                                        @endif
                                        @if(Auth::user()->id == $post->poster->id || Auth::user()->role_id == 2)
                                        <a class="dropdown-item post-{{$post->id}}">Edytuj post</a>
                                        <a class="dropdown-item" data-toggle="modal" data-target="#deletePostId{{$post->id}}">Usuń post</a>
                                        @else
                                        <a class="dropdown-item" data-toggle="modal" data-target="#reportPostId{{$post->id}}">Zgłoś post</a>
                                        @endif
                                    </div>
                                    <div class="modal fade" id="deletePostId{{$post->id}}" tabindex="-1" role="dialog" aria-labelledby="deletePostId1" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deletePostId1Title">Uwaga!</h5>
                                                </div>
                                                <div class="modal-body text-center">
                                                    <p class="mb10">Czy na pewno chcesz usunąć posta?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <a href='/tablica/post/{{$post->id}}/usun' class="btn btn-primary">Tak</a>
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Nie</button>                  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    @if($errors->has('report_post_'.$post->id))
                                   
                                    <script type="text/javascript">
                                        $(window).on('load',function(){
                                            $('#reportPostId{{$post->id}}').modal('show');
                                        });
                                    </script>
                                    @endif
                                    <div class="modal fade" id="reportPostId{{$post->id}}" tabindex="-1" role="dialog" aria-labelledby="reportPostId{{$post->id}}" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <form style="width:100%" method='post' action='/zglos/post'>
                                                {{ csrf_field() }}
                                                <input type='hidden' name="post_id" value='{{$post->id}}'>
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="reportPostId{{$post->id}}Title">Zgłoszenie posta</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-left">
                                                        <div class="form-group">
                                                            <p class="text-muted"><small>Opisz w paru słowach, czego dotyczy problem.</small></p>
                                                            <label class="form-label-custom">Treść zgłoszenia</label>

                                                            <textarea class="form-control" name='report_post_{{$post->id}}'rows="3" style="min-height:80px;"></textarea>
                                                            @if ($errors->has('report_post_'.$post->id))
                                                            <div class="error-feedback">
                                                                {!! $errors->first('report_post_'.$post->id) !!}<br>
                                                            </div>  
                                                            @endif
                                                        </div> 
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary">Wyślij</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <p><a class='author' href="/profil/{{$post->poster->id}}">{{$post->poster->login}}</a> <small class="text-muted"><span value='{{strtotime($post->created_at)}}' class='time-from'></span></small></p>
                            <p class="m0">{!! nl2br(e($post->text))!!}</p>
                            <div class="edycja-posta">
                                <form method="POST" action="/tablica/post/{{$post->id}}/edit">
                                    {{ csrf_field() }}
                                    <input type='hidden' name='post_id' value="{{$post->id}}">
                                    <div class="textarea-wrapper">
                                        <textarea name='post-edit-{{$post->id}}'class="form-control" rows="4">{!! $post->text!!}</textarea>
                                    </div>
                                    @if ($errors->has('post-edit-{{$post->id}}'))
                                    <div class="error-feedback">
                                        {!! $errors->first('post-edit-{{$post->id}}') !!}<br>
                                    </div>  
                                    @endif

                                    <div class="text-right mt5">
                                        <a href="#" class="btn-link btn-sm post-{{$post->id}}">Anuluj</a><button type='submit' class="btn-primary btn-sm">Zapisz zmiany</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id='komentarze'>
                        @foreach($post->comments as $comment)
                        <div class="komentarz clone" id='post-{{$post->id}}-comment-{{$comment->id}}'>
                                <script>
                                        $(document).ready(function () {
                                            $(".post-{{$post->id}}-comment-{{$comment->id}}").click(function () {
                                                $("#post-{{$post->id}}-comment-{{$comment->id}}").toggleClass("editon");
                                            });
                                        });
                                    </script>
                            <div class="autor-komentarza">
                                    <a href='/profil/{{$comment->comentator->id}}' class='img-link'>
                                    @if($comment->comentator->avatar == 'no-avatar.jpg')
                                    <img src="{{asset('images/avatars/')}}/{{$comment->comentator->avatar}}" class="menu-avatar" alt="Avatar">
                                @else
                                    <img src="{{ Storage::url($comment->comentator->avatar) }}" class="menu-avatar" alt="Avatar">
                                @endif
                                    </a>
                            </div>
                            <div class="tresc-komentarza">
                                <p><a class='author' href="/profil/{{$comment->comentator->id}}" class='author'>{{$comment->comentator->login}}</a> <small class="text-muted"><span value='{{strtotime($comment->created_at)}}' class='time-from'></span></small></p>
                                <p class="m0 tresc">{!! $comment->text !!}</p>
                                <div class="edycja-komentarza">
                                        <form method="POST" action="/tablica/komentarz/{{$post->id}}/edit">
                                            {{ csrf_field() }}
                                            <input type='hidden' name='post_id' value="{{$post->id}}">
                                            <input type='hidden' name='comment_id' value="{{$comment->id}}">
                                            <div class="textarea-wrapper">
                                                <textarea name="post-{{$post->id}}-comment-{{$comment->id}}" class=" form-control" rows="4">{!! $comment->text !!}</textarea>
                                            </div>
                                            @if ($errors->has('post-{{$post->id}}-comment-{{$comment->id}}'))
                                        <div class="error-feedback">
                                            {!! $errors->first('post-{{$post->id}}-comment-{{$comment->id}}') !!}<br>
                                        </div>  
                                        @endif 
                                            <div class="text-right mt5">
                                                <a href="#" class="btn-link btn-sm post-{{$post->id}}-comment-{{$comment->id}}">Anuluj</a><button type='submit' class="btn-primary btn-sm">Zapisz zmiany</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="komentarz-akcje">
                                            <div class="dropdown show">
                                                <a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuKomentarzId2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-h"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuKomentarzId2">
                                                    @if(Auth::user()->id == $comment->comentator->id || Auth::user()->role_id == 2)
                                                    <a class="dropdown-item post-{{$post->id}}-comment-{{$comment->id}}">Edytuj komentarz</a>
                                                    <a class="dropdown-item" data-toggle="modal" data-target="#deleteKomentarzId{{$comment->id}}">Usuń komentarz</a>
                                                    @else
                                                    <a class="dropdown-item" data-toggle="modal" data-target="#reportKomentarzId{{$comment->id}}">Zgłoś komentarz</a>
                                                    @endif
                                                </div>
                                                <div class="modal fade" id="deleteKomentarzId{{$comment->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteKomentarzId2" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="deleteKomentarzId{{$comment->id}}Title">Uwaga!</h5>
                                                            </div>
                                                            <div class="modal-body text-center">
                                                                <p class="mb10">Czy na pewno chcesz usunąć komentarz?</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <a href='/tablica/komentarz/{{$comment->id}}/usun' class="btn btn-primary">Tak</a>
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Nie</button>                  
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if($errors->has('report_komentarz_'.$comment->id))
                                           
                                            <script type="text/javascript">
                                                $(window).on('load',function(){
                                                    $('#reportKomentarzId{{$comment->id}}').modal('show');
                                                });
                                            </script>
                                            @endif
                                                <div class="modal fade" id="reportKomentarzId{{$comment->id}}" tabindex="-1" role="dialog" aria-labelledby="reportKomentarzId{{$comment->id}}" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <form style="width:100%" method='post' action='/zglos/komentarz'>
                                                                {{ csrf_field() }}
                                                                
                                                            <input type='hidden' name='post_id' value="{{$post->id}}">
                                                            <input type='hidden' name='comment_id' value="{{$comment->id}}">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="reportKomentarzId{{$comment->id}}Title">Zgłoszenie komentarza</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body text-left">
                                                                    <div class="form-group">
                                                                        <p class="text-muted"><small>Opisz w paru słowach, czego dotyczy problem.</small></p>
                                                                        <label class="form-label-custom">Treść zgłoszenia</label>
                                                                        <textarea class="form-control" name='report_komentarz_{{$comment->id}}' rows="3" style="min-height:80px;"></textarea>
                                                                        @if ($errors->has('report_komentarz_'.$comment->id))
                                                                    <div class="error-feedback">
                                                                        {!! $errors->first('report_komentarz_'.$comment->id) !!}<br>
                                                                    </div>  
                                                                    @endif
                                                                    </div> 
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-primary">Wyślij</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                            </div>
                        </div>
                        @endforeach
                        </div>
                        <div class="komentarz clone" id='post--comment-' style='display:none;'>
                            
                        <div class="autor-komentarza">
                                <a href='' class='img-link'>
                                
                                <img src="" class="menu-avatar" alt="Avatar">
                            
                                </a>
                        </div>
                        <div class="tresc-komentarza">
                            <p><a href="" class='author'></a> <small class="text-muted"><span value='' class='time-from'></span></small></p>
                            <p class="m0 tresc"></p>
                            <div class="edycja-komentarza">
                                    <form method="POST" action="/tablica/komentarz//edit">
                                        {{ csrf_field() }}
                                        <input type='hidden' name='post_id' value="">
                                        <input type='hidden' name='comment_id' value="">
                                        <div class="textarea-wrapper">
                                            <textarea name="post--comment-" class=" form-control" rows="4"></textarea>
                                        </div>
                                        <div class="text-right mt5">
                                            <a href="#" class="btn-link btn-sm post--comment-">Anuluj</a><button type='submit' class="btn-primary btn-sm">Zapisz zmiany</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="komentarz-akcje">
                                        <div class="dropdown show">
                                            <a class="btn btn-link dropdown-toggle" href="#" role="button" id="dropdownMenuKomentarzId2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-h"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuKomentarzId2">
                                                
                                                <a class="dropdown-item ">Edytuj komentarz</a>
                                                <a class="dropdown-item" data-toggle="modal" data-target="#deleteKomentarzId">Usuń komentarz</a>
                                                
                                                <a class="dropdown-item" data-toggle="modal" data-target="#reportKomentarzId">Zgłoś komentarz</a>
                                                
                                            </div>
                                            <div class="modal fade" id="deleteKomentarzId" tabindex="-1" role="dialog" aria-labelledby="deleteKomentarzId2" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="deleteKomentarzIdTitle">Uwaga!</h5>
                                                        </div>
                                                        <div class="modal-body text-center">
                                                            <p class="mb10">Czy na pewno chcesz usunąć komentarz?</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <a href='/tablica/komentarz//usun' class="btn btn-primary">Tak</a>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Nie</button>                  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="modal fade" id="reportKomentarzId" tabindex="-1" role="dialog" aria-labelledby="reportKomentarzId" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <form style="width:100%" method='post' action='/zglos/komentarz'>
                                                            
                                                            
                                                        <input type='hidden' name='post_id' value="">
                                                        <input type='hidden' name='comment_id' value="">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="reportKomentarzIdTitle">Zgłoszenie komentarza</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body text-left">
                                                                <div class="form-group">
                                                                    <p class="text-muted"><small>Opisz w paru słowach, czego dotyczy problem.</small></p>
                                                                    <label class="form-label-custom">Treść zgłoszenia</label>
                                                                    <textarea class="form-control" name='report_komentarz_' rows="3" style="min-height:80px;"></textarea>
                                                                   
                                                                </div> 
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="submit" class="btn btn-primary">Wyślij</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        </div>
                    </div>
                        <div class="komentarz" id='end'>
                            <div class="autor-komentarza">
                                    @if($user->avatar == 'no-avatar.jpg')
                                    <img src="{{asset('images/avatars/')}}/{{$user->avatar}}" class="menu-avatar" alt="Avatar">
                                @else
                                    <img src="{{ Storage::url($user->avatar) }}" class="menu-avatar" alt="Avatar">
                                @endif
                            </div>
                            <form class="pb15" method="post" action='/tablica/nowy/komentarz' id='komentarz'>
                                {{ csrf_field() }}
                                <input type='hidden' name='post_id' value="{{$post_id}}">
                                <input type='hidden' name='type' value="single">
                                <div class="textarea-wrapper">
                                    <textarea name='comment-{{$post->id}}'class="napisz-wiadomosc form-control" rows="1" placeholder="Odpowiedz..."></textarea>
                                    <span onClick='send();' class="btn btn-link textarea-send-btn"><i class="fas fa-paper-plane"></i></span> 
                                </div>
                                @if ($errors->has('post-'.$post->id))
                                <div class="error-feedback">
                                    
                                    {!! $errors->first('post-'.$post->id) !!}<br>
                                </div>  
                                @endif
                                
                            </form>
                        </div>
                    </div>
                    
                    
                    @endif
                </div>

            </div>

        </div>


    </div>
    
    <script src="{{ asset('js/textarea-autosize.js') }}"></script>

    @if(Session::has('post_id'))
    <script>
        document.getElementById("post-{{session('post_id')}}").scrollIntoView();
    </script>
    @endif
    <script>
            

            const ELEMENT_CLASS = '.time-from';
            const DATE_ATTR_NAME = 'value';

            const TEN_SECONDS_MILLIS = 10 * 1000;
            const MINUTE_MILLIS = 60 * 1000;
            const HOUR_MILLIS = 60 * MINUTE_MILLIS;
            const DAY_MILLIS = 24 * HOUR_MILLIS;

            const UNDER_MINUTE_REFRESH_MILLIS = TEN_SECONDS_MILLIS; // every 10 seconds
            const UNDER_HOUR_REFRESH_MILLIS = MINUTE_MILLIS; // every minute
            const UNDER_DAY_REFRESH_MILLIS = HOUR_MILLIS; // every hour
            const OVER_DAY_REFRESH_MILLIS = DAY_MILLIS; // every day


            function getRandomTimeoutId() {
            return Math.floor(Math.random() * (1e16));
            }

            function timeToText(element, timeoutId) {
            const date = parseInt(element.attr(DATE_ATTR_NAME)) * 1000;
            //const millis = (Date.now() - date);
            const now = Date.now();
            const millis = date > now ? 1 : (now - date);
            
            if (timeoutId) {
                window.clearTimeout(timeoutId);
            }
            const tId = getRandomTimeoutId();

            let text, refreshRate;

            if (millis < MINUTE_MILLIS) {
                refreshRate = UNDER_MINUTE_REFRESH_MILLIS;
                text = underMinuteText(millis);
            } else if (millis < HOUR_MILLIS) {
                refreshRate = UNDER_HOUR_REFRESH_MILLIS;
                text = underHourText(millis);
            } else if (millis < DAY_MILLIS) {
                refreshRate = UNDER_DAY_REFRESH_MILLIS;
                text = underDayText(millis);
            } else {
                refreshRate = OVER_DAY_REFRESH_MILLIS;
                text = overDayText(millis,date);
            }

            setTimeout(function() {
                timeToText(element, tId);
            }, refreshRate);

            element.text(text);
            }

            function underMinuteText(millis) {
            const tensOfSeconds = Math.floor(millis / TEN_SECONDS_MILLIS);
            if (tensOfSeconds < 1) {
                return `${Math.floor(millis / 1000)} sek. temu`;
            }
            return `${tensOfSeconds * 10} sek. temu`;
            }

            function underHourText(millis) {
            const minutes = Math.floor(millis / MINUTE_MILLIS);
            return `${minutes} min. temu`;
            }

            function underDayText(millis) {
            const hours = Math.floor(millis / HOUR_MILLIS);
            return `${hours} godz. temu`;
            }

            function overDayText(millis,date) {
            const days = Math.floor(millis / DAY_MILLIS);
            if (days === 1) {
                return "1 dzień temu";
            } else {
                if(days > 3) {
                    var date = new Date(date);
                    
                    return date.customFormat( "#DD#.#MM#.#YYYY#" ) 
                } else {
                    return `${days} dni temu`;
                }
            }
            }

            $(document).ready(function() {
                $(ELEMENT_CLASS).each(function(i, element) {
                    timeToText($(element));
                });
            });
            var test = true;

            $(function(){
                autosize($('textarea'));
                $("textarea").on("keydown",function(e){
                    if(!e.shiftKey && e.keyCode===13){
                        e.preventDefault();
                        var form = $($(this)[0].form);

                        if(form.find('textarea').val() != '') {                        
                            var url = '/tablica/nowy/komentarz';
                            $.ajax({
                                method: "POST",
                                data: form.serialize(),
                                url: url,
                            });
                            $('textarea').val('');
                        }
                        
                    }
                });
            });
            function send() {
                var form = $('#komentarz');
                
                if(form.find('textarea').val() != '') {
                    var url = '/tablica/nowy/komentarz';
                    $.ajax({
                        method: "POST",
                        data: form.serialize(),
                        url: url,
                    });
                    $('textarea').val('');
                }
                
                
            }
    </script>

    <script>
        var login = "{{Auth::user()->login}}";
        var commentsDiv = $('#komentarze');
        var newComment;
        var comment = $('.clone').first();

        document.getElementById("end").scrollIntoView();

        Echo.private('post-{{$post_id}}')
            .whisper('typing', {
                name: login+' pisze...',
                typing: true
            })
            .listenForWhisper('typing', (e) => {
                console.log(e.name);
            })
            .listen('newComment', (e) => {
                
                
                newComment = comment.clone()
                
                var d = new Date();
                
                var n = Date.now();
                
                newComment.find('p.tresc').html(e.comment['text']);
                newComment.find('.author').html(e.user['login']);
                newComment.find('.menu-avatar').attr("src",e.user['avatar']);
                newComment.find('.author').attr("href",'/profil/'+e.user['id']);
                newComment.find('.img-link').attr("href",'/profil/'+e.user['id']);
                newComment.find('.time-from').attr('value',n/1000);
                newComment.find('.time-from').html('1 sek. temu');
                
                newComment.appendTo(commentsDiv);
                newComment.show();

                timeToText(newComment.find('.time-from'));
                
                document.getElementById("end").scrollIntoView();
        });  

        
    </script>
@endsection
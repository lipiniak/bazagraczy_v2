@extends('layouts.app')

@section('content')
<?php
$user = Auth::user();
?>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.11&appId=1769261916703776';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div id="kokpit" class="content">
        <div class="container-fluid">

            <div class="row">

                <div class="col-12">
                    <h1 class="mb30">Kokpit</h1>
                </div>
                
                @foreach($user->pushMessages as $push)
                @if($push->is_seen == 0)
                <div class="col-12">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        {!!$push->content!!}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true" onClick='pushRead({{$push->id}})'>&times;</span>
                        </button>
                    </div>
                </div>
                @endif
                @endforeach
                <script>
                    function pushRead(id) {
                        var url = '/push/delete/'+id;
                        $.ajax({
                            method: "GET",
                            url: url,
                        })
                        .done(function( data ) {
                            
                        });
                    }


                    
                </script>
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-lg-auto">
                            <div class="kokpit-box fb-news">
                                <h3>Aktualności</h3>
                                <div class="kokpit-box-inner">
                                    <div class="fb-news-plugin-wrapper">
                                            <div class="fb-news-plugin-outer">
                                                <div class="fb-page" data-href="https://www.facebook.com/bazagraczy/" data-tabs="timeline" data-width="650" data-height="400" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><blockquote cite="https://www.facebook.com/bazagraczy/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/bazagraczy/">BAZAGRACZY.pl</a></blockquote></div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-xl">
                            <div id="gracze-zarzadzanie" class="kokpit-box">
                                <h3>Ostatnio zalogowani znajomi</h3>
                                @if (!empty($user->myfollowers->first()))
                                <p><a href="/profil/obserwowani">Pokaż wszystkich</a></p>
                                @endif
                                <div class="kokpit-box-inner">
                                    <ul id="gamesUL">
                                        <!-- Pojedynczy wynik /start -->
                                        @if (!empty($user->myfollowers->first()))
                                            <?php $count = 0;?>
                                            @foreach($user->myfollowers as $follower)
                                                
                                                @if($follower->isOnline())
                                                <?php $count = $count + 1; ?>
                                                <li class="lista-graczy" data-val="{{$count}}">
                                                    <div class="pojedynczy-wynik">
                                                        <div class="avatar-wrapper">
                                                            @if($follower->avatar == 'no-avatar.jpg')
                                                                <img src="{{asset('images/avatars/')}}/{{$follower->avatar}}" class="menu-avatar" alt="Avatar">
                                                            @else
                                                                <img src="{{ Storage::url($follower->avatar) }}" class="menu-avatar" alt="Avatar">
                                                            @endif
                                                            
                                                        </div>
                                                        <div class="gracz-info">
                                                            <a href="/profil/{{$follower->id}}"><span class="inline-status  @if($follower->isOnline()) online @else offline @endif " title="Online"></span> <span>{{$follower->login}}</span></a><br>
                                                            
                                                            <?php $selectedGame = $follower->selectedGame();?>
                                                            
                                                            <small class="text-muted"><span>{{$selectedGame['game']}} <span class="badge badge-info badge-muted">{{$selectedGame['platform']}}</span></span></small>
                                                        </div>
                                                        <div class="gracz-akcje">
                                                            <a class="btn btn-secondary akcja" href='/wiadomosci/{{$follower->id}}'><i class="fas fa-comments"></i></a>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </li>
                                                @endif
                                                
                                                
                                                @if($count == 5)
                                                    <?php break;?>
                                                @endif
                                            @endforeach
                                            @if($count == 0)
                                                Brak znajomych online.
                                            @endif
                                        @else 
                                            Nikogo nie obserwujesz.
                                        @endif

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="kokpit-box">
                        <h3>Statystyki</h3>
                        <div class="kokpit-box-inner">
                            <div style="display: inline-block">
                                <h5 class="pb5" style="border-bottom: 1px solid rgba(255, 255, 255, 0.5);">Ogólne</h5>
                                <p>Zarejestrowani gracze: {{$data['stats_registered']}}<br>
                                    Aktualnie online: {{$data['stats_online']}}<br>
                                    Najpopularniejszy tytuł: {{$data['stats_game']}}</p>
                                    <?php $selectedGame = $user->selectedGame();?>
                                    @if($selectedGame['game'] != 'Brak')
                                <h5 class="pb5" style="border-bottom: 1px solid rgba(255, 255, 255, 0.5);">{{$selectedGame['game']}}</h5>
                                <p>Zarejestrowani: {{$data['topbar_registred']}}<br>
                                    Online: {{$data['topbar_online']}}</p>
                                    @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

@endsection

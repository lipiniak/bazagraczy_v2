@extends('layouts.app')

@section('content')
<div id="wyszukiwarka" class="content">

    <div class="container-fluid">

        <div class="row">

            <div class="col-12">

                <div class="row">
                    <div class="col-12">
                        <h1 class="mb15">{{$data['title']}} <span class="badge badge-info">{{$data['platform']}}</span></h1>
                        <h3><small>Online: {{$online}}</small><br>Lista graczy</h3>
                    </div>
                </div>


                <div class="row">
                    <div class="col-12">
                        @if(!empty($selectedUsers))
                            @foreach($selectedUsers as $user)

                            @if($user->getStatus() == 'online' OR $user->getStatus() == 'away')
                            <!-- Pojedynczy wynik wyszukiwania /start -->
                            <div class="wynik-wrapper">
                                <div class="avatar-wrapper">
                                    <a href="/profil/{{$user->id}}">
                                        @if($user->avatar == 'no-avatar.jpg')
                                        <img src="{{asset('images/avatars/')}}/{{$user->avatar}}" class="menu-avatar" alt="Avatar">
                                    @else
                                        <img src="{{ Storage::url($user->avatar) }}" class="menu-avatar" alt="Avatar">
                                    @endif
                                </a>
                                <span class="status {{$user->getStatus()}}"></span>
                                </div>
                                <div class="nick-gracza">
                                    {{$user->login}}
                                </div>
                                <?php
                                        $note = $user->note()->where('user_id',Auth::user()->id)->get();
                                    ?>
                                <div class="gracz-info">
                                    <div class="btn-group special" role="group" aria-label="...">
                                        <a href='/profil/{{$user->id}}'class="btn btn-secondary">Zobacz profil</a>
                                        @if(!empty($note[0]))
                                        <a class="btn btn-secondary" data-toggle="modal" data-target="#profilInfoID1"><i class="fas fa-info"></i></a>
                                        @endif
                                    </div>
                                    
                                    <!-- Modal -->
                                    @if(!empty($note[0]))
                                    <div class="modal fade" id="profilInfoID1" tabindex="-1" role="dialog" aria-labelledby="profilInfoID1Title" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="profilZablokujTitle">{{$user->login}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body text-center">
                                                    <p class="text-muted">Notakta</p>
                                                    <p class="mb0 text-left">{!! nl2br($note[0]->note)!!}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            @endif
                        @endforeach
                        @else
                        Brak aktywnych graczy.
                        @endif
                        
                        <!-- Pojedynczy wynik wyszukiwania /koniec -->

                    </div>
                </div>



            </div>

        </div>

    </div>

</div>

@endsection
@extends(Auth::user() ? 'layouts.app' : 'layouts.full')

@section('content')
<div id="info-page" class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    @if(!Auth::check())
                    <div class="logreg-logo">
                            <a href="/"><img src="{{ asset('images/logo/logo-bg-small-full.png') }}" alt="BAZAGRACZY.PL Logo"></a>
                    </div>
                    @endif
                    
                    <h1>{{$page->title}}</h1>
                    {!!$page->content!!}
                </div>
            </div>

        </div>
    </div>
@endsection


@extends($type == 'full' ? 'layouts.app' : 'layouts.full')

@section('content')
@if($type == 'modal')
<script>
    $(document).ready(function(){
        $('body').addClass('modal-chatbox');
    });
</script>
@endif

<div id="komunikator" class="content">

    <div class="container-fluid">
        <div class='row'>
            <div class="col-12">
                    <div class="rozmowa-tytul">
                        @if($type == 'modal')
                            <p class="mb30 text-muted"><small><strong>POCZĄTEK ROZMOWY</strong></small></p>
                        @else
                            <h5 class="h-border-bottom">Rozmowa z {{$withUser->login}}</h5>
                        @endif
                    </div>
                </div>
                
            </div>
        <div class="row">
            
            <div class='rozmowa' id='rozmowa'>
            <a id="start"></a>
            
            @if(empty($messages))
            <div class='col-12'>
                <span id='no-messages'>Brak wiadomości</span>
            </div>
            @endif
            
            @foreach($messages as $msg)
            <?php $lastId = $msg->id;?>
            <div class="col-12 @if($msg->is_seen == 0) unseen @else seen @endif message @if($msg->sender->id == Auth::user()->id) messageowner @endif" data-id='{{$msg->id}}' id='{{$msg->id}}'>
                <div class="rozmowa-post-wrapper">
                    <div class="rozmowa-avatar">
                        @if($msg->sender->avatar == 'no-avatar.jpg')
                        <img src="{{asset('images/avatars/')}}/{{$msg->sender->avatar}}" class="menu-avatar" alt="Avatar">
                    @else
                        <img src="{{ Storage::url($msg->sender->avatar) }}" class="menu-avatar" alt="Avatar">
                    @endif
                    </div>
                    <div class="rozmowa-post">
                        <p class="rozmowa-autor"><a class='author' href="/profil/{{$msg->sender->id}}" target="_parent">{{$msg->sender->login}}</a> <small class="text-muted"><span value='{{strtotime($msg->created_at)}}' class='time-from'></span></small></p>
                        
                        <p class='tresc'>{!!$msg->message!!}</p>
                    </div>
                    
                </div>
            </div>
            @endforeach
            <a id="end"></a>    
        </div>
            
        </div>
        <div class="col-12 newMessage message" data-id='0' id='0' style='display: none;'>
                <div class="rozmowa-post-wrapper">
                    <div class="rozmowa-avatar">
                        
                        <img src="" class="menu-avatar" alt="Avatar">
                    </div>
                    <div class="rozmowa-post">
                        <p class="rozmowa-autor"><a class='author' href="" target="_parent"></a> <small class="text-muted"><span value='' class='time-from'></span></small></p>
                        <p class='tresc'></p>
                    </div>
                    
                </div>
            </div>

        
        
    </div>
    
</div>

<div class="chat-wiadomosc-fixed">
    <div class="chat-wiadomosc-wrapper">
        
            <div class="textarea-wrapper">
                <form id='message-send'>
                {{ csrf_field() }}
                <input type='hidden' name='reciver_id' value='{{$withUser->id}}'>
                <textarea class="napisz-wiadomosc form-control" name='message' rows="1" placeholder="Napisz wiadomość..." id='message'></textarea>
                <a onCLick='sendForm()' class="btn btn-link textarea-send-btn"><i class="fas fa-paper-plane"></i></a>
                </form>
            </div>
        

        <!-- Skrypt do autopowiększania textarea /start -->
       
        <!-- Skrypt do autopowiększania textarea /koniec -->

        <script>

            var login = '{{Auth::user()->login}}';
            var userId = {{Auth::user()->id}};

            var lastId = {{isset($lastId) ? $lastId : 0}};

            var offset = 1;
            var reciver_id = {{$withUser->id}};
            var message = $('.newMessage');
            var newMessage;
            var conversationDiv = $('.rozmowa');
            var messengerSound = false;
            document.getElementById(lastId).scrollIntoView();

            $('.tresc').each(function(i, obj) {
                $(obj).html(anchorme($(obj).html(),{attributes:[{name:'target',value:'blank'}]}));
            });

            $(function(){
                autosize($('textarea'));
                $("textarea").on("keydown",function(e){
                    if(!e.shiftKey && e.keyCode===13){
                        e.preventDefault();
                        var form = $('#message-send');

                        var url = '/wiadomosci/wyslij';
                        
                        if(!$('textarea').val()) {
                            e.preventDefault()
                        } else {
                            $.ajax({
                                method: "POST",
                                data: form.serialize(),
                                url: url,
                            })
                            .done(function( data ) {
    
                            });
                            
                            $('textarea').val('');
                            $('textarea').css('height','36px');
                        }
                        
                    }
                });
            });

            function sendForm() {
                if(!$('textarea').val()) {
                    e.preventDefault()
                } else {
                    var form = $('#message-send');

                    var url = '/wiadomosci/wyslij';
                    
                    $.ajax({
                        method: "POST",
                        data: form.serialize(),
                        url: url,
                    })
                    .done(function( data ) {

                    });
                    
                    $('textarea').val('');
                    $('textarea').css('height','36px');
                }
                
            }

                Echo.private('conversation-{!!$channelUsers[0]!!}-{!!$channelUsers[1]!!}')
                
                .whisper('typing', {
                    name: login+' Pisze...'
                })
                .listenForWhisper('typing', (e) => {
                    console.log(e.name);
                })
                //.joining(...)
                //.leaving(...)
                .listen('newMessage', (e) => {
                    
                    console.log(e);
                
                    var n = Date.now();
                    newMessage = message.clone();
                    
                    newMessage.attr('id',e.data.id)
                    newMessage.attr('data-id',e.data.id)
                    newMessage.find('p.tresc').html(anchorme(e.data.message,{attributes:[{name:'target',value:'blank'}]}));
                    newMessage.find('.author').html(e.user['login']);
                    newMessage.find('.menu-avatar').attr("src",e.user['avatar']);
                    newMessage.find('.author').attr("href",'/profil/'+e.user['id']);
                    newMessage.find('.img-link').attr("href",'/profil/'+e.user['id']);
                    newMessage.find('.time-from').attr('value',n/1000);
                    newMessage.find('.time-from').html('1 sek. temu');
                    newMessage.toggleClass('newMessage');
                    if(e.user['id'] == userId)
                        newMessage.toggleClass('messageowner');

                    newMessage.appendTo(conversationDiv);
                    newMessage.show();

                    timeToText(newMessage.find('.time-from'));
                    if(messengerSound) {
                        if(!document.hasFocus()) {
                            console.log('audio without focus ');
                            audio.play();
                        }
                    }
                    
                    $('#no-messages').remove();
                    
                    document.getElementById(e.data.id).scrollIntoView();
                    
                    if(userId == e.user['id']) {
                        var element = $('#conversation-'+e.reciverId);
                        
                    } else {
                        
                        var element = $('#conversation-'+e.user['id']);
                        
                    }
                    
                    //console.log(element);

                    if(element.length  > 0) {
                        element.remove();
                        console.log('mamy element');
                        newMessageInfo = true;

                        element.removeClass('nowa-wiadomosc');      

                        $('#conversations').prepend(element);
                        
                        var count = 0;

                        $.each($('.conversation'), function(key,value) {
                            if($(value).hasClass('nowa-wiadomosc')) {
                                count++;
                            } 
                        });
                        

                        if(count == 0) {
                            $('#messages-count').hide();
                        } else {
                            $('#messages-count').show();
                            $('#messages-count').html(count);
                        }
                    } else {
                        
                            $('.no-messages').remove();
                            //console.log('nie mamy elementu');
                            var element = $('.newConversation');
                            newMessageInfo = true;
                            
                            element.removeClass('nowa-wiadomosc');      

                            element.find('.nick-rozmowcy').html(e.reciverLogin);
                            element.find('.menu-avatar').attr("src",e.reciverAvatar);
                            element.attr("id",'conversation-'+e.reciverId);
                            element.find('.usun-rozmowce a').attr("onclick",'removeConversation('+e.reciverId+')');
                            element.find('.adress').attr("href",'/wiadomosci/'+e.reciverId);
                            element.show();
                            element.toggleClass('newConversation');


                            $('#conversations').prepend(element);
                            var count = 0;
                            $.each($('.conversation'), function(key,value) {
                                if($(value).hasClass('nowa-wiadomosc')) {
                                    count++;
                                } 
                            });
                            //console.log('unreaded message count:'+count);
                            if(count == 0) {
                                $('#messages-count').hide();
                            } else {
                                $('#messages-count').show();
                                $('#messages-count').html(count);
                            }
                        
                        
                    }
                    

                    // mark as seen
                    if(e.user['id'] != userId) {
                        var url = '/wiadomosci/przeczytaj/'+e.data.id;
                            
                        $.ajax({
                            method: "GET",
                            url: url,
                        })
                        .done(function( data ) {

                        });
                    }
                    
                });
                
                var loadChecker = false;
                document.getElementById('content-wrapper').addEventListener('scroll', function(e) {
                    
                    if(isScrolledIntoView($('#start'))) {
                        
                        console.log('mamy start');

                        if(!loadChecker) {
                            offset = offset + 20;
                            loadChecker = true;
                            var url = '/wiadomosci/doladuj/'+reciver_id+'/'+offset;
                            $.ajax({
                                method: "GET",
                                url: url,
                            })
                            .done(function( data ) {
                                var start = $('#start');
                                start.remove();
                                var ilosc = 0;
                                $.each( data.messages, function( key, value ) {
                                    newMessage = message.clone();
                                    var date = value.created_at;

                                    var n = Date.parse(date);
                                    
                                    ilosc++;
                                    
                                    newMessage.attr('id',value.id)
                                    newMessage.attr('data-id',value.id)
                                    newMessage.find('p.tresc').html(anchorme(value.message));
                                    newMessage.find('.author').html(value.sender.login);
                                    newMessage.find('.menu-avatar').attr("src",value.sender.avatar);
                                    newMessage.find('.author').attr("href",'/profil/'+value.sender.id);
                                    newMessage.find('.img-link').attr("href",'/profil/'+value.sender.id);
                                    newMessage.find('.time-from').attr('value',n/1000);
                                    newMessage.toggleClass('newMessage');
                                    if(value.sender.id == userId)
                                        newMessage.toggleClass('messageowner');
                                    var $current_top_element = $('#content-wrapper .rozmowa').children().first();
                                    $('.rozmowa').prepend(newMessage);
                                    
                                    newMessage.show();
                                    
                                    timeToText(newMessage.find('.time-from'));

                                    var previous_height = 0;

                                    $current_top_element.prevAll().each(function() {
                                    previous_height += $(this).outerHeight();
                                    });

                                    $('#content-wrapper').scrollTop(previous_height);
                                });

                                $('.rozmowa').prepend(start);
                                setTimeout(function(){
                                    if(ilosc == 0) {
                                        loadChecker = true;
                                    } else {
                                        loadChecker = false;
                                    } 
                                },2000);
                                
                            });
                        }
                        
                          
                                
                                   
                                
                                
                        
                      }
                }, true);

                function isScrolledIntoView(elem){
                    var $elem = $(elem);
                    var $window = $(window);
                
                    var docViewTop = $window.scrollTop();
                    var docViewBottom = docViewTop + $window.height();
                
                    var elemTop = $elem.offset().top;
                    var elemBottom = elemTop + $elem.height();
                
                    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
                }

        </script>
        <script src="{{ asset('js/textarea-autosize.js') }}"></script>
        <script>
            

                const ELEMENT_CLASS = '.time-from';
                const DATE_ATTR_NAME = 'value';
    
                const TEN_SECONDS_MILLIS = 10 * 1000;
                const MINUTE_MILLIS = 60 * 1000;
                const HOUR_MILLIS = 60 * MINUTE_MILLIS;
                const DAY_MILLIS = 24 * HOUR_MILLIS;
    
                const UNDER_MINUTE_REFRESH_MILLIS = TEN_SECONDS_MILLIS; // every 10 seconds
                const UNDER_HOUR_REFRESH_MILLIS = MINUTE_MILLIS; // every minute
                const UNDER_DAY_REFRESH_MILLIS = HOUR_MILLIS; // every hour
                const OVER_DAY_REFRESH_MILLIS = DAY_MILLIS; // every day
    
    
                function getRandomTimeoutId() {
                return Math.floor(Math.random() * (1e16));
                }
    
                function timeToText(element, timeoutId) {
                const date = parseInt(element.attr(DATE_ATTR_NAME)) * 1000;
                //const millis = (Date.now() - date);
                const now = Date.now();
                const millis = date > now ? 1 : (now - date);
                
                if (timeoutId) {
                    window.clearTimeout(timeoutId);
                }
                const tId = getRandomTimeoutId();
    
                let text, refreshRate;
    
                if (millis < MINUTE_MILLIS) {
                    refreshRate = UNDER_MINUTE_REFRESH_MILLIS;
                    text = underMinuteText(millis);
                } else if (millis < HOUR_MILLIS) {
                    refreshRate = UNDER_HOUR_REFRESH_MILLIS;
                    text = underHourText(millis);
                } else if (millis < DAY_MILLIS) {
                    refreshRate = UNDER_DAY_REFRESH_MILLIS;
                    text = underDayText(millis);
                } else {
                    refreshRate = OVER_DAY_REFRESH_MILLIS;
                    text = overDayText(millis,date);
                }
    
                setTimeout(function() {
                    timeToText(element, tId);
                }, refreshRate);
    
                element.text(text);
                }
    
                function underMinuteText(millis) {
                const tensOfSeconds = Math.floor(millis / TEN_SECONDS_MILLIS);
                if (tensOfSeconds < 1) {
                    return `${Math.floor(millis / 1000)} sek. temu`;
                }
                return `${tensOfSeconds * 10} sek. temu`;
                }
    
                function underHourText(millis) {
                const minutes = Math.floor(millis / MINUTE_MILLIS);
                return `${minutes} min. temu`;
                }
    
                function underDayText(millis) {
                const hours = Math.floor(millis / HOUR_MILLIS);
                return `${hours} godz. temu`;
                }
    
                function overDayText(millis,date) {
                const days = Math.floor(millis / DAY_MILLIS);
                if (days === 1) {
                    return "1 dzień temu";
                } else {
                    if(days > 3) {
                        var date = new Date(date);
                        
                        return date.customFormat( "#DD#.#MM#.#YYYY#" ) 
                    } else {
                        return `${days} dni temu`;
                    }
                }
                }
    
                $(document).ready(function() {
                $(ELEMENT_CLASS).each(function(i, element) {
                    timeToText($(element));
                });
                });

        </script>
    </div>
</div>

@endsection
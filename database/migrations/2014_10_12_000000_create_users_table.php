<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->default(1);
            $table->string('login');
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->string('provider');
            $table->string('provider_id')->nullable();
            $table->boolean('activated')->default(false);
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('description')->nullable();
            $table->string('localization')->nullable();
            $table->string('city')->nullable();
            $table->string('voivodeship')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->date('birth_date')->nullable();
            $table->enum('sex', array('meale', 'female','none'))->default('none');
            $table->string('www')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

<?php

if(!function_exists('format_time')) {

    function format_time($t,$f=':') // t = seconds, f = separator
    {
      return sprintf("%02d%s%02d%s%02d", floor($t/3600), $f, ($t/60)%60, $f, $t%60);
    }
}

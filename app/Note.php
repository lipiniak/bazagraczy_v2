<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    //
    protected $table = 'notes';


    protected $fillable = ['user_id','note_for_id','note'];
 
    
    public function from() {
        return $this->belongsTo('App\User','user_id');
    }

    public function for() {
        return $this->belongsTo('App\User','note_for_id');
    }
}


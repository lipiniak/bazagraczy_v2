<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'teams';

    protected $fillable = ['clan_id','leader_id','game_id','platform_id','name'];

    public function leader()
    {
        return $this->belongsTo('App\User','leader_id');
    }

    public function members() {
        return $this->belongsToMany('App\User', 'users_teams', 'teams_id', 'users_id');
    }

    public function clan() {
        return $this->belongsTo('App\Clan','clan_id');
    }

    public function game()
    {
        return $this->belongsTo('App\Games','game_id');
    }

    public function platform()
    {
        return $this->belongsTo('App\Platforms','platform_id');
    }

    public function hostMatches() {
        return $this->hasMany('App\FriendlyMatch','host_team');
    }

    public function enemyMatches() {
        return $this->hasMany('App\FriendlyMatch','enemy_team');
    }

    public function allMatches() {
        return $this->hostMatches->merge($this->enemyMatches)->sortByDesc('match_date');;
    }
}

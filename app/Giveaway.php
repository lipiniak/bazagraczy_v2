<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Auth;

class Giveaway extends Model
{

    protected $table = 'giveaway';

    protected $fillable = ['title','description','main_img','ending_at','status','carousel_img'];


    public $joinAvalible = true;

    public function userJoined() {
        $user = Auth::user();



        return false;
    }

    public function rules() {
        return $this->belongsToMany('App\GiveawayRules','giveaway_rules_pivot','giveaway_id','rule_id');
    }

    public function participants() {
        return $this->belongsToMany('App\User','giveaway_joins','giveaway_id','user_id')->withPivot('tickets','rules');
    }


    public function checkRule($rule_key) {

        $userRules = $this->participants()->where('user_id',Auth::user()->id)->first()->pivot->rules;

        if($userRules != '')
        foreach(json_decode($userRules) as $key => $rules) {
            if($key == $rule_key) {
                if($rules == false) 
                    $this->joinAvalible = false;

                return $rules;
            }
                
        }
    }
}


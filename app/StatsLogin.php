<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatsLogin extends Model
{
    protected $table = 'stats_login';

    protected $fillable = [
        'stat_date', 'count'
    ];

}

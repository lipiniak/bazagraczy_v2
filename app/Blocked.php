<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blocked extends Model
{
    //
    protected $table = 'blocked_user';


    protected $fillable = ['user_id','blocked_id'];
    
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSettings extends Model
{
    protected $table = 'user_settings';
    
    protected $fillable = [
        'user_id', 'lfc','selected_game_id','selected_platform_id','friends_list_status','notification_sound','messenger_sound','notify_all_library_games','selected_notify_games',
    ];

}

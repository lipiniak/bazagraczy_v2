<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Storage;

class newComment implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public $post_id;

    public $comment;

    public $user;
    public function __construct($comment,$post_id,$user)
    {
        $this->comment = $comment;
        $this->post_id = $post_id;

        if($user->avatar == 'no-avatar.jpg') {
            $avatar = asset('images/avatars/').'/'.$user->avatar;
        } else {
            $avatar = Storage::url($user->avatar);
        }
        $user = [
            'id' => $user->id,
            'login' => $user->login,
            'avatar' => $avatar,

        ];

        $this->user = $user;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('post-'.$this->post_id);
    }
}

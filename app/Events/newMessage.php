<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Storage;

class newMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

     public $message;
     public $data;
     public $user;
     public $reciverId;
     public $reciverAvatar;
     public $reciverLogin;
     private $channelId;

    public function __construct($message, $user,$channelId,$reciverId,$reciverAvatar,$reciverLogin)
    {   
        $this->message = $message;
        $this->channelId = $channelId;

        if($user->avatar == 'no-avatar.jpg') {
            $avatar = asset('images/avatars/').'/'.$user->avatar;
        } else {
            $avatar = Storage::url($user->avatar);
        }
        $user = [
            'id' => $user->id,
            'login' => $user->login,
            'avatar' => $avatar,

        ];
        $this->reciverId = (int)$reciverId;
        $this->reciverLogin = $reciverLogin;
        $this->reciverAvatar = $reciverAvatar;
        $this->user = $user;
        $this->data = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {   
        return new PrivateChannel('conversation-'.$this->channelId);
    }
}

<?php

namespace App\Http\Controllers\Giveaway\Tickets;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\GiveawayQuests;
use Auth;


class TicketsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

     public function list() {
        
        $quests = GiveawayQuests::where('status',1)->get();


        $questsStatic = $quests->where('type',1);
        $questsAdditional = $quests->where('type',2);

        //sprawdzenie czy quest jest zrobiony
        $userQuestsStatic = Auth::user()->questDone->where('type',1);
        $userQuestsAdditional = Auth::user()->questDone->where('type',2);
        
        $pointsStatic = 0;
        $pointsAdditional = 0;


        foreach($userQuestsStatic as $quest) {
            $pointsStatic += $quest->points;
        }

        foreach($userQuestsAdditional as $quest) {
            $pointsAdditional += $quest->points;
        }

        $questsStatic = $questsStatic->diff($userQuestsStatic);
        $questsAdditional = $questsAdditional->diff($userQuestsAdditional);

        return view('giveaway.tickets.tickets',compact('questsStatic','questsAdditional','pointsAdditional','pointsStatic','userQuestsStatic','userQuestsAdditional'));
     }

     
}

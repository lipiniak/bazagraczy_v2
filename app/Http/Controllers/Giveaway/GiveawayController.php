<?php

namespace App\Http\Controllers\Giveaway;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Giveaway;
use Auth;
use GiveawayRules;

class GiveawayController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

      public function list() {

         $giveaways = Giveaway::all();

         return view('giveaway.list',compact('giveaways'));
      }

     public function show($id) {

        $giveaway = Giveaway::find($id);
        
         $rules = array();
         $giveaway->participants()->sync(Auth::user()->id,false);

         
         if($giveaway->participants()->where('user_id',Auth::user()->id)->where('joined',0)->exists()) {
            
            foreach($giveaway->rules as $rule) {
               
               $result = GiveawayRules::checkRule($rule,$giveaway->id);

               $rules[$rule->rule_key] = $result;
            }
            $giveaway->participants()->updateExistingPivot(Auth::user()->id,['rules'=> json_encode($rules)]); 
         }
         


        
        
        return view('giveaway.show',compact('giveaway'));
     }

     public function join($id) {
        $giveaway = Giveaway::find($id);
        $user = Auth::user();
        $userTickets = $user->tickets + $user->tmp_tickets;
        $user->tmp_tickets = 0;
        $user->save();
        $giveaway->participants()->updateExistingPivot(Auth::user()->id,['joined'=>1,'tickets'=>$userTickets]);

        return redirect()->back();
     }
}
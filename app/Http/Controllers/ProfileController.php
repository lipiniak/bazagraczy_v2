<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redis;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Tags;
use App\Note;
use App\Games;
use App\Platforms;

use Session;
use Validator;
use Storage;
use DB;
use Hash;
use Quest;
class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showProfile($user = 0)
    {
        $page = 'profile.show';

        
            $user = User::where('id',$user)->where('activated',1)->first();
            
            if(!empty($user))   
                return view('profile.show',compact('user','page'));
            else 
                return redirect()->back()->with('error','Nie ma takiego profilu.');
        

        
        
    }
    
    public function profileValidate($data) {
        return Validator::make($data, [
            'steam' => 'nullable|min:2|max:50',
            'origin' => 'nullable|min:2|max:50',
            'uplay' => 'nullable|min:2|max:50',
            'battlenet' => 'nullable|min:2|max:50',
            'epicgames' => 'nullable|min:2|max:50',
            'psn' => 'nullable|min:2|max:50',
            'xboxlive' => 'nullable|min:2|max:50',
            'description' => 'max:2500',
            'login' => 'required|min:2|max:25|regex:/^[a-zA-Z0-9-_.AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpRrSsTtUuWwYyZz]+$/u',
            'name' => 'nullable|min:3|max:50',
            'surname' => 'nullable|min:3|max:50',
            'sex' => 'required',
            'birth_date' => 'required',
            'www' => "nullable|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/",

        ],[
            'steam.min' => 'Wartość za krótka (min. 2 znaki)',
            'steam.max' => 'Wartość za długa (max. 50 znaków)',

            'origin.min' => 'Wartość za krótka (min. 2 znaki).',
            'origin.max' => 'Wartość za długa (max. 50 znaków)',

            'uplay.min' => 'Wartość za krótka (min. 2 znaki)',
            'uplay.max' => 'Wartość za długa (max. 50 znaków)',

            'battlenet.min' => 'Wartość za krótka (min. 2 znaki)',
            'battlenet.max' => 'Wartość za długa (max. 50 znaków)',

            'epicgames.min' => 'Wartość za krótka (min. 2 znaki).',
            'epicgames.max' => 'Wartość za długa (max. 50 znaków)',

            'psn.min' => 'Wartość za krótka (min. 2 znaki)',
            'psn.max' => 'Wartość za długa (max. 50 znaków)',

            'xboxlive.min' => 'Wartość za krótka (min. 2 znaki)',
            'xboxlive.max' => 'Wartość za długa (max. 50 znaków)',

            'description.max' => 'Niedozwolona ilość znaków (max. 2500 znaków)',

            'login.required' => 'Uzupełnij pole',
            'login.min' => 'Nazwa za krótka (min. 2 znaki)',
            'login.max' => 'Nazwa za długa (max. 25 znaków)',
            'login.regex' => 'Użyto niedozwolonych znaków <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Nazwa może składać się z cyfr (0-9), liter (a-z; A-Z; bez polskich znaków) oraz \'-\' (myślnik) i \'_\' (podkreślenie). Nie może zawierać spacji."><i class="fas fa-question-circle"></i></a>',
            
            'name.min' => 'Niedozwolona ilość znaków (min. 3 znaki)',
            'name.max' => 'Niedozwolona ilość znaków (max. 50 znaków)',
            'name.regex' => 'Użyto niedozwolonych znaków <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Imię może składać się jedynie z liter (a-z; A-Z; w tym polskie znaki) oraz \'-\' (myślnik) i znaku spacji."><i class="fas fa-question-circle"></i></a>',
            
            'surname.min' => 'Niedozwolona ilość znaków (min. 3 znaki)',
            'surname.max' => 'Niedozwolona ilość znaków (max. 50 znaków)',
            'surname.regex' => 'Użyto niedozwolonych znaków <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Imię może składać się jedynie z liter (a-z; A-Z; w tym polskie znaki) oraz \'-\' (myślnik) i znaku spacji."><i class="fas fa-question-circle"></i></a>',

            'sex.required' => 'Uzupełnij pole',

            'birth_date.required' => 'Uzupełnij pole',

            'www.regex' => 'Podany URL jest nieprawidłowy',
        ]);
    }

    public function showEdit()
    {
        $page = 'profile.edit';
        
        $voivodeship = \App\Voivodeship::all();
        $void[0] = "";
        foreach($voivodeship as $item) {
            $void[$item->id] = $item->name;   
        }
        $user = User::find(Auth::user()->id);

        if(Auth::user()->id != $user->id)
            return redirect()->back()->with('error','Brak dostępu.');



        $city = array();
        $city[0] = "";
        if($user->voivodeship != NULL) {
            $cities = \App\Cities::where('voivodeship_id',$user->voivodeship)->get();

            foreach($cities as $item) {
                $city[$item->id] = $item->name;
            }
        } 

        $tags = array();
        foreach($user->tags as $item) {
            $tags[$item->id] = $item->tag;
        }
        return view('profile.edit',compact('user','page','void','city','tags'));
    }

    public function profileUpdate(Request $request) {

        $data = $request->all();

        $validator = $this->profileValidate($request->all());
        
        $city = array();
        $city[0] = "";
        
            $cities = \App\Cities::where('voivodeship_id',$data['voivodeship'])->get();

            foreach($cities as $item) {
                $city[$item->id] = $item->name;
            }
        
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->with('city',$city)
                        ->withInput()
                        ->with('error','Błąd. Popraw formularz.');
        }
        unset($data['_token']);
        
        if(isset($data['show_age']))
            if($data['show_age'] == 'on')
                $data['show_age'] = 1;
            else 
                $data['show_age'] = 0;
        else 
            $data['show_age'] = 0;

        $data['birth_date'] = date("Y-m-d", strtotime($data['birth_date']));

        Tags::where('user_id',Auth::user()->id)->delete();
        $i = 0;
        if(isset($data['tags'])) {
            foreach($data['tags'] as $tag) {
                $tags[$i]['user_id'] = Auth::user()->id;
                $tags[$i]['tag'] = $tag;
                $i++;
                
            }

            Tags::insert($tags);

            Quest::doneQuest('quest_tags');
            
            unset($data['tags']);
        }
        if(isset($data['description']))
            Quest::doneQuest('quest_description');
        
        User::where('id',Auth::user()->id)->update($data);

        return redirect()->route('profile.edit')->with('success','Pomyślnie zapisano zmiany');
    }

    public function showLibrary() {
        $page = 'profile.library';
        $user = Auth::user();

        if(Auth::user()->id != $user->id)
            return redirect()->back()->with('error','Brak dostępu.');


        return view('profile.library',compact('page','user'));
    }

    public function validateLibrary($data) {
        return Validator::make($data,[
            'game' => 'required',
            'platform' => 'required',
        ],[
            'game.required' => ' Wybierz tytuł gry',
            'platform.required' => 'Wybierz platformę',
        ]);
    }
    public function addToLibrary(Request $request) {
        $data = $request->all();

        $this->validateLibrary($data)->validate();

        unset($data['_token']);
        
        
        $user = User::find(Auth::user()->id);
        
        if(Auth::user()->id != $user->id)
        return redirect()->back()->with('error','Brak dostępu.');


        $game = DB::table('users_games_platforms')->where('user_id', '=', Auth::user()->id)->where('games_id','=',$data['game'])->where('platforms_id','=',$data['platform'])->first();

        
        if(!$game) {
            $result = array('user_id'=> Auth::user()->id, 'games_id'=>$data['game'],'platforms_id'=>$data['platform']);
                
            DB::table('users_games_platforms')->insert($result);
            
            $value = $data['game'].'-'.$data['platform'];
            // Dodaj do settingsów jeśli nie ma? 
            $setting = \App\UserSettings::where('user_id',Auth::user()->id)->first();
                if($setting->selected_notify_games == '')
                    $selected_games = array();
                else {
                    $selected_games = json_decode($setting->selected_notify_games, true);
                }
                if($selected_games == NULL)
                $selected_games = array();
                

                $el = array_search($value, $selected_games);
                
                if($el !== false) {
                    unset($selected_games[$el]);
                } else {
                    
                    array_push($selected_games,$value);
                }

                $setting->selected_notify_games = json_encode($selected_games);
                $setting->save();
                Quest::doneQuest('quest_game');
            return redirect()->back()->with('success','Dodano grę do biblioteki.');
        } else {
            return redirect()->back()->withErrors(['game'=>'Wybrana gra znajduje się już w bibliotece']);
        }
        

        
    }

    public function removeFromLibrary($game_id,$platform_id) {
        
        DB::table('users_games_platforms')->where('user_id', '=', Auth::user()->id)->where('games_id','=',$game_id)->where('platforms_id','=',$platform_id)->delete();
        
        $value = $game_id.'-'.$platform_id;
            // Dodaj do settingsów jeśli nie ma? 
            $setting = \App\UserSettings::where('user_id',Auth::user()->id)->first();
                if($setting->selected_notify_games == '')
                    $selected_games = array();
                else {
                    $selected_games = json_decode($setting->selected_notify_games, true);
                }
                if($selected_games == NULL)
                $selected_games = array();

                $el = array_search($value, $selected_games);
                
                if($el !== false) {
                    unset($selected_games[$el]);
                } 

                $setting->selected_notify_games = json_encode($selected_games);
                $setting->save();

        return redirect()->back()->with('success','Pomyślnie usunięto grę z biblioteki.');
    }
    public function showAvatar() {
        $page = 'profile.avatar';

        $user = Auth::user();
        if(Auth::user()->id != $user->id)
        return redirect()->back()->with('error','Brak dostępu.');

        return view('profile.avatar',compact('page','user'));
    }
    public function validateAvatar($data) {
        return Validator::make($data, [
            'avatar' => 'required|max:500|mimes:jpeg,jpg,gif,png',
        ], [
            'avatar.required' => 'Proszę wybrać plik',
            'avatar.max' => 'Wybrany plik jest za duży (max. 500kB)',
            'avatar.mimes' => 'Niedozwolony format pliku (tylko .jpg, .gif, .png)'
        ]);
    }
    public function avatarChange(Request $request) {
        $user = Auth::user();
        
        if(Auth::user()->id != $user->id)
        return redirect()->back()->with('error','Brak dostępu.');


        $this->validateAvatar($request->all())->validate();
        
        $avatar = $request->file('avatar');
        
        $user->avatar = Storage::putFileAs(
            'public/avatars', $request->file('avatar'), time().'-'.$request->user()->id
        );
        $user->save();

        //Done quest
        Quest::doneQuest('quest_avatar');

        return redirect()->back()->with('success','Avatar został zmieniony.');
    }
    public function deleteAvatar() {
        $user = Auth::user();

        if(Auth::user()->id != $user->id)
        return redirect()->back()->with('error','Brak dostępu.');


        Storage::delete($user->avatar);

        $user->avatar = 'no-avatar.jpg';
        $user->save();

        
        return redirect()->back()->with('success','Pomyślnie usunięto avatar.');
    }
    public function showSettings() {
        $user = Auth::user();
        if(Auth::user()->id != $user->id)
        return redirect()->back()->with('error','Brak dostępu.');

        //Games settings
        $userGames = $user->games();
        

        $page = 'profile.settings';
        return view('profile.settings',compact('page','user'));
    }

    public function showAccount() {
        $user = Auth::user();
        if(Auth::user()->id != $user->id)
        return redirect()->back()->with('error','Brak dostępu.');

        $page = 'profile.account';
        
        return view('profile.account',compact('user','page'));
    }

    
    public function showNotifications() {
        $user = Auth::user();
        if(Auth::user()->id != $user->id)
        return redirect()->back()->with('error','Brak dostępu.');

        $page = 'profile.notifications.show';
        
        return view('profile.notifications',compact('user','page'));
    }

    public function deleteNotification(Request $request){
        $data = $request->all();

        $notification_id = $data['notification_id'];

        $user = Auth::user();

        if(Auth::user()->id != $user->id)
        return redirect()->back()->with('error','Brak dostępu.');


        $notification = $user->notifications()->where('id',$notification_id)->first();
        
        if ($notification)
        {
            $notification->delete();
            return back()->with('success','Powiadomienie zostało usunięte.');
        }
        else
            return back()->withErrors('Wystąpił nieoczekiwany błąd.');
    }

    public function markAndRedirect(Request $request) {

        $data = $request->all();

        $notification_id = $data['notificationid'];

        $user = Auth::user();

        if(Auth::user()->id != $user->id)
        return redirect()->back()->with('error','Brak dostępu.');


        $notification = $user->notifications()->where('id',$notification_id)->first();
        
        if ($notification)
        {
            $notification->markAsRead();
            return redirect($data['redirect'])->with('success','Powiadomienie zostało oznaczone.');
        }
        else
            return back()->withErrors('Wystąpił nieoczekiwany błąd.');
    }
    
    public function markNotification(Request $request){
        $data = $request->all();

        $notification_id = $data['notification_id'];

        $user = Auth::user();

        if(Auth::user()->id != $user->id)
            return redirect()->back()->with('error','Brak dostępu.');


        $notification = $user->notifications()->where('id',$notification_id)->first();
        
        if ($notification)
        {
            $notification->markAsRead();
            return back()->with('success','Powiadomienie zostało oznaczone.');
        }
        else
            return back()->withErrors('Wystąpił nieoczekiwany błąd.');
    }
    
    public function unMarkNotification(Request $request){
        $data = $request->all();

        $notification_id = $data['notification_id'];

        $user = Auth::user();

        if(Auth::user()->id != $user->id)
        return redirect()->back()->with('error','Brak dostępu.');


        $notification = $user->notifications()->where('id',$notification_id)->first();
        
        if ($notification)
        {
            $notification->markAsUnread();
            return back()->with('success','Powiadomienie zostało odznaczone.');
        }
        else
            return back()->withErrors('Wystąpił nieoczekiwany błąd.');
    }

    public function markAllNotifications() {
        $user = Auth::user();

        if(Auth::user()->id != $user->id)
        return redirect()->back()->with('error','Brak dostępu.');


        $user->unreadNotifications->markAsRead();

        return redirect()->back()->with('success','Wszystkie powiadomienia zostały oznaczone.');
    }

    public function deleteAllNotifications() {
        $user = Auth::user();

        $user->notifications()->delete();

        return redirect()->back()->with('success','Wszystkie powiadomienia zostały usunięte.');
    }


    public function showFollowers() {
        $user = Auth::user();

        if(Auth::user()->id != $user->id)
        return redirect()->back()->with('error','Brak dostępu.');


        return view('profile.followers',compact('user'));
    }
    public function showBlocked() {
        $user = Auth::user();

        if(Auth::user()->id != $user->id)
        return redirect()->back()->with('error','Brak dostępu.');


        return view('profile.blocked',compact('user'));
    }

    public function validateNote($data) {
        return Validator::make($data, [
            'note' => 'max:250|required'
        ],[
            'note.max' => 'Niedozwolona ilość znaków (max. 250 znaków)',
            'note.required' => 'Uzupełnij pole'
        ]);
    }

    public function addNote(Request $request, $user_id) {

        $data = $request->all();

        $this->validateNote($data)->validate();
        
        unset($data['_token']);
        
        $note = Note::where('user_id',Auth::user()->id)->where('note_for_id',$user_id)->first();
        if($note) {
            $note->note = $data['note'];
            $note->save();

            return redirect()->back()->with('success','Notatka została zmieniona.');
        } else {
            $data['note_for_id'] = $user_id;
            $data['user_id'] = Auth::user()->id;

            Note::create($data);
            
            return redirect()->back()->with('success','Dodano notatkę.');
        }
        

        
    }

    public function deleteNote($id) {

        $note = Note::where('user_id',Auth::user()->id)->where('note_for_id',$id)->first();

        $note->delete();

        return redirect()->back()->with('success','Notatka została usunięta.');
    }

    public function chageSetting($setting,$value) {

        switch($setting) {
            case 'klan': 
                
                $setting = \App\UserSettings::where('user_id',Auth::user()->id)->first();
                $setting->lfc = $value;
                $setting->save();

            break;
            case 'powiadomienia': 
                
                $setting = \App\UserSettings::where('user_id',Auth::user()->id)->first();
                $setting->notification_sound = $value;
                $setting->save();

            break;
            case 'wiadomosci': 
                
                $setting = \App\UserSettings::where('user_id',Auth::user()->id)->first();
                $setting->messenger_sound = $value;
                $setting->save();

            break;
            case 'gra':
                $setting = \App\UserSettings::where('user_id',Auth::user()->id)->first();
                $setting->selected_game_id = $value;
                $setting->save();
            break;
            
            case 'platforma':
                $setting = \App\UserSettings::where('user_id',Auth::user()->id)->first();
                $setting->selected_platform_id = $value;
                $setting->save();
            break;
            case 'notify_all_games':
                $setting = \App\UserSettings::where('user_id',Auth::user()->id)->first();
                $setting->notify_all_library_games = $value;
                $setting->save();
            break;
            case 'select_games':
                $setting = \App\UserSettings::where('user_id',Auth::user()->id)->first();
                if($setting->selected_notify_games == '')
                    $selected_games = array();
                else {
                    $selected_games = json_decode($setting->selected_notify_games, true);
                }


                $el = array_search($value, $selected_games);
                
                if($el !== false) {
                    unset($selected_games[$el]);
                } else {
                    
                    array_push($selected_games,$value);
                }
                
                $setting->selected_notify_games = json_encode($selected_games);
                $setting->save();
                
            break;
        }

        return response()->json(['result' => 'success']);
    }

    public function changePickedGame($game_id,$platform_id) {
        $setting = \App\UserSettings::where('user_id',Auth::user()->id)->first();
        $setting->selected_game_id = $game_id;
        $setting->save();

        $setting = \App\UserSettings::where('user_id',Auth::user()->id)->first();
        $setting->selected_platform_id = $platform_id;
        $setting->save();

        $game = Games::find($game_id);
        $gameName = $game->name;

        $platform = Platforms::find($platform_id);
        $platformName = $platform->name;

        $followers = Auth::user()->followers;

        foreach($followers as $follower) {
            broadcast(new \App\Events\FollowerChangedGame($follower->id,Auth::user()->id,$gameName,$platformName));
        }

        return response()->json(['result' => 'success']);
    }


    public function validateEmailChange($data) {
        return Validator::make($data, [
            'email' => 'required|confirmed|unique:users',
            'email_confirmation' => 'required',
            'password' => 'required',
        ],[
            'email.required' => 'Uzupełnij pole',
            'email.confirmed' => 'Email i jego powtórzenie są różne',
            'email.unique' => 'Email jest już zajęty',

            'email_confirmation.required' => 'Uzupełnij pole',

            'password.required' => 'Uzupełnij pole',
        ]);
    }

    public function validatePasswordChange($data) {
        return Validator::make($data, [
            'password_old' => 'required',
            'password' => 'required|min:4|max:50|confirmed',
            'password_confirmation' => 'required',
        ],[
            'password_old.required' => 'Uzupełnij pole',

            'password.required' => 'Uzupełnij pole',
            'password.min' => 'Nowe hasło za krótkie (min. 4 znaki)',
            'password.max' => 'Nowe hasło za długie (max. 50 znaków)',
            'password.confirmed' => 'Nowe hasło i powtórzenie są różne',

            'password_confirmation.required' => 'Uzupełnij pole'
        ]);
    }

    public function profilePasswordUpdate(Request $request) {
        $data = $request->all();

        $this->validatePasswordChange($data)->validate();
        
        $dane['password'] = bcrypt($data['password']);
        
        if(Hash::check($data['password_old'], Auth::user()->password)) {
            User::where('id',Auth::user()->id)->update($dane);
            Session::flush(); 
            return redirect('/')->with('success','Hasło zostało zmienione.');
        } else {
            return redirect()->back()->withErrors(['password_old'=>'Aktualne hasło jest niepoprawne.']);
        }

        return redirect()->back()->with(['error'=>'Wystąpił niespodziewany błąd.']);
    }

    public function profileEmailUpdate(Request $request) {
        
        $data = $request->all();
        
        $this->validateEmailChange($data)->validate();
        
        $dane['email'] = $data['email'];
        
        if(Hash::check($data['password'], Auth::user()->password)) {
            User::where('id',Auth::user()->id)->update($dane);

            return redirect()->back()->with('success','Adres email został zmieniony.');
        } else {
            return redirect()->back()->withErrors(['password'=>'Aktualne hasło jest niepoprawne.']);
        }
        
        return redirect()->back()->with(['error'=>'Wystąpił niespodziewany błąd.']);
    }
}

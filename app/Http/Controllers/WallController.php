<?php

namespace App\Http\Controllers;

use App\UserSettings;
use Illuminate\Http\Request;
use App\User;
use App\Post;
use Auth;
use DB;
use App\Comments;
use Validator;
use Notification;
use App\Notifications\NewComment;
use App\Notifications\CommentReply;
use App\Notifications\PostCreated;
use App\FollowPost;

class WallController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showWall()
    {
        $user = User::find(Auth::user()->id);

        $selectedGame = $user->selectedGame();
        
        $wall = Post::where('game_id',$selectedGame['game_id'])->where('platform_id',$selectedGame['platform_id'])->orderBy('created_at','DESC')->limit(50)->get();

        $user = Auth::user();
        
        $selectedGame = $user->selectedGame();

        $selectedUsers = \App\UserSettings::where('selected_game_id',$selectedGame['game_id'])->where('selected_platform_id',$selectedGame['platform_id'])->get();
        
        $userList = array();
        foreach($selectedUsers as $user) {
            $userList[] = $user->user_id;
        }

        $selectedUsers = User::whereIn('id',$userList)->get();
        $topbarCount = 0;
        $topbarOnlineCount = 0;
        foreach($selectedUsers as $user1) {
            $topbarCount++;
            if($user1->getStatus() == 'online')
                $topbarOnlineCount++;
        }

        $online = $topbarOnlineCount;

        return view('wall.wall',compact('wall','user','selectedGame','online'));
    }

    public function showOnline() {

        $user = Auth::user();
        
        $selectedGame = $user->selectedGame();

        $selectedUsers = \App\UserSettings::where('selected_game_id',$selectedGame['game_id'])->where('selected_platform_id',$selectedGame['platform_id'])->get();
        
        $userList = array();
        foreach($selectedUsers as $user) {
            $userList[] = $user->user_id;
        }

        $selectedUsers = User::all();
        
        $topbarCount = 0;
        $topbarOnlineCount = 0;
        foreach($selectedUsers as $user) {
            $topbarCount++;
            if($user->getStatus() == 'online')
                $topbarOnlineCount++;
        }
        $online = $topbarOnlineCount;

        $data = array('title'=>$selectedGame['game'],'platform'=>$selectedGame['platform']);

        return view('wall.online',compact('selectedUsers','online','data'));

    }

    public function showPost($post_id) {
        
        $user = User::find(Auth::user()->id);

        $selectedGame = $user->selectedGame();
        
        $post = Post::find($post_id);
        
        return view('wall.single',compact('post','user','selectedGame','post_id'));
    }

    public function validateNewPost($data) {
        return Validator::make($data, [
            'text' => 'max:1500'
        ],[
            'text.max' => 'Niedozwolona ilość znaków (max. 1500 znaków)'
        ]);
    }

    public function validateNewComment($data) {
        return Validator::make($data, [
            'comment-'.$data['post_id'] => 'max:500'
        ],[
            'comment-'.$data['post_id'].'.max' => 'Niedozwolona ilość znaków (max. 500 znaków)'
        ]);
    }
    public function newPost(Request $request) {
        $data = $request->all();

        $this->validateNewPost($data)->validate();

        unset($data['_token']);

        $data['user_id'] = Auth::user()->id;

        $post = Post::create($data);

        // User selected
        $users = UserSettings::where('selected_game_id',$data['game_id'])->where('selected_platform_id',$data['platform_id'])->where('user_id','!=',Auth::user()->id)->get();
        $selectedUsers = array();
        foreach($users as $user) {
            $selectedUsers[] = $user->user_id;
        }
        
        

        // users with all setting
        $usersSettings = UserSettings::where('notify_all_library_games',1)->where('user_id','!=',Auth::user()->id)->get();
        
        $list = array();
        foreach($usersSettings as $user) {
            $list[] = $user->user_id;
        }
                
        $usersList = array();

        $usersList = User::whereIn('id', $list)->get();
        
        $allGamesUsers = array();

        foreach($usersList as $user) {
            $hits = DB::table('users_games_platforms')->where('user_id',$user->id)->where('games_id',$data['game_id'])->where('platforms_id',$data['platform_id'])->get();
            
            if(count($hits) > 0) 
                $allGamesUsers[] = $user->id;
            
        }

        $usersSelectedGames = UserSettings::where('notify_all_library_games',2)->where('user_id','!=',Auth::user()->id)->get();
        $selectedGames = array();
        
        foreach($usersSelectedGames as $user) {
            
            $games = array();
            $games = json_decode($user->selected_notify_games,true);
            
            if($games == NULL)
                $games = array();

            if(array_search(($data['game_id'].'-'.$data['platform_id']),$games) !== false) {
                $selectedGames[] = $user->user_id;
            }    
        }
        

        //Merge all users for notification
        $users = array_merge($allGamesUsers,$selectedGames);
        $users = array_unique($users);

        $users = User::whereIn('id',$users)->get();

        $info = array(
            'user_id' => Auth::user()->id,
            'login' => Auth::user()->login,
            'post_id' => $post->id,
            'game' => $post->game->name,
            'platform' => $post->platform->name
        );

        Notification::send($users, new PostCreated($info));
        
        return redirect()->back()->with('success','Dodano nowy post.');
    }

    public function newComment(Request $request) {
        $data = $request->all();

        $validator = $this->validateNewComment($data);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors(['post-'.$data['post_id'] => 'Niedozwolona ilość znaków (max. 500 znaków)'])
                        ->withInput();
        }

        

        unset($data['_token']);

        $data['user_id'] = Auth::user()->id;
        
        $data['text'] = nl2br(e(str_replace('<br />','',$data['comment-'.$data['post_id']])));

        Comments::create($data);
        
        $post_id = $data['post_id'];

        $post = Post::find($post_id);
        

        if($post->user_id != Auth::user()->id) {
            $postUser = User::find($post->user_id) ;
            
            $info = array(
                'user_id' => Auth::user()->id,
                'login' => Auth::user()->login,
                'post_id' => $post_id,
                'game' => $post->game->name,
                'platform' => $post->platform->name
            );

            $postUser->notify(new NewComment($info));
        }
        
        $followPost = FollowPost::where('user_id',Auth::user()->id)->where('post_id',$post_id)->delete();

        // Notifiy all koments users 
        // sprawdzić czy dany user jest w tabeli z nieobserwowanymi postami

        $commentsUsers = array();
        foreach($post->comments as $comment) {
            if($comment->user_id != Auth::user()->id and $comment->user_id != $post->user_id)
                $commentsUsers[] = $comment->user_id;

        }
        $uniqueCommentsUsers = array_unique($commentsUsers);


        $notFollowingUsers = array();
        // zwraca liste userów którzy nie chcą otrzymywaćpowiadomięń
        $notFollowingUsersModel = FollowPost::where('post_id',$post->id)->get();

        foreach($notFollowingUsersModel as $user) {
            if($user->user_id != Auth::user()->id)
                $notFollowingUsers[] = $user->user_id;
        }

        $uniqueNotFollowingUsers = array_unique($notFollowingUsers);
        $usersToNotify = array_diff($uniqueCommentsUsers,$uniqueNotFollowingUsers);

        $notifyUsers = User::whereIn('id',array_unique($usersToNotify))->get();

        $info = array(
            'user_id' => Auth::user()->id,
            'login' => Auth::user()->login,
            'post_id' => $post_id,
            'game' => $post->game->name,
            'platform' => $post->platform->name
        );
        if(!empty($notifyUsers))
            Notification::send($notifyUsers, new CommentReply($info));
        
        //if(isset($data['type']) == 'single') {
            // broadcast to post id;
            broadcast(new \App\Events\newComment($data,$post_id,Auth::user()));
        //}

        return redirect()->back()->with('success','Dodano nowy komentarz.')->with('post_id',$data['post_id']);
    }
    public function editPost(Request $request) {
        $data = $request->all();

        $post = Post::find($data['post_id']);

        $post->text = $data['post-edit-'.$data['post_id']];

        $post->save();

        return redirect()->back()->with('success','Zapisano zmiany.')->with('post_id',$data['post_id']);
    }

    public function editComment(Request $request) {
        $data = $request->all();

        $comment = Comments::find($data['comment_id']);

        $comment->text = $data['post-'.$data['post_id'].'-comment-'.$data['comment_id']];
        
        $comment->save();

        return redirect()->back()->with('success','Zapisano zmiany.')->with('post_id',$data['post_id']);
    }

    public function deleteComment($comment_id) {

        Comments::find($comment_id)->delete();

        return redirect()->back()->with('success','Usunięto komentarz.');
    }

    public function deletePost($post_id) {

        $post = Post::find($post_id);
        
        
        if(count($post->comments) > 0) {
            foreach($post->comments as $comment) {
                Comments::find($comment->id)->delete();
            }
            $post->delete();
        } else {
            $post->delete();
        }
        
        return redirect()->back()->with('success','Usunięto post.');
    }

    public function unfollowPost($post_id) {
        
        $data = [
            'user_id' => Auth::user()->id,
            'post_id' => $post_id
        ];

        
        FollowPost::create($data);

        return redirect()->back()->with('success','Sukces!');
    }

    public function followPost($post_id) {

        $followPost = FollowPost::where('user_id',Auth::user()->id)->where('post_id',$post_id)->delete();
        
        
        return redirect()->back()->with('success','Sukces!');
        
    }
}

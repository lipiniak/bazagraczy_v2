<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\User;
use Messenger;
use Carbon\Carbon;
use Cache;

class PublicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }


    public function getUserInfo(Request $request) {
        $data = $request->all();

        if(empty($data['user_id']))
            return response()->json(['error' => 'No user ID']);
        
        
        $user = User::find($data['user_id']);
       
        if($user) {
            // w teorii można założyć że dany user jest online bądź dodać kolejny status? bądź dwa statusy online mobile

            $content = [
                'notifications' => count($user->unreadNotifications),
                'messages' => Messenger::countUnSeenByUserId($user->id)
            ];

            $expiresAt = Carbon::now()->addMinutes(5);
        
            $status = Cache::get('user-is-online-'.$user->id);
            
            if($status == 'away')
                Cache::put('user-is-online-' . $user->id, 'online', $expiresAt);
            else {
                Cache::put('user-is-online-' . $user->id, 'online', $expiresAt);
            
                // pobrać all online userów i wysłać im info ze jestem away.
                //Ci mnie followują
                if(Cache::has('online_users_list')) {
                    $onlineUsers = Cache::get('online_users_list');
                    
                    foreach($onlineUsers as $onlineUser) {
                        
                        broadcast(new \App\Events\FollowerLogin($onlineUser,$user->id));
                    }
                }
            }

            return response()->json($content);
        } 

        return response()->json(['error'=>'User not found']);
    }
}

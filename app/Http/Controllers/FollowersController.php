<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Followers;
use Auth;

use Quest;
class FollowersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function followerAdd($followerId) {

        $data = array(
            'user_id' => Auth::user()->id,
            'followers_id' => $followerId,
        );

        Followers::create($data);

        Quest::doneQuest('quest_follow');
        
        return redirect()->back()->with('success','Dodano do obserwowanych.');
    }

    public function followerRemove($followerId) {
 
        Followers::where('user_id', Auth::user()->id)->where('followers_id',$followerId)->delete();
        

        return redirect()->back()->with('success','Usunięto z obserwowanych.');
    }

}

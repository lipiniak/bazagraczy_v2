<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Validator;
use Mail;
class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function showPage(Request $request)
    {
        $key = $request->route()->getName();
        $page = Page::where('key_value',$key)->firstOrFail();

        return view('content',compact('page'));
    }

    public function showContact() {

        return view('contact');
    }

    public function validateContactForm($data) {
        return Validator::make($data,[
            'email' => 'required|max:50|email',
            'content' => 'required|max:15000',
            'g-recaptcha-response' => 'required|recaptcha',
        ],[
            'email.max' => 'Email za długi (max. 50 znaków)',
            'email.email' => 'Nieprawidłowy email',
            'email.required' => 'Uzupełnij pole',
            'content.required' => 'Uzupełnij pole',
            'content.max' => 'Wiadomość za długa (max. 15000 znaków)',
            'g-recaptcha-response:required' => 'Pole jest wymagane',
            'g-recaptcha-response:recaptcha' => 'Jesteś robotem?'
        ]);
    }

    public function sendForm(Request $request) {
        $data = $request->all();

        $validator = $this->validateContactForm($data);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with('error','Błąd. Popraw formularz.');
        }
        
        $content = $data['content'];
        $email = $data['email'];
        Mail::to('kontakt@s2g.gg')->send(new \App\Mail\Contact($content,$email));

        return redirect()->back()->with('success','Wiadomość wysłana.');
    }
}

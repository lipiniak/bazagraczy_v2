<?php

namespace App\Http\Controllers\Match;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Auth;
use App\FriendlyMatch;
use App\FriendlyMatchMembers;
use App\FriendlyMatchAplication;
use App\FriendlyMatchResult;
use App\Clan;
use App\User;
use App\Team;
use DB;
use Notification;
use App\Notifications\FriendlyMatchNewAplication;
use App\Notifications\FriendlyMatchAplicationAccepted;
use App\Notifications\FriendlyMatchAplicationRejected;
use App\Notifications\FriendlyMatchResultSend;
use App\Notifications\FriendlyMatchResultRejected;
use App\Notifications\FriendlyMatchEnded;
use App\Notifications\FriendlyMatchCancel;
use App\Notifications\FriendlyMatchWinner;


class FriendlyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    public function showFriendly() {

        $user = Auth::user();
        
        $selectedGame = $user->selectedGame();

        $data = array();

        $matches = FriendlyMatch::where('status',1)->where('game_id',$selectedGame['game_id'])->where('platform_id',$selectedGame['platform_id'])->get();

        $data['matches'] = $matches;
        
        return view('match.friendly.list',compact('data','selectedGame'));
    }

    public function doSearching(Request $request) {
        $formData = $request->all();

        $searchText = $formData['searchText'];


        $user = Auth::user();
        
        $selectedGame = $user->selectedGame();

        $data = array();
        
        
        //pregmatch date
        preg_match("/([0-9]{2}.[0-9]{2}.[0-9]{4})|([0-9]{2}.[0-9]{2})|([0-9]{2})/",$searchText,$data);

        $date = '';
        if(isset($data[0])) {
            $split = explode('.',$data[0]);

            $rewerse = array_reverse($split);

            $date = implode('-',$rewerse);
            $date = 'OR m.match_date LIKE "%'.$date.'%"';
        }
        
       
        $matches = DB::select('SELECT m.id, m.status FROM match_friendly m LEFT JOIN clans c ON m.host_clan = c.id LEFT JOIN teams t ON m.host_team = t.id
                                    WHERE m.match_time LIKE "%'.$searchText.'%"
                                    '.$date.' 
                                    OR m.id LIKE "%'.$searchText.'%"
                                    OR c.name LIKE "%'.$searchText.'%"
                                    OR c.tag LIKE "%'.$searchText.'%"
                                    OR t.name LIKE "%'.$searchText.'%"
                                    AND m.status = 1
                                    AND m.game_id = '.$selectedGame['game_id'].'
                                    AND m.platform_id = '.$selectedGame['platform_id'].'

                                ') ; 

        
        $list = array();
        foreach($matches as $m) 
        {   
            if($m->status == 1)
                $list[] = $m->id;
            
        }
        
        if(!empty($list))
            $matches = FriendlyMatch::find($list);
        else 
            $matches = collect();                    
                                    
        $data['matches'] = $matches;

        
        
                                
        
        return view('match.friendly.list',compact('data','selectedGame','formData'));
    }

    public function createFriendly() {

        $user = Auth::user();
        
        //czy ma wybrana gierke

        $userClans = $user->clans;
        $userTeams = $user->teams;
        
        $selectedGame = $user->selectedGame();



        // czy jest liderem klanu
        $isClanLeader = false;
        $hasGameTeam = false;
        $isTeamLeader = false;
        foreach($userClans as $clan) {
            if($clan->leader_id == $user->id)
                $isClanLeader = true;
            
            $clanTeams = $clan->teams;

            foreach($clanTeams as $cTeam) {
                if($cTeam->game_id == $selectedGame['game_id'] && $cTeam->platform_id == $selectedGame['platform_id'])
                    $hasGameTeam = true;

                if($cTeam->leader_id == $user->id && ($cTeam->game_id == $selectedGame['game_id'] && $cTeam->platform_id == $selectedGame['platform_id']))
                    $isTeamLeader = true;
            }

            

            if(!$hasGameTeam)
                $isClanLeader = false;
        }

        
        // czy jest kapitanem druzyny
    
        $noPermission = false;
        if(!$isTeamLeader && !$isClanLeader) {
            $noPermission = true;
        } else {
            if(!$hasGameTeam) 
                $noPermission = true;
        }



        //die();
        // jesli lider clanu to wyswietlaja sie klany z teamami ktore sa akceptowalne
        // jesli kapitan to sie wyswietlaja klany z druzynami gdize jest kapitanem
        $clans = array();
        foreach($userClans as $uClan) {
            if($user->id == $uClan->leader_id) {
                foreach($uClan->teams as $team) {
                    if($team->game_id == $selectedGame['game_id'] && $team->platform_id == $selectedGame['platform_id'])
                        $clans[$uClan->id] = $uClan;
                }
            } else {
                $cTeams = $uClan->teams;

                foreach($cTeams as $team) {
                    if($team->leader_id == $user->id && $team->game_id == $selectedGame['game_id'] && $team->platform_id == $selectedGame['platform_id'])
                        $clans[$uClan->id] = $uClan;
                }
            }
        }
        
        
        $noEdit = false;    

        return view('match.friendly.create',compact('userClans','noPermission','noEdit','selectedGame','clans'));
    }

    public function validateMatch($data) {
        return Validator::make($data,[
            'players_count' => 'required|numeric',
            'match_date' => 'required',
            'match_time' => 'required',
            'description' => 'max:2500',
            
        ],[
            'players_count.required' => 'Uzupełnij pole',
            'players_count.number' => 'To pole musi być liczbą',
            'match_date.required' => 'Uzupełnij pole',
            'match_time.required' => 'Uzupełnij pole',
            'description.max' => 'Wartość za długa (max. 2500 znaków)',
            
        ]);
    }

    public function saveFriendly(Request $request) {
    
        $formData = $request->all();

        $validator = $this->validateMatch($formData);
        


        if(isset($formData['clan_id'])) {
            $clan = Clan::find($formData['clan_id']);
            if(!$clan)
                $validator->after(function ($validator) {
                        
                    $validator->errors()->add('clan_id', 'Wybrany klan nie istnieje');
                    
                });
        }

        if(isset($formData['team_id'])) {
            $team = Team::find($formData['team_id']);
            if(!$team)
                $validator->after(function ($validator) {
                        
                    $validator->errors()->add('team_id', 'Wybrana drużyna nie istnieje');
                    
                });
        }

        if(isset($formData['team_id'])) {
        if($formData['team_id'] == 0) {
            $validator->after(function ($validator) {
                    
                $validator->errors()->add('team_id', 'Uzupełnij pole');
                
            });
        }
        } else {
            $validator->after(function ($validator) {
                    
                $validator->errors()->add('team_id', 'Uzupełnij pole');
                
            });
        }

        if(isset($formData['clan_id'])) {
            if($formData['clan_id'] == 0) {
                $validator->after(function ($validator) {
                        
                    $validator->errors()->add('clan_id', 'Uzupełnij pole');
                    
                });
            }
        } else {
            $validator->after(function ($validator) {
                        
                $validator->errors()->add('clan_id', 'Uzupełnij pole');
                
            });
        }
         

        $user = Auth::user();
        
        $userClans = $user->clans;
        $userTeams = $user->teams;
        
        $selectedGame = $user->selectedGame();

        // czy jest liderem klanu
        $isClanLeader = false;
        $hasGameTeam = false;
        $isTeamLeader = false;

        foreach($userClans as $clan) {
            if($clan->leader_id == $user->id)
                $isClanLeader = true;
            
            $clanTeams = $clan->teams;

            foreach($clanTeams as $cTeam) {
                if($cTeam->game_id == $selectedGame['game_id'] && $cTeam->platform_id == $selectedGame['platform_id'])
                    $hasGameTeam = true;

                if($cTeam->leader_id == $user->id && ($cTeam->game_id == $selectedGame['game_id'] && $cTeam->platform_id == $selectedGame['platform_id']))
                    $isTeamLeader = true;
            }
            if(!$hasGameTeam)
                $isClanLeader = false;
        }
        // return withErrors and Input, and selected clan and selected team and members ??? :P
        $noPermission = false;

        if(!$isTeamLeader && !$isClanLeader) {
            $noPermission = true;
        } else {
            if(!$hasGameTeam) 
                $noPermission = true;
        }
        
        

        if($noPermission)
            return redirect()->back();

            if(!isset($formData['members'])) {
                $validator->after(function ($validator) {
                        
                    $validator->errors()->add('members', 'Uzupełnij pole');
                    
                });
            } else {
                if(isset($formData['team_id']))
        if($formData['team_id'] > 0) {
            $selectedTeam = Team::find($formData['team_id']);

            if($team) {
                foreach($selectedTeam->members as $mates) {
                    $allMembers[] = $mates->id;
                }
                
                $membersChange = !array_diff($formData['members'], $allMembers);
        
                $errors = array();
                if(!$membersChange) {
                    
                    $validator->after(function ($validator) {
                        
                        $validator->errors()->add('members', 'Skład drużyny uległ zmianie');
                        
                    });
                    
                    
                }
            }
            
        }
            }

        
        
         
        if(!empty($formData['members'])) {
            if($formData['players_count'] > count($formData['members'])) { 
            
                $validator->after(function ($validator) {
                    
                    $validator->errors()->add('members', 'Niewystarczająca liczba graczy');
                    
                });
                
            }
        }
        
        $today = date('Y-m-d');

        if(strtotime($formData['match_date']) == strtotime($today)) {
            if(!empty($formData['match_time'])) {
                if(strtotime($formData['match_time']) < strtotime(date('Y-m-d H:i:s'))) {
                    $validator->after(function ($validator) {
                        
                        $validator->errors()->add('match_time', 'Nieprawidłowa data meczu');
                        
                    });
                }
            }
        }
        
        

        $clan_id = $formData['clan_id'];
        //team list and 
        $teams = array();
        if($clan_id == 0) {
            $teams[] = array(
                'id' => 0,
                'text' => '',
            );
        } else {
            $clan = Clan::find($clan_id);
            if($clan) {
                $teams[] = array(
                    'id' => 0,
                    'text' => '',
                );
                foreach($clan->teams as $team) {
                    if($clan->leader_id == $user->id) {
                        if($team->game_id == $selectedGame['game_id'] && $team->platform_id == $selectedGame['platform_id']) {
                            $teams[] = array(
                                'id' => $team->id,
                                'text' => $team->name,
                            );
                        }
                    } elseif($team->leader_id == $user->id) {
                        if($team->game_id == $selectedGame['game_id'] && $team->platform_id == $selectedGame['platform_id'] && $team->leader_id == $user->id) {
                            $teams[] = array(
                                'id' => $team->id,
                                'text' => $team->name,
                            );
                        }
                    }
                }
            }
            
        }
        $members = array();
        if(isset($formData['team_id'])) {
            $team_id = $formData['team_id'];
            if($team_id ==0) {
                $members[] = array(
                    'id' => 0,
                    'text' => '',
                );
            } else {
                $team = Team::find($team_id);
                if($team) {
                    $members[] = array(
                        'id' => 0,
                        'text' => '',
                    );
        
                    foreach($team->members as $member) {
                        $members[] = array(
                            'id'=> $member->id,
                            'text'=>$member->login,
                        );
                    }
                }
                
                
            }
        }
        

        if($validator->fails()) {
            return redirect('/mecz/towarzyski/stworz')
            ->withErrors($validator)
            //->wtihErrors($errors)
            ->with('teams',$teams)
            ->with('members',$members)
            ->with('error','Niepowodzenie!')
            ->withInput();
        }


        
        // czy jest kapitanem druzyny
        
        //var_dump($isClanLeader);
        //echo "---";
        //var_dump($isTeamLeader);
        //echo "---";
        //var_dump($hasGameTeam);
        
        
        $data['status'] = 1;
        $data['players_count'] = $formData['players_count'];
        $data['match_date'] = date('Y-m-d H:i:s',strtotime($formData['match_date']));
        $data['match_time'] = $formData['match_time'];
        $data['description'] = $formData['description'];
        $data['author_id'] = Auth::user()->id;
        $data['host_clan'] = $formData['clan_id'];
        $data['host_team'] = $formData['team_id'];
        $data['game_id'] = $formData['game_id'];
        $data['platform_id'] = $formData['platform_id'];
        $data['result'] = 0;

        $friendlyMatch = FriendlyMatch::create($data);

        if($formData['players_count'] <= count($formData['members'])) {
            
            $members = json_encode($formData['members']);

            $set = array(
                'match_id' => $friendlyMatch->id,
                'host_members' => $members
            );

            FriendlyMatchMembers::create($set);

        } else {
            return redirect()->back()->withErrors(['members', 'Niewystarczająca liczba graczy']);
        }
       
        return redirect()->route('match.friendly.list')->with('success','Klanówka została stowrzona.');
    }

    public function showMatch($match_id) {

        $match = FriendlyMatch::find($match_id);

        if (!$match) { 
            return redirect()->route('match.friendly.list')->with('error','Wybrany mecz jest już nieaktualny.');
        }


        $selectedGame = Auth::user()->selectedGame();
        $userClans = Auth::user()->clans;
        $user = Auth::user();

        $clans = array();
            foreach($userClans as $uClan) {
                if($user->id == $uClan->leader_id) {
                    foreach($uClan->teams as $team) {
                        if($team->game_id == $selectedGame['game_id'] && $team->platform_id == $selectedGame['platform_id'])
                            $clans[$uClan->id] = $uClan;
                    }
                } else {
                    $cTeams = $uClan->teams;
    
                    foreach($cTeams as $team) {
                        if($team->leader_id == $user->id && $team->game_id == $selectedGame['game_id'] && $team->platform_id == $selectedGame['platform_id'])
                            $clans[$uClan->id] = $uClan;
                    }
                }
            }

        return view('match.friendly.show',compact('match','selectedGame','clans'));
    }

    public function editMatch($match_id) {
        $user = Auth::user();
        $selectedGame = $user->selectedGame();
        
        // czas 5 minut dla edycji po 5 minutach reload strony

        $match = FriendlyMatch::find($match_id);
        
        if(!$match)
            return redirect()->route('match.friendly.list')->with('error','Niepowodzenie!');
        

        if(!$match->isHost())    
            return redirect()->back()->with('error','Niepowodzenie!');

        $noEdit = false;

        $matchAplications = FriendlyMatchAplication::where('match_id',$match->id)->get();

        if(($matchAplications->count() > 0) || $match->hidden_status == 2 && (strtotime($match->aplication_at) > strtotime(date('Y-m-d H:i:s')))) {
            $noEdit = true;
            // user clan list
            $userClans= Auth::user()->clans;
            // user team list 
            $clan_id = $match->host_clan;
            //team list and 
            if($clan_id == 0) {
                $teams[] = array(
                    'id' => 0,
                    'text' => '',
                );
            } else {
                $clan = Clan::find($clan_id);
                if($clan) {
                    
                    foreach($clan->teams as $team) {
                        if($team->game_id == $selectedGame['game_id'] && $team->platform_id == $selectedGame['platform_id']) {
                            $teams[] = array(
                                'id' => $team->id,
                                'text' => $team->name,
                            );
                        }
                    }
                }
                
            }
            // user team members list
            
            $team_id = $match->host_team;
                if($team_id ==0) {
                    
                } else {
                    $team = Team::find($team_id);
        
        
                    foreach($team->members as $member) {
                        $members[] = array(
                            'id'=> $member->id,
                            'text'=>$member->login,
                        );
                    }
                    
                }
                
            return view('match.friendly.edit',compact('match','teams','members','userClans','selectedGame','noEdit'));
        }
            
        

        
        // sprawdza czy czy osoba ktora kliknela jest autorem edycji 
        // jelsli druga osoba wejdzie to czeka te piec minut.
        if(strtotime($match->edit_at) > strtotime(date('Y-m-d H:i:s'))) {
            if(Auth::user()->id != $match->edit_user_id and $match->edit_user_id != null )
                return redirect()->back()->with('error','Edycja chwilowo niedostępna. Spróbuj za chwilę.');   
        } 
        
        
        // if current time is < then edit time 
        if($match->edit_at != null and Auth::user()->id != $match->edit_user_id)
        if(strtotime(date('Y-m-d H:i:s')) < strtotime($match->edit_at))
            return redirect()->back()->with('error','Nie możesz jeszcze edytować meczu, nie minął wymagany czas.');
        // hidden status 1 = edycja meczu

        $match->hidden_status = 1;
        $match->edit_user_id = Auth::user()->id;
        $match->edit_at = date('Y-m-d H:i:s', strtotime("+5 min"));
        $match->save();

        // user clan list
        $userClans= Auth::user()->clans;
        // user team list 
        $clan_id = $match->host_clan;
        //team list and 
        if($clan_id == 0) {
            $teams[] = array(
                'id' => 0,
                'text' => '',
            );
        } else {
            $clan = Clan::find($clan_id);
            if($clan) {
                
                foreach($clan->teams as $team) {
                    if($team->game_id == $selectedGame['game_id'] && $team->platform_id == $selectedGame['platform_id']) {
                        $teams[] = array(
                            'id' => $team->id,
                            'text' => $team->name,
                        );
                    }
                }
            }
            
        }
        // user team members list
        $team_id = $match->host_team;
            if($team_id ==0) {
                
            } else {
                $team = Team::find($team_id);
    
                
    
                foreach($team->members as $member) {
                    $members[] = array(
                        'id'=> $member->id,
                        'text'=>$member->login,
                    );
                }
                
            }
        
            $clans = array();
            foreach($userClans as $uClan) {
                if($user->id == $uClan->leader_id) {
                    foreach($uClan->teams as $team) {
                        if($team->game_id == $selectedGame['game_id'] && $team->platform_id == $selectedGame['platform_id'])
                            $clans[$uClan->id] = $uClan;
                    }
                } else {
                    $cTeams = $uClan->teams;
    
                    foreach($cTeams as $team) {
                        if($team->leader_id == $user->id && $team->game_id == $selectedGame['game_id'] && $team->platform_id == $selectedGame['platform_id'])
                            $clans[$uClan->id] = $uClan;
                    }
                }
            }

        return view('match.friendly.edit',compact('match','teams','members','userClans','selectedGame','noEdit','clans'));
    }

    public function updateMatch(Request $request) { 
        

        $user = Auth::user();
        
        $formData = $request->all();

        $match = FriendlyMatch::find($formData['match_id']);

        if(!$match)
            return redirect()->route('match.friendly.list')->with('error','Niepowodzenie!');


        if(!$match->isHost())    
            return redirect()->back()->with('error','Niepowodzenie!');
        

        $validator = $this->validateMatch($formData);

        if(isset($formData['clan_id'])) {
            $clan = Clan::find($formData['clan_id']);
            if(!$clan)
                $validator->after(function ($validator) {
                        
                    $validator->errors()->add('clan_id', 'Wybrany klan nie istnieje');
                    
                });
        }

        if(isset($formData['team_id'])) {
            $team = Team::find($formData['team_id']);
            if(!$team)
                $validator->after(function ($validator) {
                        
                    $validator->errors()->add('team_id', 'Wybrana drużyna nie istnieje');
                    
                });
        }

        $selectedGame = $user->selectedGame();
        // user clan list
        $userClans= Auth::user()->clans;
        // user team list 
        $clan_id = $match->host_clan;
        //team list and 
        $teams = array();
        if($clan_id == 0) {
            $teams[] = array(
                'id' => 0,
                'text' => '',
            );
        } else {
            $clan = Clan::find($clan_id);
            if($clan) {
                $teams[] = array(
                    'id' => 0,
                    'text' => '',
                );
                foreach($clan->teams as $team) {
                    if($team->game_id == $selectedGame['game_id'] && $team->platform_id == $selectedGame['platform_id']) {
                        $teams[] = array(
                            'id' => $team->id,
                            'text' => $team->name,
                        );
                    }
                }
            }
            
        }
        // user team members list
        $team_id = $match->host_team;
            if($team_id ==0) {
                $members[] = array(
                    'id' => 0,
                    'text' => '',
                );
            } else {
                $team = Team::find($team_id);

                if($team) {
                    $members[] = array(
                        'id' => 0,
                        'text' => '',
                    );
    
                    foreach($team->members as $member) {
                        $members[] = array(
                            'id'=> $member->id,
                            'text'=>$member->login,
                        );
                    }
                }
                
                
            }

        if(isset($formData['team_id'])) {
        if($formData['team_id'] == 0) {
            $validator->after(function ($validator) {
                    
                $validator->errors()->add('team_id', 'Uzupełnij pole');
                
            });
        }
        } else {
            $validator->after(function ($validator) {
                    
                $validator->errors()->add('team_id', 'Uzupełnij pole');
                
            });
        }

        if(isset($formData['clan_id'])) {
            if($formData['clan_id'] == 0) {
                $validator->after(function ($validator) {
                        
                    $validator->errors()->add('clan_id', 'Uzupełnij pole');
                    
                });
            }
        } else {
            $validator->after(function ($validator) {
                        
                $validator->errors()->add('clan_id', 'Uzupełnij pole');
                
            });
        }
         

        $user = Auth::user();
        
        $userClans = $user->clans;
        $userTeams = $user->teams;
        
        $selectedGame = $user->selectedGame();

        // czy jest liderem klanu
        $isClanLeader = false;
        $hasGameTeam = false;
        $isTeamLeader = false;

        foreach($userClans as $clan) {
            if($clan->leader_id == $user->id)
                $isClanLeader = true;
            
            $clanTeams = $clan->teams;

            foreach($clanTeams as $cTeam) {
                if($cTeam->game_id == $selectedGame['game_id'] && $cTeam->platform_id == $selectedGame['platform_id'])
                    $hasGameTeam = true;

                if($cTeam->leader_id == $user->id && ($cTeam->game_id == $selectedGame['game_id'] && $cTeam->platform_id == $selectedGame['platform_id']))
                    $isTeamLeader = true;
            }
            if(!$hasGameTeam)
                $isClanLeader = false;
        }
        // return withErrors and Input, and selected clan and selected team and members ??? :P
        $noPermission = false;
        if(!$isTeamLeader && !$isClanLeader) {
            $noPermission = true;
        } else {
            if(!$hasGameTeam) 
                $noPermission = true;
        }
        
        if($noPermission)
            return redirect()->back();

            if(!isset($formData['members'])) {
                $validator->after(function ($validator) {
                        
                    $validator->errors()->add('members', 'Uzupełnij pole');
                    
                });
            } else {
                if(isset($formData['team_id']))
                    if($formData['team_id'] > 0) {
                        $selectedTeam = Team::find($formData['team_id']);
                        if($selectedTeam) {
                            foreach($selectedTeam->members as $mates) {
                                $allMembers[] = $mates->id;
                            }
                            
                            $membersChange = !array_diff($formData['members'], $allMembers);
                    
                            $errors = array();
                            if(!$membersChange) {
                                
                                $validator->after(function ($validator) {
                                    
                                    $validator->errors()->add('members', 'Skład drużyny uległ zmianie');
                                    
                                });
                                
                                
                            }
                        }
                        
                    }
            }

        
        
         
        if(!empty($formData['members'])) {
            if($formData['players_count'] > count($formData['members'])) { 
            
                $validator->after(function ($validator) {
                    
                    $validator->errors()->add('members', 'Niewystarczająca liczba graczy');
                    
                });
                
            }
        }
        

        if($validator->fails()) {
            return redirect('/mecz/towarzyski/edytuj/'.$formData['match_id'])
            ->withErrors($validator)
            ->with('error','Niepowodzenie!')
            ->with('teams',$teams)
            ->with('members',$members)
            ->withInput();
        }


        $formData['match_date'] = date('Y-m-d H:i:s',strtotime($formData['match_date']));
        
        $match->update($formData);
        $match->host_clan = $formData['clan_id'];
        $match->host_team = $formData['team_id'];
        
        $membersOld = FriendlyMatchMembers::where('match_id',$formData['match_id'])->first();

    
        $members = json_encode($formData['members']);

        $membersOld->host_members = $members;
        $membersOld->save();
        
        $match->hidden_status = 0;
        $match->edit_at = NULL;
        $match->edit_user_id = NULL;
        $match->save();

        return redirect()->route('match.friendly.show',['id'=>$match->id])->with('success','Poprawnie zapisano zmiany.');
    }

    public function stopEdit($match_id) {

        $match = FriendlyMatch::find($match_id);

        if(!$match)
            return redirect()->route('match.friendly.list')->with('error','Niepowodzenie!');


        if(!$match->isHost())    
            return redirect()->back()->with('error','Niepowodzenie!');
        

        $match->hidden_status = 0;
        $match->edit_at = NULL;
        $match->edit_user_id = NULL;
        $match->save();

        return redirect()->route('match.friendly.show',['id'=>$match->id]);
    }

    public function joinMatch(Request $request) {

        $user = Auth::user();
        
        $userClans = $user->clans;
        $userTeams = $user->teams;
        
        $selectedGame = $user->selectedGame();

        // czy jest liderem klanu
        $isClanLeader = false;
        $hasGameTeam = false;
        $isTeamLeader = false;

        foreach($userClans as $clan) {
            if($clan->leader_id == $user->id)
                $isClanLeader = true;
            
            $clanTeams = $clan->teams;

            foreach($clanTeams as $cTeam) {
                if($cTeam->game_id == $selectedGame['game_id'] && $cTeam->platform_id == $selectedGame['platform_id'])
                    $hasGameTeam = true;

                if($cTeam->leader_id == $user->id && ($cTeam->game_id == $selectedGame['game_id'] && $cTeam->platform_id == $selectedGame['platform_id']))
                    $isTeamLeader = true;
            }
            if(!$hasGameTeam)
                $isClanLeader = false;
        }
        // return withErrors and Input, and selected clan and selected team and members ??? :P
        $noPermission = false;
        if(!$isTeamLeader && !$isClanLeader) {
            $noPermission = true;
        } else {
            if(!$hasGameTeam) 
                $noPermission = true;
        }
        
        if($noPermission)
            return redirect()->back();

        $formData = $request->all();

        $match = FriendlyMatch::find($formData['match_id']);

        if (!$match) { 
            return redirect()->route('match.friendly.list')->with('error','Wybrany mecz jest już nieaktualny.');
        }
        if($match->status > 1)
            return redirect()->route('match.friendly.list')->with('error','Niepowodzenie!');

        // status 2 = jest podsumowanie
        $validator = Validator::make($formData,['members'=>'required'],['members.required'=>'Uzupełnij pole']);

        if(isset($formData['team_id'])) {
            if($formData['team_id'] == 0) {
                $validator->after(function ($validator) {
                        
                    $validator->errors()->add('team_id', 'Uzupełnij pole');
                    
                });
            }
            } else {
                $validator->after(function ($validator) {
                        
                    $validator->errors()->add('team_id', 'Uzupełnij pole');
                    
                });
            }
    
            if(isset($formData['clan_id'])) {
                if($formData['clan_id'] == 0) {
                    $validator->after(function ($validator) {
                            
                        $validator->errors()->add('clan_id', 'Uzupełnij pole');
                        
                    });
                }
            } else {
                $validator->after(function ($validator) {
                            
                    $validator->errors()->add('clan_id', 'Uzupełnij pole');
                    
                });
            }
        
        if($match->hidden_status == 1 && strtotime($match->edit_at) > strtotime(date('Y-m-d H:i:s')) ) {
            
            $validator->after(function ($validator) {
                            
                $validator->errors()->add('members', 'Nie można dołączyć do meczu ponieważ Gospodarz jest w trakcie jego edycji. Spróbuj za chwilę.');
                
            });
        } 
        
        if(!empty($formData['members'])) {
            if($match->players_count > count($formData['members'])) { 
            
                $validator->after(function ($validator) {
                    
                    $validator->errors()->add('members', 'Niewystarczająca liczba graczy');
                    
                });
                
            }
        }
        
        
        

        $clan_id = $formData['clan_id'];
        //team list and 
        if($clan_id == 0) {
            $teams[] = array(
                'id' => 0,
                'text' => '',
            );
        } else {
            $clan = Clan::find($clan_id);
            $teams[] = array(
                'id' => 0,
                'text' => '',
            );
            foreach($clan->teams as $team) {
                if($team->game_id == $selectedGame['game_id'] && $team->platform_id == $selectedGame['platform_id']) {
                    $teams[] = array(
                        'id' => $team->id,
                        'text' => $team->name,
                    );
                }
            }
        }
        $members = array();
        if(isset($formData['team_id'])) {
            $team_id = $formData['team_id'];
            if($team_id ==0) {
                $members[] = array(
                    'id' => 0,
                    'text' => '',
                );
            } else {
                $team = Team::find($team_id);
    
                $members[] = array(
                    'id' => 0,
                    'text' => '',
                );
    
                foreach($team->members as $member) {
                    $members[] = array(
                        'id'=> $member->id,
                        'text'=>$member->login,
                    );
                }
                
            }
        }
        if(!empty($formData['members'])) {
            $matchMembers = FriendlyMatchMembers::where('match_id',$formData['match_id'])->first();

            if($matchMembers) {
                foreach($formData['members'] as $member) {
                    if(in_array($member,json_decode($matchMembers->host_members))) {
                        $validator->after(function ($validator) {
                                
                            $validator->errors()->add('members', 'Jeden z graczy gra w przeciwnej drużynie.');
                            
                        });
                        break;   
                    }
                }
            }
            
        }

        if($validator->fails()) {
            return redirect()->route('match.friendly.show',['id'=>$match->id])
            ->withErrors($validator)
            //->wtihErrors($errors)
            ->with('teams',$teams)
            ->with('members',$members)
            ->with('userClans',$userClans)
            ->withInput();
        }


        
        $aplicationTime = strtotime(date('Y-m-d H:i:s',strtotime('+2 minutes')));


        $match->hidden_status = 2;
        $match->aplication_at = date('Y-m-d H:i:s',$aplicationTime);
        $match->edit_user_id = NULL;
        $match->save();

        


        $enemyClan = Clan::find($formData['clan_id']);
        $enemyTeam = Team::find($formData['team_id']);
        $enemyMembers = User::whereIn('id',$formData['members'])->get();


        $enemy['clan'] = $enemyClan;
        $enemy['team'] = $enemyTeam;
        $enemy['members'] = $enemyMembers;
        $members = json_encode($formData['members']);

        return view('match.friendly.summary',compact('match','enemy','members'));
        

    }

    public function applyMatch(Request $request) {
        
        $data = $request->all();
        $match = FriendlyMatch::find($data['match_id']);


        if (!$match) { 
            return redirect()->route('match.friendly.list')->with('error','Wybrany mecz jest już nieaktualny.');
        }
        if($match->status > 1)
            return redirect()->route('match.friendly.list')->with('error','Niepowodzenie!');

        $set = array(
            'match_id' => $data['match_id'],
            'clan_id' => $data['clan_id'],
            'team_id' => $data['team_id'],
            'author_id' => Auth::user()->id,
            'members' => $data['members']
        );

        // Need action marker;
        $match->host_need_action = 1;
        $match->save();

        $haveAplication = FriendlyMatchAplication::where('clan_id',$data['clan_id'])->where('team_id',$data['team_id'])->where('match_id',$data['match_id'])->first();
        
        if($haveAplication) 
           return redirect()->route('match.friendly.show',['id'=>$match->id])->with('error','Niepowodzenie!');

        FriendlyMatchAplication::create($set);
        
        $aplicant = Clan::find($data['clan_id']);

        $hostClan = Clan::find($match->host_clan);
        $hostTeam = Team::find($match->host_team);

        $info = [
            'match_id' => $match->id,
            'aplicant_link' => $aplicant->link,
            'aplicant_id' => $aplicant->id,
            'aplicant_name' => $aplicant->name,
        ];

        if($hostClan->leader_id == $hostTeam->leader_id)
            $isCouple = true;
        else 
            $isCouple = false;

        if($isCouple) {
            if($hostClan && $hostTeam) {
                $user = User::find($hostClan->leader_id);
                if($user) {
                    $user->notify(new FriendlyMatchNewAplication($info));

                    broadcast(new \App\Events\MatchHostAction($user->id));
                    
                }
            }
        } else {
            if($hostClan) {
                $clanLeader = User::find($hostClan->leader_id);
                if($clanLeader) 
                    $clanLeader->notify(new FriendlyMatchNewAplication($info));    
                    broadcast(new \App\Events\MatchHostAction($clanLeader->id));
            } 
    
            if($hostTeam) {
                $teamLeader = User::find($hostTeam->leader_id);
    
                if($teamLeader) 
                    $teamLeader->notify(new FriendlyMatchNewAplication($info));
                    broadcast(new \App\Events\MatchHostAction($teamLeader->id));
            }
        }
        
        
        return redirect()->route('match.friendly.show',['id'=>$data['match_id']])->with('success','Prośba o dołączenie do meczu została wysłana.');
    }

    public function acceptAplication($aplication_id) {
        $aplication = FriendlyMatchAplication::find($aplication_id);

        if(!$aplication) {
            return redirect()->back()->with('error','Niepowodzenie!');
        }

        $match = FriendlyMatch::find($aplication->match_id);

        if (!$match) { 
            return redirect()->route('match.friendly.list')->with('error','Niepowodzenie!');
        }
        if($match->status > 1)
            return redirect()->back()->with('error','Niepowodzenie!');

        $set = array(
            'status' => 2,
            'enemy_clan' => $aplication->clan_id,
            'enemy_team' => $aplication->team_id,
        );

        $match->status = 2;
        $match->enemy_clan = $aplication->clan_id;
        $match->enemy_team = $aplication->team_id;
        $match->host_need_action = 0;
        $match->enemy_need_action = 0;
        $match->expire_at = date('Y-m-d H:i:s', strtotime("$match->match_date $match->match_time +3 hours"));
        $match->save();

        $matchMembers = FriendlyMatchMembers::where('match_id',$match->id)->first();

        $matchMembers->enemy_members = $aplication->members;
        $matchMembers->save();

        $aplication->delete();

        $matchAplications = FriendlyMatchAplication::where('match_id',$match->id)->get();

        // usuwamy zbędne aplikacje.
        if($matchAplications)
            foreach($matchAplications as $aplication)
                $aplication->delete();

        $aplicantClan = Clan::find($match->enemy_clan);
        $aplicantTeam = Team::find($match->enemy_team);
        

        $info = [
            'match_id' => $aplication->match_id,
            'host_clan_name' => $match->hostClan->name,
            'host_clan_link' => $match->hostClan->link,
        ];
        

        if($aplicantClan->leader_id == $aplicantTeam->leader_id)
            $isCouple = true;
        else 
            $isCouple = false;

        if($isCouple) {
            // send one 
            if($aplicantClan && $aplicantTeam)  {
                $user = User::find($aplicantClan->leader_id);
                if($user)
                    $user->notify(new FriendlyMatchAplicationAccepted($info));
                    
            }
                
              
        } else {
            if($aplicantClan)  {
                $clanLeader = User::find($aplicantClan->leader_id);
                if($clanLeader)
                    $clanLeader->notify(new FriendlyMatchAplicationAccepted($info));
            }
                
            if($aplicantTeam)  {
                $teamLeader = User::find($aplicantTeam->leader_id);
                if($teamLeader)
                    $teamLeader->notify(new FriendlyMatchAplicationAccepted($info));
            }
        }
        

        return redirect()->back()->with('success','Sukces!');

    }

    public function aplicationCancel($aplication_id,$match_id) {

        $aplication = FriendlyMatchAplication::find($aplication_id);
        $match = FriendlyMatch::find($match_id);

        if(!$match)
            return redirect()->route('match.friendly.list')->with('error','Niepowodzenie!');

        if(!$aplication && $match->status > 1) {
            return redirect()->back()->with('error','Niepowodzenie!');
        } elseif(!$aplication) {
            return redirect()->back()->with('success','Sukces!');
        }
            

        $aplicantClan = Clan::find($aplication->clan_id);
        $aplicantTeam = Team::find($aplication->team_id);

        $aplicationCount = FriendlyMatchAplication::where('match_id',$match->id)->count();
        
        $info = [
            'match_id' => $aplication->match_id,
            'host_clan_name' => $match->hostClan->name,
            'host_clan_link' => $match->hostClan->link,
        ];

        if(!$aplication) {
            return redirect()->back()->with('success','Sukces!');
        } else {
            $aplication->delete();

            if($aplicationCount == 1) {
                $match->host_need_action = 0;
                broadcast(new \App\Events\MatchActionRemove($match->hostClan->leader_id));
                broadcast(new \App\Events\MatchActionRemove($match->hostTeam->leader_id));
                $match->save();
            } 

            if($aplicantClan->leader_id == $aplicantTeam->leader_id)
                $isCouple = true;
            else 
                $isCouple = false;
            if($isCouple) {
                // send one 
                if($aplicantClan && $aplicantTeam)  {
                    $user = User::find($aplicantClan->leader_id);
                    if($user)
                        $user->notify(new FriendlyMatchAplicationRejected($info));
                }
                    
                
            } else {
                if($aplicantClan)  {
                    $clanLeader = User::find($aplicantClan->leader_id);
                    if($clanLeader)
                        $clanLeader->notify(new FriendlyMatchAplicationRejected($info));
                }
                    
                if($aplicantTeam)  {
                    $teamLeader = User::find($aplicantTeam->leader_id);
                    if($teamLeader)
                        $teamLeader->notify(new FriendlyMatchAplicationRejected($info));
                }
            }

        }

        return redirect()->back()->with('success','Sukces!');
    }

    public function deleteMatch($match_id) {
        // usuwanie permanente meczu

        $match = FriendlyMatch::find($match_id);

        $matchAplications = FriendlyMatchAplication::where('match_id',$match_id)->get();
        $matchMembers = FriendlyMatchMembers::where('match_id',$match_id)->first();
        
        if(!$match)
            return redirect()->route('match.friendly.list')->with('success','Mecz został usunięty.');

        if($match->status > 1) 
            return redirect()->route('match.friendly.list')->with('error','Niepowodzenie!');

        if($match->isHost()) {
            $match->delete();
            if($matchAplications->count() > 0)
                foreach($matchAplications as $app) 
                    $app->delete();
                
            if($matchMembers->count() > 0)
                $matchMembers->delete();

            return redirect()->route('match.friendly.list')->with('success','Mecz został usunięty.');
        } else {
            return redirect()->back()->with('error','Nie masz uprawnień do usunięcia tego meczu.');
        }
        
    }

    public function showActive() {

        $user = Auth::user();
        $type = 2;
        // sprawdz czy sa mecze dla klanow usera 

        $userClans = array();

        foreach($user->clans as $clan) {
            if($user->id == $clan->leader_id)
                $userClans[] = $clan->id;
        }

        $userTeams = array();
        
        foreach($user->teams as $team) {
            if($team->leader_id == $user->id)
                $userTeams[] = $team->id;
        }
        
        $matchMembers = FriendlyMatchMembers::whereRaw('enemy_members LIKE "%\"'.$user->id.'\"%" OR host_members LIKE "%\"'.$user->id.'\"%"')->get();
        
        $userMembers = array();
        foreach($matchMembers as $m) {
            $userMembers[] = $m->match_id;
        }

        
        $matchesClanLeader = FriendlyMatch::whereIn('host_clan',$userClans)->where('status',2)->get();

        $matchesTeamLeader = FriendlyMatch::whereIn('host_team',$userTeams)->where('status',2)->get();
        
        $matchesMember = FriendlyMatch::whereIn('id',$userMembers)->where('status',2)->get();
        
        $matchesEnemyClanLeader = FriendlyMatch::whereIn('enemy_clan',$userClans)->where('status',2)->get();
        $matchesEnemyTeamLeader = FriendlyMatch::whereIn('enemy_team',$userTeams)->where('status',2)->get();
        
        $matches = $matchesClanLeader->merge($matchesTeamLeader);
        $matches = $matches->merge($matchesEnemyClanLeader);
        $matches = $matches->merge($matchesMember);
        $matches = $matches->merge($matchesEnemyTeamLeader)->sortBy('match_time')->sortBy('match_date');
        
        $matchesCount = $this->countMatches();

        return view('match.friendly.my-list', compact('matches','type','matchesCount'));
    }

    public function countMatches() {


        $user = Auth::user();
        $type = 2;
        // sprawdz czy sa mecze dla klanow usera 

        $userClans = array();

        foreach($user->clans as $clan) {
            if($user->id == $clan->leader_id)
                $userClans[] = $clan->id;
        }

        $userTeams = array();
        
        foreach($user->teams as $team) {
            if($team->leader_id == $user->id)
                $userTeams[] = $team->id;
        }
        
        $matchMembers = FriendlyMatchMembers::whereRaw('enemy_members LIKE "%\"'.$user->id.'\"%" OR host_members LIKE "%\"'.$user->id.'\"%"')->get();
        
        $userMembers = array();
        foreach($matchMembers as $m) {
            $userMembers[] = $m->match_id;
        }
        
        $matchesClanLeader = FriendlyMatch::whereIn('host_clan',$userClans)->where('status',2)->get();

        $matchesTeamLeader = FriendlyMatch::whereIn('host_team',$userTeams)->where('status',2)->get();
        $matchesMember = FriendlyMatch::whereIn('id',$userMembers)->where('status',2)->get();
        
        $matchesEnemyClanLeader = FriendlyMatch::whereIn('enemy_clan',$userClans)->where('status',2)->get();
        $matchesEnemyTeamLeader = FriendlyMatch::whereIn('enemy_team',$userTeams)->where('status',2)->get();
        
        $matches = $matchesClanLeader->merge($matchesTeamLeader);
        $matches = $matches->merge($matchesEnemyClanLeader);
        $matches = $matches->merge($matchesMember);
        $matches = $matches->merge($matchesEnemyTeamLeader)->sortByDesc('match_time')->sortByDesc('match_date');

        $activeMatchesCount = $matches->count();

        $matchesClanLeader = FriendlyMatch::whereIn('host_clan',$userClans)->where('status',1)->get();

        $matchesTeamLeader = FriendlyMatch::whereIn('host_team',$userTeams)->where('status',1)->get();
        
        $matchesMember = FriendlyMatch::whereIn('id',$userMembers)->where('status',1)->get();
        $matchesEnemyClanLeader = FriendlyMatch::whereIn('enemy_clan',$userClans)->where('status',1)->get();
        $matchesEnemyTeamLeader = FriendlyMatch::whereIn('enemy_team',$userTeams)->where('status',1)->get();
        
        $matches = $matchesClanLeader->merge($matchesTeamLeader);
        $matches = $matches->merge($matchesEnemyClanLeader);
        $matches = $matches->merge($matchesMember);
        $matches = $matches->merge($matchesEnemyTeamLeader)->sortByDesc('match_time')->sortByDesc('match_date');

        $openMatchesCount = $matches->count();

        $matchesClanLeader = FriendlyMatch::whereIn('host_clan',$userClans)->where('status',3)->get();

        $matchesTeamLeader = FriendlyMatch::whereIn('host_team',$userTeams)->where('status',3)->get();
        
        $matchesMember = FriendlyMatch::whereIn('id',$userMembers)->where('status',3)->get();

        $matchesEnemyClanLeader = FriendlyMatch::whereIn('enemy_clan',$userClans)->where('status',3)->get();
        $matchesEnemyTeamLeader = FriendlyMatch::whereIn('enemy_team',$userTeams)->where('status',3)->get();
        
        $matches = $matchesClanLeader->merge($matchesTeamLeader);
        $matches = $matches->merge($matchesEnemyClanLeader);
        $matches = $matches->merge($matchesMember);
        $matches = $matches->merge($matchesEnemyTeamLeader)->sortByDesc('match_time')->sortByDesc('match_date');

        $closedMatchesCount = $matches->count();

        return [$openMatchesCount,$activeMatchesCount,$closedMatchesCount];
    }

    public function showOpen() {

        $user = Auth::user();
        $type = 1;
        // sprawdz czy sa mecze dla klanow usera 

        $userClans = array();

        foreach($user->clans as $clan) {
            if($user->id == $clan->leader_id)
                $userClans[] = $clan->id;
        }

        $userTeams = array();
        
        foreach($user->teams as $team) {
            if($team->leader_id == $user->id)
                $userTeams[] = $team->id;
        }

        $matchMembers = FriendlyMatchMembers::whereRaw('enemy_members LIKE "%\"'.$user->id.'\"%" OR host_members LIKE "%\"'.$user->id.'\"%"')->get();
        
        $userMembers = array();
        foreach($matchMembers as $m) {
            $userMembers[] = $m->match_id;
        }
        
        $matchesClanLeader = FriendlyMatch::whereIn('host_clan',$userClans)->where('status',1)->get();

        $matchesTeamLeader = FriendlyMatch::whereIn('host_team',$userTeams)->where('status',1)->get();
        
        $matchesMember = FriendlyMatch::whereIn('id',$userMembers)->where('status',1)->get();

        $matchesEnemyClanLeader = FriendlyMatch::whereIn('enemy_clan',$userClans)->where('status',1)->get();
        $matchesEnemyTeamLeader = FriendlyMatch::whereIn('enemy_team',$userTeams)->where('status',1)->get();
        
        $matches = $matchesClanLeader->merge($matchesTeamLeader);
        $matches = $matches->merge($matchesEnemyClanLeader);
        $matches = $matches->merge($matchesMember);
        $matches = $matches->merge($matchesEnemyTeamLeader)->sortBy('match_time')->sortBy('match_date');

        $matchesCount = $this->countMatches();

        return view('match.friendly.my-list', compact('matches','type','matchesCount'));
    }

    public function showClosed() {

        $user = Auth::user();

        // sprawdz czy sa mecze dla klanow usera 
        $type = 3;
        $userClans = array();

        foreach($user->clans as $clan) {
            if($user->id == $clan->leader_id)
                $userClans[] = $clan->id;
        }

        $userTeams = array();
        
        foreach($user->teams as $team) {
            if($team->leader_id == $user->id)
                $userTeams[] = $team->id;
        }

        $matchMembers = FriendlyMatchMembers::whereRaw('enemy_members LIKE "%\"'.$user->id.'\"%" OR host_members LIKE "%\"'.$user->id.'\"%"')->get();
        
        $userMembers = array();
        foreach($matchMembers as $m) {
            $userMembers[] = $m->match_id;
        }


        $matchesClanLeader = FriendlyMatch::whereIn('host_clan',$userClans)->where('status',3)->get();

        $matchesTeamLeader = FriendlyMatch::whereIn('host_team',$userTeams)->where('status',3)->get();
        
        $matchesMember = FriendlyMatch::whereIn('id',$userMembers)->where('status',3)->get();

        $matchesEnemyClanLeader = FriendlyMatch::whereIn('enemy_clan',$userClans)->where('status',3)->get();
        $matchesEnemyTeamLeader = FriendlyMatch::whereIn('enemy_team',$userTeams)->where('status',3)->get();
        
        $matches = $matchesClanLeader->merge($matchesTeamLeader);
        $matches = $matches->merge($matchesEnemyClanLeader);
        $matches = $matches->merge($matchesMember);
        $matches = $matches->merge($matchesEnemyTeamLeader)->sortBy('match_time')->sortBy('match_date');

        $matchesCount = $this->countMatches();

        return view('match.friendly.my-list', compact('matches','type','matchesCount'));
    }

    public function sendResult(Request $request) {
        $data = $request->all();

        if($data['result'] == 0) { 
            return redirect()->back()->withErrors(['result'=>'Uzupełnij pole'])->with('showModal','true');
        }
            
        
        $match = FriendlyMatch::find($data['match_id']);

        $matchResult = FriendlyMatchResult::where(['match_id'=> $match->id])->firstOrCreate(['match_id'=>$match->id]);

        if($match->status == 3) {
            return redirect()->back()->with('error','Niepowodzenie!');
        }

        if($match->result > 0)
            return redirect()->back()->with('error','Niepowodzenie!');

        if($match->isHost() && $matchResult->enemy_aproved) 
            return redirect()->back()->with('error','Niepowodzenie!');
        
        if($match->isEnemy() && $matchResult->host_aproved) 
            return redirect()->back()->with('error','Niepowodzenie!');
        
        if($match->isEnemy() && $matchResult->enemy_aproved)
            return redirect()->back()->with('error','Niepowodzenie. Wynik został już przesłany.');

        if($match->isHost() && $matchResult->host_aproved)
            return redirect()->back()->with('error','Niepowodzenie. Wynik został już przesłany.');
        /* odrzucenie pierwszej propozycji nie zamyka meczu tylko dodaje czas jesli ma aktywną próbę.*/
        $notify = false;

        switch($data['result']) {
            case 1: // Host
                if($match->isHost()) {
                    $matchResult->host_result = 1;
                    $matchResult->host_aproved = 1;
                    $match->enemy_need_action = 1;
                    $side = 2;
                    $notify = true;
                }
                if($match->isEnemy()) {
                    // niby dajemy result na hosta
                    // winner host
                    $match->status = 3;
                    $match->result = 1;
                    $match->winner_id = $match->host_clan;
                    $match->enemy_need_action = 0;
                    $match->host_need_action = 0;

                    $info = [
                        'match_id' => $match->id,
                        'host_clan_id' => $match->hostClan->id,
                        'host_clan_link' => $match->hostClan->link,
                        'host_clan_name' => $match->hostClan->name,
                        'enemy_clan_id' => $match->enemyClan->id,
                        'enemy_clan_link' => $match->enemyClan->link,
                        'enemy_clan_name' => $match->enemyClan->name,
                        'winner_clan_link' => $match->winnerClan->link,
                        'winner_clan_name' => $match->winnerClan->name,
                    ];
                    // członkowie meczu wszyscy
                    $hostLeader = User::find($match->hostClan->leader_id);
                    $hostTeamLeader = User::find($match->hostTeam->leader_id);
        
                    $enemyLeader = User::find($match->enemyClan->leader_id);
                    $enemyTeamLeader = User::find($match->enemyTeam->leader_id);
                    
                    if($hostLeader->id == $hostTeamLeader->id) 
                        $hostLeader->notify(new FriendlyMatchWinner($info));
                    else {
                        $hostLeader->notify(new FriendlyMatchWinner($info));
                        $hostTeamLeader->notify(new FriendlyMatchWinner($info));
                    }
                    if($enemyLeader->id == $enemyTeamLeader->id) {
                        $enemyLeader->notify(new FriendlyMatchWinner($info));
                        
                    } else {
                        $enemyLeader->notify(new FriendlyMatchWinner($info));
                        $enemyTeamLeader->notify(new FriendlyMatchWinner($info));    
                    }
                    
                    $participants = $match->hostMembers();
                    
                    $participants = $participants->merge($match->enemyMembers());

                    $users = array();
                    foreach($participants as $member) {
                        if($member->id != $hostLeader->id && $member->id != $hostTeamLeader->id && $member->id != $enemyLeader->id && $member->id != $enemyTeamLeader->id)
                            $users[] = $member->id;
                    }
                    
                    $participants = User::find($users);
                    
                    Notification::send($participants, new FriendlyMatchWinner($info));

                    $match->save();
                    $matchResult->delete();
                    // powiadomienie o zakonczeniu meczu.
                    return redirect()->back()->with('success','Sukces!');
                }
                break;
            case 2: // Enemy
                if($match->isHost()) {
                        // result na enemy 
                        //winner enemy
                    $match->status = 3;
                    $match->result = 1;

                    $match->winner_id = $match->enemy_clan;
                    
                    $match->enemy_need_action = 0;
                    $match->host_need_action = 0;

                    $info = [
                        'match_id' => $match->id,
                        'host_clan_id' => $match->hostClan->id,
                        'host_clan_link' => $match->hostClan->link,
                        'host_clan_name' => $match->hostClan->name,
                        'enemy_clan_id' => $match->enemyClan->id,
                        'enemy_clan_link' => $match->enemyClan->link,
                        'enemy_clan_name' => $match->enemyClan->name,
                        'winner_clan_link' => $match->winnerClan->link,
                        'winner_clan_name' => $match->winnerClan->name,
                    ];
                    // członkowie meczu wszyscy
                    $hostLeader = User::find($match->hostClan->leader_id);
                    $hostTeamLeader = User::find($match->hostTeam->leader_id);
        
                    $enemyLeader = User::find($match->enemyClan->leader_id);
                    $enemyTeamLeader = User::find($match->enemyTeam->leader_id);
                    
                    if($hostLeader->id == $hostTeamLeader->id) 
                        $hostLeader->notify(new FriendlyMatchWinner($info));
                    else {
                        $hostLeader->notify(new FriendlyMatchWinner($info));
                        $hostTeamLeader->notify(new FriendlyMatchWinner($info));
                    }
                    if($enemyLeader->id == $enemyTeamLeader->id) {
                        $enemyLeader->notify(new FriendlyMatchWinner($info));
                        
                    } else {
                        $enemyLeader->notify(new FriendlyMatchWinner($info));
                        $enemyTeamLeader->notify(new FriendlyMatchWinner($info));    
                    }
                    
                    $participants = $match->hostMembers();
                    
                    $participants = $participants->merge($match->enemyMembers());
        
                    $users = array();
                    foreach($participants as $member) {
                        if($member->id != $hostLeader->id && $member->id != $hostTeamLeader->id && $member->id != $enemyLeader->id && $member->id != $enemyTeamLeader->id)
                            $users[] = $member->id;
                    }
                    
                    $participants = User::find($users);

                    Notification::send($participants, new FriendlyMatchWinner($info));

                    $match->save();
                    $matchResult->delete();
                    return redirect()->back()->with('success','Sukces!');
                }
                if($match->isEnemy()) {
                    $matchResult->enemy_result = 2;
                    $matchResult->enemy_aproved = 1;
                    $match->host_need_action = 1;
                    $side = 1;
                    $notify = true;
                }
                break;
            case 3:
                if($match->isHost()) {
                    $matchResult->host_result = 3;
                    $matchResult->host_aproved = 1;
                    
                    $match->enemy_need_action = 1;

                    $side = 2;
                }
                if($match->isEnemy()) {
                    $matchResult->enemy_result = 3;
                    $matchResult->enemy_aproved = 1;
                    $match->host_need_action = 1;
                    $side = 1;
                }
                $notify = true;
                break;
            case 4:
                if($match->isHost()) {
                    $matchResult->host_result = 4;
                    $matchResult->host_aproved = 1;
                    $match->enemy_need_action = 1;
                    $side = 2;
                }
                if($match->isEnemy()) {
                    $matchResult->enemy_result = 4;
                    $matchResult->enemy_aproved = 1;
                    $match->host_need_action = 1;
                    $side = 1;
                }
                $notify = true;
                break;
        }
        $match->save();
        $matchResult->expire_at = date('Y-m-d H:i:s',strtotime("+30 minutes"));
        $matchResult->save();

        if($match->isHost()) {
            $match->host_result_count -= 1;
        } elseif($match->isEnemy()) {
            $match->enemy_result_count -= 1;
        }

        if($notify) {
            // powiadom konkretną stronę .... fuk.
            
            switch($side) {
                case 1: 
                    // host
                    $match->host_need_action = 1;
                    $match->save();

                    $info = [
                        'match_id' => $match->id,
                        'aplicant_id' => $match->enemyClan->id,
                        'aplicant_link' => $match->enemyClan->link,
                        'aplicant_name' => $match->enemyClan->name,
                    ];

                    $hostClan = Clan::find($match->host_clan);
                    $hostTeam = Team::find($match->host_team);

                    if($hostClan->leader_id == $hostTeam->leader_id) {
                        $isCouple = true;
                    } else 
                        $isCouple = false;

                    if($isCouple) {
                        if($hostClan && $hostTeam) {
                            $user = User::find($hostClan->leader_id);
                            if($user)
                                $user->notify(new FriendlyMatchResultSend($info));
                                broadcast(new \App\Events\MatchHostAction($user->id));
                        }
                    } else {
                        if($hostClan) {
                            $clanLeader = User::find($hostClan->leader_id);
                            if($clanLeader) 
                                $clanLeader->notify(new FriendlyMatchResultSend($info));
                                broadcast(new \App\Events\MatchHostAction($clanLeader->id));
                        }
                        if($hostTeam) {
                            $teamLeader = User::find($hostTeam->leader_id);
                            if($teamLeader) 
                                $teamLeader->notify(new FriendlyMatchResultSend($info));
                                broadcast(new \App\Events\MatchHostAction($teamLeader->id));
                        }
                    }
                break;
                case 2:
                    // endmy

                    $info = [
                        'match_id' => $match->id,
                        'aplicant_id' => $match->hostClan->id,
                        'aplicant_link' => $match->hostClan->link,
                        'aplicant_name' => $match->hostClan->name,
                    ];

                    $match->enemy_need_action = 1;
                    $match->save();


                    $hostClan = Clan::find($match->enemy_clan);
                    $hostTeam = Team::find($match->enemy_team);

                    if($hostClan->leader_id == $hostTeam->leader_id) {
                        $isCouple = true;
                    } else 
                        $isCouple = false;

                    if($isCouple) {
                        if($hostClan && $hostTeam) {
                            $user = User::find($hostClan->leader_id);
                            if($user)
                                $user->notify(new FriendlyMatchResultSend($info));
                                broadcast(new \App\Events\MatchHostAction($user->id));
                        }
                    } else {
                        if($hostClan) {
                            $clanLeader = User::find($hostClan->leader_id);
                            if($clanLeader) 
                                $clanLeader->notify(new FriendlyMatchResultSend($info));
                                broadcast(new \App\Events\MatchHostAction($clanLeader->id));
                        }
                        if($hostTeam) {
                            $teamLeader = User::find($hostTeam->leader_id);
                            if($teamLeader) 
                                $teamLeader->notify(new FriendlyMatchResultSend($info));
                                broadcast(new \App\Events\MatchHostAction($teamLeader->id));
                        }
                    }
                break;
            }
            
        }
        return redirect()->back()->with('success','Wynik został przesłany.');

    }

    public function rejectResult($match_id) {

        $match = FriendlyMatch::find($match_id);

        if(!$match)
            return redirect()->back()->with('error','Niepowodzenie!'); 

        if($match->status == 3) {
            return redirect()->back()->with('error','Niepowodzenie!');
        }

        if($match->result > 0)
            return redirect()->back()->with('error','Niepowodzenie!');

        $matchResult = FriendlyMatchResult::where('match_id',$match_id)->first();

        if($matchResult) {
            // odrzucamy wynik 
            // jesli czas przekroczony dodac 5 minut 
            // jesli strony mają po 0 to anulowany mecz
            if($match->host_result_count == 0 AND $match->enemy_result_count == 0) {
                $match->result = 4;
                $match->status = 3;
                $match->host_need_action = 0;
                $match->enemy_need_action = 0;

                // powiadom strony o wyniku. 
                $info = [
                    'match_id' => $match->id,
                    'host_clan_id' => $match->hostClan->id,
                    'host_clan_link' => $match->hostClan->link,
                    'host_clan_name' => $match->hostClan->name,
                    'enemy_clan_id' => $match->enemyClan->id,
                    'enemy_clan_link' => $match->enemyClan->link,
                    'enemy_clan_name' => $match->enemyClan->name,
                ];
                // członkowie meczu wszyscy
                $hostLeader = User::find($match->hostClan->leader_id);
                $hostTeamLeader = User::find($match->hostTeam->leader_id);

                $enemyLeader = User::find($match->enemyClan->leader_id);
                $enemyTeamLeader = User::find($match->enemyTeam->leader_id);
                
                if($hostLeader->id == $hostTeamLeader->id) 
                    $hostLeader->notify(new FriendlyMatchCancel($info));
                else {
                    $hostLeader->notify(new FriendlyMatchCancel($info));
                    $hostTeamLeader->notify(new FriendlyMatchCancel($info));
                }
                if($enemyLeader->id == $enemyTeamLeader->id) {
                    $enemyLeader->notify(new FriendlyMatchCancel($info));
                    
                } else {
                    $enemyLeader->notify(new FriendlyMatchCancel($info));
                    $enemyTeamLeader->notify(new FriendlyMatchCancel($info));    
                }

                $participants = $match->hostMembers();
                
                $participants = $participants->merge($match->enemyMembers());

                 $users = array();
                foreach($participants as $member) {
                    if($member->id != $hostLeader->id && $member->id != $hostTeamLeader->id && $member->id != $enemyLeader->id && $member->id != $enemyTeamLeader->id)
                        $users[] = $member->id;
                }
                
                $participants = User::find($users);

                Notification::send($participants, new FriendlyMatchCancel($info));

                
                $match->save();
                $matchResult->delete();

                return redirect()->back()->with('success','Sukces!');
            }

            if($match->isHost()) {
                if($match->enemy_result_count > 0 && $match->expire_at < date('Y-m-d H:i:s')) {
                    // przeciwnik ma szanse ale jest juz po czasie
                    $match->expire_at = date('Y-m-d H:i:s',strtotime($match->expire_at."+ 5 minutes"));
                    $match->enemy_need_action = 1;
                    $match->host_need_action = 0;
                    $match->save();
                }
                    // nadawaj wykrzyknik
                    $info = [
                        'match_id' => $match->id,
                        'aplicant_id' => $match->hostClan->id,
                        'aplicant_link' => $match->hostClan->link,
                        'aplicant_name' => $match->hostClan->name,
                    ];

                    $hostClan = Clan::find($match->enemy_clan);
                    $hostTeam = Team::find($match->enemy_team);

                    if($hostClan->leader_id == $hostTeam->leader_id) {
                        $isCouple = true;
                    } else 
                        $isCouple = false;

                    if($isCouple) {
                        if($hostClan && $hostTeam) {
                            $user = User::find($hostClan->leader_id);
                            if($user)
                                $user->notify(new FriendlyMatchResultRejected($info));
                                broadcast(new \App\Events\MatchHostAction($user->id));
                        }
                    } else {
                        if($hostClan) {
                            $clanLeader = User::find($hostClan->leader_id);
                            if($clanLeader) 
                                $clanLeader->notify(new FriendlyMatchResultRejected($info));
                                broadcast(new \App\Events\MatchHostAction($clanLeader->id));
                        }
                        if($hostTeam) {
                            $teamLeader = User::find($hostTeam->leader_id);
                            if($teamLeader) 
                                $teamLeader->notify(new FriendlyMatchResultRejected($info));
                                broadcast(new \App\Events\MatchHostAction($teamLeader->id));
                        }
                    }
                    

                
                $matchResult->enemy_aproved = 0;
                $match->host_need_action = 1;
                $side = 1;
            } elseif($match->isEnemy()) {
                if($match->host_result_count > 0 && $match->expire_at < date('Y-m-d H:i:s')) {
                    // przeciwnik ma szanse ale jest juz po czasie
                    $match->expire_at = date('Y-m-d H:i:s',strtotime($match->expire_at."+ 5 minutes"));
                    $match->host_need_action = 1;
                    $match->save();
                }
                    // nadawaj wykrzyknik
                    $info = [
                        'match_id' => $match->id,
                        'aplicant_id' => $match->enemyClan->id,
                        'aplicant_link' => $match->enemyClan->link,
                        'aplicant_name' => $match->enemyClan->name,
                    ];

                    $hostClan = Clan::find($match->host_clan);
                    $hostTeam = Team::find($match->host_team);

                    if($hostClan->leader_id == $hostTeam->leader_id) {
                        $isCouple = true;
                    } else 
                        $isCouple = false;

                    if($isCouple) {
                        if($hostClan && $hostTeam) {
                            $user = User::find($hostClan->leader_id);
                            if($user)
                                $user->notify(new FriendlyMatchResultRejected($info));
                                broadcast(new \App\Events\MatchHostAction($user->id));
                        }
                    } else {
                        if($hostClan) {
                            $clanLeader = User::find($hostClan->leader_id);
                            if($clanLeader) 
                                $clanLeader->notify(new FriendlyMatchResultRejected($info));
                                broadcast(new \App\Events\MatchHostAction($clanLeader->id));
                        }
                        if($hostTeam) {
                            $teamLeader = User::find($hostTeam->leader_id);
                            if($teamLeader) 
                                $teamLeader->notify(new FriendlyMatchResultRejected($info));
                                broadcast(new \App\Events\MatchHostAction($teamLeader->id));
                        }
                    }
                    

                
                $matchResult->host_aproved = 0;
                $match->enemy_need_action = 1;
                $side = 2;
            }
            $match->save();
            $matchResult->save();
        }
        else 
            return redirect()->back()->with('error','Niepowodzenie!');
        // powiadomienie o odrzuceniu odpiwiednie osoby ... 
        
        return redirect()->back()->with('success','Sukces!');
    }

    public function acceptResult($match_id) {
        $matchResult = FriendlyMatchResult::where('match_id',$match_id)->first();
        
        if($matchResult) {

            $match = FriendlyMatch::find($match_id);


            if($match->status == 3) {
                return redirect()->back()->with('error','Niepowodzenie!');
            }
    
            if($match->result > 0)
                return redirect()->back()->with('error','Niepowodzenie!');

                /*
                    Result 1 = wskazuje siebie
                    Result 2 = wskazuje przeciwnika

                    
                */
            if($match->isHost()) {
                // jest hostem wiec pobieramy z pol enemy
                $match->status = 3;
                if($matchResult->enemy_aproved == 1) {
                    $match->result = $matchResult->enemy_result;

                    if($matchResult->enemy_result == 1) {
                        $match->winner_id = $match->host_clan;
                        $match->result = 1;
                    }    
                    elseif($matchResult->enemy_result == 2) {
                        $match->winner_id = $match->enemy_clan;
                        $match->result = 1;
                    }
                        
                }
                elseif($matchResult->host_aproved == 1) {
                    $match->result = $matchResult->host_result;

                    if($matchResult->host_result == 1) {
                        $match->winner_id = $match->host_clan;
                        $match->result = 1;
                    }
                        
                    elseif($matchResult->host_result == 2) {
                        $match->winner_id = $match->enemy_clan;
                        $match->result = 1;
                    }
                        
                
                }    
                    
                
                    
                $match->host_need_action = 0;
                $match->enemy_need_action = 0;   
            
                $match->save();
                $matchResult->delete();

            } elseif($match->isEnemy()) {
                $match->status = 3;
                if($matchResult->enemy_aproved == 1) {
                    $match->result = $matchResult->enemy_result;

                    if($matchResult->enemy_result == 1) {
                        $match->winner_id = $match->host_clan;
                        $match->result = 1;
                    }    
                    elseif($matchResult->enemy_result == 2) {
                        $match->winner_id = $match->enemy_clan;
                        $match->result = 1;
                    }
                        
                }
                elseif($matchResult->host_aproved == 1) {
                    $match->result = $matchResult->host_result;

                    if($matchResult->host_result == 1) {
                        $match->winner_id = $match->host_clan;
                        $match->result = 1;
                    }
                        
                    elseif($matchResult->host_result == 2) {
                        $match->winner_id = $match->enemy_clan;
                        $match->result = 1;
                    }
                        
                
                }  
                $match->host_need_action = 0;
                $match->enemy_need_action = 0;   
            
                $match->save();
                $matchResult->delete();
            }

            // Host notification
            // host

            $info = [
                'match_id' => $match->id,
                'host_clan_link' => $match->hostClan->link,
                'host_clan_name' => $match->hostClan->name,
                'enemy_clan_link' => $match->enemyClan->link,
                'enemy_clan_name' => $match->enemyClan->link,
            ];

            $hostClan = Clan::find($match->host_clan);
            $hostTeam = Team::find($match->host_team);

            if($hostClan->leader_id == $hostTeam->leader_id) {
                $isCouple = true;
            } else 
                $isCouple = false;

            if($isCouple) {
                if($hostClan && $hostTeam) {
                    $user = User::find($hostClan->leader_id);
                    if($user)
                        $user->notify(new FriendlyMatchEnded($info));
                        broadcast(new \App\Events\MatchHostAction($user->id));
                }
            } else {
                if($hostClan) {
                    $clanLeader = User::find($hostClan->leader_id);
                    if($clanLeader) 
                        $clanLeader->notify(new FriendlyMatchEnded($info));
                        broadcast(new \App\Events\MatchHostAction($clanLeader->id));
                }
                if($hostTeam) {
                    $teamLeader = User::find($hostTeam->leader_id);
                    if($teamLeader) 
                        $teamLeader->notify(new FriendlyMatchEnded($info));
                        broadcast(new \App\Events\MatchHostAction($teamLeader->id));
                }
            }



            // Enemy notification !
            // endmy
            $info = [
                'match_id' => $match->id,
                'host_clan_link' => $match->hostClan->link,
                'host_clan_name' => $match->hostClan->name,
                'enemy_clan_link' => $match->enemyClan->link,
                'enemy_clan_name' => $match->enemyClan->link,
            ];

            $hostClan = Clan::find($match->enemy_clan);
            $hostTeam = Team::find($match->enemy_team);

            if($hostClan->leader_id == $hostTeam->leader_id) {
                $isCouple = true;
            } else 
                $isCouple = false;

            if($isCouple) {
                if($hostClan && $hostTeam) {
                    $user = User::find($hostClan->leader_id);
                    if($user)
                        $user->notify(new FriendlyMatchEnded($info));
                        broadcast(new \App\Events\MatchHostAction($user->id));
                }
            } else {
                if($hostClan) {
                    $clanLeader = User::find($hostClan->leader_id);
                    if($clanLeader) 
                        $clanLeader->notify(new FriendlyMatchEnded($info));
                        broadcast(new \App\Events\MatchHostAction($clanLeader->id));
                }
                if($hostTeam) {
                    $teamLeader = User::find($hostTeam->leader_id);
                    if($teamLeader) 
                        $teamLeader->notify(new FriendlyMatchEnded($info));
                        broadcast(new \App\Events\MatchHostAction($teamLeader->id));
                }
            }



            return redirect()->back()->with('success','Sukces!');
            // powiadomienie o akceptacji wyniku. ze mecz został zakończony ...

            // Dodać skrypt archiwizacji jakos .... 
        } else {
            return redirect()->back()->with('error','Niepowodzenie!');
        }

    }

    public function cancelMatch($match_id) {

        $match = FriendlyMatch::find($match_id);

        if($match) {
            $match->result = 4;
            $match->status = 3;
            $match->host_need_action = 0;
            $match->enemy_need_action = 0;

            $info = [
                'match_id' => $match->id,
                'host_clan_id' => $match->hostClan->id,
                'host_clan_link' => $match->hostClan->link,
                'host_clan_name' => $match->hostClan->name,
                'enemy_clan_id' => $match->enemyClan->id,
                'enemy_clan_link' => $match->enemyClan->link,
                'enemy_clan_name' => $match->enemyClan->name,
            ];
            // członkowie meczu wszyscy
            $hostLeader = User::find($match->hostClan->leader_id);
            $hostTeamLeader = User::find($match->hostTeam->leader_id);

            $enemyLeader = User::find($match->enemyClan->leader_id);
            $enemyTeamLeader = User::find($match->enemyTeam->leader_id);
            
            $hostLeader->notify(new FriendlyMatchCancel($info));
            $hostTeamLeader->notify(new FriendlyMatchCancel($info));
            $enemyLeader->notify(new FriendlyMatchCancel($info));
            $enemyTeamLeader->notify(new FriendlyMatchCancel($info));
            
            $participants = $match->hostMembers();
            
            $participants = $participants->merge($match->enemyMembers());

            
            
            Notification::send($participants, new FriendlyMatchCancel($info));

            $match->save();
            return redirect()->back()->with('success','Sukces!');
        } else {
            return redirect()->back()->with('error','Niepowodzenie!');
        }

    }

    public function showArchive() {
        

        $selectedGames = Auth::user()->selectedGame();

        $game_id = $selectedGames['game_id'];
        $platform_id = $selectedGames['platform_id'];


        $result = DB::select('select distinct(clans.id) from clans LEFT JOIN teams t ON t.clan_id = clans.id WHERE t.game_id = '.$game_id.' and t.platform_id = '.$platform_id.'');

        $clans = array();
        foreach($result as $clan) {
            $clans[] = $clan->id;
        }
        
        $clans =!empty($clans) ? Clan::whereIn('id',$clans)->orderBy('updated_at','DESC')->paginate(500) : false;
        

        $matches = FriendlyMatch::where('game_id',$selectedGames['game_id'])->where('platform_id',$selectedGames['platform_id'])->where('status',3)->orderBy('match_date','DESC')->get();

        return view('match.friendly.archive', compact('matches','selectedGames','clans'));
    }

    public function archiveSearching(Request $request) {
        $formData = $request->all();
        
        $selectedGames = Auth::user()->selectedGame();

        $game_id = $selectedGames['game_id'];
        $platform_id = $selectedGames['platform_id'];


        $result = DB::select('select distinct(clans.id) from clans LEFT JOIN teams t ON t.clan_id = clans.id WHERE t.game_id = '.$game_id.' and t.platform_id = '.$platform_id.'');

        $clans = array();
        foreach($result as $clan) {
            $clans[] = $clan->id;
        }
        
        $clans =!empty($clans) ? Clan::whereIn('id',$clans)->orderBy('updated_at','DESC')->paginate(500) : false;
        
        $where = '';
        $date  = '';
        if(!empty($formData['enemy_clan']) && empty($formData['host_clan'])) {
            $where = "where host_clan = '".$formData['enemy_clan']."' OR enemy_clan = '".$formData['enemy_clan']."'";
            
        } elseif(!empty($formData['host_clan']) and empty($formData['enemy_clan'])) {
            $where = "where host_clan = '".$formData['host_clan']."' OR enemy_clan = '".$formData['host_clan']."'";
            
        } elseif(!empty($formData['host_clan']) && !empty($formData['enemy_clan'])) {
            $where = "where (host_clan = '".$formData['host_clan']."' AND enemy_clan = '".$formData['enemy_clan']."') OR (host_clan = '".$formData['enemy_clan']."' AND enemy_clan = '".$formData['host_clan']."')";
        }
        
        if(!empty($formData['date_from']) && !empty($formData['date_to'])) {
            $from = date('Y-m-d',strtotime($formData['date_from']));
            $to = date('Y-m-d',strtotime($formData['date_to']));
            $date = " match_date BETWEEN '".$from."' AND '".$to."'";
        } elseif(!empty($formData['date_from']) && empty($formData['date_to'])) {
            $from = date('Y-m-d',strtotime($formData['date_from']));
            
            $date = " match_date >= '".$from."'";
        } elseif(!empty($formData['date_to']) && empty($formData['date_from'])) {
            
            $to = date('Y-m-d',strtotime($formData['date_to']));
            $date = " match_date <= '".$to."'";
        }



        if($where != '' and $date !='')
            $date = ' and '.$date;
        elseif($date !='') 
            $date = 'where '.$date;

            
            // sleect id from friendly_matches where 
        $sqlOrder = '';
        
        $result = DB::select('SELECT DISTINCT(match_friendly.id)
                                FROM match_friendly
                                 '
                                .$where.''
                                .$date);

        
        $matches = array();
        foreach($result as $match)
            $matches[] = $match->id;
        

        $matches = !empty($matches) ? FriendlyMatch::whereIn('id',$matches)->where('game_id',$selectedGames['game_id'])->where('platform_id',$selectedGames['platform_id'])->where('status',3)->orderBy('match_date','DESC')->get() : array();


        return view('match.friendly.archive', compact('matches','selectedGames','clans','formData'));
    }
}

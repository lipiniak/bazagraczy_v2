<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\GiveawayQuests;

class QuestController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    public function list() {
        $quests = GiveawayQuests::all();

        return view('admin.quests.list',compact('quests'));
    }

    public function show($id) {
        $quest = GiveawayQuests::find($id);

        return view('admin.quests.show',compact('quest'));
    }

    public function create() {
        
        return view('admin.quests.create');
    }

    public function store(Request $request) {
        $data = $request->all();

        $quest = GiveawayQuests::create($data);

        return redirect()->back();  
    }

    public function activate($id) {
        $quest = GiveawayQuests::find($id);

        $quest->status = 1;
        $quest->save();
        return redirect()->back();
    }
    public function deactivate($id) {
        $quest = GiveawayQuests::find($id);

        $quest->status = 0;
        $quest->save();
        return redirect()->back();
    }
    public function edit($id) {
        $quest = GiveawayQuests::find($id);

        return view('admin.quests.edit',compact('quest'));
    }

    public function update(Request $request,$id) {
        $quest = GiveawayQuests::find($id);
        $data = $request->all();

        return redirect()->back();
    }

    public function delete($id) {
        $post = GiveawayQuests::find($id);

        $post->delete();

        return redirect()->route('admin.post.list');
    }

}

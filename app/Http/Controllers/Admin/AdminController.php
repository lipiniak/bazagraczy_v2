<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Settings;
use App\User;
use App\Clan;
use App\FriendlyMatch;
use App\Post;
use App\Comments;
use App\Ticket;
use App\Message;
use DB;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboardShow()
    {
        $users = User::all();
        $count = 0;
        $online =0;
        foreach($users as $user) {
            $count++;
            if($user->getStatus() == 'online' or $user->getStatus() == 'away') 
                $online++;
        }


        $stats = [
            'users_online' => $online,
            'users_registerd' => User::count(),
            'clans_registerd' => Clan::count(),
            'matches_played' => FriendlyMatch::where('status',3)->count(),
            'posts' => Post::count(),
            'comments' => Comments::count(),
            'new_tickets' => Ticket::where('status',0)->count(),
            'messages' => Message::count(),
        ];
        return view('admin.dashboard',compact('stats'));
    }
    public function getUsersDailyCount(){
        $users = DB::select("Select DATE_FORMAT(created_at, '%Y-%m-%d') as date, count(*) as howMany from users group by DATE_FORMAT(created_at, '%Y-%m-%d')");

        foreach($users as $user) {
            $data[] = array(
                'name' => $user->date,
                'value' => $user->howMany
            );
        }

        return response()->json($data);
    }


    public function getPostsDailyCount(){
        $posts = DB::select("Select DATE_FORMAT(created_at, '%Y-%m-%d') as date, count(*) as howMany from post group by DATE_FORMAT(created_at, '%Y-%m-%d')");

        foreach($posts as $post) {
            $data[] = array(
                'name' => $post->date,
                'value' => $post->howMany
            );
        }

        return response()->json($data);
    }

    public function getLoginsDailyCount(){
        $logins = DB::select("Select stat_date, count from stats_login");

        foreach($logins as $day) {
            $data[] = array(
                'name' => $day->stat_date,
                'value' => $day->count
            );
        }

        return response()->json($data);
    }

    public function getCommentsDailyCount(){
        $logins = DB::select("Select DATE_FORMAT(created_at, '%Y-%m-%d') as date, count(*) as howMany from comments group by DATE_FORMAT(created_at, '%Y-%m-%d')");

        foreach($logins as $day) {
            $data[] = array(
                'name' => $day->date,
                'value' => $day->howMany
            );
        }

        return response()->json($data);
    }

}

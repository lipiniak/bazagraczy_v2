<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Storage;
use App\Giveaway;
use App\GiveawayRules;
class GiveawayController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    public function list() {
        $quests = Giveaway::all();
        
        

        return view('admin.giveaway.list',compact('quests'));
    }

    public function show($id) {
        $quest = Giveaway::find($id);

        return view('admin.giveaway.show',compact('quest'));
    }

    public function create() {
        $rules = GiveawayRules::all();
        return view('admin.giveaway.create',compact('rules'));
    }

    public function store(Request $request) {
        $data = $request->all();
        

        $mainImg  = $request->file('main_img');

        $mainImg = Storage::putFileAs(
            'public/giveaway', $request->file('main_img'), time().'-'.str_replace(' ','-',$data['title'])
        );
        
        $data['main_img'] = $mainImg;

        $carouselImages = $request->file('carousel_img');
        $count = 0;
        
        if(count($carouselImages) > 0) {
            foreach($carouselImages as $image) {
                $carousel[] = Storage::putFileAs(
                    'public/giveaway',$image, time().'-carousel-'.$count.'-'.str_replace(' ','-',$data['title'])
                );
                $count++;
            }
            $data['carousel_img'] = json_encode($carousel);
        } else {
            $data['carousel_img'] = '';
        }
        

        $data['ending_at'] = date('Y-m-d H:i:s',strtotime($data['ending_at'].' '.$data['ending_time']));

        
        $giveaway = Giveaway::create($data);

        $giveaway->rules()->attach($data['rules']);

        return redirect()->back();  
    }

    public function activate($id) {
        $quest = Giveaway::find($id);

        $quest->status = 1;
        $quest->save();
        return redirect()->back();
    }
    public function deactivate($id) {
        $quest = Giveaway::find($id);

        $quest->status = 0;
        $quest->save();
        return redirect()->back();
    }
    public function edit($id) {
        $quest = Giveaway::find($id);

        return view('admin.giveaway.edit',compact('quest'));
    }

    public function update(Request $request,$id) {
        $quest = Giveaway::find($id);
        $data = $request->all();

        return redirect()->back();
    }

    public function delete($id) {
        $post = Giveaway::find($id);

        $post->delete();

        return redirect()->route('admin.giveaway.list');
    }

}

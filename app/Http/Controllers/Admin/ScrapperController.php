<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Settings;
use App\User;
use App\Games;
use App\Platforms;
use Goutte;

class ScrapperController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $games = Games::all();

        $gameCount = $games->count();
        $page = 1;
        $games = [];
        $game = [];

        for($page = 1;$page<= 2;$page++) {
          $crawler = Goutte::request('GET', 'https://www.gry-online.pl/gry/kooperacja/22-'.$page);
          
          $game[] = $crawler->filter('.lista-gry .box')->each(function($node) {
            $game = NULL;
            $childrens = $node->children();

            $title = $childrens->filter('h5')->each(function ($node,$i) use ($game) {
              
              return $node->text();
              //dump($node->text());
            });
          
            
            $platforms = $childrens->filter('.plat a')->each(function ($node,$i) {
              
              //dump($node->text());
              return $node->text();
            });
            $game[$title[0]] = [
              'name' => $title[0], 
              'platforms' => $platforms
            ];

            return $game;
          }); 
          
        }
        dd($game);
        return view('admin.games.scrapper', compact('gameCount'));
    }
    
    public function scrap() {

        return view('admin.games.show',compact('game'));
    }

    
}

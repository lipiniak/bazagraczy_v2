<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Settings;
use App\User;
use App\Games;
use App\Platforms;

class GamesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gierki = Games::all();
        
        return view('admin.games.list',compact('gierki'));
    }
    
    public function showGame($id) {
        $game = Games::find($id);

        return view('admin.games.show',compact('game'));
    }

    public function createGame() {
        
        $platformsAll = Platforms::all();

        return view('admin.games.create',compact('platformsAll'));
    }
    public function saveGame(Request $request) {
        $data = $request->all();
        
        $platforms = $data['platforms'];

        if(!empty($data['feeds'])) {
            foreach($data['feeds'] as $feed) {
                $feeds[]['name'] = $feed;
            }
            $data['feeds'] = json_encode($feeds);
        }
        
        unset($data['platforms']);
        
        

        $game = Games::create($data);

        $game->platforms()->attach($platforms);

        return redirect()->route('admin.game.list');
    }
    public function editGame($id) {
        $game = Games::find($id);

        $platformsAll = Platforms::all();

        return view('admin.games.edit',compact('game','platformsAll'));
    }

    public function updateGame(Request $request,$id) {
        $data = $request->all();

        
        if(!empty($data['feeds'])) {
            foreach($data['feeds'] as $feed) {
                $feeds[]['name'] = $feed;
            }
        }
        
        unset($data['feeds']);
        $game = Games::find($id);

        $platforms = $data['platforms'];
        unset($data['platforms']);
        $game->update($data);
        $game->feeds = json_encode($feeds);
        $game->save();
        $game->platforms()->sync($platforms);
        
        
        return redirect()->route('admin.game.list');
    }

    public function deleteGame($id) {
        $game = Games::find($id);

        $game->platforms()->detach();

        $game->delete();

        return redirect()->route('admin.game.list');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Post;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    public function list() {
        $posts = Post::all();

        return view('admin.posts.list',compact('posts'));
    }

    public function show($id) {
        $post = Post::find($id);

        return view('admin.posts.show',compact('post'));
    }

    public function create() {
        
        return view('admin.posts.create');
    }

    public function store(Request $request) {
        $data = $request->all();

        $post = Post::create($data);

        return redirect()->back();  
    }

    public function edit($id) {
        $post = Post::find($id);

        return view('admin.posts.edit');
    }

    public function update(Request $request,$id) {
        $post = Post::find($id);
        $data = $request->all();

        return redirect()->back();
    }

    public function delete($id) {
        $post = Post::find($id);

        $post->delete();

        return redirect()->route('admin.post.list');
    }

}

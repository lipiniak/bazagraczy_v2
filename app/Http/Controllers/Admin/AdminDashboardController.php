<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Settings;
use App\User;

class AdminDashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function countUsers()
    {
        $users = User::all();

        $usersCount = $users->count();

        $onlineUsers = 0;
        foreach($users as $user)
            if($user->getStatus() == 'online') 
                $countOnlineUsers++;

        $data = [
            'usersCount' => $usersCount,
            'onlineUsersCount' => $countOnlineUsers,
        ];

        return $data->toJson();
    }


    
    public function settingsShow() {

        $settings = Settings::all();
        
        return view('admin.settings.list',compact('settings'));
    }

    public function usersShow() {
        $users = User::all();

        return view('admin.users.list',compact('users'));
    }
}

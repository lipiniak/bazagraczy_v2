<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    public function list() {
        $users = User::all();

        return view('admin.users.list',compact('users'));
    }

    public function show($id) {
        $user = User::find($id);

        return view('admin.users.show',compact('user'));
    }
}

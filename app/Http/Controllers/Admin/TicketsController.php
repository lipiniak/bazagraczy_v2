<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ticket;
use Auth;
use App\User;
use Messenger;
use DB;
use Cache;


class TicketsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','isAdmin']);
    }

    public function list() {
        $tickets = Ticket::orderBy('created_at','DESC')->get();

        return view('admin.tickets.list',compact('tickets'));
    }

    public function show($id) {
        
        $ticket = Ticket::find($id);
        if($ticket->status == 0) {
            $ticket->status = 1;
            $ticket->save();
        }
            

        $user_id = $ticket->user->id;

        $channelUsers = array($user_id,0);

        sort($channelUsers);

        Messenger::setAuthUserId(0);

        $conversation = Messenger::getMessagesByUserId($user_id,0,5000);
        
        $conversation_all = Messenger::getMessagesByUserId($user_id,0,5000);
        
        $messages = [];

        if($conversation_all) {
            $messagesSeen = $conversation_all->messages;
            foreach($messagesSeen as $msg) {
                if(0 != $msg->user_id)
                    Messenger::makeSeen($msg->id);
            }
        }
        
        if($conversation) {
            
            $messages = $conversation->messages;
        }
        
        $conversation_id = Messenger::isConversationExists($user_id,0);
        if($conversation_id != NULL) {
            $conversationInfo = DB::select('select * from conversations where id ='.$conversation_id)[0];

            if($conversationInfo->deleted_by_users != '') {
                $deletedUser = explode(',',$conversationInfo->deleted_by_users);
    
                $count = count($deletedUser);
    
                if($count == 1) {
                    if(isset($deletedUser[0])) {
                        if($deletedUser[0] == 0) {
                            $deleted_by_users = '';
                        } else 
                            $deleted_by_users = $deletedUser[0];
    
                    }
                } elseif($count == 2) {
                    if(isset($deletedUser[0])) {
                        if($deletedUser[0] == 0) {
                            unset($deletedUser[0]);
                        } 
    
                    }
                    if(isset($deletedUser[1])) {
                        if($deletedUser[1] == 0) {
                           unset($deletedUser[1]);
                        } 
                    }
    
                    $deleted_by_users = implode(',',$deletedUser);
                }
                DB::update('update conversations set deleted_by_users="'.$deleted_by_users.'" where id='.$conversation_id);
            }
        }
            
        //$data = $request->all();

        //if(isset($data['type']))
          //  $type = $data['type'];
        //else 
            $type = 'modal';
        
            //hmm mozna zapisać czy jest rozmowa aktywna czy nie jesli jest to nie wysyłaj powiadomienia jeśli nie ma to wyślij
            
        if($type == 'modal') {
            $convId1 = $user_id.'-0';
            $convId2 = '0-'.$user_id;
            if(Cache::has('conversation-'.$convId1)) {

                $cachedUsers = Cache::get('conversation-'.$convId1);
                
                if(isset($cachedUsers['user_one'])) {
                    
                    if($cachedUsers['user_one'] != 0){
                        
                        $cachedUsers['user_two'] = 0;
                    } else {
                        $cachedUsers['user_one'] = 0;
                    }
                } else {
                    $cachedUsers['user_one'] = 0;
                }
                
                Cache::forever('conversation-'.$convId1,$cachedUsers);
                    
            } elseif(Cache::has('conversation-'.$convId2)) {
                $cachedUsers = Cache::get('conversation-'.$convId2);
                
                if(isset($cachedUsers['user_one'])) {
                    
                    if($cachedUsers['user_one'] != 0){
                        
                        $cachedUsers['user_two'] = 0;
                    } else {
                        $cachedUsers['user_one'] = 0;
                    }
                } else {
                    $cachedUsers['user_one'] = 0;
                }
                
                Cache::forever('conversation-'.$convId2,$cachedUsers);
            } else {
                $users = ['user_one' => 0];
                Cache::forever('conversation-'.$convId1,$users);
            }

        }
        
        return view('admin.tickets.show',compact('ticket','messages','channelUsers','type'));
    }

    public function create() {
        
        return view('admin.tickets.create');
    }

    public function store(Request $request) {
        $data = $request->all();

        $tickets = Ticket::create($data);

        return redirect()->back();  
    }

    public function edit($id) {
        $tickets = Ticket::find($id);

        return view('admin.tickets.edit');
    }

    public function update(Request $request,$id) {
        $post = Post::find($id);
        $data = $request->all();

        return redirect()->back();
    }

    public function delete($id) {
        $post = Post::find($id);

        $post->delete();

        return redirect()->route('admin.post.list');
    }

    public function messageSend(Request $request) {
        
        Messenger::setAuthUserId(0);
        
        $data = $request->all();
        
        Messenger::sendMessageByUserId($data['reciver_id'],nl2br($data['message']));
        
        $conversation_id = Messenger::isConversationExists($data['reciver_id'],0);

        DB::update('update conversations set deleted_by_users="" where id='.$conversation_id);

        return response()->json([
            'status' => 'success',
            
        ]);
    }

    public function closeTicket($ticket_id) {

        $ticket = Ticket::find($ticket_id);

        $ticket->status = 2;
        
        $ticket->save();
        
        return redirect()->back();
    }

    public function openTicket($ticket_id) {

        $ticket = Ticket::find($ticket_id);

        $ticket->status = 1;
        
        $ticket->save();
        
        return redirect()->back();
    }
}

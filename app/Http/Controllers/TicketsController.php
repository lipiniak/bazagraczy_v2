<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use Auth;
use Validator;
use Mail;
class TicketsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function validateTicket($data,$type,$id) {

        return Validator::make($data, [
            'report_'.$type.'_'.$id => 'required|max:250'
        ],[
            'report_'.$type.'_'.$id.'.required' => 'Pole jest wymagane',
            'report_'.$type.'_'.$id.'.max' => 'Niedozwolona ilość znaków (max. 250 znaków)'
        ]);
    }

    public function makeReport(Request $request, $type) {
        
        if($type == 'post') {
            $data = $request->all();
            
            $post_id = $data['post_id'];

            $this->validateTicket($data,$type,$post_id)->validate();

            $json = array(
                'post_id' => $post_id,
            );
            $insert = array('type'=> $type,'data'=>json_encode($json),'user_id'=>Auth::user()->id,'message'=>$data['report_post_'.$post_id]);

            Ticket::create($insert);

            return redirect()->back()->with('success','Zgłoszenie zostało przyjęte.');
        } elseif($type == 'komentarz') {
            $data = $request->all();
        
            $post_id = $data['comment_id'];
            $this->validateTicket($data,$type,$post_id)->validate();
            $json = array(
                
                'comment_id' => $post_id,
            );
            $insert = array('type'=> $type,'data'=>json_encode($json),'user_id'=>Auth::user()->id,'message'=>$data['report_komentarz_'.$post_id]);

            Ticket::create($insert);

            return redirect()->back()->with('success','Zgłoszenie zostało przyjęte.'); 
        } elseif($type == 'profil') {
            $data = $request->all();

            $user_id = $data['user_id'];

            $this->validateTicket($data,$type,$user_id)->validate();

            $json = array(
                'user_id' => $user_id
                
            );

            $insert = array('type'=> $type,'data'=>json_encode($json),'user_id'=>Auth::user()->id,'message'=>$data['report_profil_'.$user_id]);

            Ticket::create($insert);

            return redirect()->back()->with('success','Zgłoszenie zostało przyjęte.'); 
        } elseif($type == 'klan') {
            $data = $request->all();

            $clan_id = $data['clan_id'];

            $this->validateTicket($data,$type,$clan_id)->validate();

            $json = array(
                'clan_id' => $clan_id
                
            );

            $insert = array('type'=> $type,'data'=>json_encode($json),'user_id'=>Auth::user()->id,'message'=>$data['report_klan_'.$clan_id]);

            Ticket::create($insert);

            return redirect()->back()->with('success','Zgłoszenie zostało przyjęte.'); 
        }

        return redirect()->back();
        
    }

    
    public function addTicket(Request $request) {

        $formData = $request->all();
        
        if(!isset($formData['content']))
            return redirect()->back()->with('error','Błąd');

        $content = $formData['content'];

        switch($formData['type']) {
            case "new_game":
                $type = 'nowa gra';
                
                $data = '';
            break;
            case "errors":
                $type = 'błąd';
                $data = '';
            break;
            case "new_feature":
                $type = 'pomysł';
                $data = '';
            break;
        }

        $ticketBody = [
            'type' => $type,
            'data' => $data,
            'user_id' => Auth::user()->id,
            'message' => $content,
        ];

        Ticket::create($ticketBody);

        return redirect()->back()->with('success','Zgłoszenie zostało przyjęte.'); 
    }

}

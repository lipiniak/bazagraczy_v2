<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Notification;
use Validator;
use Illuminate\Validation\Rule;

use App\Team;
use App\Clan;
use App\User;
use App\Games;
use App\Notifications\ClanNewTeam;
use App\Notifications\ClanDeleteTeam;

class TeamController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function validateCreate($data) {
        return Validator::make($data,[
            'game_id' => ['required',Rule::notIn([0])],
            'platform_id' => ['required',Rule::notIn([0])],
            'members' => 'required',
            'leader_id' => ['required',Rule::notIn([0])],
            'name' => 'required|max:25|regex:/^[a-zA-Z0-9-_.AĄąaBbCĆćcDdEęĘeFfGgHhIiJjKkLłŁlMmNńŃnOÓóoPpRrSŚśsTtUuWwYyZŻżŹźz ]+$/u'

        ],[
            'game_id.required' => 'Uzupełnij pole',
            'game_id.not_in' => 'Uzupełnij pole',
            'platform_id.required' => 'Uzupełnij pole',
            'platform_id.not_in' => 'Uzupełnij pole',
            'members.required' => 'Uzupełnij pole',
            'leader_id.required' => 'Uzupełnij pole',
            'leader_id.not_in' => 'Uzupełnij pole',

            'name.required' => 'Uzupełnij pole',
            'name.max' => 'Nazwa za długa (max. 25 znaków)',
            'name.regex' => 'Użyto niedozwolonych znaków <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Nazwa może składać się z cyfr (0-9), liter (a-z; A-Z; wraz z polskimi znakami) oraz \'-\' (myślnik), \'_\' (podkreślenie) i znaku spacji."><i class="fas fa-question-circle"></i></a>',
        ]);
    }
    
    public function validateEdit($data) {
        return Validator::make($data,[
            'game_id-edit-'.$data['team_id'] => ['required',Rule::notIn([0])],
            'platform_id-edit-'.$data['team_id'] => ['required',Rule::notIn([0])],
            'members-edit-'.$data['team_id'] => 'required',
            'leader_id-edit-'.$data['team_id'] => ['required',Rule::notIn([0])],
            'name-edit-'.$data['team_id'] => 'required|max:25|regex:/^[a-zA-Z0-9-_.AĄąaBbCĆćcDdEęĘeFfGgHhIiJjKkLłŁlMmNńŃnOÓóoPpRrSŚśsTtUuWwYyZŻżŹźz ]+$/u'

        ],[
            'game_id-edit-'.$data['team_id'].'.required' => 'Uzupełnij pole',
            'game_id-edit-'.$data['team_id'].'.not_in' => 'Uzupełnij pole',
            'platform_id-edit-'.$data['team_id'].'.required' => 'Uzupełnij pole',
            'platform_id-edit-'.$data['team_id'].'.not_in' => 'Uzupełnij pole',
            'members-edit-'.$data['team_id'].'.required' => 'Uzupełnij pole',
            'leader_id-edit-'.$data['team_id'].'.required' => 'Uzupełnij pole',
            'leader_id-edit-'.$data['team_id'].'.not_in' => 'Uzupełnij pole',

            'name-edit-'.$data['team_id'].'.required' => 'Uzupełnij pole',
            'name-edit-'.$data['team_id'].'.max' => 'Nazwa za długa (max. 25 znaków)',
            'name-edit-'.$data['team_id'].'.regex' => 'Użyto niedozwolonych znaków <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Nazwa może składać się z cyfr (0-9), liter (a-z; A-Z; wraz z polskimi znakami) oraz \'-\' (myślnik), \'_\' (podkreślenie) i znaku spacji."><i class="fas fa-question-circle"></i></a>',
        ]);
    }

    public function createTeam(Request $request) {
        $data = $request->all();
        
        $data['platform'][0] = '';
        if(!empty($data['game_id'])) {
            $game = Games::find($data['game_id']);

            foreach($game->platforms as $platform) {
                $data['platform'][$platform->id] = $platform->short_name;
            }
        }

        $validator = $this->validateCreate($data);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->with(['fail'=>'Błąd','teamData'=>$data])
                        ->withInput();
        }
        
        // szukamy team z takimi samymi paramterami 

        $team = Team::where('clan_id',$data['clan_id'])->where('name',$data['name'])->where('game_id',$data['game_id'])->where('platform_id',$data['platform_id'])->get();

        if(count($team) > 0) {
            return redirect()->back()
                        ->withErrors(['name'=>'UWAGA: W tym klanie istnieje już drużyna grająca w wybraną grę na wybranej platformie. Możesz dodać kolejną, ale jej nazwa nie może być taka sama.'])
                        ->with(['fail'=>'Błąd','teamData'=>$data])
                        ->withInput();
        }
        $members = $data['members'];

        $clan = Clan::find($data['clan_id']);

        print_r($data);
        $team = Team::create($data);

        $team->members()->sync($members);

        $clan->teams()->attach($team->id);

        $clan->touch();

        foreach($clan->members as $member) {
            if($member->id != $clan->leader_id)
                $clanMembers[] = $member->id;
        }

        if(!empty($clanMembers)) {
            $clanMembers = User::whereIn('id', $clanMembers)->get();

            $info = array(
                'clan_name' => $clan->name,
                'clan_id' => $clan->id,
                'clan_link' => $clan->link,
                'team_name' => $team->name,
                'team_game' => $team->game->name,
                'team_platform' => $team->platform->name,
            );

            Notification::send($clanMembers, new ClanNewTeam($info));
        }

        

        return redirect()->back()->with('success','Stworzono nową drużynę.');
    }

    public function updateTeam(Request $request) {
        $data = $request->all();

        $data['platform'][0] = '';
        if(!empty($data['game_id'])) {
            $game = Games::find($data['game_id']);

            foreach($game->platforms as $platform) {
                $data['platform'][$platform->id] = $platform->short_name;
            }
        }
        

        $validator = $this->validateEdit($data);
        
        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->with(['edit'=>$data['team_id'],'teamData'=>$data])
                        ->withInput();
        }
        
        // szukamy team z takimi samymi paramterami 

        $team = Team::where('clan_id',$data['clan_id'])->where('name',$data['name-edit-'.$data['team_id']])->where('game_id',$data['game_id-edit-'.$data['team_id']])->where('platform_id',$data['platform_id-edit-'.$data['team_id']])->get();

        if(count($team) > 0) {
            return redirect()->back()
                        ->withErrors(['name-edit-'.$data['team_id']=>'UWAGA: W tym klanie istnieje już drużyna grająca w wybraną grę na wybranej platformie. Możesz dodać kolejną, ale jej nazwa nie może być taka sama.'])
                        ->with(['edit'=>$data['team_id'],'teamData'=>$data])
                        ->withInput();
        }

        $data = [
            'game_id' => $data['game_id-edit-'.$data['team_id']],
            'platform_id' => $data['platform_id-edit-'.$data['team_id']],
            'members' => $data['members-edit-'.$data['team_id']],
            'leader_id' => $data['leader_id-edit-'.$data['team_id']],
            'name' => $data['name-edit-'.$data['team_id']],
            'team_id' => $data['team_id'],
            'clan_id' => $data['clan_id']
        ];
        
        
        $members = $data['members'];

        $team = Team::find($data['team_id']);

        $team->members()->sync($members);
        
        $team->update($data);

        $clan = Clan::find($data['clan_id']);

        $clan->touch();
        
        return redirect()->back()->with('success','Zapisano zmiany.');
    }
   
    public function deleteTeam(Request $request) {
        $data = $request->all();

        $team = Team::find($data['team_id']);

        $clan = Clan::find($team->clan_id);

        $matches = $team->allMatches();
        
        foreach($matches as $match)
            if($match->status > 1)
                return redirect()->back()->with('error','Nie możesz usunąć drużyny króra rozgrywa klanówkę.');
        
        

        $team->members()->detach();

        $clan->teams()->detach($team->id);
        

        foreach($clan->members as $member) {
            if($member->id != $clan->leader_id)
                $clanMembers[] = $member->id;
        }
        if(!empty($clanMembers)) {
            $clanMembers = User::whereIn('id', $clanMembers)->get();

            $info = array(
                'clan_name' => $clan->name,
                'clan_id' => $clan->id,
                'clan_link' => $clan->link,
                'team_name' => $team->name,
                'team_game' => $team->game->name,
                'team_platform' => $team->platform->name,
            );

            Notification::send($clanMembers, new ClanDeleteTeam($info));
        }
        


        $clan->touch();
        $team->delete();


        return redirect()->back()->with('success','Drużyna została usunięta.');
    }

}

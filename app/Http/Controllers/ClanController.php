<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use Storage;
use DB;
use App\Clan;
use App\User;
use App\ClanPost;
use App\Games;
use App\Platforms;
use App\Team;
use App\Notifications\ClanJoinRequest;
use App\Notifications\ClanAccepted;
use App\Notifications\ClanRejected;
use App\Notifications\ClanNewPost;
use App\Notifications\ClanLeaderChange;
use App\Notifications\ClanMemberAccepted;
use App\Notifications\ClanRemoveMember;
use App\Notifications\ClanRemoveMemberUser;
use App\Notifications\ClanMemberRemoved;
use App\Notifications\ClanMemberLeave;
use App\Notifications\ClanDelete;
use App\Push;
use Notification;

class ClanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function validateClan($data) {
        return Validator::make($data,[
            'name' => 'required|min:2|max:25|regex:/^[a-zA-Z0-9-_.AĄąaBbCĆćcDdEęĘeFfGgHhIiJjKkLłŁlMmNńŃnOÓóoPpRrSŚśsTtUuWwYyZŻżŹźz ]+$/u',
            'tag' => 'required|max:5|regex:/^[a-zA-Z0-9-_.AĄąaBbCĆćcDdEęĘeFfGgHhIiJjKkLłŁlMmNńŃnOÓóoPpRrSŚśsTtUuWwYyZŻżŹźz]+$/u',
            'description' => 'max:2500',
            'avatar' => 'max:500|mimes:jpeg,jpg,gif,png',
            'recruitment_info' => 'max:500',
        ],[
            'name.required' => 'Uzupełnij pole',
            'name.regex' => 'Użyto niedozwolonych znaków <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Nazwa może składać się z cyfr (0-9), liter (a-z; A-Z; wraz z polskimi znakami) oraz \'-\' (myślnik), \'_\' (podkreślenie) i znaku spacji."><i class="fas fa-question-circle"></i></a><br>',
            'name.min' => 'Niedozwolona ilość znaków (min. 2 znaki)',
            'name.max' => 'Niedozwolona ilość znaków (max. 25 znaki)',

            'tag.required' => 'Uzupełnij pole',
            'tag.regex' => 'Użyto niedozwolonych znaków <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Tag może składać się z cyfr (0-9), liter (a-z; A-Z; wraz z polskimi znakami) oraz \'-\' (myślnik), \'_\' (podkreślenie) i bez znaku spacji."><i class="fas fa-question-circle"></i></a><br>',
            'tag.max' => 'Niedozwolona ilość znaków (max. 5 znaki)',

            'description.required' => 'Uzupełnij pole',
            'description.max' => 'Niedozwolona ilość znaków (max. 2500 znaki)',

            'avatar.required' => 'Proszę wybrać plik',
            'avatar.max' => 'Wybrany plik jest za duży (max. 500kB)',
            'avatar.mimes' => 'Niedozwolony format pliku (tylko .jpg, .gif, .png)',

            'recruitment_info.max' => 'Niedozwolona ilość znaków (max. 500 znaki)',
        ]);
    }

    public function showCreate() {
        $clan = array();
        $title = 'Stwórz nowy klan';
        $type = 'create';
        return view('clan.create',compact('clan','title','type'));
    }

    public function editClan($id) {

        $clan = Clan::find($id);

        if(Auth::user()->id != $clan->leader_id)
            return redirect()->back()->with('error','Brak dostępu.');


        $title = 'Edytuj klan';
        $type = 'edit';



        return view('clan.create',compact('clan','title','type'));
    }

    public function updateClan(Request $request) {
        
        $data = $request->all();
        
        $clan_id = $data['id'];

        $clan = Clan::find($clan_id);
        
        if(Auth::user()->id != $clan->leader_id)
            return redirect()->back()->with('error','Brak dostępu.');

        $this->validateClan($data)->validate();

        $avatar = $request->file('avatar');
        
        if(isset($data['recruitment'])) {
            $data['recruitment'] = ($data['recruitment'] == 'on') ? 1 : 0;
        } else {
            $data['recruitment'] = 0;
        }

        if($data['name'] != $clan->name) {
            $link = $data['name'];
            $link = $this->removeAccents($link);
            $link = str_replace(" ", ".", $link);
            
            $clans = Clan::where('link','LIKE',$link.'%')->get();
            $countClans = $clans->count();
    
            if($countClans == 0) {
                $data['link'] = $link;    
            } else {
                $data['link'] = $link.$countClans;    
            }
        }
        

        
        
        
        $clan->update($data);
        $clan->touch();
        if($request->file('avatar') != '') {

            Storage::delete($clan->logo);
            
            $clan->logo = Storage::putFileAs(
                'public/clans', $request->file('avatar'), time().'-'.$clan->id
            );
            $clan->save();
        }

        return redirect()->back()->with('success','Poprawnie zmieniono dane.');
    }

    public function deleteAvatar($clan_id) {
        $clan = Clan::find($clan_id);

        if(Auth::user()->id != $clan->leader_id)
            return redirect()->back()->with('error','Brak dostępu.');

        Storage::delete($clan->logo);
        
        $clan->logo = 'no-clan-logo.jpg';
        $clan->touch();
        $clan->save();
        
        return redirect()->back()->with('success','Logo klanu zostało usunięte.');

    }

    public function clanCreate(Request $request) {
        $data = $request->all();
        
        $this->validateClan($data)->validate();

        $data['leader_id'] = Auth::user()->id;

        $avatar = $request->file('avatar');
        
        if(isset($data['recruitment'])) {
            $data['recruitment'] = ($data['recruitment'] == 'on') ? 1 : 0;
        } else {
            $data['recruitment'] = 0;
        }

        $link = $data['name'];
        $link = $this->removeAccents($link);
        $link = str_replace(" ", ".", $link);
        
        $clans = Clan::where('link','LIKE',$link.'%')->get();
        $countClans = $clans->count();

        if($countClans == 0) {
            $data['link'] = $link;    
        } else {
            $data['link'] = $link.$countClans;    
        }
        
        $clan = Clan::create($data);

        if($request->file('avatar') != '') {
            
            $clan->logo = Storage::putFileAs(
                'public/clans', $request->file('avatar'), time().'-'.$clan->id
            );
            
            $clan->save();
        }
        
        

        $user = Auth::user();
        $user->clans()->attach($clan->id,['accepted' => 1]);
                
        $push = array('content' => '<span class="lider-only"></span><small><strong>TIP:</strong> W ramach danego klanu możesz tworzyć drużyny. Utworzenie drużyn dla konkretnych gier pozwoli na tworzenie  i dołączanie do kolanówek oraz umożliwi innym użytkownikom znalezienie twojego klanu w wyszukiwarce.</small>','user_id'=>$clan->leader_id,'route_name'=>'clan.clan');
        
        Push::create($push);

        return redirect('/klan/'.$clan->link)->with('success','Klan został stworzony.');
    }

    public function showEdit($clan_id) {
        
        $clan = Clan::find($clan_id);
        
        if(Auth::user()->id != $clan->leader_id)
            return redirect()->back()->with('error','Brak dostępu.');

        return view('clan.create',compact('clan'));
        
    }

    public function showMembers($clan_id) {
        $clan = Clan::find($clan_id);

        if(Auth::user()->id != $clan->leader_id)
            return redirect()->back()->with('error','Brak dostępu.');

        $members = $clan->members->all();
        $aplicants = $clan->aplicants->all();

        return view('clan.menegment',compact('clan','members','aplicants'));
    }

    public function joinClan($clan_id) {
        $user = Auth::user();

        $user->clans()->attach($clan_id);
        $clan = Clan::find($clan_id);

        $leader = User::find($clan->leader_id);

        $info = array(
            'user_name' => $user->login,
            'user_id' => $user->id,
            'clan_name' => $clan->name,
            'clan_id' => $clan->id,
            'clan_link' => $clan->link,
        );
        
        $leader->notify(new ClanJoinRequest($info));

        broadcast(new \App\Events\ClanJoin($clan->leader_id));

        return redirect()->back()->with('success','Wysłano prośbę o dołączenie do klanu.');
    }

    public function acceptMember(Request $request,$clan_id) {
        
        
        $data = $request->all();

        $user = User::find($data['user_id']);

        $clan = Clan::find($clan_id);
        
        if(Auth::user()->id != $clan->leader_id)
            return redirect()->back()->with('error','Brak dostępu.');

        $info = array(
            'clan_name' => $clan->name,
            'clan_id' => $clan->id,
            'clan_link' => $clan->link,
            'user_id' => $user->id,
            'user_name' => $user->login,

        );

        foreach($clan->members as $member) {
            if($member->id != $clan->leader_id)
                $clanMembers[] = $member->id;
        }
        if(!empty($clanMembers)) {
            $clanMembers = User::whereIn('id', $clanMembers)->get();

            Notification::send($clanMembers, new ClanMemberAccepted($info));
        }

        $info = array(
            'clan_name' => $clan->name,
            'clan_id' => $clan->id,
            'clan_link' => $clan->link,
        );

        $user->applyToClans()->updateExistingPivot($clan_id, ['accepted' => 1]);
        $clan->touch();                

        $user->notify(new ClanAccepted($info));

        return redirect()->back()->with('success','Nowy członek został przyjęty.');
    }

    public function rejectMember(Request $request,$clan_id) {
        $data = $request->all();

        $clan = Clan::find($clan_id);

        if(Auth::user()->id != $clan->leader_id)
            return redirect()->back()->with('error','Brak dostępu.');

        $user = User::find($data['user_id']);

        $user->applyToClans()->detach($clan_id);
        
       


        $clan->touch();
        $info = array(
            'clan_name' => $clan->name,
            'clan_id' => $clan->id,
            'clan_link' => $clan->link,
        );
        
        $user->notify(new ClanRejected($info));

        return redirect()->back()->with('success','Odrzucono zgłoszenie.');
    }


    public function showClans() {
        
        $user = Auth::user();
        
        
        $clans = $user->clans->all();

        return view('clan.list',compact('clans'));
    }    

    public function showClan($link) {
        $page = 'about';
        $clan = Clan::where('link','LIKE',$link)->first();

        if(empty($clan)) {
            return redirect('/klan/lista')->with('error','Nie ma takiego klanu.');
        }

        $games = Games::orderBy('name')->get();

        $data['games'][0] = '';
        foreach($games as $game) {
            $data['games'][$game->id] = $game->name;
        }
        $i = 0;
        foreach($games as $game){
                
            $data['games_edit'][$i]['id'] = $game->id;
            $data['games_edit'][$i]['text'] = $game->name;
            $i++;
        }
        $voivodeship = \App\Voivodeship::all();
        
        $data['voivodeship'][0] = '';
        foreach($voivodeship as $voivode) {
            $data['voivodeship'][$voivode->id] = $voivode->name; 
        }
        $data['city'][0] = '';
        $data['platform'][0] = '';

        $platforms = Platforms::orderBy('name')->get();
        $w = 0;
        foreach($platforms as $platform) {
            $data['platforms_edit'][$w]['id']= $platform->id;
            $data['platforms_edit'][$w]['text']= $platform->name;
            $w++;
        }
        $new = true;

        return view('clan.show',compact('clan','data','page'));
    }

    public function showClanMembers($link) {
        $clan = Clan::where('link','LIKE',$link)->first();
        $page = 'members';
        if(empty($clan)) {
            return redirect('/klan/lista')->with('error','Nie ma takiego klanu.');
        }

        $games = Games::orderBy('name')->get();

        $data['games'][0] = '';
        foreach($games as $game) {
            $data['games'][$game->id] = $game->name;
        }
        $i = 0;
        foreach($games as $game){
                
            $data['games_edit'][$i]['id'] = $game->id;
            $data['games_edit'][$i]['text'] = $game->name;
            $i++;
        }
        $voivodeship = \App\Voivodeship::all();
        
        $data['voivodeship'][0] = '';
        foreach($voivodeship as $voivode) {
            $data['voivodeship'][$voivode->id] = $voivode->name; 
        }
        $data['city'][0] = '';
        $data['platform'][0] = '';

        $platforms = Platforms::orderBy('name')->get();
        $w = 0;
        foreach($platforms as $platform) {
            $data['platforms_edit'][$w]['id']= $platform->id;
            $data['platforms_edit'][$w]['text']= $platform->name;
            $w++;
        }
        $new = true;

        return view('clan.members',compact('clan','data','page'));
    }

    public function showClanTeams($link) {
        $clan = Clan::where('link','LIKE',$link)->first();
        $page = 'teams';
        if(empty($clan)) {
            return redirect('/klan/lista')->with('error','Nie ma takiego klanu.');
        }

        $games = Games::orderBy('name')->get();

        $data['games'][0] = '';
        foreach($games as $game) {
            $data['games'][$game->id] = $game->name;
        }
        $i = 0;
        foreach($games as $game){
                
            $data['games_edit'][$i]['id'] = $game->id;
            $data['games_edit'][$i]['text'] = $game->name;
            $i++;
        }
        $voivodeship = \App\Voivodeship::all();
        
        $data['voivodeship'][0] = '';
        foreach($voivodeship as $voivode) {
            $data['voivodeship'][$voivode->id] = $voivode->name; 
        }
        $data['city'][0] = '';
        $data['platform'][0] = '';

        $platforms = Platforms::orderBy('name')->get();
        $w = 0;
        foreach($platforms as $platform) {
            $data['platforms_edit'][$w]['id']= $platform->id;
            $data['platforms_edit'][$w]['text']= $platform->name;
            $w++;
        }
        $new = true;

        return view('clan.teams',compact('clan','data','page'));
    }

    public function showClanMatches($link) {
        $clan = Clan::where('link','LIKE',$link)->first();
        $page = 'matches';
        if(empty($clan)) {
            return redirect('/klan/lista')->with('error','Nie ma takiego klanu.');
        }

        $games = Games::orderBy('name')->get();

        $data['games'][0] = '';
        foreach($games as $game) {
            $data['games'][$game->id] = $game->name;
        }
        $i = 0;
        foreach($games as $game){
                
            $data['games_edit'][$i]['id'] = $game->id;
            $data['games_edit'][$i]['text'] = $game->name;
            $i++;
        }
        $voivodeship = \App\Voivodeship::all();
        
        $data['voivodeship'][0] = '';
        foreach($voivodeship as $voivode) {
            $data['voivodeship'][$voivode->id] = $voivode->name; 
        }
        $data['city'][0] = '';
        $data['platform'][0] = '';

        $platforms = Platforms::orderBy('name')->get();
        $w = 0;
        foreach($platforms as $platform) {
            $data['platforms_edit'][$w]['id']= $platform->id;
            $data['platforms_edit'][$w]['text']= $platform->name;
            $w++;
        }
        $new = true;

        return view('clan.matches',compact('clan','data','page'));
    }

    public function clanPostValidate($data) {
        return Validator::make($data,[
            'content' => 'required|max:2500',
        ],[
            'content.required' => '',
            'content.max' => '',
        ]);
    }

    public function addPost(Request $request) {
        $data = $request->all();

        $clan = Clan::find($data['clan_id']);
        if(Auth::user()->id != $clan->leader_id)
            return redirect()->back()->with('error','Brak dostępu.');

        
        $this->clanPostValidate($data)->validate();
        
        unset($data['_token']);

        

        $data['author_id'] = Auth::user()->id;
        
        $post = new ClanPost($data);
        

        
        $clan->posts()->save($post);

        $clan->touch();
        
        // Powiadomienie o nowym poście na tablicy dla all userów;

        foreach($clan->members as $member) {
            if($member->id != $clan->leader_id)
                $clanMembers[] = $member->id;
        }

        if(!empty($clanMembers)) {

            $clanMembers = User::whereIn('id', $clanMembers)->get();
            
            $info = array(
                'clan_name' => $clan->name,
                'clan_id' => $clan->id,
                'clan_link' => $clan->link,
            );
    
            Notification::send($clanMembers, new ClanNewPost($info));
        }
        
        
        return redirect()->back()->with('success','Nowy post został dodany.');
    }
    public function editPost(Request $request) {
        
        $data = $request->all();

        $post = ClanPost::find($data['post_id']);

        $post->content = $data['editPost'.$data['post_id']];
        
        $post->save();
        
        return redirect()->back()->with('success','Post został wyedytowany.');

    }

    public function deletePost(Request $request) {
        $data = $request->all();

        $post = ClanPost::find($data['post_id']);

        $post->delete();

        return redirect()->back()->with('success','Post został usunięty.');
    }


    public function searchClan() {

        $games = Games::orderBy('name')->get();

        $data['games'][0] = '';
        foreach($games as $game) {
            $data['games'][$game->id] = $game->name;
        }
        
        
        $data['platform'][0] = '';
        $new = true;
        

        $selectedGame = Auth::user()->selectedGame();

        $game_id = $selectedGame['game_id'];
        $platform_id = $selectedGame['platform_id'];


        $result = DB::select('select distinct(clans.id) from clans LEFT JOIN teams t ON t.clan_id = clans.id where t.game_id = '.$game_id.' and t.platform_id = '.$platform_id.'');

        $clans = array();
        foreach($result as $clan) {
            $clans[] = $clan->id;
        }
        
        $clans =!empty($clans) ? Clan::whereIn('id',$clans)->orderBy('updated_at','DESC')->paginate(500) : false;
        
        return view('clan.search', compact('data','new','clans'));
    }


    public function doSearching(Request $request) {

        $formData = $request->all();
        
        $games = Games::orderBy('name')->get();

        $data['games'][0] = '';
        foreach($games as $game) {
            $data['games'][$game->id] = $game->name;
        }
        
        $data['platform'][0] = '';
        if(!empty($formData['game'])) {
            $game = Games::find($formData['game']);

            foreach($game->platforms as $platform) {
                $data['platform'][$platform->id] = $platform->short_name;
            }
        }
        
        $new = false;

        if(!empty($formData['game']) && $formData['game'] !=0) {
            $join['game'] = "LEFT JOIN teams t ON t.clan_id = clans.id";
            $where['game'] = "t.game_id = ".$formData['game'];
        }

        if(!empty($formData['platform']) && $formData['platform'] != 0) {
            
            $where['platform'] = "t.platform_id = ".$formData['platform'];
        }
        if(!empty($formData['keyword'])) {

            $formData['keyword'] = str_replace('[','',$formData['keyword']);
            $formData['keyword'] = str_replace(']','',$formData['keyword']);
            
            $where['name'] = "(clans.name LIKE '%".$formData['keyword']."%' OR clans.tag LIKE '%".$formData['keyword']."%')";
            
        }
        if(!empty($formData['filters'])) {
            
            foreach($formData['filters'] as $filter) {
                  
                if($filter == 1) {
                    $where['recruiting'] = 'recruitment = 1';
                }
                
            }
        }

        if(isset($order)) {
            $sqlOrder = " ORDER BY ".$order['o'];
        } else {
            $sqlOrder = '';
        }

        if(isset($where))
            $sqlWhere = 'WHERE '.implode(' and ',$where);
            
        else 
            $sqlWhere = '';

        if(isset($join))
            $sqlJoin = implode(' ',$join);
        else 
            $sqlJoin = '';
      
        

        $result = DB::select('SELECT DISTINCT(clans.id)
                                FROM clans '.$sqlJoin.'
                                '
                                .$sqlWhere.''
                                );
       
        $clans = array();
        foreach($result as $clan) {
            $clans[] = $clan->id;
        }
        
        $clans =!empty($clans) ? Clan::whereIn('id',$clans)->orderBy('updated_at','DESC')->paginate(500) : false;
        
        return view('clan.search', compact('data','new','clans','formData'));

        
    }

    public function leaveClan(Request $request, $clan_id) {
        $data = $request->all();
        
        // user id do usunięcia
        $user = User::find($data['user_id']);

        $clan = Clan::find($clan_id);

        if(count($clan->members) == 1) {

            $matches = $clan->allMatches();

            foreach($matches as $match)
                if($match->status > 1)
                    return redirect()->back()->with('error','Nie możesz opóścić klanu gdy rozgrywane są klanówki.');
            
                    
            // usuwamy klan
            $user->clans()->detach($clan->id);   
            $user->teams()->detach();

            Storage::delete($clan->logo);
            
            $clan->delete();
            
            return redirect('klan/lista')->with('success','Opuściłeś klan. Klan został usunięty.');
        } else {
            if(isset($data['next_id'])) {
                
                if($data['next_id'] != 0) {
                    $clan->leader_id = $data['next_id'];
                    $clan->save();
                    echo "next_id != 0";
                    //Powiadomienie o przekazaniu wladzy?
                    $info = array(
                        'clan_name' => $clan->name,
                        'clan_id' => $clan->id,
                        'clan_link' => $clan->link,
                        'user_id' => $user->id,
                        'user_name' => $user->login
                    );
                    
                    $user->clans()->detach($clan->id);  
                    $user->teams()->detach();

                    $clan->touch();
                    $nextUser = User::find($data['next_id']);
                    $nextUser->notify(new ClanLeaderChange($info));

                    return redirect()->route('clan.list')->with('success','Opuściłeś klan.');
                } else {
                    return redirect()->back()->withErrors(['next_id'=>'Pole jest wymagane'])->with(['fail'=>$user->id]);
                }
            } 

            if($data['type'] == 'remove') {
                
                $info = array(
                    'clan_name' => $clan->name,
                    'clan_id' => $clan->id,
                    'clan_link' => $clan->link,
                    'user_id' => $user->id,
                    'user_name' => $user->login
                );

                $user->clans()->detach($clan->id);   

                foreach($clan->teams as $team) {
                    // if is leader set leader to 0
                    if($user->id == $team->leader_id) {
                        $team = Team::find($team->id);
                        $team->leader_id = 0;
                        $team->save();
                    }

                    // if is member
                    $team->members()->detach($user->id);
                }

                $user->notify(new ClanRemoveMemberUser($info));

                foreach($clan->members as $member) {    
                    if($member->id != $clan->leader_id)
                        if($member->id != $user->id)    
                            $clanMembers[] = $member->id;
                }
                
                $clan->touch();

                if(!empty($clanMembers)) {
                    $clanMembers = User::whereIn('id', $clanMembers)->get();
        
                    $info = array(
                        'clan_name' => $clan->name,
                        'clan_id' => $clan->id,
                        'clan_link' => $clan->link,
                        'user_id' => $user->id,
                        'user_name' => $user->login
                    );
        
                    Notification::send($clanMembers, new ClanRemoveMember($info));
                }

                return redirect()->back()->with('success','Usunąłeś członka.');
            } 

            if($data['type'] == 'leave') {
                $user->clans()->detach($clan->id); 
                
                // trzeba sprawdzić w jakich drużynach klanu user jest liderem 
                // trzeba sprawdzić w jakich teamch user jest memberam

                foreach($clan->teams as $team) {
                    // if is leader set leader to 0
                    if($user->id == $team->leader_id) {
                        $team = Team::find($team->id);
                        $team->leader_id = 0;
                        $team->save();
                    }

                    // if is member
                    $team->members()->detach($user->id);
                }

                $clan = Clan::find($clan->id);

                foreach($clan->members as $member) {    
                    if($member->id != $user->id)
                        $clanMembers[] = $member->id;
                }

                if(!empty($clanMembers)) {
                    $clanMembers = User::whereIn('id', $clanMembers)->get();
        
                    $info = array(
                        'clan_name' => $clan->name,
                        'clan_id' => $clan->id,
                        'clan_link' => $clan->link,
                        'user_id' => $user->id,
                        'user_name' => $user->login
                    );
        
                    Notification::send($clanMembers, new ClanMemberRemoved($info));
                }

                return redirect()->route('clan.list')->with('success','Opuściłeś klan.');
            }
        }
        
        //return redirect()->back()->with('success','Opuściłeś klan.');

    }

    public function changeLeader(Request $request,$clan_id) {
        
        $data = $request->all();

        $clan = Clan::find($clan_id);

        if(isset($data['user_id'])) {
            $clan->leader_id = $data['user_id'];
            $clan->save();
        } else {
            return redirect()->back()->with('error','Musisz wskazać następce.');
        }


        return redirect('/klan/'.$clan->link)->with('success','Przekazałeś dowództwo.');
    }


    public function deleteClan(Request $request) {

        $data = $request->all();

        $clan_id = $data['clan_id'];

        if(empty($clan_id))
            return redirect()->back()->with('error','Wystąpił nieoczekiwany błąd.');
        
        $clan = Clan::find($clan_id);

        if(Auth::user()->id != $clan->leader_id) 
            return redirect()->back()->with('error','Nie masz uprawnień do tej akcji.');


        $matches = $clan->allMatches();

        foreach($matches as $match)
            if($match->status > 1)
                return redirect()->back()->with('error','Nie możesz usunąć klanu gdy rozgrywane są klanówki.');
        
        
        $info = array(
            'clan_name' => $clan->name,
            'clan_link' => $clan->link,
        );

        $clanMembers = array();
        foreach($clan->members as $member) {    
            if($member->id != $clan->leader_id)
                $clanMembers[] = User::find($member->id);
        }

        // remove all team members
        $clanTeams = $clan->teams;

        $clan->teams()->detach();
        
        foreach($clanTeams as $team) {
            $team->members()->detach();
            $team->delete();
        }
        // remove all teams
        
        // remove all users
        
       
        $clan->members()->detach();
        // remove clan
        $clan->delete();

        // send notification
        if(!empty($clanMembers))
            Notification::send($clanMembers, new ClanDelete($info));
        
        return redirect()->route('clan.list')->with('success','Klan został usunięty.');

    }

    public function removeAccents($str) {
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η');
        return str_replace($a, $b, $str);
      }
}

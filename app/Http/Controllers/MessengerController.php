<?php

namespace App\Http\Controllers;
use Auth;
use App\user;
use Illuminate\Http\Request;
use Messenger;
use DB;
use Cache;

class MessengerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');


    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function conversationShow(Request $request, $user_id) {
       
        if($user_id == Auth::user()->id) {
            return redirect()->back()->with('error','Błąd. Operacja niedozwolona.');
        }

        $messages = [];

        $withUser = User::find($user_id);
        
        $channelUsers = array($user_id,Auth::user()->id);

        sort($channelUsers);

        Messenger::setAuthUserId(Auth::user()->id);

        $conversation = Messenger::getMessagesByUserId($user_id,0,5000);
        
        $conversation_all = Messenger::getMessagesByUserId($user_id,0,5000);
        
        if($conversation_all) {
            $messagesSeen = $conversation_all->messages;
            foreach($messagesSeen as $msg) {
                if(Auth::user()->id != $msg->user_id)
                    Messenger::makeSeen($msg->id);
            }
        }
        
        if($conversation) {
            
            $messages = $conversation->messages;
        }
        
        $conversation_id = Messenger::isConversationExists($user_id,Auth::user()->id);

        //print_r($conversation_id);
        //dd($conversation);

        if($conversation_id != NULL) {
            $conversationInfo = DB::select('select * from conversations where id ='.$conversation_id)[0];

            if($conversationInfo->deleted_by_users != '') {
                $deletedUser = explode(',',$conversationInfo->deleted_by_users);
    
                $count = count($deletedUser);
    
                if($count == 1) {
                    if(isset($deletedUser[0])) {
                        if($deletedUser[0] == Auth::user()->id) {
                            $deleted_by_users = '';
                        } else 
                            $deleted_by_users = $deletedUser[0];
    
                    }
                } elseif($count == 2) {
                    if(isset($deletedUser[0])) {
                        if($deletedUser[0] == Auth::user()->id) {
                            unset($deletedUser[0]);
                        } 
    
                    }
                    if(isset($deletedUser[1])) {
                        if($deletedUser[1] == Auth::user()->id) {
                           unset($deletedUser[1]);
                        } 
                    }
    
                    $deleted_by_users = implode(',',$deletedUser);
                }
                DB::update('update conversations set deleted_by_users="'.$deleted_by_users.'" where id='.$conversation_id);
            }
        }
            
        $data = $request->all();

        if(isset($data['type']))
            $type = $data['type'];
        else 
            $type = 'full';
        
            //hmm mozna zapisać czy jest rozmowa aktywna czy nie jesli jest to nie wysyłaj powiadomienia jeśli nie ma to wyślij
            
        if($type == 'modal') {
            $convId1 = $user_id.'-'.Auth::user()->id;
            $convId2 = Auth::user()->id.'-'.$user_id;
            if(Cache::has('conversation-'.$convId1)) {

                $cachedUsers = Cache::get('conversation-'.$convId1);
                
                if(isset($cachedUsers['user_one'])) {
                    
                    if($cachedUsers['user_one'] != Auth::user()->id){
                        
                        $cachedUsers['user_two'] = Auth::user()->id;
                    } else {
                        $cachedUsers['user_one'] = Auth::user()->id;
                    }
                } else {
                    $cachedUsers['user_one'] = Auth::user()->id;
                }
                
                Cache::forever('conversation-'.$convId1,$cachedUsers);
                    
            } elseif(Cache::has('conversation-'.$convId2)) {
                $cachedUsers = Cache::get('conversation-'.$convId2);
                
                if(isset($cachedUsers['user_one'])) {
                    
                    if($cachedUsers['user_one'] != Auth::user()->id){
                        
                        $cachedUsers['user_two'] = Auth::user()->id;
                    } else {
                        $cachedUsers['user_one'] = Auth::user()->id;
                    }
                } else {
                    $cachedUsers['user_one'] = Auth::user()->id;
                }
                
                Cache::forever('conversation-'.$convId2,$cachedUsers);
            } else {
                $users = ['user_one' => Auth::user()->id];
                Cache::forever('conversation-'.$convId1,$users);
            }

        }
        return view('messenger.conversation', compact('messages','withUser','channelUsers','type'));
    }
    
    public function messageSend(Request $request) {
        
        Messenger::setAuthUserId(Auth::user()->id);
        
        $data = $request->all();
        
        Messenger::sendMessageByUserId($data['reciver_id'],nl2br($data['message']));
        
        $conversation_id = Messenger::isConversationExists($data['reciver_id'],Auth::user()->id);

        DB::update('update conversations set deleted_by_users="" where id='.$conversation_id);

        return response()->json([
            'status' => 'success',
            
        ]);
    }

    public function loadMessages($user_id,$offset) {
        Messenger::setAuthUserId(Auth::user()->id);
        
        $conversations = Messenger::getMessagesByUserId($user_id,$offset);
       
    return response()->json([
        'messages' => $conversations->messages,
        
    ]);
    }

    public function deleteConversation($user_id) {

        Messenger::setAuthUserId(Auth::user()->id);
        
        $conversation_id = Messenger::isConversationExists($user_id,Auth::user()->id);

        // raw db? :P
        $conversation = DB::select('select * from conversations where id ='.$conversation_id)[0];
        
        
        if($conversation->deleted_by_users == '') {
            $deleted_by_users = Auth::user()->id;
        } else {
            $delete_users = explode(',',$conversation->deleted_by_users);

            $count = count($delete_users);

            if($count == 1) {
                if($delete_users[0] == Auth::user()->id) {
                    $deleted_by_users = Auth::user()->id;
                    
                }
                    
                else {
                    
                    $deleted_by_users = $delete_users[0].','.Auth::user()->id;
                }
            } 
            
        }
        
        $conversation_all = Messenger::getMessagesByUserId($user_id,0,5000);
        
        if($conversation_all) {
            $messagesSeen = $conversation_all->messages;
            foreach($messagesSeen as $msg) {
                if(Auth::user()->id != $msg->user_id)
                    Messenger::makeSeen($msg->id);
            }
        }
        
        
        DB::update('update conversations set deleted_by_users="'.$deleted_by_users.'" where id='.$conversation_id);

        
        return response()->json([
            'id' => $user_id,
            'status' => 1
        ]);
    }

    public function makeSeen($id) {
        Messenger::setAuthUserId(Auth::user()->id);

        Messenger::makeSeen($id);

        return response()->json(['status'=>1]);
    }
}

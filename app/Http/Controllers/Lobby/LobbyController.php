<?php

namespace App\Http\Controllers\Lobby;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\User;
use App\LobbyInfo;
use App\LobbyLikes;

class LobbyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

     public function show() {
        $user = Auth::user();

        $selectedGames = $user->selectedGame();
        
        broadcast(new \App\Events\userJoinLobby($user,$selectedGames['game_id'],$selectedGames['platform_id']));

        return view('lobby.show',compact('user','selectedGames'));
     }

     public function getUser($id) {
         $user = User::find($id);

         return view('lobby.user',compact('user'));

     }

    public function setInfo(Request $request) {
        $user = Auth::user();

        $data = $request->all();

        $data['user_id'] = $user->id;
        unset($data['_token']);
        $lobbyInfo = LobbyInfo::firstOrCreate(['user_id'=>$data['user_id'],'game_id'=>$data['game_id'],'platform_id'=>$data['platform_id']]);
        if($lobbyInfo) {
            $lobbyInfo->info = $data['info'];
            $lobbyInfo->save();
        }
        return redirect()->back()->with('success','Sukces!');
    }
    
    public function markInfo($status,$user_id) {
        if(Auth::check()) {
            $likes = LobbyLikes::firstOrCreate(['user_id'=>Auth::user()->id,'for_id'=>$user_id]);

            if($likes) {
                if($status != 'neutral') {
                    $likes->value = $status;
                    $likes->save();
                } else {
                    $likes->delete();
                }
            }
        }
        
        return response()->json(['changed'=>true]);
    }

}

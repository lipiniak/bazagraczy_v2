<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Games;
class FeedsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function list(){
        $user = Auth::user();

        $game_id = $user->selectedGame()['game_id'];
        
        
        $selectedGame = $user->selectedGame();
        if($game_id != 0) {
            $game = Games::find($game_id);
        
            $feeds = json_decode($game->feeds,true);
            
        } else {
            $feeds = [];
        }
        
        return view('feeds.feeds',compact('feeds','selectedGame'));
    }

}

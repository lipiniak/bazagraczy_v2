<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Factories\ActivationFactory;
use Socialite;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Mail;
use Validator;

use App\Push;
use Quest;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }
    
    public function logout(Request $request)
{
    $this->performLogout($request);
    return redirect('/login');
}

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/login';
    protected $activationFactory;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ActivationFactory $activationFactory)
    {
        $this->middleware('guest')->except('logout');
        $this->activationFactory = $activationFactory;
    }

    public function authenticated(Request $request, $user)
    {
        if (!$user->activated) {
            $this->activationFactory->sendActivationMail($user);
            
            auth()->logout();

            return back()->with('error','Aktywuj konto przed zalogowaniem.');
        }
        
        
        $user->login_at = date("Y-m-d H:i:s");
        $user->save();

        return redirect()->intended($this->redirectPath());
    }

    public function login(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'email'    => 'required|email',
            'password' => 'required',
        ], [
            'email.email' => 'Nieprawidłowy email',
            'email.required'=> 'Uzupełnij pole',
            'password.required'=>'Uzupełnij pole',
            ]
        );

        $validator->validate();
        

        if (Auth::attempt($request->only('email', 'password'))) {
            $user = Auth::user();

            if (!$user->activated) {
                $this->activationFactory->sendActivationMail($user);
                
                auth()->logout();
    
                return back()->with('error','Aktywuj konto przed zalogowaniem.');
            }

            $user->login_at = date("Y-m-d H:i:s");
            $user->save();

            Quest::doneQuest('quest_register');
            // zalogowany 


            Quest::checkUserQuests();

            return redirect()->intended($this->redirectPath());
        }
        
        $user = User::where('email',$request->email)->first();
        
        if($user) {
            if($user->provider == 'facebook') {
                return redirect('/')
                ->withInput()
                ->withErrors([
                    'email' => 'Konto już istnieje. Zaloguj się przez Facebook',
                ]);
            }
            if($user->provider == 'google') {
                return redirect('/login')
                ->withInput()
                ->withErrors([
                    'email' => 'Konto już istnieje. Zaloguj się przez Google',
                ]);
            }
        }

        return redirect('/login')
            ->withInput()
            ->withErrors([
                'email' => 'Niepoprawny email lub hasło',
            ]);
         
     
    }


    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that 
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($user, $provider);
        
        if(!$authUser) {
            return redirect('/login')->with('error','Adres email z konta socjalnego jest juz zarejestrowny jako konto nie socjalne.');
        } 

        $authUser->login_at = date("Y-m-d H:i:s");
        $authUser->save();

        Auth::login($authUser, true);
        Quest::doneQuest('quest_register');
        // zalogowany 
        Quest::checkUserQuests();   
        return redirect($this->redirectTo);
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider)
    {
        
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            $authUser->login_at = date("Y-m-d H:i:s");
            $authUser->save();
            return $authUser;
        }
        
        // szukamy po majlu
        $emailUser = User::where('email',$user->email)->first();

        // jesli jest po majlu to false
        if($emailUser) {
            return false;
        }

        // Mail o rejestracji przez socjal. 
        // Wyłączam z powodu problemu z facebookiem.
        
        //Mail::to($user->email)->send(new \App\Mail\UserActivationSocial);

        $str = $user->name;
        $str = $this->removeAccents($str);
        $user->name = str_replace(" ", "_", $str);
        
        $user1 = User::create([
            'login'     => $user->name,
            'email'    => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id,
            'login_at' => date("Y-m-d H:i:s"),
            'activated' => true,
        ]);
        
        $push = array('content' => '<small><strong>TIP:</strong> Wybierz tytuł gry i platformę w prawym górnym rogu, aby wyświetlać interesującą cię zawartość serwisu, gdyż jest ona uzależniona od tego wyboru.</small>','user_id'=>$user1->id,'route_name'=>'');
        $push2 = array('content' => '<small><strong>TIP:</strong> Wyszukiwarka umożliwia znalezienie graczy, którzy wśród gier dodanych w swojej bibliotece posiadają tytuł identyczny z tym, który masz aktualnie wybrany w prawym górnym rogu serwisu.</small>','user_id'=>$user1->id,'route_name'=>'search.search');
        $push3 = array('content' => '<small><strong>TIP:</strong> Wyszukiwarka umożliwia znalezienie klanów, które wśród utworzonych drużyn posiadają drużynę grającą w grę identyczną z tą, którą masz aktualnie wybraną w prawym górnym rogu serwisu.</small>','user_id'=>$user1->id,'route_name'=>'clan.search');
        $push4 = array('content' => '<small><strong>TIP:</strong> Uzupełnij swoją bibliotekę gier! Uzupełnienie biblioteki umożliwi innym użytkownikom wyszukanie twojego profilu w wyszukiwarce.</small>','user_id'=>$user1->id,'route_name'=>'profile.user');

        Push::create($push);
        Push::create($push2);
        Push::create($push3);
        Push::create($push4);
        

        $data['user_id'] = $user1->id;
        
        \App\UserSettings::create($data);

        
        return $user1;
    }

    public function removeAccents($str) {
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η');
        return str_replace($a, $b, $str);
      }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use DB;
use App\User;
use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use Hash;
class PasswordResetController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showSendEmailForm() {
        return view('auth.passwords.email');
    }

    
    public function sendResetEmail(Request $request) {

        $data = $request->all();
        $validator = Validator::make($data, [
            'email' => 'required|email'
        ],[
            'email.required' => 'Pole jest wymagane',
            'email.email' => 'Nieprawidłowy email',
        ]);
        
        $validator->validate();

        $user = User::where ('email', $data['email'])->first();

        if ( !$user ) 
            return redirect()->back()->withErrors(['email'=>'Konto o padanym adresie email nie istnieje.'])->withInput();

        // Tworzymy token z wpisem
        $token = str_random(60);
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        $email = $data['email'];

        Mail::to($email)->send(new \App\Mail\UserResetPasswordEmail($token));



        return redirect('/login')->with('success','Hasło zostało zresetowane. Odbierz pocztę, aby nadać nowe hasło.');
    }

    public function showResetForm($token) {
        $reset = DB::table('password_resets')->select('token','email')->where('token',$token)->first();
        if ( !$reset ) return redirect('/login')->withErrors('Błędny token');
        
        return view('auth.passwords.reset',compact('reset'));
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            
            'password' => 'required|string|min:4|max:50|confirmed',
            'password_confirmation' => 'required',
        
        ],[


            'password.min' => 'Hasło za krótkie (min. 4 znaki)',
            'password.max' => 'Hasło za długie (max. 50 znaków)',
            'password.required' => 'Uzupełnij pole',
            'password.confirmed' => 'Hasło i jego powtórzenie muszą być jednakowe',
            
            'password_confirmation.required' => 'Uzupełnij pole',

        ]);
    }

    public function resetPassword(Request $request) {

        $data = $request->all();

        $this->validator($data)->validate();

        $user = User::where('email', $data['email'])->first();

        if ( !$user ) return redirect('/login')->withErrors('Nie można znaleść użytkownika'); //or wherever you want

        $user->password = Hash::make($data['password']);
        $user->update(); //or $user->save();

        DB::table('password_resets')->where('email', $user->email)->delete();

        return redirect('/login')->with('success','Hasło zostało zmienione.');
    }
}

<?php

namespace App\Http\Controllers\Auth;
use App\Factories\ActivationFactory;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\UserSettings;
use App\Push;
use Quest;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    protected $activationFactory;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ActivationFactory $activationFactory)
    {
        $this->middleware('guest');
        $this->activationFactory = $activationFactory;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'login' => 'required|max:25|min:2|regex:/^[a-zA-Z0-9-_.AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpRrSsTtUuWwYyZz]+$/u',
            'email' => 'required|string|email|max:50|unique:users',
            'password' => 'required|string|min:4|max:50|confirmed',
            'password_confirmation' => 'required',
            'regulations' => 'required',
            //'g-recaptcha-response' => 'required|recaptcha',
        ],[
            'login.required' => 'Uzupełnij pole',
            'login.max' => 'Nazwa za długa (max. 25 znaków)',
            'login.min' => 'Nazwa za króka (min. 2 znaki)',
            'login.regex' => 'Uzyto niedozwolonych znaków <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Nazwa może składać się z cyfr (0-9), liter (a-z; A-Z; bez polskich znaków) oraz \'-\' (myślnik) i \'_\' (podkreślenie). Nie może zawierać spacji."><i class="fas fa-question-circle"></i></a>',
            'email.unique' => 'Konto już istnieje',
            'email.required' => 'Uzupełnij pole',
            'email.email' => 'Nieprawidłowy email',
            'email.max' => 'Email za długi (max. 50 znaków)',

            'password.min' => 'Hasło za krótkie (min. 4 znaki)',
            'password.max' => 'Hasło za długie (max. 50 znaków)',
            'password.required' => 'Uzupełnij pole',
            'password.confirmed' => 'Hasło i jego powtórzenie muszą być jednakowe',
            
            'password_confirmation.required' => 'Uzupełnij pole',

            'regulations.required' => 'Wymagana akceptacja',

            'g-recaptcha-response:required' => 'Pole jest wymagane',
            'g-recaptcha-response:recaptcha' => 'Jesteś robotem?'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $user = User::create([
            'login' => $data['login'],
            'email' => $data['email'],
            'provider' => 'bazagraczy',
            'password' => bcrypt($data['password']),
        ]);

        $data['user_id'] = $user->id;
        
        UserSettings::create($data);
        // Przygotowanie tokena
        // Wyslanie maila 
        // strona z informacja o wyslanym mailu.

        return $user;
    }

    public function register(Request $request) {

        $data = $request->all();

        $this->validator($data)->validate();

        $user = $this->create($data);

        $push = array('content' => '<strong>Witaj Graczu!</strong> Aby rozpocząć, ustaw w prawym górnym rogu ekranu tytuł gry, co powozwoli na wyświetlanie interesującej cię zawartość serwisu.','user_id'=>$user->id);
        $push2 = array('content' => '<strong>Uwaga!</strong> Jeżeli nie możesz znaleźć interesującej cię gry, zgłoś nam to, a rozważymy jej dodanie.','user_id'=>$user->id);

        Push::create($push);
        Push::create($push2);

        $this->activationFactory->sendActivationMail($user);

        
        return redirect('/login')->with('success', 'Konto zostało zarejestrowane.');
    }

    public function activateUser($token)
    {
        if ($user = $this->activationFactory->activateUser($token)) {
            
            //auth()->login($user);
            return redirect('/login')->with('success', 'Konto zostało aktywowane. Możesz się teraz zalogować.');
        }
        // redirecct with error
            return redirect('/login')->with('error', 'Link aktywacyjny został już wykorzystany.');
        abort(404);
    }



    public function aregisterFinish() {
        return view('auth.finish');
    }

}

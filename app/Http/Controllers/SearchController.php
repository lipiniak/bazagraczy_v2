<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Games;
use App\User;
use DB;
use Auth;
use Cache;

class SearchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        $games = Games::orderBy('name')->get();

        $data['games'][0] = '';
        foreach($games as $game) {
            $data['games'][$game->id] = $game->name;
        }
        
        $voivodeship = \App\Voivodeship::all();
        
        $data['voivodeship'][0] = '';
        foreach($voivodeship as $voivode) {
            $data['voivodeship'][$voivode->id] = $voivode->name; 
        }
        $data['city'][0] = '';
        $data['platform'][0] = '';
        $new = true;

        $selectedGame = Auth::user()->selectedGame();
        
   
        

        $users =  User::where('activated',1)->orderBy('login_at','DESC')->paginate(500);


        return view('search.search', compact('data','new','selectedGame','users'));
    }

    public function doSearching(Request $request) {

        
        $formData = $request->all();

        
        $games = Games::orderBy('name')->get();

        $data['games'][0] = '';
        foreach($games as $game) {
            $data['games'][$game->id] = $game->name;
        }
        
        $voivodeship = \App\Voivodeship::all();
        
        $data['voivodeship'][0] = '';
        foreach($voivodeship as $voivode) {
            $data['voivodeship'][$voivode->id] = $voivode->name; 
        }

        $new = false;


        // składamy zapytanie

        
        
        if(!empty($formData['voivodeship'])) {
            $where['voivodeship'] = "voivodeship = ".$formData['voivodeship']."";
        }
        if(!empty($formData['city'])) {
            $where['city'] = "city = ".$formData['city']."";
        }
        if(!empty($formData['sex'])) {
            $where['sex'] = "sex = '".$formData['sex']."'";
        }
        if(!empty($formData['game']) && $formData['game'] !=0) {
            $join['game'] = "LEFT JOIN users_games_platforms ugp ON ugp.user_id = users.id";
            $where['game'] = "ugp.games_id = ".$formData['game'];
        }
        if(!empty($formData['age'])) {
            switch($formData['age']) {
                case 1:
                    $where['date'] = "TIMESTAMPDIFF(YEAR,birth_date,CURDATE()) < 10";
                break;
                case 2:
                    $where['date'] = "TIMESTAMPDIFF(YEAR,birth_date,CURDATE()) BETWEEN 10 AND 12";
                break;
                case 3:
                    $where['date'] = "TIMESTAMPDIFF(YEAR,birth_date,CURDATE()) BETWEEN 13 AND 15";
                break;
                case 4:
                    $where['date'] = "TIMESTAMPDIFF(YEAR,birth_date,CURDATE()) >= 16";
                break;
                case 5:
                    $where['date'] = "TIMESTAMPDIFF(YEAR,birth_date,CURDATE()) >= 18";
                break;
                case 6:
                    $where['date'] = "TIMESTAMPDIFF(YEAR,birth_date,CURDATE()) >= 25";
                break;
                case 7:
                    $where['date'] = "TIMESTAMPDIFF(YEAR,birth_date,CURDATE()) >= 30";
                break;
            }
            
        }
        $online = false;
        $clan = false;
        if(!empty($formData['filters'])) {
            /*
                <option value="1">Obserwuję</option>
                <option value="2">Szuka klanu</option>
                <option value="3">Z notatką</option>
                <option value="4">Tylko online</option>
            */
            foreach($formData['filters'] as $filter) {
                if($filter == 4) {
                    $online = true;
                } 
                if($filter == 1) {
                    $where['followers'] = 'fol.user_id ='.Auth::user()->id;
                    $join['followers'] = 'left join followers_user fol on followers_id = users.id';
                }
                if($filter == 3) {
                    $join['note'] = 'left join notes on notes.note_for_id = users.id';
                    $where['note'] = 'notes.user_id ='.Auth::user()->id;
                }
                if($filter == 2) {
                    $join['clan'] = 'left join user_settings on user_settings.user_id = users.id';
                    $where['clan'] = 'user_settings.lfc = 1';
                }
            }
        }



        if(!empty($formData['keyword'])) {
            $formData['keyword'] = str_replace('_','\_',$formData['keyword']);
            //$join['keyword'] = 'inner join tags on tags.user_id = users.id';
            //$where['keyword'] = "login like '%".$formData['keyword']."%'";
            $usersNicks = DB::select("select distinct(id) from users where login like '%".$formData['keyword']."%'");
            
            $nicks = array();
            
            foreach($usersNicks as $nick) {
                $nicks[] = $nick->id;
            }

            $userTags = DB::select("select distinct(user_id) from tags where tag like '%".$formData['keyword']."%'");
            $keyword = array();

            foreach($userTags as $usr) {
                $keyword[] = $usr->user_id ;
            }
        
        } 


        if(!empty($formData['platform']) && $formData['platform'] != 0) {
            
            $where['platform'] = "ugp.platforms_id = ".$formData['platform'];
        }

        if(isset($order)) {
            $sqlOrder = " ORDER BY ".$order['o'];
        } else {
            $sqlOrder = '';
        }

        if(isset($where))
            $sqlWhere = 'WHERE '.implode(' and ',$where);
            
        else 
            $sqlWhere = '';

        if(isset($join))
            $sqlJoin = implode(' ',$join);
        else 
            $sqlJoin = '';

        $order['field'] = 'login_at';
        $order['order'] = 'DESC';

        $result = DB::select('SELECT DISTINCT(users.id)
                                FROM users '.$sqlJoin.'
                                '
                                .$sqlWhere.''
                                .$sqlOrder);

        $userss = array();
        foreach($result as $user) {
            //print_r($user->id);
            

            if($online) {
                if(Cache::has('user-is-online-' . $user->id))
                    $userss[] = $user->id;
            } else {
                $userss[] = $user->id;
            }
        }

        $users = array();

        
        
        if(empty($userss) && empty($keyword) && empty($nicks)) {
            $users = array();
        } elseif(empty($userss) && !empty($keyword) && empty($nicks)) {
            $users = $keyword;
        } elseif(empty($userss) && empty($keyword) && !empty($nicks)) {
            $users = $nicks;
        } elseif(empty($userss) && !empty($keyword) && !empty($nicks)) {
            $users = array_merge($keyword,$nicks);
        } elseif(!empty($userss) && empty($keyword) && empty($nicks)) {
            if(empty($formData['keyword']))
                $users = $userss;
            else 
                $users = array();
        } elseif(!empty($userss) && !empty($keyword) && empty($nicks)) {
            $users = array_intersect($userss,$keyword);
        } elseif(!empty($userss) && empty($keyword) && !empty($nicks)) {
            
            $users = array_intersect($userss,$nicks);
        } else {
         
            
            $array = array_merge($keyword,$nicks);
         
            $users = array_intersect($userss,$array);

        }

     
        $users = !empty($users) ? User::whereIn('id', $users)->where('activated',1)->orderBy($order['field'],$order['order'])->paginate(500) : false;


        $data['city'][0] = '';

        $cities = \App\Cities::where('voivodeship_id',$formData['voivodeship'])->get();

        foreach($cities as $item) {
            $data['city'][$item->id] = $item->name;
        }
        
        $data['platform'][0] = '';
        if(!empty($formData['game'])) {
            $game = Games::find($formData['game']);

            foreach($game->platforms as $platform) {
                $data['platform'][$platform->id] = $platform->short_name;
            }
        }
        
        
        $selectedGame = Auth::user()->selectedGame();

        return view('search.search', compact('users','data','formData','new','selectedGame'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blocked;
use Auth;
use App\Followers;

class BlockController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function blockAdd($blockedId) {

        if(!Auth::check())
        {
            return view('errors.dostep');
        }
        $followers = Followers::where('followers_id',$blockedId)->where('user_id',Auth::user()->id);
        

        if(!empty($followers->first())) {
            Followers::where('id', $followers->first()->id)->delete();
        }

        $data = array(
            'user_id' => Auth::user()->id,
            'blocked_id' => $blockedId,
        );

        Blocked::create($data);

        return redirect()->back()->with('success','Gracz został zablokowany.');
    }

    public function blockRemove($blockedId) {
        
        if(!Auth::check())
        {
            return view('errors.dostep');
        }
        
        Blocked::where('user_id', Auth::user()->id)->where('blocked_id',$blockedId)->delete();
        

        return redirect()->back()->with('success','Gracz został odblokowany.');
    }

}

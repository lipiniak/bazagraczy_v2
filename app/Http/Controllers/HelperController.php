<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Voivodeship;

use App\Cities;
use App\Games;
use App\Clan;
use App\Team;
use App\UserSettings;
use Auth;

class HelperController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getVoivodeships() {

        $voivodeship = Voivodeship::all();
        
        $d = 0;
        $data[$d]['id'] = 0;
        $data[$d]['text'] = '';
        $d++;
        foreach($voivodeship as $item){
            $data[$d]['id'] = $item->id;
            $data[$d]['text'] = $item->name;
            $d++;
        }

        $result['results'] = $data;

        return response()->json($result);
    }

    public function getCities($voivodeship_id) {

        $cities = Cities::where('voivodeship_id',$voivodeship_id)->get();

        $d = 0;
        $data[$d]['id'] = 0;
        $data[$d]['text'] = '';
        $d++;
        foreach($cities as $item){
            $data[$d]['id'] = $item->id;
            $data[$d]['text'] = $item->name;
            $d++;
        }
        $result['results'] = $data;

        return response()->json($result);
    }

    public function getGames() {
        
        $games = Games::orderBy('name')->get();
        
        $i = 0;

        foreach($games as $game){
            
                $data[$i]['id'] = $game->id;
                $data[$i]['text'] = $game->name;
                $i++;
            
        }

        $result['results'] = $data;

        return response()->json($result);
    }
    public function getGamesPlatform($game_id) {

        if($game_id == 0){
            $platforms[] = array(
                'id' => 0,
                'text' => ''
            );


        } else {
            $game = Games::find($game_id);
        

            foreach($game->platforms as $platform) {
                $platforms[] = array(
                    'id' => $platform->id,
                    'text' => $platform->short_name,
                );
    
            }
            
        }
        return response()->json($platforms);
    }

    public function getClanTeams($clan_id) {

        $user = Auth::user();

        $selectedGame = $user->selectedGame();

        if($clan_id == 0) {
            $teams[] = array(
                'id' => 0,
                'text' => '',
            );
        } else {
            $clan = Clan::find($clan_id);
            
            foreach($clan->teams as $team) {
                if($clan->leader_id == $user->id) {
                    if($team->game_id == $selectedGame['game_id'] && $team->platform_id == $selectedGame['platform_id']) {
                        $teams[] = array(
                            'id' => $team->id,
                            'text' => $team->name,
                        );
                    }
                } elseif($team->leader_id == $user->id) {
                    if($team->game_id == $selectedGame['game_id'] && $team->platform_id == $selectedGame['platform_id'] && $team->leader_id == $user->id) {
                        $teams[] = array(
                            'id' => $team->id,
                            'text' => $team->name,
                        );
                    }
                }
                
            }
        }

        return response()->json($teams);
    }
    
    public function getTeamMembers($team_id) {
        
        if($team_id ==0) {
            $members[] = array(
                'id' => 0,
                'text' => '',
            );
        } else {

            $team = Team::find($team_id);
            
            foreach($team->members as $member) {
                $members[] = array(
                    'id'=> $member->id,
                    'text'=>$member->login,
                );
            }
            
        }

        if(empty($members))
            $members = array();

        return response()->json($members);
    }

    public function setFriendList(){
        
        $setting = UserSettings::where('user_id',Auth::user()->id)->first();
        
        if($setting->friends_list_status == 0)
            $setting->friends_list_status = 1;
        else
            $setting->friends_list_status = 0;

        $setting->save();
        
        return 'true';

    }

    public function getUserInfo(Request $request) {
        $data = $request->all();


        if(empty($data['user_id']))
            return response()->json(['error' => 'No user ID']);

        
        $user = User::findOrFail($data['user_id']);
        
    }
}

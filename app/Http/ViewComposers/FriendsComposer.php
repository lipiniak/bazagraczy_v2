<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Repositories\UserRepository;
use App\User;
use Auth;
use Cache;

class FriendsComposer
{
    
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        
    }

    /**
     * Bind data to the view.\
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if(Auth::check()) {

            $friends = array();

            $friends = Auth::user()->myfollowers;
            
            $friends_count = count($friends);
            
            $view->with('friends', ($friends));
            $view->with('friends_count', ($friends_count));
            
        }
        
    }
}
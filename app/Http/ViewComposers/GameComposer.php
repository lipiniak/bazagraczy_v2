<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Repositories\UserRepository;
use App\Games;
use App\User;
use Auth;
use Cache;
class GameComposer
{
    
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if(Auth::check()) {

            $games = Cache::remember('composer_games', $minutes='1440', function()
            {
                return Games::orderBy('name')->get();
            }); 
            
            $i = 0;

            foreach($games as $game){
                
                    $data[$i]['id'] = $game->id;
                    $data[$i]['text'] = $game->name;
                    $i++;
                
            }
        
            $user = User::find(Auth::user()->id);
            
            $selectedGame = $user->selectedGame();
            
            if($selectedGame['game_id'] == 0) 
                $game = array();
            else
                $game = Games::find($selectedGame['game_id']);
            
            $platforms = [];
            if(!empty($game)) {
                
                foreach($game->platforms as $platform) {
                    
                    $platforms[] = array(
                        'id' => $platform->id,
                        'text' => $platform->short_name,
                    );
    
                }
            } else {
                $platforms[] = array(
                    'id' => 'empty',
                    'text' => '',
                );
            }

            
            $view->with('user_select', ($user->selectedGame()));
            $view->with('platforms', ($platforms));
            $view->with('games', ($data));
        }
        
    }
}
<?php

namespace App\Http\Middleware;

use Closure;
use Menu;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('mainMenu', function ($menu) {
            $menu->add('Dashboard', ['route'=>'admin.dashboard'])->prepend('<i class="fa fa-dashboard "></i><span>')->append('</span>');
            $menu->add('Użytkownicy')->prepend('<i class="fa fa-users "></i><span>')->append('</span>')->nickname('users');
                $menu->item('users')->add('Lista użytkowników', ['route'=>'admin.user.list'])->prepend('<i class="fa fa-circle-o"></i><span>')->append('</span>');
            $menu->add('Gry')->prepend('<i class="fa fa-shield "></i><span>')->append('</span>')->nickname('games');
                $menu->item('games')->add('Lista gier', ['route'=>'admin.game.list'])->prepend('<i class="fa fa-circle-o"></i><span>')->append('</span>');
                $menu->item('games')->add('Dodaj grę', ['route'=>'admin.game.create'])->prepend('<i class="fa fa-circle-o"></i><span>')->append('</span>');
                $menu->item('games')->add('Scrapper', ['route'=>'admin.game.scrapper'])->prepend('<i class="fa fa-circle-o"></i><span>')->append('</span>');
            $menu->add('Posty')->prepend('<i class="fa fa-list "></i><span>')->append('</span>')->nickname('posts');
                $menu->item('posts')->add('Lista postów', ['route'=>'admin.post.list'])->prepend('<i class="fa fa-circle-o"></i><span>')->append('</span>');
                $menu->item('posts')->add('Dodaj post', ['route'=>'admin.post.create'])->prepend('<i class="fa fa-circle-o"></i><span>')->append('</span>');
            $menu->add('Zgłoszenia')->prepend('<i class="fa fa-list "></i><span>')->append('</span>')->nickname('tickets');
                $menu->item('tickets')->add('Lista zgłoszeń', ['route'=>'admin.tickets.list'])->prepend('<i class="fa fa-circle-o"></i><span>')->append('</span>');
            $menu->add('Giveawaye')->prepend('<i class="fa fa-star "></i><span>')->append('</span>')->nickname('giveaway');
                $menu->item('giveaway')->add('Lista konkursów', ['route'=>'admin.giveaway.list'])->prepend('<i class="fa fa-circle-o"></i><span>')->append('</span>'); 
                $menu->item('giveaway')->add('Lista wymagań', ['route'=>'admin.quest.list'])->prepend('<i class="fa fa-circle-o"></i><span>')->append('</span>'); 
                $menu->item('giveaway')->add('Lista zadań', ['route'=>'admin.quest.list'])->prepend('<i class="fa fa-circle-o"></i><span>')->append('</span>'); 
        });


        return $next($request);
    }
}

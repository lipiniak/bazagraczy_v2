<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Carbon\Carbon;
use Cache;

class LogLastUserActivity
{
    protected $except = [
        '/heartbeep','/online/check'
    ];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()) {
            $user = Auth::user();
            //$expiresAt = Carbon::now()->addMinutes(5);
            //Cache::put('user-is-online-' . Auth::user()->id, true, $expiresAt);
            if(Cache::has('user-is-online-'.Auth::user()->id)) {
                $expiresAt = Carbon::now()->addMinutes(5);
                $status = Cache::get('user-is-online-' . Auth::user()->id);

                if($status == 'online')
                    Cache::put('user-is-online-' . Auth::user()->id, 'online', $expiresAt);
                else {
                    Cache::put('user-is-online-' . Auth::user()->id, 'online', $expiresAt);
                    
                    if(Cache::has('online_users_list')) {
                        $onlineUsers = Cache::get('online_users_list');
                        
                        foreach($onlineUsers as $follower) {
                            broadcast(new \App\Events\FollowerLogin($follower,$user->id));
                        }
                    }
                }
            } else {
                $minutes = 5;

                $isOnline = Cache::remember('user-is-online-'.Auth::user()->id, $minutes, function() {
                    $user = Auth::user();
                    
                    $user->login_at = date('Y-m-d H:i:s');
                    $user->save();

                    if(Cache::has('online_users_list')) {
                        $onlineUsers = Cache::get('online_users_list');
                        
                        foreach($onlineUsers as $follower) {
                            broadcast(new \App\Events\FollowerLogin($follower,$user->id));
                        }
                    }

                    

                    return 'online';
                });
            }
            

        }
        return $next($request);
    }
}

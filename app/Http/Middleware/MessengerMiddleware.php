<?php
namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;

use Messenger;

class MessengerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            Messenger::setAuthUserId(Auth::guard($guard)->user()->id);
        }
        return $next($request);
    }
}
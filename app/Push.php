<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Push extends Model
{
    protected $table = 'push_messages';
    
    protected $fillable = [
        'user_id', 'is_seen', 'content','route_name'
    ];

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';
    
    protected $fillable = [
        'user_id', 'data','message','type','status','readed'
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getData() {
        $dataParam = json_decode($this->data,true);

        if(empty($dataParam))
            return ['type'=>'empty'];
        $types = array_keys($dataParam);
        
        switch($types[0]) {
            case 'user_id':
                $data['type'] = 'user';
                $data['content'] = User::find($dataParam['user_id']);
            break;
            case 'clan_id':
                $data['type'] = 'clan';
                $data['content'] = Clan::find($dataParam['clan_id']);
            break;
        }

        return $data;
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;



use App\User;

class FillNotificationSetting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:fill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Wypełnienie ustawień notyfikacji listą gier z biblioteki';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $users = User::all();

        foreach($users as $user) {
            $games = '';
            foreach($user->games as $game) {
                foreach($game->userPlatforms()->where('user_id', $user->id)->get() as $item) {
                    
                    $games[] = $game->id.'-'.$item->id;
                }
            }
            

            $settings = \App\UserSettings::where('user_id',$user->id)->first();
            $settings->selected_notify_games = json_encode($games);
            $settings->save();
            
        }
        

    }
}

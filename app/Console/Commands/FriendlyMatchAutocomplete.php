<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\FriendlyMatch;
use DB;
class FriendlyMatchAutocomplete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'match:autocomplete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Need action info';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * 
     * @return mixed
     */
    public function handle()
    {
        //Setting need action flag for all sides of the match on last 30 minutes to match expire.
        
        $matches = FriendlyMatch::where('status',2)->whereRaw('expire_at >= NOW()')->get();

        // sprawdzic statusy meczu warunki i zaakceptowaća albo odrzucic ... kurwa 
        foreach($matches as $match) {
            if($match->hasResult()) {
                // to sprawdzic kto wysłał i wysłać wyniki i powiadomienai
                
            } else {
                
                $match->result = 4;
                $match->status = 3;
                $match->host_need_action = 0;
                $match->enemy_need_action = 0;

                // powiadom strony o wyniku. 
                $info = [
                    'match_id' => $match->id,
                    'host_clan_id' => $match->hostClan->id,
                    'host_clan_link' => $match->hostClan->link,
                    'host_clan_name' => $match->hostClan->name,
                    'enemy_clan_id' => $match->enemyClan->id,
                    'enemy_clan_link' => $match->enemyClan->link,
                    'enemy_clan_name' => $match->enemyClan->name,
                ];
                // członkowie meczu wszyscy
                $hostLeader = User::find($match->hostClan->leader_id);
                $hostTeamLeader = User::find($match->hostTeam->leader_id);

                $enemyLeader = User::find($match->enemyClan->leader_id);
                $enemyTeamLeader = User::find($match->enemyTeam->leader_id);
                
                if($hostLeader->id == $hostTeamLeader->id) 
                    $hostLeader->notify(new FriendlyMatchCancel($info));
                else {
                    $hostLeader->notify(new FriendlyMatchCancel($info));
                    $hostTeamLeader->notify(new FriendlyMatchCancel($info));
                }
                if($enemyLeader->id == $enemyTeamLeader->id) {
                    $enemyLeader->notify(new FriendlyMatchCancel($info));
                    
                } else {
                    $enemyLeader->notify(new FriendlyMatchCancel($info));
                    $enemyTeamLeader->notify(new FriendlyMatchCancel($info));    
                }

                $participants = $match->hostMembers();
                
                $participants = $participants->merge($match->enemyMembers());

                 $users = array();
                foreach($participants as $member) {
                    if($member->id != $hostLeader->id && $member->id != $hostTeamLeader->id && $member->id != $enemyLeader->id && $member->id != $enemyTeamLeader->id)
                        $users[] = $member->id;
                }
                
                $participants = User::find($users);

                Notification::send($participants, new FriendlyMatchCancel($info));

                
                $match->save();
                
                $matchResult = FriendlyMatchResult::where('match_id',$match->id)->firstOrFail();

                if($matchResult)
                    $matchResult->delete();
            }
            
        }
        
        
    }
}

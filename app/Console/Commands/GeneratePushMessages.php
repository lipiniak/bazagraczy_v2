<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Push;

class GeneratePushMessages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate push mesages for all users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        $users = User::all();


        foreach($users as $user) {

            
            $push = array('content' => '<small><strong>TIP:</strong> Wybierz tytuł gry i platformę w prawym górnym rogu, aby wyświetlać interesującą cię zawartość serwisu, gdyż jest ona uzależniona od tego wyboru.</small>','user_id'=>$user->id,'route_name'=>'');
            $push2 = array('content' => '<small><strong>TIP:</strong> Wyszukiwarka umożliwia znalezienie graczy, którzy wśród gier dodanych w swojej bibliotece posiadają tytuł identyczny z tym, który masz aktualnie wybrany w prawym górnym rogu serwisu.</small>','user_id'=>$user->id,'route_name'=>'search.search');
            $push3 = array('content' => '<small><strong>TIP:</strong> Wyszukiwarka umożliwia znalezienie klanów, które wśród utworzonych drużyn posiadają drużynę grającą w grę identyczną z tą, którą masz aktualnie wybraną w prawym górnym rogu serwisu.</small>','user_id'=>$user->id,'route_name'=>'clan.search');
            $push4 = array('content' => '<small><strong>TIP:</strong> Uzupełnij swoją bibliotekę gier! Uzupełnienie biblioteki umożliwi innym użytkownikom wyszukanie twojego profilu w wyszukiwarce.</small>','user_id'=>$user->id,'route_name'=>'profile.user');

            Push::create($push);
            Push::create($push2);
            Push::create($push3);
            Push::create($push4);

        }
    }
}

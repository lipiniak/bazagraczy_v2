<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;


use App;
use App\User;
use Redis;
use Cache;

class ChangeUserStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:change';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Zmiana statusu usera';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(App::environment('local'))
            $keys = Redis::keys('laravel_cache:user-is-online-*');
        else {
            $keys = Redis::keys('bazagraczy_cache:user-is-online-*');
        }

        $users = array();
        foreach($keys as $user) {
            $split = explode('-',$user);

            $users[] = array_pop($split);
            
        }
        
        if(Cache::has('online_users_list')) {
            $oldUsers = Cache::get('online_users_list');
            
            
            $offlineUsers = array_diff($oldUsers,$users);

            foreach($offlineUsers as $user) {
                $userModel = User::find($user);
                // set online time
                $loginTime = strtotime($userModel->login_at);
                $now = time();

                $onlineTime = $now - $loginTime;

                $userModel->online_time += $onlineTime;
                
                $userModel->save();

                foreach($users as $follower)
                    broadcast(new \App\Events\FollowerLogout($follower,$userModel->id));
                 
                Cache::forget('lobby-status-'.$userModel->id);
                Cache::forget('user-is-in-lobby-'.$userModel->id);
            }

            Cache::forever('online_users_list',$users);
        } else {
            Cache::forever('online_users_list',$users);
        }
        
    }
}

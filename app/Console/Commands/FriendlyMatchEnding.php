<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\FriendlyMatch;

class FriendlyMatchEnding extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'match:ending';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Zmiana statusu meczy co minute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // sprawdzamy czy czas jest taki jak czas konca meczu i zmieniamy status meczu na 3

        $today = date('Y-m-d');
        $now = date('H:i:s');
        $todayMatches = FriendlyMatch::where('match_date',$today)->where('match_time',$now)->get();

        foreach($todayMatches as $match) {
            $match->status = 3;
            $match->save();
        }

    }
}

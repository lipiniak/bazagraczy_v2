<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\FriendlyMatch;
use App\FriendlyMatchResult;
use DB;
use App\User;
use App\Notifications\FriendlyMatchEnded;
use Notifications;

class FriendlyMatchAutoAccept extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'match:autoaccept';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Autoaccept match result';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * 
     * @return mixed
     */
    public function handle()
    {
        
        $results = FriendlyMatchResult::whereRaw('expire_at <= NOW()')->get();

        
        foreach($results as $result) {

            $match = FriendlyMatch::find($result->match_id);
            
            if($match->status == 3) {
                continue;
            }
    
            if($match->result > 0)
                continue;

            

            if($result->host_aproved == 1) {
                $match->status = 3;
                $match->result = $result->host_result;
                
                if($result->host_result == 1)
                    $match->winner_id = 1;

                if($result->host_result == 2)
                    $match->winner_id = 1;

                $match->host_need_action = 0;
                $match->enemy_need_action = 0;   
                
                $info = [
                    'match_id' => $match->id,
                    'host_clan_link' => $match->hostClan->link,
                    'host_clan_name' => $match->hostClan->name,
                    'enemy_clan_link' => $match->enemyClan->link,
                    'enemy_clan_name' => $match->enemyClan->link,
                ];
                
                $match->save();
                $result->delete();

                $hostLeader = User::find($match->hostClan->leader_id);
                $hostTeamLeader = User::find($match->hostTeam->leader_id);

                $enemyLeader = User::find($match->enemyClan->leader_id);
                $enemyTeamLeader = User::find($match->enemyTeam->leader_id);
                
                if($hostLeader->id == $hostTeamLeader->id) 
                    $hostLeader->notify(new FriendlyMatchEnded($info));
                else {
                    $hostLeader->notify(new FriendlyMatchEnded($info));
                    $hostTeamLeader->notify(new FriendlyMatchEnded($info));
                }
                if($enemyLeader->id == $enemyTeamLeader->id) {
                    $enemyLeader->notify(new FriendlyMatchEnded($info));
                    
                } else {
                    $enemyLeader->notify(new FriendlyMatchEnded($info));
                    $enemyTeamLeader->notify(new FriendlyMatchEnded($info));    
                }

                $participants = $match->hostMembers();
                
                $participants = $participants->merge($match->enemyMembers());

                 $users = array();
                foreach($participants as $member) {
                    if($member->id != $hostLeader->id && $member->id != $hostTeamLeader->id && $member->id != $enemyLeader->id && $member->id != $enemyTeamLeader->id)
                        $users[] = $member->id;
                }
                
                $participants = User::find($users);

                Notification::send($participants, new FriendlyMatchEnded($info));
                
                
                
            } elseif($result->enemy_aproved == 1) {
                $match->status = 3;
                $match->result = $result->enemy_result;

                if($result->enemy_result == 1)
                    $match->winner_id = 1;
                    
                if($result->enemy_result == 2)
                    $match->winner_id = 1;

                $match->host_need_action = 0;
                $match->enemy_need_action = 0;   
                
                $info = [
                    'match_id' => $match->id,
                    'host_clan_link' => $match->hostClan->link,
                    'host_clan_name' => $match->hostClan->name,
                    'enemy_clan_link' => $match->enemyClan->link,
                    'enemy_clan_name' => $match->enemyClan->link,
                ];
                
                $match->save();
                $result->delete();
                
                $hostLeader = User::find($match->hostClan->leader_id);
                $hostTeamLeader = User::find($match->hostTeam->leader_id);

                $enemyLeader = User::find($match->enemyClan->leader_id);
                $enemyTeamLeader = User::find($match->enemyTeam->leader_id);
                
                if($hostLeader->id == $hostTeamLeader->id) 
                    $hostLeader->notify(new FriendlyMatchEnded($info));
                else {
                    $hostLeader->notify(new FriendlyMatchEnded($info));
                    $hostTeamLeader->notify(new FriendlyMatchEnded($info));
                }
                if($enemyLeader->id == $enemyTeamLeader->id) {
                    $enemyLeader->notify(new FriendlyMatchEnded($info));
                    
                } else {
                    $enemyLeader->notify(new FriendlyMatchEnded($info));
                    $enemyTeamLeader->notify(new FriendlyMatchEnded($info));    
                }

                $participants = $match->hostMembers();
                
                $participants = $participants->merge($match->enemyMembers());

                 $users = array();
                foreach($participants as $member) {
                    if($member->id != $hostLeader->id && $member->id != $hostTeamLeader->id && $member->id != $enemyLeader->id && $member->id != $enemyTeamLeader->id)
                        $users[] = $member->id;
                }
                
                $participants = User::find($users);

                Notification::send($participants, new FriendlyMatchEnded($info));

                
            }
            
            
            
        }
        
        
    }
}

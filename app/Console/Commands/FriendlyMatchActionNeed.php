<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\FriendlyMatch;
use DB;
class FriendlyMatchActionNeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'match:action';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Need action info';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * 
     * @return mixed
     */
    public function handle()
    {
        //Setting need action flag for all sides of the match on last 30 minutes to match expire.
        
        $matches = FriendlyMatch::where('status',2)->whereRaw('expire_at <= (now() + INTERVAL 30 MINUTE)')->get();

        foreach($matches as $match) {

            $match->host_need_action = 1;
            $match->enemy_need_action = 1;
            $match->save();

        }
        
        
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\FriendlyMatch;
class FriendlyMatchKill extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'match:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kill all useless matches';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $today = date('Y-m-d');
        $now = date('H:i');
        
        $todayMatches = FriendlyMatch::where('match_date',$today)->where('match_time','<=',$now)->where('enemy_clan',NULL)->get();
        
        foreach($todayMatches as $match) {
            
            $match->delete();
        }
    }
}

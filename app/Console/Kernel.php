<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\FriendlyMatchEnding::class,
        Commands\FriendlyMatchKill::class,
        Commands\FriendlyMatchActionNeed::class,
        Commands\FriendlyMatchAutoAccept::class,
        Commands\GeneratePushMessages::class,
        Commands\FillNotificationSetting::class,
        Commands\ChangeUserStatus::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('match:ending')
                 ->everyMinute();

        $schedule->command('match:delete')
                 ->everyMinute();
        $schedule->command('users:change')
                 ->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

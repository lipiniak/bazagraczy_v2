<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\FollowPost;
use Auth;
class Post extends Model
{
   protected $table = 'post';

   public $timestamps = true;

   protected $fillable = [
    'game_id','platform_id','user_id','text'
    ];

    protected $dates = ['created_at'];
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function poster()
    {
        return $this->user();
    }

    public function comments() {
        return $this->hasMany('App\Comments');
    }

    public function game() {
        return $this->belongsTo('App\Games','game_id');
    }

    public function platform() {
        return $this->belongsTo('App\Platforms','platform_id');
    }

    public function userFollow() {
        $user = Auth::user();

        $followedPost = FollowPost::where('post_id',$this->id)->where('user_id',$user->id)->get();
        

        if($followedPost->count() > 0) 
            return true;
        else 
            return false;
    }
}

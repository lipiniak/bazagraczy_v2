<?php

namespace App\Repositories;


use App\Settings;
use Cache;
use App\NavigationLinks;

class NavigationRepository
{
    public function __construct()
    {
        
    }

    public function getMenu($group_id) {

        $links = NavigationLinks::where('group_id',$group_id)->where('active',1)->orderBy('order')->get();
        
        $menu = $this->buildNavigation($links);

        return $menu;
    }


    public function buildNavigation($data, $parent = 0) {
        $menu = array();
        foreach($data as $item) {
            if($item->parent_id == $parent) {
                $menu[$item->id]['name'] = $item->name;
                $menu[$item->id]['route'] = $item->route_name;
                $menu[$item->id]['icon'] = $item->icon;
                if($this->hasChildren($data,$item->id)) {
                    $menu[$item->id]['childrens'] = $this->buildNavigation($data,$item->id);
                }
            }   
        }

        return $menu;
    } 
    public function hasChildren($data,$id) {
        foreach ($data as $item) {
            if ($item->parent_id == $id)
              return true;
          }
          return false;
    }
    

}


<?php

namespace App\Repositories;


use App\Settings;
use Cache;

class SettingsRepository
{
    public function __construct()
    {
        Cache::remember('site.settings',60, function() {
            return Settings::pluck('value','key')->toArray();
        });
    }

    public function get($key) {
        return array_get(Cache::get('site.settings'), $key);
    }

    

}


<?php

namespace App\Repositories;


use App\Language;
use Cache;

class LanguageRepository
{
    public function __construct()
    {
        Cache::remember('site.language',60, function() {
            return Language::pluck('value','key','group')->toArray();
        });
    }

    public function get($key) {
        return array_get(Cache::get('site.language'), $key);
    }

    
}


<?php

namespace App\Repositories;

use App\FriendlyMatch;
use App\Clan;
use App\User;
use DB;
use Auth;
class StatsRepository
{
    public function countCancelMatchesPerMembersPerGame($game_id,$platform_id) {
        $matches = FriendlyMatch::where('result',4)->where('game_id',$game_id)->where('platform_id',$platform_id)->get();

        $hostPlayers = array();
        $enemyPlayers = array();
        
        foreach($matches as $match) {
            $hostMembers = $match->hostMembers();
            
            $enemyMembers = $match->enemyMembers();
            
            
                foreach($hostMembers as $memberH) {
                
                    $hostPlayers[$memberH->id] = isset($hostPlayers[$memberH->id]) ? $hostPlayers[$memberH->id]+1 : 1;
                }
            
                foreach($enemyMembers as $memberE) {
                
                    $enemyPlayers[$memberE->id] = isset($enemyPlayers[$memberE->id]) ? $enemyPlayers[$memberE->id]+1 : 1;
                }
            
            
        }
        
        $sums = $this->sum_associatve(array($hostPlayers,$enemyPlayers));

        return $sums;
    }

    public function countCancelMatchesPerMembers() {
        $matches = FriendlyMatch::where('result',4)->get();

        $hostPlayers = array();
        $enemyPlayers = array();
        
        foreach($matches as $match) {
            $hostMembers = $match->hostMembers();
            
            $enemyMembers = $match->enemyMembers();
            
            
                foreach($hostMembers as $memberH) {
                
                    $hostPlayers[$memberH->id] = isset($hostPlayers[$memberH->id]) ? $hostPlayers[$memberH->id]+1 : 1;
                }
            
                foreach($enemyMembers as $memberE) {
                
                    $enemyPlayers[$memberE->id] = isset($enemyPlayers[$memberE->id]) ? $enemyPlayers[$memberE->id]+1 : 1;
                }
            
            
        }
        
        $sums = $this->sum_associatve(array($hostPlayers,$enemyPlayers));

        return $sums;
    }

    public function getPlayerMaxProcentageCancel() {
        $count_matches = $this->countMatchesPerMembers();
        $cancels = $this->countCancelMatchesPerMembers();
        
        if(!empty($cancels) and !empty($count_matches)) {
            foreach($cancels as $cancel => $value) {
                $can[$cancel] = ($value / $count_matches[$cancel]) * 100;
            }
    
            $max_clan = array_search(max($can),$can);
    
            $procentage = round($can[$max_clan]);
    
            $clan = User::find($max_clan);

            if(!$clan) 
                $clan = false;

            $result = [
                'user'=> $clan,
                'procentage' => $procentage
            ];
        } else {
            $result = [
                'user' => false,
                'procentage'=> 0,
            ];
        }
        

        return $result;
    }

    public function getPlayerMaxProcentageCancelPerGame() {
        $selected_game = Auth::user()->selectedGame();
        $count_matches = $this->countMatchesPerMembersPerGame($selected_game['game_id'],$selected_game['platform_id']);
        $cancels = $this->countCancelMatchesPerMembersPerGame($selected_game['game_id'],$selected_game['platform_id']);
        
        if(!empty($cancels) and !empty($count_matches)) {
            foreach($cancels as $cancel => $value) {
                $can[$cancel] = ($value / $count_matches[$cancel]) * 100;
            }
    
            $max_clan = array_search(max($can),$can);
    
            $procentage = round($can[$max_clan]);
    
            $clan = User::find($max_clan);

            if(!$clan) 
                $clan = false;

            $result = [
                'user'=> $clan,
                'procentage' => $procentage
            ];
        } else {
            $result = [
                'user' => false,
                'procentage'=> 0,
            ];
        }
        

        return $result;
    }


    public function getPlayerMaxProcentageWins() {
        $count_matches = $this->countMatchesPerMembers();
        $winners = $this->countWinnedMatchesPerMembers();
        
        
        if(!empty($winners) and !empty($count_matches))  {
            $win = array();
            foreach($winners as $winner => $value) {
            
                $win[$winner] = ($value / $count_matches[$winner])*100;
            }
    
            $max_clan = array_search(max($win),$win);
    
            $procentage = round($win[$max_clan]);
            
            $clan = User::find($max_clan);
            if(!$clan) 
                $clan = false;
            $result = [
                'user' => $clan,
                'procentage' => $procentage
            ];
        } else {
            $result = [
                'user' => false,
                'procentage' => 0
            ];
        }
        

        return $result;
    }

    public function getPlayerMaxProcentageWinsPerGame() {
        $selected_game = Auth::user()->selectedGame();
        $count_matches = $this->countMatchesPerMembersPerGame($selected_game['game_id'],$selected_game['platform_id']);
        $winners = $this->countWinnedMatchesPerMembersPerGame($selected_game['game_id'],$selected_game['platform_id']);
        
        
        if(!empty($winners) and !empty($count_matches))  {
            $win = array();
            foreach($winners as $winner => $value) {
            
                $win[$winner] = ($value / $count_matches[$winner])*100;
            }
    
            $max_clan = array_search(max($win),$win);
    
            $procentage = round($win[$max_clan]);
            
            $clan = User::find($max_clan);
            if(!$clan) 
                $clan = false;
            $result = [
                'user' => $clan,
                'procentage' => $procentage
            ];
        } else {
            $result = [
                'user' => false,
                'procentage' => 0
            ];
        }
        

        return $result;
    }

    public function countWinnedMatchesPerMembers() {
        $matches = FriendlyMatch::where('result',1)->get();

        $hostPlayers = array();
        $enemyPlayers = array();
        
        foreach($matches as $match) {
            $hostMembers = $match->hostMembers();
            
            $enemyMembers = $match->enemyMembers();
            
            if($match->winner_id == $match->host_clan) {
                foreach($hostMembers as $memberH) {
                
                    $hostPlayers[$memberH->id] = isset($hostPlayers[$memberH->id]) ? $hostPlayers[$memberH->id]+1 : 1;
                }
            }
            if($match->winner_id == $match->enemy_clan) {
                foreach($enemyMembers as $memberE) {
                
                    $enemyPlayers[$memberE->id] = isset($enemyPlayers[$memberE->id]) ? $enemyPlayers[$memberE->id]+1 : 1;
                }
            }
            
            
        }
        
        $sums = $this->sum_associatve(array($hostPlayers,$enemyPlayers));

        return $sums;
    }

    public function countWinnedMatchesPerMembersPerGame($game_id,$platform_id) {
        $matches = FriendlyMatch::where('result',1)->where('game_id',$game_id)->where('platform_id',$platform_id)->get();

        $hostPlayers = array();
        $enemyPlayers = array();
        
        foreach($matches as $match) {
            $hostMembers = $match->hostMembers();
            
            $enemyMembers = $match->enemyMembers();
            
            if($match->winner_id == $match->host_clan) {
                foreach($hostMembers as $memberH) {
                    
                    $hostPlayers[$memberH->id] = isset($hostPlayers[$memberH->id]) ? $hostPlayers[$memberH->id]+1 : 1;
                }
            } 
            if($match->winner_id == $match->enemy_clan) {
                foreach($enemyMembers as $memberE) {
                    
                    $enemyPlayers[$memberE->id] = isset($enemyPlayers[$memberE->id]) ? $enemyPlayers[$memberE->id]+1 : 1;
                }
            }
            
        }
        
        $sums = $this->sum_associatve(array($hostPlayers,$enemyPlayers));

        return $sums;
    }

    // liczy mecze per uczestnik meczu
    public function countMatchesPerMembers() {
        $matches = FriendlyMatch::where('status',3)->get();

        $hostPlayers = array();
        $enemyPlayers = array();
        
        foreach($matches as $match) {
            $hostMembers = $match->hostMembers();
            
            $enemyMembers = $match->enemyMembers();
            
            
            foreach($hostMembers as $memberH) {
                
                $hostPlayers[$memberH->id] = isset($hostPlayers[$memberH->id]) ? $hostPlayers[$memberH->id]+1 : 1;
            }
                
            foreach($enemyMembers as $memberE) {
                
                $enemyPlayers[$memberE->id] = isset($enemyPlayers[$memberE->id]) ? $enemyPlayers[$memberE->id]+1 : 1;
            }
            
            
        }
        
        $sums = $this->sum_associatve(array($hostPlayers,$enemyPlayers));

        return $sums;
    }
    // liczy mecze per gra per uczestnik meczu
    public function countMatchesPerMembersPerGame($game_id,$platform_id) {
        $matches = FriendlyMatch::where('status',3)->where('game_id',$game_id)->where('platform_id',$platform_id)->get();

        $hostPlayers = array();
        $enemyPlayers = array();
        
        foreach($matches as $match) {
            $hostMembers = $match->hostMembers();
            
            $enemyMembers = $match->enemyMembers();
            
            
            foreach($hostMembers as $memberH) {
                
                $hostPlayers[$memberH->id] = isset($hostPlayers[$memberH->id]) ? $hostPlayers[$memberH->id]+1 : 1;
            }
                
            foreach($enemyMembers as $memberE) {
                
                $enemyPlayers[$memberE->id] = isset($enemyPlayers[$memberE->id]) ? $enemyPlayers[$memberE->id]+1 : 1;
            }
            
            
        }
        
        $sums = $this->sum_associatve(array($hostPlayers,$enemyPlayers));

        return $sums;
    }

    public function getPlayerMaxMetchesPerGame() {
        
        $selected_game = Auth::user()->selectedGame();

        $count = $this->countMatchesPerMembersPerGame($selected_game['game_id'],$selected_game['platform_id']);
        if(!empty($count))  {
            $max_user = array_search(max($count),$count);

            $count = $count[$max_user];
            
            $user = User::find($max_user);

            if(!$user)
                $user = false;

            $result = [
                'user'=>$user,
                'count'=>$count
            ];
        } else {
            $result = [
                'user' => false,
                'count'=> 0
            ];
        }
        return $result;
    }
    public function getPlayerMaxMetches() {
        $count = $this->countMatchesPerMembers();

        if(!empty($count))  {
            $max_user = array_search(max($count),$count);

            $count = $count[$max_user];
            
            $user = User::find($max_user);

            if(!$user)
                $user = false;

            $result = [
                'user'=>$user,
                'count'=>$count
            ];
        } else {
            $result = [
                'user' => false,
                'count'=> 0
            ];
        }
        return $result;
    }

    //Liczy wszystkie mecze per klan
    public function countMatches() {

        $host_matches = DB::table('match_friendly')
        ->select('host_clan', DB::raw('count(*) as matches'))
        ->where('status',3)
        ->groupBy('host_clan')
        ->pluck('matches','host_clan')->all();

        $enemy_matches = DB::table('match_friendly')
        ->select('enemy_clan', DB::raw('count(*) as matches'))
        ->where('status',3)
        ->groupBy('enemy_clan')
        ->pluck('matches','enemy_clan')->all();
        
        $sums = $this->sum_associatve(array($host_matches,$enemy_matches));

        return $sums;
    }

    // liczy mecze per klan i per gra
    public function countMatchesPerGame($game_id,$platform_id) {
        $host_matches = DB::table('match_friendly')
        ->select('host_clan', DB::raw('count(*) as matches'))
        ->where('status',3)
        ->where('game_id',$game_id)
        ->where('platform_id',$platform_id)
        ->groupBy('host_clan')
        ->pluck('matches','host_clan')->all();

        $enemy_matches = DB::table('match_friendly')
        ->select('enemy_clan', DB::raw('count(*) as matches'))
        ->where('status',3)
        ->where('game_id',$game_id)
        ->where('platform_id',$platform_id)
        ->groupBy('enemy_clan')
        ->pluck('matches','enemy_clan')->all();
        
        $sums = $this->sum_associatve(array($host_matches,$enemy_matches));

        return $sums;
    }

    //liczy zwycięzkie mecze per klan
     public function countWinnedMatches() {

        
        $winners = DB::table('match_friendly')
        ->select('winner_id', DB::raw('count(*) as matches'))
        ->where('result',1)
        ->groupBy('winner_id')
        ->pluck('matches','winner_id')->all();

        return $winners;
    }

    public function countWinnedMatchesPerGame($game_id,$platform_id) {
        $winners = DB::table('match_friendly')
        ->select('winner_id', DB::raw('count(*) as matches'))
        ->where('result',1)
        ->where('game_id',$game_id)
        ->where('platform_id',$platform_id)
        ->groupBy('winner_id')
        ->pluck('matches','winner_id')->all();    

        return $winners; 
    }

    public function countCancelMatches() {

        
        $host_matches = DB::table('match_friendly')
        ->select('host_clan', DB::raw('count(*) as matches'))
        ->where('result',4)
        ->groupBy('host_clan')
        ->pluck('matches','host_clan')->all();

        $enemy_matches = DB::table('match_friendly')
        ->select('enemy_clan', DB::raw('count(*) as matches'))
        ->where('result',4)
        ->groupBy('enemy_clan')
        ->pluck('matches','enemy_clan')->all();
        
        $sums = $this->sum_associatve(array($host_matches,$enemy_matches));

        return $sums;
    }

    public function countCancelMatchesPerGame($game_id,$platform_id) {
        $host_matches = DB::table('match_friendly')
        ->select('host_clan', DB::raw('count(*) as matches'))
        ->where('result',4)
        ->where('game_id',$game_id)
        ->where('platform_id',$platform_id)
        ->groupBy('host_clan')
        ->pluck('matches','host_clan')->all();

        $enemy_matches = DB::table('match_friendly')
        ->select('enemy_clan', DB::raw('count(*) as matches'))
        ->where('result',4)
        ->where('game_id',$game_id)
        ->where('platform_id',$platform_id)
        ->groupBy('enemy_clan')
        ->pluck('matches','enemy_clan')->all();
        
        $sums = $this->sum_associatve(array($host_matches,$enemy_matches));

        return $sums;
    }

    //Get clan with most matches
    public function getClanMaxMatches() {

        $count = $this->countMatches();
        if(!empty($count))  {
            $max_clan = array_search(max($count),$count);

            $count = $count[$max_clan];
            
            $clan = Clan::find($max_clan);

            if(!$clan)
                $clan = false;

            $result = [
                'clan'=>$clan,
                'count'=>$count
            ];
        } else {
            $result = [
                'clan' => false,
                'count'=> 0
            ];
        }
        return $result;
    }  

    public function getClanMaxMatchesPerGame() {
        $selected_game = Auth::user()->selectedGame();

        $count = $this->countMatchesPerGame($selected_game['game_id'],$selected_game['platform_id']);

        if(!empty($count))  {
            $max_clan = array_search(max($count),$count);

            $count = $count[$max_clan];
            
            $clan = Clan::find($max_clan);
            if(!$clan)
                $clan = false;
            $result = [
                'clan'=>$clan,
                'count'=>$count
            ];
        } else {
            $result = [
                'clan' => false,
                'count'=> 0
            ];
        }
        return $result;
    }
    
    // get clan with max procentage wins
    public function getClanMaxProcentageWins() {
        $count_matches = $this->countMatches();
        $winners = $this->countWinnedMatches();
        
        if(!empty($winners) and !empty($count_matches))  {
            foreach($winners as $winner => $value) {
            
                $win[$winner] = ($value / $count_matches[$winner])*100;
            }
    
            $max_clan = array_search(max($win),$win);
    
            $procentage = round($win[$max_clan]);
            
            $clan = Clan::find($max_clan);
            if(!$clan) 
                $clan = false;
            $result = [
                'clan' => $clan,
                'procentage' => $procentage
            ];
        } else {
            $result = [
                'clan' => false,
                'procentage' => 0
            ];
        }
        

        return $result;
    }

    public function getClanMaxProcentageWinsPerGame() {
        $selected_game = Auth::user()->selectedGame();
        $count_matches = $this->countMatchesPerGame($selected_game['game_id'],$selected_game['platform_id']);
        $winners = $this->countWinnedMatchesPerGame($selected_game['game_id'],$selected_game['platform_id']);

        if(!empty($winners) and !empty($count_matches))  {
            foreach($winners as $winner => $value) {
                
                $win[$winner] = ($value / $count_matches[$winner])*100;
            }

            $max_clan = array_search(max($win),$win);

            $procentage = round($win[$max_clan]);
            
            $clan = Clan::find($max_clan);
            if(!$clan) 
                $clan = false;

            $result = [
                'clan' => $clan,
                'procentage' => $procentage
            ];
        } else {
            $result = [
                'clan' => false,
                'procentage' => 0
            ];
        }
        return $result;
    }

    

    public function getClanMaxProcentageCancel() {
        $count_matches = $this->countMatches();
        $cancels = $this->countCancelMatches();
        
        if(!empty($cancels) and !empty($count_matches)) {
            foreach($cancels as $cancel => $value) {
                $can[$cancel] = ($value / $count_matches[$cancel]) * 100;
            }
    
            $max_clan = array_search(max($can),$can);
    
            $procentage = round($can[$max_clan]);
    
            $clan = Clan::find($max_clan);

            if(!$clan) 
                $clan = false;

            $result = [
                'clan'=> $clan,
                'procentage' => $procentage
            ];
        } else {
            $result = [
                'clan' => false,
                'procentage'=> 0,
            ];
        }
        

        return $result;
    }

    public function getClanMaxProcentageCancelPerGame() {
        $selected_game = Auth::user()->selectedGame();
        $count_matches = $this->countMatchesPerGame($selected_game['game_id'],$selected_game['platform_id']);
        $cancels = $this->countCancelMatchesPerGame($selected_game['game_id'],$selected_game['platform_id']);
        
        if(!empty($cancels) and !empty($count_matches)) {
            foreach($cancels as $cancel => $value) {
                $can[$cancel] = ($value / $count_matches[$cancel]) * 100;
            }
    
            $max_clan = array_search(max($can),$can);
    
            $procentage = round($can[$max_clan]);
    
            $clan = Clan::find($max_clan);
            
            if(!$clan)
                $clan = false;

            $result = [
                'clan'=> $clan,
                'procentage' => $procentage
            ];
        } else {
            $result = [
                'clan' => false,
                'procentage' => 0,
            ];
        }
        

        return $result;
    }

    public function sum_associatve($arrays){
        $sum = array();
        foreach ($arrays as $array) {
            foreach ($array as $key => $value) {
                if (isset($sum[$key])) {
                    $sum[$key] += $value;
                } else {
                    $sum[$key] = $value;
                }
            }
        } 
        return $sum;
    }
    
}


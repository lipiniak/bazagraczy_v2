<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    protected $content;
    protected $email;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content,$email)
    {
        $this->content = $content;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $content = $this->content;
        $email = $this->email;
        
        $this->replyTo($email,$email);

        return $this->view('email.contact',compact('content','email'));
    }
}

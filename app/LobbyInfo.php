<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LobbyInfo extends Model
{
    protected $table = 'lobby_info';

    protected $fillable = ['user_id','info','game_id','platform_id'];

}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cache;
use App\Cities;
use App\Voivodeship;
use App\Games;
use App\Platforms;
use App\FriendlyMatch;
use App\LobbyInfo;
use Collective\Html\Eloquent\FormAccessible;
use DB;

class User extends Authenticatable
{
    use Notifiable;
    use FormAccessible;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'email', 'password', 'provider', 'provider_id','activated','name','surname','www','show_age','description','steam','origin','uplay','battlenet','epicgames','psn','xboxlive','online_time','gog'
    ];


    protected $dates = ['login_at','birth_date'];

    public function receivesBroadcastNotificationsOn()
    {
        return 'user-'.$this->id;
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getStatus()
    {   
        if(Cache::has('user-is-online-' . $this->id)) {
            $status = Cache::get('user-is-online-' . $this->id);

            return $status;
        } else 
            return 'offline';
        
    }

    public function age() {
        return $this->birth_date->diffInYears(\Carbon\Carbon::now());
    }

    public function sex() {
        $sex = array('none'=>'','female'=>'Kobieta','meale'=>'Mężczyzna');

        return $sex[$this->sex];
    }
    
    

    public function voivode() {
        $name = '';

        if($this->voivodeship != '') {
            $voivode = Voivodeship::where('id',$this->voivodeship)->get();

            foreach($voivode as $item) {
                $name = $item->name;
            }
        }

        return $name;
    }
    public function city(){
        $name = '';
        if($this->city != '') {
            $city = Cities::where('id',$this->city)->get();
            
            foreach($city as $item) {
                $name = $item->name;
            }
            
        }
        

        return $name;
    }

    public function selectedGame() {
        $game_id = $this->settings()->first()->selected_game_id;

        $platform_id = $this->settings()->first()->selected_platform_id;

        $game = Games::find($game_id);
        $platform = Platforms::find($platform_id);
        if(!$game) {
            $game = 'Brak';
            $game_id = 0;
        } else {
            $game_id = $game->id;
            $game = $game->name;

        }
        if(!$platform) {
            $platform = 'Brak';
            $platform_id = 0;
        } else {
            $platform_id = $platform->id;
            $platform = $platform->name;
        }
        
        return array('game'=>$game,'platform'=>$platform,'game_id'=>$game_id,'platform_id'=>$platform_id);
    }

    public function checkWallNotifications() {
        $selectedGame = $this->selectedGame();

        
        if($this->settings()->first()->notify_all_library_games == 0) {
            return true;
        } elseif($this->settings()->first()->notify_all_library_games == 1) {
            //wszystkie gry czyli czy selected id is in library 
            $hits = DB::table('users_games_platforms')->where('user_id',$this->id)->where('games_id',$selectedGame['game_id'])->where('platforms_id',$selectedGame['platform_id'])->get();
            
            if($hits->count() > 0) {
                return false;
            } else {
                return true;
            }
        
        } elseif($this->settings()->first()->notify_all_library_games == 2) {
            // wybrane gry czyli czy gra z biblioteki jest w liście gier
            $games = json_decode($this->settings()->first()->selected_notify_games,true);

            $selectedGames = array();
            if($games == NULL)
                $games = array();

            if(array_search(($selectedGame['game_id'].'-'.$selectedGame['platform_id']),$games) !== false) {
                return false;
            }  

            return true;
        }

        
    }
    public function tags() {
        return $this->hasMany('App\Tags','user_id');
    }
    // zwrocic id roli 
    // 3 user
    // 2 moder
    // 1 admin
    /*
        if 1 adn if 2 
    */
    // relacje
// klany usera
    public function clans() {
        return $this->belongsToMany('App\Clan', 'users_clans', 'users_id', 'clans_id')->wherePivot('accepted', 1);
    }

    public function UserClanAplications() {
        $applicants = 0;
        foreach($this->clans as $clan) {
            
            if($this->id == $clan->leader_id) {
                
                $applicants += count($clan->aplicants);
                
            }
        }
        
        if($applicants>0)
            return true;    
        else 
            return false;
    }


    public function UserMatchCreateGames() {
        $table = array();

        foreach($this->clans as $clan) {
            if($this->id == $clan->leader_id) {
                foreach($clan->teams as $team) {
                    $game['game'] = $team->game->name;
                    $game['platform'] = $team->platform->name;

                    $table[$team->game_id.'-'.$team->platform_id] = $game;
                }
            }
        }

        return $table;
    }

    public function applyToClans() {
        return $this->belongsToMany('App\Clan', 'users_clans', 'users_id', 'clans_id')->wherePivot('accepted', 0);
    }
    // druzyny usera
    public function teams() {
        return $this->belongsToMany('App\Team', 'users_teams', 'users_id', 'teams_id');
    }

    public function teamsLeader() {
        return $this->hasMany('App\Team','leader_id');
    }

    public function games() {
        return $this->belongsToMany('App\Games','users_games_platforms');
    }
    public function pushMessages() {
        return $this->hasMany('App\Push','user_id');
    }
    public function platforms() {
        return $this->belongsToMany('App\Platforms','users_games_platforms');
    }

    public function followers()
    {
        return $this->belongsToMany('App\User', 'followers_user', 'followers_id', 'user_id');
    }
    
    public function myfollowers()
    {
        return $this->belongsToMany('App\User', 'followers_user', 'user_id', 'followers_id');
    }

    public function blocked()
    {
        return $this->belongsToMany('App\User', 'blocked_user', 'blocked_id', 'user_id');
    }
    
    public function myblocked()
    {
        return $this->belongsToMany('App\User', 'blocked_user', 'user_id', 'blocked_id');
    }

    public function note()
    {
        return $this->hasMany('App\Note', 'note_for_id');
    }
    public function mynote() {
        return $this->hasMany('App\Note', 'user_id');
    }
 
    public function settings() {
        return $this->hasMany('App\UserSettings','user_id');
    }

    public function post() {
        return $this->hasMany('App\Post','user_id');
    }

    public function infoL() {
        return $this->hasMany('App\LobbyInfo','user_id');
    }
    
    public function lobbyInfo($game_id,$platform_id) {
        $info = $this->infoL()->where('game_id',$game_id)->where('platform_id',$platform_id)->first();
        
        if($info)
            return $info->info;
        else 
            return NULL;
    }

    public function AllFriendlyMatchesAction() {
        
        $userClans = array();

        foreach($this->clans as $clan) {
            if($this->id == $clan->leader_id)
                $userClans[] = $clan->id;
        }

        $userTeams = array();
        
        foreach($this->teams as $team) {
            if($team->leader_id == $this->id)
                $userTeams[] = $team->id;
        }

        $matchesClanLeader = FriendlyMatch::whereIn('host_clan',$userClans)->get();

        $matchesTeamLeader = FriendlyMatch::whereIn('host_team',$userTeams)->get();
        
        
        $matchesEnemyClanLeader = FriendlyMatch::whereIn('enemy_clan',$userClans)->get();
        $matchesEnemyTeamLeader = FriendlyMatch::whereIn('enemy_team',$userTeams)->get();
        
        $matches = $matchesClanLeader->merge($matchesTeamLeader);
        $matches = $matches->merge($matchesEnemyClanLeader);
        $matches = $matches->merge($matchesEnemyTeamLeader)->sortByDesc('match_time')->sortByDesc('match_date');

        $actionNumber = 0;
        foreach($matches as $match) {
            if($match->isHost() && $match->host_need_action == 1)
                $actionNumber++;
            if($match->isEnemy() && $match->enemy_need_action == 1)
                $actionNumber++;
        }

        if($actionNumber > 0)
            return true;
        else 
            return false;
    }

    public function OpenFriendlyMatchesAction() {

        $userClans = array();

        foreach($this->clans as $clan) {
            if($this->id == $clan->leader_id)
                $userClans[] = $clan->id;
        }

        $userTeams = array();
        
        foreach($this->teams as $team) {
            if($team->leader_id == $this->id)
                $userTeams[] = $team->id;
        }
        $matchesClanLeader = FriendlyMatch::whereIn('host_clan',$userClans)->where('status',1)->get();

        $matchesTeamLeader = FriendlyMatch::whereIn('host_team',$userTeams)->where('status',1)->get();
        
        
        $matchesEnemyClanLeader = FriendlyMatch::whereIn('enemy_clan',$userClans)->where('status',1)->get();
        $matchesEnemyTeamLeader = FriendlyMatch::whereIn('enemy_team',$userTeams)->where('status',1)->get();
        
        
        $matches = $matchesClanLeader->merge($matchesTeamLeader);
        $matches = $matches->merge($matchesEnemyClanLeader);
        $matches = $matches->merge($matchesEnemyTeamLeader)->sortByDesc('match_time')->sortByDesc('match_date');

        $actionNumber = 0;
        foreach($matches as $match) {
            if($match->isHost() && $match->host_need_action == 1)
                $actionNumber++;
            if($match->isEnemy() && $match->enemy_need_action == 1)
                $actionNumber++;
        }

        if($actionNumber > 0)
            return true;
        else 
            return false;
    }

    public function ActiveFriendlyMatchesAction() {

        $userClans = array();

        foreach($this->clans as $clan) {
            if($this->id == $clan->leader_id)
                $userClans[] = $clan->id;
        }

        $userTeams = array();
        
        foreach($this->teams as $team) {
            if($team->leader_id == $this->id)
                $userTeams[] = $team->id;
        }

        $matchesClanLeader = FriendlyMatch::whereIn('host_clan',$userClans)->where('status',2)->get();

        $matchesTeamLeader = FriendlyMatch::whereIn('host_team',$userTeams)->where('status',2)->get();
        
        
        $matchesEnemyClanLeader = FriendlyMatch::whereIn('enemy_clan',$userClans)->where('status',2)->get();
        $matchesEnemyTeamLeader = FriendlyMatch::whereIn('enemy_team',$userTeams)->where('status',2)->get();
        
        $matches = $matchesClanLeader->merge($matchesTeamLeader);
        $matches = $matches->merge($matchesEnemyClanLeader);
        $matches = $matches->merge($matchesEnemyTeamLeader)->sortByDesc('match_time')->sortByDesc('match_date');

        $actionNumber = 0;
        foreach($matches as $match) {
            if($match->isHost() && $match->host_need_action == 1)
                $actionNumber++;
            if($match->isEnemy() && $match->enemy_need_action == 1)
                $actionNumber++;
        }

        if($actionNumber > 0)
            return true;
        else 
            return false;
    }

    public function ClosedFriendlyMatchesAction() {
        $userClans = array();

        foreach($this->clans as $clan) {
            if($this->id == $clan->leader_id)
                $userClans[] = $clan->id;
        }

        $userTeams = array();
        
        foreach($this->teams as $team) {
            if($team->leader_id == $this->id)
                $userTeams[] = $team->id;
        }
        
        $matchesClanLeader = FriendlyMatch::whereIn('host_clan',$userClans)->where('status',3)->get();

        $matchesTeamLeader = FriendlyMatch::whereIn('host_team',$userTeams)->where('status',3)->get();
        
        
        $matchesEnemyClanLeader = FriendlyMatch::whereIn('enemy_clan',$userClans)->where('status',3)->get();
        $matchesEnemyTeamLeader = FriendlyMatch::whereIn('enemy_team',$userTeams)->where('status',3)->get();
        
        $matches = $matchesClanLeader->merge($matchesTeamLeader);
        $matches = $matches->merge($matchesEnemyClanLeader);
        $matches = $matches->merge($matchesEnemyTeamLeader)->sortByDesc('match_time')->sortByDesc('match_date');
        
        $actionNumber = 0;
        foreach($matches as $match) {
            if($match->isHost() && $match->host_need_action == 1)
                $actionNumber++;
            if($match->isEnemy() && $match->enemy_need_action == 1)
                $actionNumber++;
        }

        if($actionNumber > 0)
            return true;
        else 
            return false;
    }

    public function lobbyLikes() {
        return $this->hasMany('App\LobbyLikes','for_id');
    }

    public function lobbyStatus($user_id) {
        $status = $this->lobbyLikes()->where('user_id',$user_id)->first();
        if($status)
            return $status->value;
        else 
            return '';

    }

    public function isInLobby() {
        if(Cache::has('user-is-in-lobby-'.$this->id)) {
            $status = '';
            if(Cache::get('lobby-status-'.$this->id) == 'szukam')  {
                $status = '<i class="fas fa-clock text-muted"></i>';
            }
            if(Cache::get('lobby-status-'.$this->id) == 'afk')  {
                $status = '<i class="fas fa-bed text-muted"></i>';
            }
            if(Cache::get('lobby-status-'.$this->id) == 'gram')  {
                $status = '<i class="fas fa-gamepad text-muted"></i>';
            }
            return '<js class="_lobby_info">'.$status.' <u>'.$this->selectedGame()['game'].' <b>'.$this->selectedGame()['platform'].'</b></u></js>';
        } else {
            return '';
        }
    }


    public function questDone() {
        return $this->belongsToMany('App\GiveawayQuests','users_quest','user_id','quest_id');
    }
    
}



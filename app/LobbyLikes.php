<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LobbyLikes extends Model
{
    protected $table = 'users_lobby_likes';

    protected $fillable = ['user_id','value','for_id'];


    public function user() {
        return $this->hasMany('App\User','user_id');
    }

    public function forId() {
        return $this->hasMany('App\User','for_id');
    }
}

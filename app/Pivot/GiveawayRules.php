<?php

namespace App\Pivot;

use Illuminate\Database\Eloquent\Model;

class GiveawayRules extends Model
{
    //
    protected $table = 'giveaway_rules_pivot';


    protected $fillable = ['user_id','giveaway_id'];
    
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Games extends Model
{
    //
    protected $table = 'games';

    protected $fillable = ['name','description','img','feeds'];
    
    public function platforms()
    {
        return $this->belongsToMany('App\Platforms');
    }

    public function users() {
        return $this->belongsToMany('App\User');
    }

    public function userPlatforms() {
        return $this->belongsToMany('App\Platforms','users_games_platforms');
    }
    
 
    public function userGames() {
        return $this->belongsToMany('App\User','users_games_platforms');
    }

}

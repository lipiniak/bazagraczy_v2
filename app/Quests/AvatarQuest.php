<?php
namespace App\Quests;


use Auth;

class AvatarQuest 
{    
    public function check() {

        if(Auth::user()->avatar != 'no-avatar.jpg') {
            return true;
        } else 
            return false;
    }
}

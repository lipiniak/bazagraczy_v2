<?php
namespace App\Quests;


use Auth;

class TagQuest 
{    
    public function check() {

        if(Auth::user()->tags->count() > 0) {
            return true;
        } else 
            return false;
    }
}

<?php
namespace App\Quests;


use Auth;

class FollowerQuest 
{    
    public function check() {

        if(Auth::user()->myfollowers->count() > 0) {
            return true;
        } else 
            return false;
    }
}

<?php

namespace App\Factories;

use App\Quests\AvatarQuest;
use App\Quests\DescriptionQuest;
use App\Quests\FollowerQuest;
use App\Quests\LibraryQuest;
use App\Quests\RegisterQuest;
use App\Quests\TagQuest;

use App\GiveawayQuests;

use Auth;
class QuestFactory
{
    

    public function checkQuest($quest_key) {

        
        
        switch($quest_key) {
            case 'quest_avatar': 
                $result = new AvatarQuest();
                $result = $result->check();
                if($result) 
                    $this->doneQuest($quest_key);
            break;
            case 'quest_follow':
            $result = new FollowerQuest();
            $result = $result->check();
                if($result) 
                    $this->doneQuest($quest_key);
            break;
            case 'quest_game':
            $result = new LibraryQuest();
            $result = $result->check();
                if($result) 
                    $this->doneQuest($quest_key);
            break;
            case 'quest_description':
            $result = new DescriptionQuest();
            $result = $result->check();
                if($result) 
                    $this->doneQuest($quest_key);
            break;
            case 'quest_tags':
            $result = new TagQuest();
            $result = $result->check() ;
                if($result) 
                    $this->doneQuest($quest_key);
            break;
            case 'quest_register':
            $result = new RegisterQuest();
            $result = $result->check(); 
                if($result) 
                    $this->doneQuest($quest_key);
            break;
            
        }

        return true;

    }

    public function checkUserQuests() {
        $quests = GiveawayQuests::where('status',1)->get();

        foreach($quests as $quest) {
            $this->checkQuest($quest->quest_key);
        }

        return true;
    }

    public function doneQuest($quest_key) {
        $quests = GiveawayQuests::where('quest_key',$quest_key)->Where('status',1)->get();
        

        $user = Auth::user();

        foreach($quests as $quest) {
            if(!$user->questDone()->where('quest_id',$quest->id)->exists()) {
                $user->questDone()->attach($quest->id);
                $user->tickets = $user->tickets + $quest->points;
                $user->save();
            }
            
        }
            

        return true;

    }

    public function unDoneQuest($quest_key) {

        
        $quests = GiveawayQuests::where('quest_key',$quest_key)->get();

        $user = Auth::user();
        foreach($quests as $quest){
            if($user->questDone()->where('quest_id',$quest->id)->exists()) {
                $user->questDone()->detach($quest->id);
                $user->tickets = $user->tickets - $quest->points;
                $user->save();
            }
            
        }   
        return true;
        
    }
}
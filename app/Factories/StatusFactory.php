<?php

namespace App\Factories;

use App\User;
use Cache;

class StatusFactory
{
    public function getAllOnlineUsers() {

        if(Cache::has('online_users_list')) {
            $onlineUsers = Cache::get('online_users_list');

            $users = User::find($onlineUsers);
        } else 
            $users = array();

        
            
        return $users;
    }
}
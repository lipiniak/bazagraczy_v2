<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClanPost extends Model
{
   protected $table = 'clans_posts';

   protected $fillable = ['clan_id','author_id','content','status'];

   protected $dates = ['updated_at','created_at','accepted_at'];

   

}

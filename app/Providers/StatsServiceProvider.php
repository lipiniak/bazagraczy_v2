<?php

namespace App\Providers;

use App\Settings;
use Illuminate\Support\ServiceProvider;
        
class StatsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('stats', function ($app) {

            return new \App\Repositories\StatsRepository;
            
        });
    }

    
}

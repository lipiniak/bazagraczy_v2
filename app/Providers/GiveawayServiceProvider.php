<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
        
class GiveawayServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('GiveawayRules', function ($app) {
            
            return new \App\Giveaway\RulesFactory();
            
        });
    }

    
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
        
class QuestServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('quest', function ($app) {

            return new \App\Factories\QuestFactory();
            
        });
    }

    
}

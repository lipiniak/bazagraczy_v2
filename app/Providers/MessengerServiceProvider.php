<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\ConversationRepository;
use App\Repositories\MessageRepository;
use App\Roles;
use Illuminate\Container\Container;

class MessengerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBroadcast();
        $this->registerMessenger();
    }

    public function registerMessenger() {
        
        $this->app->singleton('messenger', function (Container $app) {
            return new \App\Factories\MessengerFactory($app['config'], $app[ConversationRepository::class], $app[MessageRepository::class]);
        });
        $this->app->alias('messenger', MessengerFactory::class);
    }

    protected function registerBroadcast()
    {
        $this->app->singleton('talk.broadcast', function (Container $app) {
            return new \App\Repositories\LanguageRepository;
        });
        
    }

    public function provides()
    {
        return [
            'messenger',
            'talk.broadcast',
        ];
    }
}

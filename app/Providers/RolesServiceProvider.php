<?php

namespace App\Providers;

use App\Roles;
use Illuminate\Support\ServiceProvider;
        
class RolesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('roles', function ($app) {

            return new \App\Repositories\RolesRepository;
            
        });
    }

    
}

<?php

namespace App\Providers;

use App\Language;
use Illuminate\Support\ServiceProvider;

class LanguageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('language', function ($app) {

            return new \App\Repositories\LanguageRepository;
            
        });
    }

    
}

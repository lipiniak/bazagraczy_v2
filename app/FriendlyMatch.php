<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\FriendlyMatchMembers;
use App\User;
use Auth;
use App\FriendlyMatchResult;


class FriendlyMatch extends Model
{
    //
    protected $table = 'match_friendly';

    public $timestamps = true;
    protected $dates = ['match_date'];

    public $fillable = [
        'status',
        'players_count',
        'match_date',
        'match_time',
        'description',
        'author_id',
        'result',
        'edit_at',
        'edit_user_id',
        'host_clan',
        'host_team',
        'enemy_clan',
        'enemy_team',
        'game_id',
        'platform_id',
        'winner_id',
        'aplication_at',
        'host_result_count',
        'enemy_result_count',
        'host_need_action',
        'enemy_need_action',
        'expire_at'
    ];

    protected $casts = [
        'match_time' => 'datetime:H:i',
        'match_date' => 'datetime:Y.m.d',
    ];


    public function hostClan() {
        return $this->belongsTo('App\Clan', 'host_clan');
    }

    public function hostTeam() {
        return $this->belongsTo('App\Team', 'host_team');
    }

    public function enemyTeam() {
        return $this->belongsTo('App\Team', 'enemy_team');
    }

    public function enemyClan() {
        return $this->belongsTo('App\Clan', 'enemy_clan');
    }

    public function winnerClan() {
        return $this->belongsTo('App\Clan','winner_id');
    }
    public function game() {
        return $this->belongsTo('App\Games','game_id');
    }

    public function platform() {
        return $this->belongsTo('App\Platforms','platform_id');
    }


    public function matchResult() {
        return $this->hasOne('App\FriendlyMatchResult','match_id');
    }

    public function hostMembers() {
        
        
        $members = FriendlyMatchMembers::where('match_id',$this->id)->first();
        
        $members = json_decode($members->host_members);
        
        $users = User::find($members);
        
        return $users;
    }

    public function enemyMembers() {
        $members = FriendlyMatchMembers::where('match_id',$this->id)->first();
        
        
        $members = json_decode($members->enemy_members);
        
        $users = User::find($members);
        
        return $users;
    }

    public function applications() {
        return $this->hasMany('App\FriendlyMatchAplication', 'match_id');
    }


    public function isAplicant() {

        $user = Auth::user(); 

        $isClanLeader = false;
        $isTeamLeader = false;
        
        foreach($this->applications as $application) {
            if($application->clan->leader_id == $user->id)
                $isClanLeader = true;
            if($application->team->leader_id == $user->id)
                $isTeamLeader = true;
        }

        if($isClanLeader || $isTeamLeader)
            return true;
        else 
            return false;
    }

    public function getAplicationId() {
        $user = Auth::user(); 

        $isClanLeader = false;
        $isTeamLeader = false;
        
        foreach($this->applications as $application) {
            if($application->clan->leader_id == $user->id)
                $isClanLeader = true;
            if($application->team->leader_id == $user->id)
                $isTeamLeader = true;
            
            if($isClanLeader || $isTeamLeader)
                return $application->id;   
            
        }

            return false;
    }

    public function isHost() {
        $user = Auth::user();

        $isClanLeader = false;
        $isTeamLeader = false;

        if($this->hostClan->leader_id == $user->id)
            $isClanLeader = true;
        if($this->hostTeam->leader_id == $user->id) 
            $isClanLeader = true;
        
        if($isClanLeader || $isTeamLeader) 
            return true;
        else 
            return false;
        
    }
    public function isEnemy() {
        if($this->enemy_clan != NULL && $this->enemy_team != NULL) {
            $user = Auth::user(); 

            $isClanLeader = false;
            $isTeamLeader = false;
            
            if($this->enemyClan->leader_id == $user->id)
                $isClanLeader = true;
            if($this->enemyTeam->leader_id == $user->id) 
                $isClanLeader = true;
            
            if($isClanLeader || $isTeamLeader) 
                return true;
            else 
                return false;
        } else {
            return false;
        }
        
        
    }
    public function isGuest() {
        // jest zdolny do podjecia wyzwania

        $user = Auth::user();

        $selectedGame = $user->selectedGame();


        $isClanLeader = false;
        $hasGameTeam = false;
        $isTeamLeader = false;
        foreach($user->clans as $clan) {
            if($clan->leader_id == $user->id)
                $isClanLeader = true;
            
            $clanTeams = $clan->teams;

            foreach($clanTeams as $cTeam) {
                if($cTeam->game_id == $selectedGame['game_id'] && $cTeam->platform_id == $selectedGame['platform_id'])
                    $hasGameTeam = true;

                if($cTeam->leader_id == $user->id && ($cTeam->game_id == $selectedGame['game_id'] && $cTeam->platform_id == $selectedGame['platform_id']))
                    $isTeamLeader = true;
            }

            if(!$hasGameTeam)
                $isClanLeader = false;
        
        }

        
        if(!$isTeamLeader && !$isClanLeader) {
            return false;
        } else {
            if(!$hasGameTeam)
                return false;
            else 
                return true;
        }
            
    
        
    }
    public function isPlayer() {
        $user = Auth::user();

        $hostMembers = $this->hostMembers();

        $enemyMembers = $this->enemyMembers();

        if($hostMembers->contains('id',$user->id))
            return true;
        if($enemyMembers->contains('id',$user->id))
            return true;
        
        return false;
    }

    public function enemySendResult() {
        $result = FriendlyMatchResult::where('match_id',$this->id)->first();
        
        if(!isset($result))
            return false;
        
        if($result->enemy_aproved == 1)
            return $result->enemy_result;
        else 
            return false;
    }

    public function hostSendResult() {
        $result = FriendlyMatchResult::where('match_id',$this->id)->first();
        if(!isset($result))
            return false;
        if($result->host_aproved == 1)
            return $result->host_result;
        else 
            return false;
    }

    public function hasResult() {
        $result = FriendlyMatchResult::where('match_id',$this->id)->first();

        if($result) {
            if($result->enemy_aproved == 1)
                return true;
            if($result->host_aproved == 1) 
                return true;
        } else 
            return false;
    }
    
}

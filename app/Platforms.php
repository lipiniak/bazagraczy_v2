<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Platforms extends Model
{
    //
    protected $table = 'platforms';
    

    public function games() {
        return $this->belongsToMany('App\Games','users_games_platforms');
    }

    public function user() {
        return $this->belongsToMany('App\User','users_games_platforms');
    }

}

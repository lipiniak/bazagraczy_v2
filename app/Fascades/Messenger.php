<?php 

namespace App\Fascades;

use Illuminate\Support\Facades\Facade;

class Messenger extends Facade {
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'messenger'; // the IoC binding.
    }
}
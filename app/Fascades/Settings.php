<?php 

namespace App\Fascades;

use Illuminate\Support\Facades\Facade;

class Settings extends Facade {
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'settings'; // the IoC binding.
    }
}
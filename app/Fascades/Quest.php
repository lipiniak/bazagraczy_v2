<?php 

namespace App\Fascades;

use Illuminate\Support\Facades\Facade;

class Quest extends Facade {
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'quest'; // the IoC binding.
    }
}
<?php 

namespace App\Fascades;

use Illuminate\Support\Facades\Facade;

class GiveawayRules extends Facade {
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'GiveawayRules'; // the IoC binding.
    }
}
<?php 

namespace App\Fascades;

use Illuminate\Support\Facades\Facade;

class Stats extends Facade {
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'stats'; // the IoC binding.
    }
}
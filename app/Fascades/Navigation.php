<?php 

namespace App\Fascades;

use Illuminate\Support\Facades\Facade;

class Navigation extends Facade {
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'navigation'; // the IoC binding.
    }
}
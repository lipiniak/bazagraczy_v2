<?php 

namespace App\Fascades;

use Illuminate\Support\Facades\Facade;

class Language extends Facade {
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'language'; // the IoC binding.
    }
}
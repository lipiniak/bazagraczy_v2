<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
   protected $table = 'comments';
   public $timestamps = true;
    protected $fillable = [
        'post_id','text','user_id'
    ];

    protected $dates = ['created_at'];
    
   public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function comentator()
    {
        return $this->user();
    }
}

<?php

namespace App\Giveaway;

use App\Giveaway;
use App\Giveaway\Rules\RegisteredRule;
use Auth;
class RulesFactory
{
    

    public function checkRule($rule,$giveaway_id) {

        $giveaway = Giveaway::find($giveaway_id);
        
        $result =false;
        switch($rule->rule_key) {
            case 'rule_online': 
                $result = false;
                

            break;
            case 'rule_registered': 
                $result = new RegisteredRule();
                $result = $result->check();
                
            break;
        }
        return $result;

    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Quest;

class GiveawayQuests extends Model
{
    //
    protected $table = 'giveaway_tickets_quests';

    protected $fillable = ['quest_key','title','points','type','config','status'];


    public function quest() {
        return $this->hasMany('App\UsersQuest', 'user_id','quest_id');
    }

    public function isCompleted() {
        
        //$result = Quest::checkQuest($this->quest_key,$this->id);
        //dd($result);
        // jesli jest w pivot table to znaczy ze jest true
        // jesli nie ma to musimy sprwdzić warunek tego zadania
        // nawygodniej quest_key::check(); albo Quest::check(quest_key);

        return true;
    }
}



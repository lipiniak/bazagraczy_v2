<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FriendlyMatchResult extends Model
{
    //
    protected $table = 'match_friendly_results';

    public $timestamps = true;
    public $fillable = [
        'match_id',
        'host_result',
        'host_aproved',
        'enemy_result',
        'enemy_aproved',
        'expire_at'
    ];

    public function match() {
        return $this->belongsTo('App\FriendlyMatch','match_id');
    }

}

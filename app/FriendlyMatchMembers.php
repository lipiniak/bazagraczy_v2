<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FriendlyMatchMembers extends Model
{
    //
    protected $table = 'match_friendly_members';

    public $timestamps = true;
    public $fillable = [
        'match_id',
        'host_members',
        'enemy_members'
    ];


}

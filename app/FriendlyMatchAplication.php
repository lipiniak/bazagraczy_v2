<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FriendlyMatchAplication extends Model
{
    //
    protected $table = 'match_friendly_aplications';

    public $timestamps = true;
    public $fillable = [
        'match_id',
        'clan_id',
        'team_id',
        'members',
        'author_id'
    ];

    public function clan() {
        return $this->belongsTo('App\Clan','clan_id');
    }

    public function team() {
        return $this->belongsTo('App\Team','team_id');
    }
}

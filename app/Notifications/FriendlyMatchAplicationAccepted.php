<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class FriendlyMatchAplicationAccepted extends Notification
{
    use Queueable;


    private $data;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast','database'];
    }

    public function toDatabase($notifiable)
    {
        return [
            'data' => 'Zgłoszenie do <a href="#" data-href="/mecz/towarzyski/zobacz/'.$this->data['match_id'].'" data-notificationid="'.$this->id.'" class="highlight" onclick="redirect(this)">meczu</a> hostowanego przez klan <a href="#" data-href="/klan/'.$this->data['host_clan_link'].'" data-notificationid="'.$this->id.'" class="highlight" onclick="redirect(this)">'.$this->data['host_clan_name'].'</a> zostało zaakceptowane. Powodzenia!'
        ];
    }
    
    
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'data' => 'Zgłoszenie do <a href="#" data-href="/mecz/towarzyski/zobacz/'.$this->data['match_id'].'" data-notificationid="'.$this->id.'" class="highlight" onclick="redirect(this)">meczu</a> hostowanego przez klan <a href="#" data-href="/klan/'.$this->data['host_clan_link'].'" data-notificationid="'.$this->id.'" class="highlight" onclick="redirect(this)">'.$this->data['host_clan_name'].'</a> zostało zaakceptowane. Powodzenia!'
        ]);
    }
}

<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class ClanNewTeam extends Notification
{
    use Queueable;


    private $data;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast','database'];
    }

    public function toDatabase($notifiable)
    {
        return [
            'data' => 'W klanie <a href="#" data-href="/klan/'.$this->data['clan_link'].'" data-notificationid="'.$this->id.'" class="highlight" onclick="redirect(this)">'.$this->data['clan_name'].'</a> została utworzona nowa drużyna '.$this->data['team_name'].' '.$this->data['team_game'].' '.$this->data['team_platform']
        ];
    }
    
    
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'data' => 'W klanie <a href="#" data-href="/klan/'.$this->data['clan_link'].'" data-notificationid="'.$this->id.'" class="highlight" onclick="redirect(this)">'.$this->data['clan_name'].'</a> została utworzona nowa drużyna '.$this->data['team_name'].' '.$this->data['team_game'].' '.$this->data['team_platform']
        ]);
    }
}

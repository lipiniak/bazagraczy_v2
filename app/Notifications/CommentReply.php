<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class CommentReply extends Notification
{
    use Queueable;


    private $data;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast','database'];
    }

    public function toDatabase($notifiable)
    {
        return [
            'data' => '<a href="#" data-href="/profil/'.$this->data['user_id'].'" class="highlight" onclick="redirect(this)" data-notificationid="'.$this->id.'">'.$this->data['login'].'</a> także skomentował <a href="#" data-href="/tablica/post/'.$this->data['post_id'].'" class="highlight" onclick="redirect(this)" data-notificationid="'.$this->id.'">post</a> na tablicy '.$this->data['game'].' ['.$this->data['platform'].']'
        ];
    }
    
    
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'data' => '<a href="#" data-href="/profil/'.$this->data['user_id'].'" class="highlight" onclick="redirect(this)" data-notificationid="'.$this->id.'">'.$this->data['login'].'</a> także skomentował <a href="#" data-href="/tablica/post/'.$this->data['post_id'].'" class="highlight" onclick="redirect(this)" data-notificationid="'.$this->id.'">post</a> na tablicy '.$this->data['game'].' ['.$this->data['platform'].']'
        ]);
    }
}

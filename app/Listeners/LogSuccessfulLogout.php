<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Logout;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\StatsLogin;
use Cache;
class LogSuccessfulLogout
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Logout  $event
     * @return void
     */
    public function handle(Logout $event)
    {
        //
        $today = date('Y-m-d');
        
        $user = $event->user;

        $followers = $user->followers;
        
        Cache::forget('user-is-online-' . $user->id);

        foreach($followers as $follower) {
            broadcast(new \App\Events\FollowerLogout($follower->id,$user->id));
        }
            
    }
}

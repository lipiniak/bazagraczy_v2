<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\StatsLogin;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        //
        $today = date('Y-m-d');

        $record = StatsLogin::where('stat_date',$today)->firstOrCreate(['stat_date'=>$today]);

        if($record) {
            $record->count++;
            $record->save();
        }
        
        $user = $event->user;
        

        $followers = $user->followers;
        

        foreach($followers as $follower) {
            broadcast(new \App\Events\FollowerLogin($follower->id,$user->id));
        }
            
    }
}
